﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Accounting
{
    public partial class AutoNationACH : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            if (!IsPostBack)
            {
                GetServerInfo();
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                CheckToDo();
            }
            FillPaymentTable(); 
        }
        private void FillPaymentTable()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (rdpEndWeek.SelectedDate == null) 
            { 
                rgPayment.DataSource = null;
                rgPayment.Rebind();
                lblTotalAmt.Text = "0";
                return;
            }
            SQL = "select * from claimautonationach where canaid > 0 ";
            if (rdpEndWeek.SelectedDate != null)
            {
                SQL = SQL + "and perioddate = '" + rdpEndWeek.SelectedDate + "' ";
            }
            SQL = SQL + "order by dateapprove ";

            rgPayment.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgPayment.Rebind();

            SQL = "update ClaimAutoNationACH " +
                  "set DatePaid = null, ConfirmNumber = null " +
                  "where PeriodDate = ' " + rdpEndWeek.SelectedDate + "' " +
                  "and (not DatePaid is null " +
                  "or not ConfirmNumber is null) ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);

            SQL = "select sum(paidamt) as PaidAmt, datepaid, ConfirmNumber from claimautonationach where canaid > 0 ";
            if (rdpEndWeek.SelectedDate != null)
            {
                SQL = SQL + "and perioddate = '" + rdpEndWeek.SelectedDate + "' ";
            }
            SQL = SQL + "group by datepaid, ConfirmNumber ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lblTotalAmt.Text = Convert.ToDouble(clR.GetFields("paidamt")).ToString("C");
                txtConfirm.Text = clR.GetFields("confirmnumber");
                if (clR.GetFields("datepaid").Length > 0)
                {
                    rdpPaymentDate.SelectedDate = DateTime.Parse(clR.GetFields("datepaid"));
                }
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnCommission_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/Commission.aspx?sid=" + hfID.Value);
        }

        protected void btnPaylink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/Paylink.aspx?sid=" + hfID.Value);
        }

        protected void btnAutoNationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnWex_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/wex.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountAdj_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/AccountAdjustments.aspx?sid=" + hfID.Value);
        }

        protected void btnCommissionCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/CommissionCancel.aspx?sid=" + hfID.Value);
        }

        protected void rgPayment_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgPayment.ExportSettings.ExportOnlyData = false;
                rgPayment.ExportSettings.IgnorePaging = true;
                rgPayment.ExportSettings.OpenInNewWindow = true;
                rgPayment.ExportSettings.UseItemStyles = true;
                rgPayment.ExportSettings.FileName = "AN-ACH-Payment";
                rgPayment.ExportSettings.Csv.FileExtension = "csv";
                rgPayment.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgPayment.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgPayment.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "DeleteRow")
            {
                DeletePayment(long.Parse(rgPayment.Items[e.Item.ItemIndex].GetDataKeyValue("ClaimID").ToString()));
                FillPaymentTable();
            }
        }

        private void DeletePayment(long xID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "delete claimautonationach where claimid = " + xID;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            FillPaymentTable();
        }

        protected void rdpEndWeek_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            FillPaymentTable();
        }

        protected void btnMakePayment_Click(object sender, EventArgs e)
        {
            if (rdpPaymentDate.SelectedDate == null)
            {
                return;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            VeritasGlobalToolsV2.clsAutoNationACH clA = new VeritasGlobalToolsV2.clsAutoNationACH();
            clA.ConnectiionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            SQL = "select * from claimautonationach where perioddate = '" + rdpEndWeek.SelectedDate + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    clA.ClaimID = long.Parse(clR.GetFields("claimid"));
                    clA.DealerNo = clR.GetFields("dealerno");
                    clA.UserID = long.Parse(hfUserID.Value);
                    clA.PaidDate = rdpPaymentDate.SelectedDate.ToString();
                    clA.DateApproved = clR.GetFields("dateapprove");
                    clA.ConfirmNumber = txtConfirm.Text;
                    clA.ProcessClaim();
                }
            }
            RadWindowManager1.RadAlert("Process Complete.", 400, 100, "Done", "");
        }
    }
}