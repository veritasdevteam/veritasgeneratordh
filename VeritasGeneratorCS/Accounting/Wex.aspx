﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Wex.aspx.cs" Inherits="VeritasGeneratorCS.Accounting.Wex" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Wex</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" BorderStyle="None" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCommission" OnClick="btnCommission_Click" runat="server" BorderStyle="None" Text="Commission"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnPaylink" OnClick="btnPaylink_Click" runat="server" BorderStyle="None" Text="Paylink/Mepco"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAutoNationACH" OnClick="btnAutoNationACH_Click" runat="server" BorderStyle="None" Text="AutoNation ACH"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnWex" OnClick="btnWex_Click" runat="server" BorderStyle="None" Text="Wex"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccountAdj" OnClick="btnAccountAdj_Click" runat="server" BorderStyle="None" Text="Account Adjustment"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCommissionCancel" OnClick="btnCommissionCancel_Click" runat="server" BorderStyle="None" Text="Commission Cancel"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" BorderStyle="None" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" BorderStyle="None" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" BorderStyle="None" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" BorderStyle="None" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" BorderStyle="None" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" BorderStyle="None" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" OnClick="btnSettings_Click" runat="server" BorderStyle="None" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" BorderStyle="None" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" BorderStyle="None" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlHead">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Wex Account:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:DropDownList ID="cboWexAcct" OnSelectedIndexChanged="cboWexAcct_SelectedIndexChanged" AutoPostBack="true" DataSourceID="dsWexAcct" DataTextField="companyname" DataValueField="wexapiid" runat="server"></asp:DropDownList>
                                                                    <asp:SqlDataSource ID="dsWexAcct" SelectCommand="select wexapiid, companyname from wexapi order by wexapiid" runat="server"></asp:SqlDataSource>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlTotal">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Total Deposit:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtDeposit" runat="server"></asp:TextBox>
                                                                    <asp:Button ID="btnAddDeposit" OnClick="btnAddDeposit_Click" runat="server" Text="Add" />
                                                                    <asp:Button ID="btnViewDeposit" OnClick="btnViewDeposit_Click" runat="server" Text="View" />
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Total Paid:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtPaid" runat="server"></asp:TextBox>
                                                                    <asp:Button ID="btnViewPaid" OnClick="btnViewPaid_Click" runat="server" Text="View" />
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Total Pending:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtPending" runat="server"></asp:TextBox>
                                                                    <asp:Button ID="btnViewPending" OnClick="btnViewPending_Click" runat="server" Text="View" />
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Total Void:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtVoid" runat="server"></asp:TextBox>
                                                                    <asp:Button ID="btnViewVoid" OnClick="btnViewVoid_Click" runat="server" Text="View" />
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Total Balance:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtBalance" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlAdd">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Transfer Date:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadDatePicker ID="rdpTransferDate" OnSelectedDateChanged="rdpTransferDate_SelectedDateChanged" runat="server"></telerik:RadDatePicker>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Transfer Amt:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadNumericTextBox ID="txtTransferAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" BackColor="#1eabe2" Text="Close" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" BackColor="#1eabe2" Text="Save" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlTransferList">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadGrid ID="rgWexTransfer" runat="server" AutoGenerateColumns="false" DataSourceID="dsWexTransfer" 
                                                            AllowSorting="true" Width="500" ShowFooter="true" AllowPaging="true" PageSize="25">
                                                            <MasterTableView AutoGenerateColumns="false" DataKeyNames="WexTransferID" ShowFooter="true">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="WexTransferID"  ReadOnly="true" Visible="false" UniqueName="WexTransferID"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="TransferDate" UniqueName="TransferDate" HeaderText="Transfer Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="dsWexTransfer" ProviderName="System.Data.SqlClient" 
                                                            SelectCommand="Select wextransferid, transferdate, amt
                                                            from wextransfer where wexapiid = @WexAPIID  " 
                                                            runat="server">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="hfWexAPIID" Name="WexAPIID" PropertyName="Value" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Button ID="btnTransferClose" OnClick="btnTransferClose_Click" runat="server" BackColor="#1eabe2" Text="Close" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlPaid">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadGrid ID="rgPaid" runat="server" AutoGenerateColumns="false" DataSourceID="dsPaid" 
                                                            AllowSorting="true" Width="500" ShowFooter="true" AllowPaging="true" PageSize="20">
                                                            <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimWexPaymentID" ShowFooter="true">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="ClaimWexPaymentID"  ReadOnly="true" Visible="false" UniqueName="WexTransferID"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="RONo" UniqueName="RONo" HeaderText="RO No"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ClaimPayeename" UniqueName="ClaimPayeename" HeaderText="Claim Payee"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="sent" UniqueName="sent" HeaderText="Sent" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="paiddate" UniqueName="paiddate" HeaderText="Paid Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="dsPaid" ProviderName="System.Data.SqlClient" 
                                                            SelectCommand="select ClaimWexPaymentID , claimno, RONo, ClaimPayeename, sent, VoidDate, paiddate, cast(paymentamt as money) as Amt from ClaimWexPayment cwp
                                                            inner join wexapi wa on wa.CompanyNo = cwp.CompanyNo 
                                                            where reasoncode = 'success' and not paiddate is null and WEXAPIID = @WexAPIID" 
                                                            runat="server">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="hfWexAPIID" Name="WexAPIID" PropertyName="Value" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Button ID="btnPaidClose" OnClick="btnPaidClose_Click" runat="server" BackColor="#1eabe2" Text="Close" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlPending">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadGrid ID="rgPending" runat="server" AutoGenerateColumns="false" DataSourceID="dsPending" 
                                                            AllowSorting="true" Width="500" ShowFooter="true" AllowPaging="true" PageSize="20">
                                                            <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimWexPaymentID" ShowFooter="true">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="ClaimWexPaymentID"  ReadOnly="true" Visible="false" UniqueName="WexTransferID"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="RONo" UniqueName="RONo" HeaderText="RO No"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ClaimPayeename" UniqueName="ClaimPayeename" HeaderText="Claim Payee"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="sent" UniqueName="sent" HeaderText="Sent" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="paiddate" UniqueName="paiddate" HeaderText="Paid Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="dsPending" ProviderName="System.Data.SqlClient" 
                                                            SelectCommand="select ClaimWexPaymentID , claimno, RONo, ClaimPayeename, sent, VoidDate, paiddate, cast(paymentamt as money) as Amt from ClaimWexPayment cwp
                                                            inner join wexapi wa on wa.CompanyNo = cwp.CompanyNo 
                                                            where reasoncode = 'success' and paiddate is null and voiddate is null and WEXAPIID = @WexAPIID" 
                                                            runat="server">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="hfWexAPIID" Name="WexAPIID" PropertyName="Value" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Button ID="btnPendingClose" OnClick="btnPendingClose_Click" runat="server" BackColor="#1eabe2" Text="Close" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlVoid">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadGrid ID="rgVoid" runat="server" AutoGenerateColumns="false" DataSourceID="dsVoid" 
                                                            AllowSorting="true" Width="500" ShowFooter="true" AllowPaging="true" PageSize="20">
                                                            <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimWexPaymentID" ShowFooter="true">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="ClaimWexPaymentID"  ReadOnly="true" Visible="false" UniqueName="WexTransferID"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="RONo" UniqueName="RONo" HeaderText="RO No"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ClaimPayeename" UniqueName="ClaimPayeename" HeaderText="Claim Payee"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="sent" UniqueName="sent" HeaderText="Sent" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="paiddate" UniqueName="paiddate" HeaderText="Paid Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="dsVoid" ProviderName="System.Data.SqlClient" 
                                                            SelectCommand="select ClaimWexPaymentID , claimno, RONo, ClaimPayeename, sent, VoidDate, paiddate, cast(paymentamt as money) as Amt from ClaimWexPayment cwp
                                                            inner join wexapi wa on wa.CompanyNo = cwp.CompanyNo 
                                                            where reasoncode = 'success' and paiddate is null and not voiddate is null and WEXAPIID = @WexAPIID" 
                                                            runat="server">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="hfWexAPIID" Name="WexAPIID" PropertyName="Value" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Button ID="btnVoidClose" OnClick="btnVoidClose_Click" runat="server" BackColor="#1eabe2" Text="Close" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfAdd" runat="server" />
                <asp:HiddenField ID="hfWexTransferID" runat="server" />
                <asp:HiddenField ID="hfWexAPIID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
    </form>
</body>
</html>
