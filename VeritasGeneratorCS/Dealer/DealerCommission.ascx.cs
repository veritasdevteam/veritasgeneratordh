﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Dealer
{
    public partial class DealerCommission : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsGuard.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsHighline.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsExotic.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsEssentials.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsWrap.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsProgram.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsRateType.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsRateCategory.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsAgent.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsSubAgent.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                chkNoChargeBack.Visible = false;
                trNCB.Visible = false;
                hfUserID.Value = hfUserID.Value;
                hfDealerID.Value = Request.QueryString["dealerid"];
                pnlCommission.Visible = true;
                pnlCommissionDetail.Visible = false;
                pnlSearchAgent.Visible = false;
                pnlSearchRateType.Visible = false;
                pnlSearchSubAgent.Visible = false;
                pnlAddRateType.Visible = false;
                pnlConfirmDelete.Visible = false;
            }
        }
        protected void btnAddCommission_Click(Object sender, EventArgs e)
        {
            pnlCommission.Visible = false;
            pnlCommissionDetail.Visible = true;
            ClearCommissionDetail();
        }
        private void ClearCommissionDetail()
        {
            txtCommDesc.Text = "";
            txtRateType.Text = "";
            cboProgram.ClearSelection();
            rdpStartDate.Clear();
            txtAgent.Text = "";
            txtAmount.Text = "";
            cboClass.Items.Clear();
            rdpEndDate.Clear();
            hfCommissionHeaderID.Value = "0";
            hfAgentID.Value = "0";
            hfSubAgentID.Value = "0";
        }

        protected void rgDealerCommission_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clDC = new clsDBO.clsDBO();
            SQL = "select * from commissionheader where commissionheaderid = " + rgDealerCommission.SelectedValue;
            clDC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clDC.RowCount() > 0)
            {
                clDC.GetRow();
                hfCommissionHeaderID.Value = clDC.GetFields("commissionheaderid");
                txtCommDesc.Text = clDC.GetFields("commissiondesc");
                hfRateTypeID.Value = clDC.GetFields("ratetypeid");
                txtRateType.Text = Functions.GetRateType(long.Parse(clDC.GetFields("ratetypeid")));
                cboProgram.SelectedValue = clDC.GetFields("programid");
                CalcClass();
                FillClass(clDC.GetFields("class"));
                rdpStartDate.SelectedDate = DateTime.Parse(clDC.GetFields("startdate"));
                txtAgent.Text = Functions.GetAgentInfo(long.Parse(clDC.GetFields("agentid")));
                txtAmount.Text = clDC.GetFields("amt");
                hfAgentID.Value = clDC.GetFields("agentid");
                hfSubAgentID.Value = clDC.GetFields("subagentid");
                cboClass.Text = clDC.GetFields("class");
                if (DateTime.TryParse(clDC.GetFields("enddate"), out DateTime result))
                {
                    rdpEndDate.SelectedDate = result;
                }
                lblCreBy.Text = Functions.GetUserInfo(long.Parse(clDC.GetFields("creby")));
                lblCreDate.Text = clDC.GetFields("credate");
                lblModBy.Text = Functions.GetUserInfo(long.Parse(clDC.GetFields("modby")));
                lblModDate.Text = clDC.GetFields("moddate");
                chkNoChargeBack.Checked = Convert.ToBoolean(clDC.GetFields("ncb"));
                txtNCB.Text = clDC.GetFields("NCBAmt");
                pnlCommission.Visible = false;
                pnlCommissionDetail.Visible = true;
            }
        }
        protected void btnCancelCommission_Click(object sender, EventArgs e)
        {
            pnlCommissionDetail.Visible = false;
            pnlCommission.Visible = true;
        }
        protected void btnSaveCommission_Click(object sender, EventArgs e)
        {
            bool bAdd = false;
            string SQL;
            clsDBO.clsDBO clCH = new clsDBO.clsDBO();
            bool bChange = false;
            SQL = "select * from commissionheader " +
                  "where commissionheaderid = " + hfCommissionHeaderID.Value;
            clCH.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCH.RowCount() == 0)
            {
                clCH.NewRow();
            }
            else
            {
                clCH.GetRow();
            }
            clCH.SetFields("commissiondesc", txtCommDesc.Text);
            if (clCH.GetFields("ratetypeid") != hfRateTypeID.Value) {
                bChange = true;
            }
            clCH.SetFields("ratetypeid", hfRateTypeID.Value);
            if (clCH.GetFields("programid") != cboProgram.SelectedValue)
            {
                bChange = true;
            }
            if (clCH.GetFields("programid") != cboProgram.SelectedValue)
            {
                bChange = true;
            }
            clCH.SetFields("programid", cboProgram.SelectedValue);
            clCH.SetFields("dealerid", hfDealerID.Value);
            if (clCH.GetFields("startdate").Length > 0)
            {
                if (DateTime.Parse(clCH.GetFields("startdate")) != rdpStartDate.SelectedDate)
                {
                    bChange = true;
                }
            }
            clCH.SetFields("startdate", rdpStartDate.SelectedDate.ToString());
            if (clCH.GetFields("agentid") != hfAgentID.Value)
            {
                bChange = true;
            }
            clCH.SetFields("agentid", hfAgentID.Value);
            if (clCH.GetFields("class") != cboClass.Text)
            {
                bChange = true;
            }
            clCH.SetFields("class", cboClass.Text);
            if (clCH.GetFields("amt") != txtAmount.Text)
            {
                bChange = true;
            }
            clCH.SetFields("amt", txtAmount.Text);
            clCH.SetFields("ncb", chkNoChargeBack.Checked.ToString());
            if (txtNCB.Text.Length > 0)
            {
                clCH.SetFields("ncbamt", txtNCB.Text);
            }
            if (DateTime.TryParse(rdpEndDate.SelectedDate.ToString(), out DateTime result)) {
                if (clCH.GetFields("enddate").Length > 0)
                {
                    if (DateTime.Parse(clCH.GetFields("enddate")) != rdpEndDate.SelectedDate)
                    {
                        bChange = true;
                    }
                }
                else 
                {
                    bChange = true;
                }
                clCH.SetFields("enddate", rdpEndDate.SelectedDate.ToString());
            }

            if (clCH.RowCount() == 0)
            {
                clCH.SetFields("creby", hfUserID.Value);
                clCH.SetFields("credate", DateTime.Today.ToString());
                clCH.AddRow();
                bAdd = true;
            }
            else
            {
                clCH.SetFields("moddate", DateTime.Today.ToString());
                clCH.SetFields("modby", hfUserID.Value);
            }
            clCH.SaveDB();
            if (bAdd)
            {
                GetCommissionHeaderID();
            }
            if (bAdd)
            {
                AddCommissions();
            }
            else
            {
                if (bChange)
                {
                    DeleteCommissionDetail();
                    AddCommissions();
                }
            }
            rgDealerCommission.Rebind();
            rgEssentils.Rebind();
            rgExotic.Rebind();
            rgGuard.Rebind();
            rgHighLine.Rebind();
            pnlCommissionDetail.Visible = false;
            pnlCommission.Visible = true;
        }
        private void GetCommissionHeaderID()
        {
            string SQL;
            clsDBO.clsDBO clCH = new clsDBO.clsDBO();
            SQL = "select max(commissionheaderid) as CH from commissionheader " +
                  "where creby = " + hfUserID.Value;
            clCH.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCH.RowCount() > 0)
            {
                clCH.GetRow();
                hfCommissionHeaderID.Value = clCH.GetFields("ch");
            }
        }
        private void DeleteCommission()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "delete commissions where commissionheaderid = " + hfCommissionHeaderID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "delete commissionheader where commissionheaderid = " + hfCommissionHeaderID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgDealerCommission.Rebind();
            rgEssentils.Rebind();
            rgExotic.Rebind();
            rgGuard.Rebind();
            rgHighLine.Rebind();
        }
        private void DeleteCommissionDetail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "delete commissions " +
                  "where commissionheaderid = " + hfCommissionHeaderID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void AddCommissions()
        {
            string[] sClass;
            clsDBO.clsDBO clPPT = new clsDBO.clsDBO();
            string SQL;
            clsDBO.clsDBO clAC = new clsDBO.clsDBO();
            long cnt;
            long cnt2;
            if (cboClass.Text == "All items checked")
            {
                SQL = "select * from programtemplate " +
                      "where programid = " + cboProgram.SelectedValue + " ";
                clPPT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["connstring"].ConnectionString);
                if (clPPT.RowCount() > 0)
                {
                    for (cnt = 0; cnt <= clPPT.RowCount() - 1; cnt++)
                    {
                        clPPT.GetRowNo((int)cnt);
                        SQL = "select * from commissions " +
                              "where dealerid = " + hfDealerID.Value + " " +
                              "and commissionheaderid = " + hfCommissionHeaderID.Value + " " +
                              "and ratetypeid = " + hfRateTypeID.Value + " ";
                        if (Convert.ToInt32(hfAgentID.Value) > 0)
                        {
                            SQL = SQL + "and agentid = " + hfAgentID.Value + " ";
                        }
                        if (chkNoChargeBack.Checked)
                        {
                            AddNoChargeBack(clPPT.GetFields("class"));
                        }
                        SQL = SQL + "and programid = " + cboProgram.SelectedValue + " "
                                  + "and class = '" + clPPT.GetFields("class") + "' "
                                  + "and startdate = '" + rdpStartDate.SelectedDate + "' ";
                        clAC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["connstring"].ConnectionString);
                        if (clAC.RowCount() == 0)
                        {
                            clAC.NewRow();
                        }
                        else
                        {
                            clAC.GetRow();
                        }
                        clAC.SetFields("dealerid", hfDealerID.Value);
                        clAC.SetFields("ratetypeid", hfRateTypeID.Value);
                        clAC.SetFields("agentid", hfAgentID.Value);
                        clAC.SetFields("commissionheaderid", hfCommissionHeaderID.Value);
                        clAC.SetFields("programid", cboProgram.SelectedValue);
                        clAC.SetFields("class", clPPT.GetFields("class"));
                        clAC.SetFields("amt", txtAmount.Text);
                        clAC.SetFields("startdate", rdpStartDate.SelectedDate.ToString());
                        if (rdpEndDate.SelectedDate != null)
                        {
                            clAC.SetFields("enddate", rdpEndDate.SelectedDate.ToString());
                        }
                        if (clAC.RowCount() == 0)
                        {
                            clAC.AddRow();
                        }
                        clAC.SaveDB();
                    }
                }
            }
            else
            {
                sClass = cboClass.Text.Split(',');
                for (cnt = 0; cnt <= sClass.Count() - 1; cnt++)
                {
                    SQL = "select * from programtemplate " +
                          "where programid = " + cboProgram.SelectedValue + " " +
                          "and class = '" + sClass[cnt].Trim() + "' ";
                    clPPT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clPPT.RowCount() > 0)
                    {
                        for (cnt2 = 0; cnt2 <= clPPT.RowCount() - 1; cnt2++)
                        {
                            clPPT.GetRowNo((int)cnt2);
                            SQL = "select * from commissions " +
                              "where dealerid = " + hfDealerID.Value + " " +
                              "and commissionheaderid = " + hfCommissionHeaderID.Value + " " +
                              "and ratetypeid = " + hfRateTypeID.Value + " ";
                            if (hfRateTypeID.Value == "13")
                            {
                                SQL = SQL + "and agentid = " + hfAgentID.Value + " ";
                            }
                            if (Convert.ToInt32(hfRateTypeID.Value) == 14)
                            {
                                SQL = SQL + "and subagentid = " + hfSubAgentID.Value + " ";
                            }
                            SQL = SQL + "and programid = " + cboProgram.SelectedValue + " "
                                      + "and class = '" + clPPT.GetFields("class") + "' "
                                      + "and startdate = '" + rdpStartDate.SelectedDate + "' ";
                            clAC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                            if (clAC.RowCount() == 0)
                            {
                                clAC.NewRow();
                            }
                            else
                            {
                                clAC.GetRow();
                            }
                            clAC.SetFields("ratetypeid", hfRateTypeID.Value);
                            clAC.SetFields("dealerid", hfDealerID.Value);
                            if (hfRateTypeID.Value == "13")
                            {
                                clAC.SetFields("agentid", hfAgentID.Value);
                                if (chkNoChargeBack.Checked)
                                {
                                    AddNoChargeBack(clPPT.GetFields("class"));
                                }
                            }
                            if (hfSubAgentID.Value == "14")
                            {
                                clAC.SetFields("subagentid", hfSubAgentID.Value);
                            }
                            clAC.SetFields("commissionheaderid", hfCommissionHeaderID.Value);
                            clAC.SetFields("programid", cboProgram.SelectedValue);
                            clAC.SetFields("class", clPPT.GetFields("class").Trim());
                            clAC.SetFields("amt", txtAmount.Text);
                            clAC.SetFields("startdate", rdpStartDate.SelectedDate.ToString());
                            if (rdpEndDate.SelectedDate != null)
                            {
                                clAC.SetFields("enddate", rdpEndDate.SelectedDate.ToString());
                            }
                            if (clAC.RowCount() == 0)
                            {
                                clAC.AddRow();
                            }
                            clAC.SaveDB();
                        }
                    }
                }
            }
            rgDealerCommission.Rebind();
            rgEssentils.Rebind();
            rgExotic.Rebind();
            rgGuard.Rebind();
            rgHighLine.Rebind();
        }
        private void AddNoChargeBack(string xClass)
        {
            clsDBO.clsDBO clCH = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from commissions " +
                  "where dealerid = " + hfDealerID.Value + " " +
                  "and ratetypeid = 29 " +
                  "and commissionheaderid = " + hfCommissionHeaderID.Value + " " +
                  "and agentid = " + hfAgentID.Value + " " +
                  "and programid = " + cboProgram.SelectedValue + " " +
                  "and class = '" + xClass + "' " +
                  "and startdate = '" + rdpStartDate.SelectedDate + "' ";
            clCH.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCH.RowCount() == 0)
            {
                clCH.NewRow();
            }
            else
            {
                clCH.GetRow();
            }
            clCH.SetFields("ratetypeid", 29.ToString());
            clCH.SetFields("dealerid", hfDealerID.Value);
            clCH.SetFields("agentid", hfAgentID.Value);
            clCH.SetFields("commissionheaderid", hfCommissionHeaderID.Value);
            clCH.SetFields("programid", cboProgram.SelectedValue);
            clCH.SetFields("class", xClass);
            if (txtNCB.Text == "")
            {
                clCH.SetFields("amt", ((Convert.ToDouble(txtAmount.Text) / 0.85) - Convert.ToDouble(txtAmount.Text)).ToString());
                goto MoveHere;
            }
            if (txtNCB.Text == "0")
            {
                clCH.SetFields("amt", ((Convert.ToDouble(txtAmount.Text) / 0.85) - Convert.ToDouble(txtAmount.Text)).ToString());
                goto MoveHere;
            }
            if (Convert.ToDouble(txtNCB.Text) < 2)
            {
                clCH.SetFields("amt", ((Convert.ToDouble(txtAmount.Text) / (1 - Convert.ToDouble(txtNCB.Text))) - Convert.ToDouble(txtAmount.Text)).ToString());
                goto MoveHere;
            }
            if (Convert.ToDouble(txtNCB.Text) > 2)
            {
                clCH.SetFields("amt", txtNCB.Text);
            }
        MoveHere:
            clCH.SetFields("amt", Math.Ceiling(Convert.ToDouble(clCH.GetFields("amt"))).ToString());
            clCH.SetFields("startdate", rdpStartDate.SelectedDate.ToString());
            if (rdpEndDate.SelectedDate != null)
            {
                clCH.SetFields("enddate", rdpEndDate.SelectedDate.ToString());
            }
            if (clCH.RowCount() == 0)
            {
                clCH.AddRow();
            }
            clCH.SaveDB();
            rgEssentils.Rebind();
            rgExotic.Rebind();
            rgGuard.Rebind();
            rgHighLine.Rebind();
        }
        private void FillClass(string xClass)
        {
            long cnt;
            long cnt2;
            string[] sSplit;
            if (xClass == "All items checked")
            {
                for (cnt = 0; cnt <= cboClass.Items.Count() - 1; cnt++)
                {
                    cboClass.Items[(int)cnt].Checked = true;
                }
                return;
            }
            sSplit = xClass.Split(',');
            for (cnt = 0; cnt <= sSplit.Count() - 1; cnt++)
            {
                for (cnt2 = 0; cnt <= cboClass.Items.Count() - 1; cnt2++)
                {
                    if (cboClass.Items[(int)cnt2].Text == sSplit[cnt].Trim())
                    {
                        cboClass.Items[(int)cnt2].Checked = true;
                    }
                }
            }
        }
        private void CalcClass()
        {
            string SQL;
            clsDBO.clsDBO clPPT = new clsDBO.clsDBO();
            long cnt;
            cboClass.Items.Clear();
            SQL = "select class from programtemplate " +
                  "where Programid = " + cboProgram.SelectedValue + " " +
                  "group by class " +
                  "order By class ";
            clPPT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clPPT.RowCount() > 0)
            {
                for (cnt = 0; cnt <= clPPT.RowCount() - 1; cnt++)
                {
                    clPPT.GetRowNo((int)cnt);
                    cboClass.Items.Add(clPPT.GetFields("class"));
                }
            }
        }

        protected void btnYesConfirm_Click(object sender, EventArgs e)
        {
            DeleteCommission();
            rgDealerCommission.Rebind();
            rgEssentils.Rebind();
            rgExotic.Rebind();
            rgGuard.Rebind();
            rgHighLine.Rebind();
            pnlCommission.Visible = true;
            pnlConfirmDelete.Visible = false;
        }

        protected void btnNoConfirm_Click(object sender, EventArgs e)
        {
            pnlCommissionDetail.Visible = true;
            pnlConfirmDelete.Visible = false;
        }

        protected void btnAddRateType_Click(object sender, EventArgs e)
        {
            pnlSearchRateType.Visible = false;
            pnlAddRateType.Visible = true;
        }

        protected void btnCancelRateType_Click(object sender, EventArgs e)
        {
            pnlCommissionDetail.Visible = true;
            pnlSearchRateType.Visible = false;
        }

        protected void btnSaveCategory_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clRT = new clsDBO.clsDBO();
            SQL = "select * from ratetype where ratetypename = '" + txtRateType.Text + "' ";
            clRT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clRT.RowCount() == 0)
            {
                clRT.NewRow();
            }
            else
            {
                clRT.GetRow();
            }
            clRT.SetFields("ratetypename", txtRateTypeAdd.Text);
            clRT.SetFields("ratecategoryid", cboRateCategoryAdd.SelectedValue);
            if (clRT.RowCount() == 0)
            {
                clRT.AddRow();
            }
            clRT.SaveDB();
            rgRateType.Rebind();
            pnlSearchRateType.Visible = true;
            pnlAddRateType.Visible = false;
        }

        protected void btnCancelCategory_Click(object sender, EventArgs e)
        {
            pnlSearchRateType.Visible = true;
            pnlAddRateType.Visible = false;
        }

        protected void btnClearAgentInfo_Click(object sender, EventArgs e)
        {
            hfAgentID.Value = 0.ToString();
            txtAgent.Text = "";
            pnlSearchAgent.Visible = false;
            pnlCommissionDetail.Visible = true;
        }

        protected void btnClearSubAgentInfo_Click(object sender, EventArgs e)
        {
            hfSubAgentID.Value = 0.ToString();
            pnlCommissionDetail.Visible = true;
            pnlSearchSubAgent.Visible = false;
        }

        protected void btnSearchRateType_Click(object sender, EventArgs e)
        {
            rgRateType.Rebind();
            pnlCommissionDetail.Visible = false;
            pnlSearchRateType.Visible = true;
        }

        protected void btnAgent_Click(object sender, EventArgs e)
        {
            rgAgent.Rebind();
            pnlSearchAgent.Visible = true;
            pnlCommissionDetail.Visible = false;
        }

        protected void btnDeleteCommission_Click(object sender, EventArgs e)
        {
            pnlCommissionDetail.Visible = false;
            pnlConfirmDelete.Visible = true;
            lblProgramConfirm.Text = cboProgram.Text;
            lblRateTypeConfirm.Text = txtRateType.Text;
            lblAmtConfirm.Text = txtAmount.Text;
        }

        protected void cboProgram_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CalcClass();
        }

        protected void rgRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfRateTypeID.Value = rgRateType.SelectedValue.ToString();
            txtRateType.Text = Functions.GetRateType(long.Parse(hfRateTypeID.Value));
            pnlCommissionDetail.Visible = true;
            pnlSearchRateType.Visible = false;
            if (Convert.ToInt32(rgRateType.SelectedValue) == 13)
            {
                chkNoChargeBack.Visible = true;
                trNCB.Visible = true;
            }
        }

        protected void rgAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfAgentID.Value = rgAgent.SelectedValue.ToString();
            txtAgent.Text = Functions.GetAgentInfo(long.Parse(rgAgent.SelectedValue.ToString()));
            pnlSearchAgent.Visible = false;
            pnlCommissionDetail.Visible = true;
        }

        protected void rgSubAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfSubAgentID.Value = rgSubAgent.SelectedValue.ToString();
            pnlCommissionDetail.Visible = true;
            pnlSearchSubAgent.Visible = false;
        }
    }
}