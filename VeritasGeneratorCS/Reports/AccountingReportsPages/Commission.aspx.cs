﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Reports.AccountingReportsPages
{
    public partial class Commission : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                FillCycleDate();
            }
            if (hfError.Value == "Visible")
            {
                rwError.VisibleOnPageLoad = true;
            }
            else
            {
                rwError.VisibleOnPageLoad = false;
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) 
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) 
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) 
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) 
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) 
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) 
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) 
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) 
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) 
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) 
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnUsers_Click(object sender, EventArgs e) {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e) {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e) {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/salesreportssearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e) {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e) {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e) {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnPhoneReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/phonereports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnCustomReports_Click(object sender, EventArgs e)
        {

        }
        private void FillCycleDate()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select cast(month(FinalDate) as nvarchar(2)) + '/' + cast(day(finaldate) as nvarchar(2)) + '/' + cast(year(finaldate) as nvarchar(4)) as FDate from contractcommissions " +
                  "group by finaldate " +
                  "order by finaldate desc ";
            cboCycleDate.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            cboCycleDate.DataTextField = "FDate";
            cboCycleDate.DataValueField = "FDate";
            cboCycleDate.DataBind();
        }

        protected void btnAgentSearch_Click(object sender, EventArgs e)
        {
            pnlAgentSearch.Visible = true;
            pnlCommission.Visible = false;
            FillAgent();
        }

        protected void btnAgentClose_Click(object sender, EventArgs e)
        {
            pnlAgentSearch.Visible = false;
            pnlCommission.Visible = true;
        }

        protected void rgAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlAgentSearch.Visible = false;
            pnlCommission.Visible = true;
            hfAgentID.Value = rgAgent.SelectedValue.ToString();
            txtAgent.Text = Functions.GetAgentInfo(long.Parse(rgAgent.SelectedValue.ToString()));
        }

        protected void btnGetCommission_Click(object sender, EventArgs e)
        {
            FillCommission();
        }
        private void FillAgent()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select agentid, agentno, agentname, city, state from agents " +
                  "where agentid in (" +
                  "select agentid from contractcommissions " +
                  "where finaldate = '" + cboCycleDate.SelectedValue + "') ";
            rgAgent.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgAgent.DataBind();
        }
        private void FillCommission()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            long cnt;
            double sTotal = 0;
            SQL = "select cc.contractcommissionid, c.ContractNo, c.SaleDate, c.FName + ' ' + c.LName as CustomerName, sa.SubAgentName, rt.RateTypeName, cc.Amt  " +
                  "from contractcommissions cc " +
                  "inner join contract c on c.contractid = cc.contractid " +
                  "inner join ratetype rt on cc.RateTypeID = rt.RateTypeID " +
                  "left join SubAgents sa on c.SubAgentID = sa.SubAgentID where ";
                  if (hfAgentID.Value != "") 
                  { 
                       SQL = SQL + "cc.agentid = " + hfAgentID.Value + " and "; 
                  }
            SQL = SQL + "cc.finaldate = '" + cboCycleDate.SelectedValue + "' " +
                  "union " +
                  "select cc.contractcommissionid, c.ContractNo, c.SaleDate, c.FName + ' ' + c.LName as CustomerName, sa.SubAgentName, rt.RateTypeName, (cc.cancelAmt * -1) as amt " +
                  "from contractcommissions cc " +
                  "inner join contract c on c.contractid = cc.contractid " +
                  "inner join ratetype rt on cc.RateTypeID = rt.RateTypeID " +
                  "left join SubAgents sa on c.SubAgentID = sa.SubAgentID where ";
                  if (hfAgentID.Value != "")
                  {
                      SQL = SQL + "cc.agentid = " + hfAgentID.Value + " and ";
                  }
                  SQL = SQL + "cc.cancelfinaldate = '" + cboCycleDate.SelectedValue + "' " +
                  "order by contractno ";
            rgCommission.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgCommission.DataBind();
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(Convert.ToInt32(cnt));
                    if (clR.GetFields("ratetypename") != "Agent No Charge Back")
                    {
                        sTotal = sTotal + Convert.ToDouble(clR.GetFields("amt"));
                    }
                }
                txtTotalAmt.Text = sTotal.ToString("#,##0.00");
            }
            else
            {
                txtTotalAmt.Text = "0.00";
            }
        }

        protected void rgCommission_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgCommission.ExportSettings.ExportOnlyData = false;
                rgCommission.ExportSettings.IgnorePaging = true;
                rgCommission.ExportSettings.OpenInNewWindow = true;
                rgCommission.ExportSettings.UseItemStyles = false;
                rgCommission.ExportSettings.FileName = "Commission";
                rgCommission.ExportSettings.Csv.FileExtension = "csv";
                rgCommission.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgCommission.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgCommission.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgCommission.ExportSettings.ExportOnlyData = false;
                rgCommission.ExportSettings.IgnorePaging = true;
                rgCommission.ExportSettings.OpenInNewWindow = true;
                rgCommission.ExportSettings.UseItemStyles = false;
                rgCommission.ExportSettings.FileName = "Commission";
                rgCommission.ExportSettings.Excel.FileExtension = "xlsx";
                rgCommission.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgCommission.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                rgCommission.Rebind();
            }
        }
    }
}