﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Reports.Phone
{
    public partial class AgentDailyCallVolume : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetServerInfo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
            }
            if (hfError.Value == "Visible")
            {
                rwError.VisibleOnPageLoad = true;
            }
            else
            {
                rwError.VisibleOnPageLoad = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) 
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) 
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) 
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) 
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) 
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) 
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) 
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) 
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) 
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) 
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnUsers_Click(object sender, EventArgs e) {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e) {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e) {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/salesreportssearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e) {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e) {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e) {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnPhoneReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/phonereports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnCustomReports_Click(object sender, EventArgs e)
        {

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (rdpTo.SelectedDate == null) 
            {
                return;
            }
            if (rdpFrom.SelectedDate == null)
            {
                return;
            }
            SQL = "select CallDate, fname + ' ' + lname as AgentName, " +
                  "case when active <> 0 then 'Active' else 'Not Active' end as Active, " +
                  "inboundct as Inbound, outboundct as Outbound, " +
                  "cast((AvgTalkingDur / 60) as nvarchar(10)) + ':' + case when len(cast((AvgTalkingDur - ((AvgTalkingDur /60) * 60)) as nvarchar(5))) =1 then '0' + " +
                  "cast((AvgTalkingDur - ((AvgTalkingDur /60) * 60)) as nvarchar(5)) " +
                  "else " +
                  "cast((AvgTalkingDur - ((AvgTalkingDur /60) * 60)) as nvarchar(5)) end  as AvgTalkingDur, " +
                  "cast((AvgDNDDur / 60) as nvarchar(10)) + ':' + case when len(cast((AvgDNDDur - ((AvgDNDDur /60) * 60)) as nvarchar(5))) =1 then '0' + " +
                  "cast((AvgDNDDur - ((AvgDNDDur /60) * 60)) as nvarchar(5)) " +
                  "else " +
                  "cast((AvgDNDDur - ((AvgDNDDur /60) * 60)) as nvarchar(5)) end  as AvgDNDDur, " +
                  "cast((TotDNDDur / 60) as nvarchar(10)) + ':' + case when len(cast((TotDNDDur - ((TotDNDDur /60) * 60)) as nvarchar(5))) =1 then '0' + " +
                  "cast((TotDNDDur - ((TotDNDDur /60) * 60)) as nvarchar(5)) " +
                  "else " +
                  "cast((TotDNDDur - ((TotDNDDur /60) * 60)) as nvarchar(5)) end  as TotalDNDDur, " +
                  "cast((AvgHoldDur / 60) as nvarchar(10)) + ':' + case when len(cast((AvgHoldDur - ((AvgHoldDur /60) * 60)) as nvarchar(5))) =1 then '0' + " +
                  "cast((AvgHoldDur - ((AvgHoldDur /60) * 60)) as nvarchar(5)) " +
                  "else " +
                  "cast((AvgHoldDur - ((AvgHoldDur /60) * 60)) as nvarchar(5)) end  as TotalHoldDur, " +
                  "cast((AvgReadyDur / 60) as nvarchar(10)) + ':' + case when len(cast((AvgReadyDur - ((AvgReadyDur /60) * 60)) as nvarchar(5))) =1 then '0' + " +
                  "cast((AvgReadyDur - ((AvgReadyDur /60) * 60)) as nvarchar(5)) " +
                  "else " +
                  "cast((AvgReadyDur - ((AvgReadyDur /60) * 60)) as nvarchar(5)) end  as AvgReadyDur " +
                  "from userinfo ui " +
                  "inner join VeritasPhone.dbo.AgentDailyCallSummary advs " +
                  "on advs.UserID = ui.UserID " +
                  "where calldate >= '" + rdpFrom.SelectedDate + "' " + 
                  "and calldate <= '" + rdpTo.SelectedDate + "' ";
            rgPhone.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgPhone.Rebind();
        }

        protected void rgPhone_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgPhone.ExportSettings.ExportOnlyData = false;
                rgPhone.ExportSettings.IgnorePaging = true;
                rgPhone.ExportSettings.OpenInNewWindow = true;
                rgPhone.ExportSettings.UseItemStyles = true;
                rgPhone.ExportSettings.FileName = "AgentDailyCallVolume";
                rgPhone.ExportSettings.Csv.FileExtension = "csv";
                rgPhone.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgPhone.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgPhone.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgPhone.ExportSettings.ExportOnlyData = false;
                rgPhone.ExportSettings.IgnorePaging = true;
                rgPhone.ExportSettings.OpenInNewWindow = true;
                rgPhone.ExportSettings.UseItemStyles = true;
                rgPhone.ExportSettings.FileName = "AgentDailyCallVolume";
                rgPhone.ExportSettings.Excel.FileExtension = "xlsx";
                rgPhone.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgPhone.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                btnRefresh_Click(sender, e);
            }
        }
    }
}