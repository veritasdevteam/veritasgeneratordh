﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cancellations.aspx.cs" Inherits="VeritasGeneratorCS.Reports.SalesReports.Cancellations" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" BorderStyle="None" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSalesReports" OnClick="btnSalesReports_Click" runat="server" BorderStyle="None" Text="Sales Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsReports" OnClick="btnClaimsReports_Click" runat="server" BorderStyle="None" Text="Claims Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnPhoneReports" OnClick="btnPhoneReports_Click" runat="server" BorderStyle="None" Text="Phone Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccountingReports" OnClick="btnAccountingReports_Click" runat="server" BorderStyle="None" Text="Accounting Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCustomReports" OnClick="btnCustomReports_Click" runat="server" BorderStyle="None" Text="Custom Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" BorderStyle="None" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" BorderStyle="None" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" BorderStyle="None" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" BorderStyle="None" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" BorderStyle="None" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" BorderStyle="None" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" OnClick="btnSettings_Click" runat="server" BorderStyle="None" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" BorderStyle="None" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" BorderStyle="None" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgDealer">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            From:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpFrom" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            To:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpTo" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Text="Search" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <telerik:RadGrid ID="rgDealer" OnItemCommand="rgDealer_ItemCommand" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" Height="600" 
                                    AllowSorting="true" AllowPaging="false" ShowFooter="false"  MasterTableView-CommandItemDisplay="TopAndBottom"
                                        Font-Names="Calibri" Font-Size="Small">
                                    <ClientSettings>
                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" /> 
                                    </ClientSettings>
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" Width="1500">
                                        <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="false" ShowAddNewRecordButton="false" />
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ContractID"  ReadOnly="true" Visible="false" UniqueName="CostractID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No" AllowFiltering="true" ShowFilterIcon="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="SaleDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" UniqueName="SoldDate" AllowFiltering="True" HeaderText="Sold Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DatePaid" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" UniqueName="DatePaid" AllowFiltering="True" HeaderText="Paid Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CancelEffDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" UniqueName="CancelEffDate" AllowFiltering="True" HeaderText="Cancel Eff. Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CancelDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" UniqueName="CancelDate" AllowFiltering="True" HeaderText="Cancel Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DealerName" UniqueName="Dealer" HeaderText="Dealer Name" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="150"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Customer" UniqueName="Customer" AllowFiltering="true" HeaderText="Customer" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Lienholder" UniqueName="Lienholder" AllowFiltering="true" HeaderText="Lien Holder" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="MoxyDealerCost" UniqueName="DealerCost" HeaderText="Dealer Cost" AllowFiltering="false" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CustomerCost" UniqueName="CustomerCost" HeaderText="Customer Cost" AllowFiltering="false" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AdminAmt" UniqueName="AdminAmt" HeaderText="Admin Amt" AllowFiltering="false" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DealerAmt" UniqueName="DealerAmt" HeaderText="Dealer Amt" AllowFiltering="false" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CustomerAmt" UniqueName="CustomerAmt" HeaderText="Refund Amt" AllowFiltering="false" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CancelFactor" UniqueName="CancelPercent" HeaderText="Cancel Percent" AllowFiltering="false" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:P2}"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </telerik:RadAjaxPanel>
                            <asp:SqlDataSource ID="SqlDataSource1" ConnectionString="server=198.143.98.122;database=veritasreports;User Id=sa;Password=NCC1701E"
                            ProviderName="System.Data.SqlClient" SelectCommand="select dealerid, DealerGroup, dealerno, dealername, d.dealerstate, agentname, 
                            subagentname, lossratio, earnratio, agentno, subagentno, contractcnt, reserve, reservecancel, reservetotal, datepaid
                            Earned, ClaimCnt, claimpaid, EarnProfit, slushamt, totclaim, trueloss, severity, freq from dealerratio d where contractcnt > 0 " runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
    </form>
</body>
</html>
