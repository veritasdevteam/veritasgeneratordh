﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Reports.SalesReports
{
    public partial class R0020 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            dsDealerType.ConnectionString = ConfigurationManager.ConnectionStrings["reportstring"].ConnectionString;
            dsFilterBy.ConnectionString = ConfigurationManager.ConnectionStrings["reportstring"].ConnectionString;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                rdpEnd.SelectedDate = DateTime.Today;
                rdpStart.SelectedDate = DateTime.Today;
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
            }

            if (hfError.Value == "Visible")
            {
                rwError.VisibleOnPageLoad = true;
            }
            else
            {
                rwError.VisibleOnPageLoad = false;
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) 
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true) 
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) 
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true) 
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true) 
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true) 
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true) 
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true) 
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true) 
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true) 
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnUsers_Click(object sender, EventArgs e) {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e) {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e) {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e) {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/salesreportssearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e) {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e) {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e) {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnPhoneReports_Click(object sender, EventArgs e) {
            Response.Redirect("~/reports/phonereports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnCustomReports_Click(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select c.contractid, contractno, fname + ' ' + lname as Customer, vin, cast(year as nvarchar(4)) + ' ' + make + ' ' + model as vehicle, p.programname + ' ' + pt.plantype,  " +
                  "cast(termmonth as nvarchar(4)) + '/' + cast(c.TermMile as nvarchar(10)) as term, saledate, c.datepaid, cc.canceldate, moxydealercost as cost, d.dealerno, d.dealername, a.agentno, a.agentname " +
                  "from contract c " +
                  "inner join dealer d on d.dealerid = c.dealerid " +
                  "inner join agents a on a.agentid = c.agentsid " +
                  "left join contractcancel cc on cc.contractid = c.contractid " +
                  "left join program p on p.programid = c.programid " +
                  "left join plantype pt on pt.plantypeid = c.plantypeid " +
                  "where c.contractid > 0 ";
            if (Convert.ToInt32(cboDealerGroup.SelectedValue) == 2) 
            {
                SQL = SQL + "and not dealerno like '2%' ";
            }
            if (Convert.ToInt32(cboDealerGroup.SelectedValue) == 3) 
            {
                SQL = SQL + "and dealerno like '2%' " +
                            "and not dealerno like '%cc' ";
            }
            if (Convert.ToInt32(cboDealerGroup.SelectedValue) == 4) 
            {
                SQL = SQL + "and dealerno like '%cc' ";
            }
            if (Convert.ToInt32(cboFilterBy.SelectedValue) == 1) 
            {
                SQL = SQL + "and saledate >= '" + rdpStart.SelectedDate + "' " +
                            "and saledate < '" + DateTime.Parse(rdpEnd.SelectedDate.ToString()).AddDays(1) + "' ";
            }
            if (Convert.ToInt32(cboFilterBy.SelectedValue) == 2) 
            {
                SQL = SQL + "and datepaid >= '" + rdpStart.SelectedDate + "' " +
                            "and datepaid < '" + DateTime.Parse(rdpEnd.SelectedDate.ToString()).AddDays(1) + "' ";
            }
            if (Convert.ToInt32(cboFilterBy.SelectedValue) == 3) 
            {
                SQL = SQL + "and canceldate >= '" + rdpStart.SelectedDate + "' " +
                            "and canceldate < '" + DateTime.Parse(rdpEnd.SelectedDate.ToString()).AddDays(1) + "' ";
            }
            dsSales.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsSales.SelectCommand = SQL;

            rgSales.Rebind();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string SQL;
            string SQLTemp;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQLTemp = "select c.contractid, contractno, fname + ' ' + lname as Customer, vin, cast(year as nvarchar(4)) + ' ' + make + ' ' + model as vehicle, p.programname + ' ' + pt.plantype as ProgramPlan,  " +
                      "cast(termmonth as nvarchar(4)) + '/' + cast(c.TermMile as nvarchar(10)) as term, saledate, c.datepaid, cc.canceldate, moxydealercost as cost, d.dealerno, d.dealername, a.agentno, a.agentname " +
                      "from contract c " +
                      "inner join dealer d on d.dealerid = c.dealerid " +
                      "inner join agents a on a.agentid = c.agentsid " +
                      "left join contractcancel cc on cc.contractid = c.contractid " +
                      "left join program p on p.programid = c.programid " +
                      "left join plantype pt on pt.plantypeid = c.plantypeid " +
                      "where c.contractid > 0 ";
            if (Convert.ToInt32(cboDealerGroup.SelectedValue) == 2) {
                SQLTemp = SQLTemp + "and not dealerno like '2%' ";
            }
            if (Convert.ToInt32(cboDealerGroup.SelectedValue) == 3) {
                SQLTemp = SQLTemp + "and dealerno like '2%' " +
                                    "and not dealerno like '%cc' ";
            }
            if (Convert.ToInt32(cboDealerGroup.SelectedValue) == 4) {
                SQLTemp = SQLTemp + "and dealerno like '%cc' ";
            }
            if (Convert.ToInt32(cboFilterBy.SelectedValue) == 1) {
                SQLTemp = SQLTemp + "and saledate >= '" + rdpStart.SelectedDate + "' " +
                                    "and saledate < '" + DateTime.Parse(rdpEnd.SelectedDate.ToString()).AddDays(1) + "' ";
            }
            if (Convert.ToInt32(cboFilterBy.SelectedValue) == 2) {
                SQLTemp = SQLTemp + "and datepaid >= '" + rdpStart.SelectedDate + "' " +
                                    "and datepaid < '" + DateTime.Parse(rdpEnd.SelectedDate.ToString()).AddDays(1) + "' ";
            }
            if (Convert.ToInt32(cboFilterBy.SelectedValue) == 3) {
                SQLTemp = SQLTemp + "and canceldate >= '" + rdpStart.SelectedDate + "' " +
                                    "and canceldate < '" + DateTime.Parse(rdpEnd.SelectedDate.ToString()).AddDays(1) + "' ";
            }
            SQL = "select * from veritasreports.dbo.reportrequest " +
                  "where idx = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0) {
                clR.NewRow();
                clR.SetFields("reportname", "Sales Report");
                clR.SetFields("reportfile", "salesreport.csv");
                clR.SetFields("requestbyname", Functions.GetUserInfo(long.Parse(hfUserID.Value)));
                clR.SetFields("requestbyemail", Functions.GetUserEmail(long.Parse(hfUserID.Value)));
                clR.SetFields("requestdate", DateTime.Now.ToString());
                clR.SetFields("reportheader", "contractid,contractno,Customer,vin,vehicle,ProgramPlan,term,saledate,datepaid,canceldate,cost,dealerno, dealername, agentno, agentname");
                //clR.FieldsWith1quote("reportsql", SQLTemp);
                clR.SetFields("reportsql", SQLTemp);
                clR.AddRow();
                clR.SaveDB();
            }
        }

        protected void rgSales_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgSales.ExportSettings.ExportOnlyData = false;
                rgSales.ExportSettings.IgnorePaging = true;
                rgSales.ExportSettings.OpenInNewWindow = true;
                rgSales.ExportSettings.UseItemStyles = true;
                rgSales.ExportSettings.FileName = "AgentDailyCallVolume";
                rgSales.ExportSettings.Csv.FileExtension = "csv";
                rgSales.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgSales.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgSales.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgSales.ExportSettings.ExportOnlyData = false;
                rgSales.ExportSettings.IgnorePaging = true;
                rgSales.ExportSettings.OpenInNewWindow = true;
                rgSales.ExportSettings.UseItemStyles = true;
                rgSales.ExportSettings.FileName = "AgentDailyCallVolume";
                rgSales.ExportSettings.Excel.FileExtension = "xlsx";
                rgSales.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgSales.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                btnSubmit_Click(sender, e);
            }
        }
    }
}