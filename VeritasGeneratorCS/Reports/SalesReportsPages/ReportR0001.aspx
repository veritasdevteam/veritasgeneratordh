﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportR0001.aspx.cs" Inherits="VeritasGeneratorCS.Reports.SalesReports.ReportR0001" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="Veritas.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>R0001</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" BorderStyle="None" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSalesReports" OnClick="btnSalesReports_Click" runat="server" BorderStyle="None" Text="Sales Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsReports" OnClick="btnClaimsReports_Click" runat="server" BorderStyle="None" Text="Claims Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnPhoneReports" OnClick="btnPhoneReports_Click" runat="server" BorderStyle="None" Text="Phone Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccountingReports" OnClick="btnAccountingReports_Click" runat="server" BorderStyle="None" Text="Accounting Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCustomReports" OnClick="btnCustomReports_Click" runat="server" BorderStyle="None" Text="Custom Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" BorderStyle="None" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" BorderStyle="None" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" BorderStyle="None" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" BorderStyle="None" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" BorderStyle="None" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" BorderStyle="None" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" OnClick="btnSettings_Click" runat="server" BorderStyle="None" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" BorderStyle="None" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" BorderStyle="None" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadGrid ID="rgReport" OnInsertCommand="rgReport_InsertCommand" OnExcelMLWorkBookCreated="rgReport_ExcelMLWorkBookCreated" runat="server" AllowPaging="true" PageSize="25" AutoGenerateColumns="false" AllowFilteringByColumn="true" AllowSorting="true" DataSourceID="SqlDataSource1">
                                            <ExportSettings Csv-ColumnDelimiter="Comma" IgnorePaging="true" ExportOnlyData="true"></ExportSettings>
                                            <GroupingSettings CaseSensitive="false" />
                                            <MasterTableView DataKeyNames="ContractID" DataSourceID="SqlDataSource1" AllowFilteringByColumn="true" AutoGenerateColumns="false" AllowSorting="true" CommandItemDisplay="Top">
                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToCsvButton="true"/>
                                                <Columns>
                                                    <telerik:GridBoundColumn DataField="ContractID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CustomerID" HeaderText="Customer ID" UniqueName="CustomerID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CustomerFName" HeaderText="First Name" UniqueName="CustomerFName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CustomerLName" HeaderText="Last Name" UniqueName="CustomerLName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CustomerAddr1" HeaderText="Addr 1" UniqueName="CustomerAddr1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CustomerAddr2" HeaderText="Addr 2" UniqueName="CustomerAddr2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CustomerCity" HeaderText="City" UniqueName="CustomerCity" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CustomerState" HeaderText="State" UniqueName="CustomerState" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CustomerZip" HeaderText="Zip" UniqueName="CustomerZip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CustomerPhone" HeaderText="Phone" UniqueName="CustomerPhone" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="AgentName" HeaderText="Agent Name" UniqueName="AgentName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="MGA" HeaderText="MGA" UniqueName="MGA" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="DealerNo" HeaderText="Dealer No" UniqueName="DealerNo" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="DealerName" HeaderText="Dealer Name" UniqueName="DealerName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="DealerCost" HeaderText="Dealer Cost" UniqueName="DealerCost" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CustomerCost" HeaderText="Customer Cost" UniqueName="CustomerCost" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Markup" HeaderText="Markup" UniqueName="Markup" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="TotalSurcharge" HeaderText="Total Surcharge" UniqueName="TotalSurcharge" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="AgentFee" HeaderText="Agent Fee" UniqueName="AgentFee" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="ClassFee" HeaderText="Class Fee" UniqueName="ClassFee" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="PPA" HeaderText="PPA" UniqueName="PPA" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="PPAFee" HeaderText="PPA Fee" UniqueName="PPAFee" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="SoldDate" HeaderText="Sold Date" UniqueName="SoldDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Deductible" HeaderText="Deductible" UniqueName="Deductible" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="VSCPlan" HeaderText="VSC Plan" UniqueName="VSCPlan" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="TermMonth" HeaderText="Term Month" UniqueName="TermMonth" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="TermMile" HeaderText="Term Mile" UniqueName="TermMile" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="FINTerm" HeaderText="FINTerm" UniqueName="FINTerm" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="DecForm" HeaderText="Dec Form" UniqueName="DecForm" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="TC" HeaderText="Term / Condition" UniqueName="TC" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="ContractNo" HeaderText="Contract No" UniqueName="ContractNo" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Class" HeaderText="Class" UniqueName="Class" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="VIN" HeaderText="VIN" UniqueName="VIN" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Make" HeaderText="Make" UniqueName="Make" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Model" HeaderText="Model" UniqueName="Model" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Trim" HeaderText="Trim" UniqueName="Trim" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surDiesel" HeaderText="Diesel" UniqueName="surDiesel" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="sur4WD" HeaderText="4WD" UniqueName="sur4WD" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surTurbo" HeaderText="Turbo" UniqueName="surTurbo" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surHybrid" HeaderText="Hybrid" UniqueName="surHybrid" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surCommercial" HeaderText="Commercial" UniqueName="surCommercial" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surHydraulic" HeaderText="Hydraulic" UniqueName="surHydraulic" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surAirBladder" HeaderText="Air Bladder" UniqueName="surAirBladder" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surLiftKit" HeaderText="Lift Kit" UniqueName="surLiftKit" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surLiftKit78" HeaderText="Lift Kit 78" UniqueName="surLiftKit78" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surLuxury" HeaderText="Luxury" UniqueName="surLuxury" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surSeals" HeaderText="Seals" UniqueName="surSeals" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="surSnowPlow" HeaderText="Snow Plow" UniqueName="surSnowPlow" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="DealStatus" HeaderText="Deal Status" UniqueName="DealStatus" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="PaidDate" HeaderText="Paid Date" UniqueName="PaidDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="PaidStatus" HeaderText="Paid Status" UniqueName="PaidStatus" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="BasePrice" HeaderText="Base Price" UniqueName="BasePrice" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="DateAdded" HeaderText="Date Added" UniqueName="DateAdded" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="DateUpdated" HeaderText="DateUpdated" UniqueName="DateUpdated" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Program" HeaderText="Program" UniqueName="Program" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="SaleMiles" HeaderText="Sale Miles" UniqueName="SaleMiles" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="EffDate" HeaderText="Eff Date" UniqueName="EffDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="EffMile" HeaderText="Eff Mile" UniqueName="EffMile" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="CancelDate" HeaderText="Cancel Date" UniqueName="CancelDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="AgentNo" HeaderText="Agent No" UniqueName="AgentNo" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="PlanCode" HeaderText="Plan Code" UniqueName="PlanCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>


                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                        <asp:SqlDataSource ID="SqlDataSource1"
                                        ProviderName="System.Data.SqlClient" SelectCommand="select ContractID, CustomerID, 
                                        CustomerFName, CustomerLName, CustomerAddr1, CustomerAddr2, CustomerCity, CustomerState, CustomerZip,
                                            CustomerPhone, AgentName, MGA, DealerNo, DealerName, DealerCost, CustomerCost, Markup, TotalSurcharge,
                                            AgentFee, ClassFee, PPAFee, VSCPlan, SoldDate, Deductible, FINTerm, TermMonth, TermMile, DecForm, TC,
                                            Class, VIN, ContractNo, Year, Make, Model, Trim, surDiesel, surHydraulic, sur4WD, surTurbo, surHybrid,
                                            surCommercial, surAirBladder, surLiftKit, surLiftKit78, surLuxury, surSnowPlow, surSeals, DealStatus,
                                            PaidDate, PaidStatus, BasePrice, DateAdded, DateUpdated, SaleMiles, Program, EffDate, CancelDate,
                                            EffMile, CancelDate, PlanCode, AgentNo
                                        from VeritasMoxy.dbo.moxycontract " runat="server"></asp:SqlDataSource>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
    </form>
</body>
</html>
