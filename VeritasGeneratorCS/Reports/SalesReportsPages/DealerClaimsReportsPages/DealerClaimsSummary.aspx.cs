﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Reports.SalesReportsPages.DealerClaimsReportsPages
{
    public partial class DealerClaimsSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfDealerID.Value = Request.QueryString["dealerid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                FillPage();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillPage()
        {
            double dEarn;
            double dClaim;
            double dLoss;
            dEarn = CalcEarn();
            dClaim = CalcClaim();
            dLoss = dClaim / dEarn;
            lblContractEarn.Text = dEarn.ToString("#,##0.00");
            lblClaims.Text = dClaim.ToString("#,##0.00");
            lblLossRatio.Text = dLoss.ToString("#,##0.00%");
        }
        private double CalcEarn()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select sum(EarnAmt) as EA from contract c " +
                  "inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID " +
                  "where dealerid = " + hfDealerID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return Convert.ToDouble(clR.GetFields("ea"));
            }
            return 0;
        }
        private double CalcClaim()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select sum(cd.paidamt) as PA from claim cl " +
                  "inner join claimdetail cd on cd.claimid = cl.claimid " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "inner join ClaimReason cr on cr.ClaimReasonID = cd.ClaimReasonID " +
                  "inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID " +
                  "where cl.ContractID in (select contractid from contract where dealerid = " + hfDealerID.Value + ") ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return Convert.ToDouble(clR.GetFields("pa"));
            }
            return 0;
        }
    }
}