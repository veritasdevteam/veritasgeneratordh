﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClaimsReport.ascx.cs" Inherits="VeritasGeneratorCS.Reports.SalesReportsPages.DealerClaimsReportsPages.ClaimsReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="btnRun" OnClick="btnRun_Click" runat="server" Text="Run" BackColor="#1eabe2" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgClaims">
                <telerik:RadGrid ID="rgClaims" OnPreRender="rgClaims_PreRender" OnItemCommand="rgClaims_ItemCommand" OnDataBound="rgClaims_DataBound" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                    AllowSorting="true" AllowPaging="true"  Width="99%" ShowFooter="true" DataSourceID="SqlDataSource1">
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" PageSize="10" ShowFooter="true" CommandItemDisplay="Top">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                        <Columns>
                            <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim Number"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Status" UniqueName="Status" HeaderText="Status"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="LossDate" UniqueName="LossDate" HeaderText="Loss Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="Paid Amount"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="false">
                        <Selecting AllowRowSelect="false" />
                        <Resizing EnableRealTimeResize="true" AllowColumnResize="true" ClipCellContentOnResize="true" ResizeGridOnColumnResize="true"/>
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource1"
                ProviderName="System.Data.SqlClient" 
                SelectCommand="select cl.ClaimNo, cl.Status, cl.LossDate, sum(cd.PaidAmt) as PaidAmt from Claim cl 
                               join ClaimDetail cd on cl.claimid = cd.claimid 
                               where cl.ContractID in (select clm.ContractID from claim clm 
                               join Contract con on clm.ContractID = con.ContractID 
                               where con.DealerID = @DealerID) 
                               group by cl.ClaimNo, cl.status, cl.LossDate " runat="server">
                <SelectParameters>
                    <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
                </asp:SqlDataSource>
            </telerik:RadAjaxPanel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfDealerID" runat="server" />

