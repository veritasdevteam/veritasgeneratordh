﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Reports.SalesReportsPages.DealerClaimsReportsPages
{
    public partial class DealerClaims60Day : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfDealerID.Value = Request.QueryString["dealerid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                FillGrid();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillGrid()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select cl.claimid, contractno, c.status, c.SaleDate, fname + ' ' + LName as Customer, " +
                  "pt.PlanType, claimno, lossdate, DATEDIFF(d, SaleDate, cl.CreDate) as days, " +
                  "cl.LossMile - c.SaleMile as Miles, cd.ClaimDetailStatus, case when sum(paidamt) is null then 0  else sum(paidamt) end as PaidAmt " +
                  "from claim cl " +
                  "inner join claimdetail cd on cd.claimid = cl.claimid " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "inner join ClaimReason cr on cr.ClaimReasonID = cd.ClaimReasonID " +
                  "inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID " +
                  "where cl.ContractID in (select contractid from contract where dealerid = " + hfDealerID.Value + ") " +
                  "and DATEDIFF(d, SaleDate, cl.CreDate) > 30 and DATEDIFF(d, SaleDate, cl.CreDate) < 91 " +
                  "group by contractno, claimno, cd.ClaimDetailStatus, lossdate, cl.CreDate, SaleDate,cd.ClaimReasonID, " +
                  "SaleMile, LossMile, fname, lname,pt.PlanType,c.Status, c.SaleDate, cl.claimid ";
            rgClaim.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
    }
}