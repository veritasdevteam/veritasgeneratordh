﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VeritasGeneratorCS.Class
{
    public class cDate
    {
        public string sStartDate;
        public string sEndDate;

        public cDate()
        {
            sStartDate = DateTime.Today.ToString();
            sEndDate = DateTime.Today.AddDays(-30).ToString();
        }

        public void setDates(int date)
        {
            //7 days
            if (date == 0)
            {
                sEndDate = DateTime.Today.AddDays(-7).ToString();
            }
            //30 days
            else if (date == 1)
            {
                sEndDate = DateTime.Today.AddDays(-30).ToString();
            }
            //90 days
            else if (date == 2)
            {
                sEndDate = DateTime.Today.AddDays(-90).ToString();
            }
            //1 year
            else if(date == 3)
            {
                sEndDate = DateTime.Today.AddYears(-1).ToString();
            }
            else
            {
                sEndDate = DateTime.Today.AddDays(-7).ToString();
            }
        }

        public string betweenDates(string date,int type)
        {
            string returnString = "";
            if (date == "All Time")
            {
                return returnString;
            }
            else if(type == 0)
            {
                string[] convert = date.Split('/');
                DateTime startDate = Convert.ToDateTime(convert[0] + "/1/" + convert[1]);
                DateTime endDate = startDate.AddMonths(1);
                returnString = "and cd.DatePaid >= '"+startDate.ToString()+"' and cd.DatePaid <'"+endDate.ToString()+"'";

            }
            else
            {
                string[] convert = date.Split('/');
                DateTime startDate = Convert.ToDateTime(convert[0] + "/1/" + convert[1]);
                DateTime endDate = startDate.AddMonths(1);
                returnString = "and AuthDate >= '" + startDate.ToString() + "' and AuthDate <'" + endDate.ToString() + "'";
            }
            return returnString;
        }
    }
}