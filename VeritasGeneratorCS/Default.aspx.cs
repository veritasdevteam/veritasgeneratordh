﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using VeritasGeneratorCS.Class;
using Telerik.Web.UI;

public partial class Default : System.Web.UI.Page
{
    private long lUserID;
    private long lTeamID;
    private bool bManager;
    private bool bExecutive; 

    protected void Page_Load(object sender, EventArgs e)
    {
        tbTodo.Width = pnlHeader.Width;
        if (!IsPostBack)
        {
            GetServerInfo();
            CheckToDo();
            LockButtons();
            if (lUserID == 0)
            {
                hfSigninPopup.Value = "Visible";
                string Script;
                Script = "function f(){$find(\"" + rwSignIn.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwSignIn", Script, true);
                rwSignIn.VisibleOnPageLoad = true;
                //LockButtons();
            }
            else
            {
                setDashboard();
            }
            UnlockButtons();
            if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
            }
        }
    }
    private void CheckToDo()
    {
        hlToDo.Visible = false;
        hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
        string SQL;
        clsDBO.clsDBO clTD = new clsDBO.clsDBO();
        SQL = "select * from usermessage " +
              "where toid = " + lUserID + " " +
              "and completedmessage = 0 " +
              "and deletemessage = 0 ";
        clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        if (clTD.RowCount() > 0)
        {
            hlToDo.Visible = true;
        }
        else
        {
            hlToDo.Visible = false;
        }
    }
    private void GetServerInfo()
    {
        string SQL;
        clsDBO.clsDBO clSI = new clsDBO.clsDBO();
        DateTime sStartDate;
        DateTime sEndDate;
        sStartDate = DateTime.Today;
        sEndDate = DateTime.Today.AddDays(1);
        hfID.Value = Request.QueryString["sid"];        
        SQL = "select * from serverinfo " +
              "where systemid = '" + hfID.Value + "' " +
              "and signindate >= '" + sStartDate + "' " +
              "and signindate <= '" + sEndDate + "' ";
        clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        lUserID = 0;
        if (clSI.RowCount() > 0)
        {
            clSI.GetRow();
            lUserID = long.Parse(clSI.GetFields("userid"));
        }
    }
    private void LockButtons()
    {
        btnAccounting.Enabled = false;
        btnAgents.Enabled = false;
        btnClaim.Enabled = false;
        btnDealer.Enabled = false;
        btnContract.Enabled = false;
        btnSettings.Enabled = false;
        btnUsers.Enabled = false;
        btnUsers.Enabled = false;
        btnContract.Enabled = false;
        btnReports.Enabled = false;
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        string SQL;
        lblError.Text = "";
        if (txtEMail.Text == "")
        {
            lblError.Text = "No E-Mail was entered.";
            ShowError();
            return;
        }
        if (txtPassword.Text == "")
        {
            lblError.Text = "No Password was entered.";
            ShowError();
            return;
        }
        SQL = "select * from userinfo " +
              "where email = '" + txtEMail.Text + "' " +
              "and password = '" + txtPassword.Text + "' " +
              "and active <> 0";
        clsDBO.clsDBO clU = new clsDBO.clsDBO();
        clU.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        if (clU.RowCount() == 0)
        {
            lblError.Text = "Cannot locate user name/password.";
            ShowError();
            return;
        }
        clU.GetRow();
        lUserID = long.Parse(clU.GetFields("userid"));
        hfID.Value = System.Guid.NewGuid().ToString();
        clsDBO.clsDBO clSI = new clsDBO.clsDBO();
        //'        SQL = "delete serverinfo where userid = " & lUserID
        //'clSI.RunSQL(SQL, sCON)
        SQL = "select * from serverinfo where userid = " + lUserID;
        clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        clSI.NewRow();
        clSI.SetFields("userid", lUserID.ToString());
        clSI.SetFields("systemid", hfID.Value);
        clSI.SetFields("signindate", DateTime.Now.ToString());
        clSI.AddRow();
        clSI.SaveDB();
        hfSigninPopup.Value = "";
        string script = "function f(){$find(\"" + rwSignIn.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        UnlockButtons();
        CheckToDo();
        setDashboard();
    }
    private void UnlockButtons()
    {
        string SQL;
        clsDBO.clsDBO clSI = new clsDBO.clsDBO();
        SQL = "select * from usersecurityinfo where userid = " + lUserID;
        clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        if (clSI.RowCount() > 0)
        {
            clSI.GetRow();
            btnUsers.Enabled = true;
            if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
            {
                btnAccounting.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
            {
                btnSettings.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
            {
                btnAgents.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
            {
                btnDealer.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
            {
                btnClaim.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
            {
                btnContract.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
            {
                btnReports.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
            {
                btnReports.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
            {
                btnReports.Enabled = true;
            }
            if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
            {
                btnReports.Enabled = true;
            }
        }
    }
    private void setDashboard()
    {
        string sSQL = "";
        clsDBO.clsDBO cLR = new clsDBO.clsDBO();
        getTeamInfo();
        if (bExecutive)
        {
            cDate cDate = new cDate();
            mvDashboardType.ActiveViewIndex = 0;
            cDate.setDates(rblNewClaims.SelectedIndex);            
            sSQL = "Declare @startDate as date = '" + cDate.sEndDate + "' select (case when DATEDIFF(day,@startDate, GETDATE()) < 91 then cast(CreDate as date) else CAST(cast(MONTH(CreDate) as nvarchar)+'/1/'+CAST(YEAR(CreDate) as nvarchar) as date) end) as CreateDate, COUNT(*) as numOfClaims, 0 as goal from claim where CreDate >= @startDate and ((DATEPART(dw, CreDate) + @@DATEFIRST) % 7) NOT IN (0, 1) group by (case when DATEDIFF(day,@startDate, GETDATE()) < 91 then cast(CreDate as date) else CAST(cast(MONTH(CreDate) as nvarchar)+'/1/'+CAST(YEAR(CreDate) as nvarchar) as date) end) order by CreateDate";
            htmlNewClaims.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            htmlNewClaims.DataBind();            
            sSQL = "select count(*) as counter from Claim where Status = 'Open'";
            cLR.OpenDB(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            try
            {
                cLR.GetRow();
                txtClaimOpen.Text =cLR.GetFields("counter");
            }
            catch (Exception ex)
            {
                txtClaimOpen.Text = "0";
            }     
            sSQL = "select sum(cd.totalamt) as amt, count(distinct claimno) as cnt, " +
                "FORMAT(case when (sum(cd.totalamt) / count(distinct claimno)) is null then  0 else sum(cd.totalamt) / count(distinct claimno) end,'c') as Severity " +
                "from claim cl inner join claimdetail cd on cl.ClaimID = cd.claimid " +
                "where ClaimDetailType != 'Inspect' " +
                "and ClaimDetailStatus in ('Paid') " +
                "and ClaimActivityID = 56 " + cDate.betweenDates(ddlDates.SelectedItem.ToString(),0);
            cLR.OpenDB(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);            
            try
            {
                cLR.GetRow();
                txtSeverity.Text =cLR.GetFields("Severity");
            }
            catch (Exception ex)
            {
                txtSeverity.Text = "0";
            }
            sSQL = "select sum(cd.totalamt) as amt, count(distinct claimno) as cnt, FORMAT(sum(cd.totalamt) / count(distinct claimno),'c') as Severity from claim cl " +
                "inner join claimdetail cd on cl.ClaimID = cd.claimid " +
                "where ClaimDetailType != 'Inspect' " +
                "and ClaimDetailStatus in ('Authorized', 'Approved') " + cDate.betweenDates(ddlDates.SelectedItem.ToString(),1);
            cLR.OpenDB(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            try
            {
                cLR.GetRow();
                txtClaimsOverAuth.Text = cLR.GetFields("Severity");
            }
            catch (Exception ex)
            {
                txtClaimsOverAuth.Text = "0";
            }
            cDate.setDates(rblClaimsInfo.SelectedIndex);
            sSQL = "Declare @date as date = '" + cDate.sEndDate + "' select PaidANRMF.PaidANRMF, PaidANReserve.PaidANReserve,PaidNonAN.PaidNonAN,PaidRMF.PaidRMF,PaidNonRMF.PaidNonRMF,Total.Total from (select FORMAT(sum(PaidAmt),'c') as PaidANRMF, '1' as Joiner from ClaimDetail where RateTypeID = 1101 and ClaimDetailStatus = 'PAID' and DatePaid > @date) PaidANRMF left join (select FORMAT(sum(PaidAmt),'c') as PaidANReserve, '1' as Joiner from ClaimDetail where RateTypeID = 1 and ClaimDetailStatus = 'PAID'and DatePaid > @date and ClaimID in(select ClaimID from claim where contractid in (select contractid from contract where AN = 1))) PaidANReserve on PaidANReserve.Joiner = PaidANRMF.Joiner left join (select FORMAT(sum(PaidAmt),'c') as PaidNonAN, '1' as Joiner from ClaimDetail where RateTypeID != 1101 and ClaimDetailStatus = 'PAID'and DatePaid > @date) PaidNonAN on PaidANRMF.Joiner = PaidNonAN.Joiner left join (select FORMAT(sum(PaidAmt),'c') as PaidRMF, '1' as Joiner from ClaimDetail where RateTypeID != 1 and ClaimDetailStatus = 'PAID' and DatePaid > @date) PaidRMF on PaidANRMF.Joiner = PaidRMF.Joiner left join (select FORMAT(sum(PaidAmt),'c') as PaidNonRMF, '1' as Joiner from ClaimDetail where RateTypeID = 1 and ClaimDetailStatus = 'PAID' and DatePaid > @date) PaidNonRMF on PaidANRMF.Joiner = PaidNonRMF.Joiner  left join (select FORMAT(sum(PaidAmt), 'c') as Total, '1' as Joiner from ClaimDetail where ClaimDetailStatus = 'Paid' and DatePaid > @date) as Total on PaidANRMF.Joiner = Total.Joiner";
            rgClaimsPaid.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "Declare @date as date = '" + cDate.sEndDate + "' select ANRMF.AuthANRMF,AuthAN.AuthANReserve,AuthNonAN.AuthNonAN,AuthRMF.AuthRMF,AuthNonRMF, total.total from (select FORMAT(sum(AuthAmt),'c') as AuthANRMF, '1' as Joiner from ClaimDetail where RateTypeID = 1101 and ClaimDetailStatus = 'Authorized' and DateAuth > @date) ANRMF left join (select case when sum(AuthAmt) is null then FORMAT(0,'c') else FORMAT(sum(AuthAmt),'c') end as AuthANReserve, '1' as Joiner from ClaimDetail where RateTypeID = 1 and ClaimDetailStatus = 'Authorized'and DatePaid > @date and ClaimID in(select ClaimID from claim where contractid in (select contractid from contract where AN = 1))) AuthAN on ANRMF.Joiner = AuthAN.Joiner left join (select FORMAT(sum(AuthAmt),'c') as AuthNonAN, '1' as Joiner from ClaimDetail where RateTypeID != 1101 and ClaimDetailStatus = 'Authorized'and DateAuth > @date) as AuthNonAN on ANRMF.Joiner = AuthNonAN.Joiner left join (select FORMAT(sum(AuthAmt),'c') as AuthRMF, '1' as Joiner from ClaimDetail where RateTypeID != 1 and ClaimDetailStatus = 'Authorized' and DateAuth > @date) as AuthRMF on ANRMF.Joiner = AuthRMF.Joiner left join (select FORMAT(sum(AuthAmt),'c') as AuthNonRMF, '1' as Joiner from ClaimDetail where RateTypeID = 1 and ClaimDetailStatus = 'Authorized' and DateAuth > @date) as AuthNonRMF on ANRMF.Joiner = AuthNonRMF.Joiner left join (select FORMAT(sum(AuthAmt), 'c') as total, '1' as joiner from ClaimDetail where ClaimDetailStatus = 'Authorized' and DateAuth > @date) Total  on ANRMF.Joiner = Total.joiner ";
            rgClaimsAuthorized.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            cDate.setDates(rblAverageClose.SelectedIndex);
            sSQL = "Declare @startDate as date = '" + cDate.sEndDate + "' select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(CloseDate as date) else CAST(cast(MONTH(CloseDate) as nvarchar) + '/1/' + CAST(YEAR(CloseDate) as nvarchar) as date) end) as closeDates, sum(DATEDIFF(DAY, CreDate, CloseDate)) / count(*) as average from Claim    where CloseDate >= @startDate and((DATEPART(dw, CloseDate) + @@DATEFIRST) % 7) NOT IN(0, 1)  group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(CloseDate as date) else CAST(cast(MONTH(CloseDate) as nvarchar) + '/1/' + CAST(YEAR(CloseDate) as nvarchar) as date) end) order by closeDates ";
            htmlAverageClose.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            cDate.setDates(rblHeadCount.SelectedIndex);
            sSQL = "select count(distinct userid) as numberOfUsers, cast(SignInDate as date) as date, 0 as goal from ServerInfo where SignInDate >= '" + cDate.sEndDate + "' and ((DATEPART(dw, SignInDate) + @@DATEFIRST) % 7) NOT IN (0, 1) and UserID in (select UserID from UserSecurityInfo where TeamID in (2,3,4,11,13)) group by cast(SignInDate as date) order by cast(SignInDate as date)"; 
            htmlHeadCount.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            cDate.setDates(rblSales.SelectedIndex);
            sSQL = "Declare @startDate as date = '" + cDate.sEndDate + "' " +
                "select an.SoldDate as salesDate, " +
                "case when an.cnt is null then 0 else an.cnt end as ANCnt, " +
                "case when vero.cnt is null then 0 else vero.cnt end  as VeroCnt, " +
                "case when other.cnt is null then 0 else other.cnt end as OtherCnt, " +
                "case when ep.cnt is null then 0 else ep.cnt end as EPCnt, case when an.cnt is null then 0 else an.cnt end+ case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end as TotalCnt, " +
                "AVG(case when an.cnt is null then 0 else an.cnt end+ case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end) over(order by an.solddate Rows between 29 Preceding and current Row) as average " +
                "from(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end) as SoldDate, " +
                "count(*) as cnt from VeritasMoxy.dbo.moxycontract where AgentName = 'Auto Nation Agent' and not contractno is null and SoldDate >= @startDate " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end)) " +
                "an left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end) as SoldDate, " +
                "count(*) as cnt from VeritasMoxy.dbo.moxycontract  where agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and not contractno is null and SoldDate >= @startDate " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end)) vero on vero.SoldDate = an.SoldDate " +
                "left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end) as SoldDate, " +
                "count(*) as cnt from VeritasMoxy.dbo.moxycontract where not agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and not AgentName = 'Auto Nation Agent' and not contractno is null and SoldDate >= @startDate " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end)) Other on other.SoldDate = an.SoldDate " +
                "left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end) as SoldDate, " +
                "count(*) as cnt from VeritasMoxy.dbo.epcontract where not contractno is null and SoldDate >= @startDate " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(SoldDate as date) else CAST(cast(MONTH(SoldDate) as nvarchar) + '/1/' + CAST(YEAR(SoldDate) as nvarchar) as date) end)) " +
                "EP on EP.SoldDate = an.SoldDate " +
                "order by an.SoldDate desc";             
            htmlSales.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            cDate.setDates(rblPaid.SelectedIndex);
            sSQL = "Declare @startDate as date = '" + cDate.sEndDate + "' " +
                "select an.PaidDate as paidDates, case when an.cnt is null then 0 else an.cnt end as ANCnt, " +
                "case when vero.cnt is null then 0 else vero.cnt end  as VeroCnt, " +
                "case when other.cnt is null then 0 else other.cnt end as OtherCnt, " +
                "case when ep.cnt is null then 0 else ep.cnt end as EPCnt,  " +
                "case when an.cnt is null then 0 else an.cnt end + case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end as TotalCnt, " +
                "AVG(case when an.cnt is null then 0 else an.cnt end+ case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end) over(order by an.PaidDate Rows between 29 Preceding and current Row) as average " +
                "from(select(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(DatePaid as date) else CAST(cast(MONTH(DatePaid) as nvarchar) + '/1/' + CAST(YEAR(DatePaid) as nvarchar) as date) end) as PaidDate,  " +
                "count(*) as cnt from contract where AN = 1 and Status = 'PAID' and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105) and DatePaid >= @startDate " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(DatePaid as date) else CAST(cast(MONTH(DatePaid) as nvarchar) + '/1/' + CAST(YEAR(DatePaid) as nvarchar) as date) end)) an " +
                "left join(select(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(PaidDate as date) else CAST(cast(MONTH(PaidDate) as nvarchar) + '/1/' + CAST(YEAR(PaidDate) as nvarchar) as date) end) as PaidDate, " +
                "count(*) as cnt from VeritasMoxy.dbo.moxycontract  where agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and not contractno is null and PaidDate >= @startDate " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(PaidDate as date) else CAST(cast(MONTH(PaidDate) as nvarchar) + '/1/' + CAST(YEAR(PaidDate) as nvarchar) as date) end)) vero on vero.PaidDate = an.PaidDate " +
                "left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(PaidDate as date) else CAST(cast(MONTH(PaidDate) as nvarchar) + '/1/' + CAST(YEAR(PaidDate) as nvarchar) as date) end) as PaidDate, " +
                "count(*) as cnt from VeritasMoxy.dbo.moxycontract where not agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and not AgentName = 'Auto Nation Agent' and not contractno is null and PaidDate >= @startDate " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(PaidDate as date) else CAST(cast(MONTH(PaidDate) as nvarchar) + '/1/' + CAST(YEAR(PaidDate) as nvarchar) as date) end)) Other on other.PaidDate = an.PaidDate " +
                "left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(DatePaid as date) else CAST(cast(MONTH(DatePaid) as nvarchar) + '/1/' + CAST(YEAR(DatePaid) as nvarchar) as date) end) as PaidDate, " +
                "count(*) as cnt from Contract where ContractNo like '%EP%' and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105) and Status = 'Paid' and DatePaid >= @startDate " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(DatePaid as date) else CAST(cast(MONTH(DatePaid) as nvarchar) + '/1/' + CAST(YEAR(DatePaid) as nvarchar) as date) end)) EP on EP.PaidDate = an.PaidDate " +
                "order by an.PaidDate desc";
            htmlPaid.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            cDate.setDates(rblCancel.SelectedIndex);
            sSQL = "Declare @startDate as date = '" + cDate.sEndDate + "' " +
                "select an.CancelDate, " +
                "case when an.cnt is null then 0 else an.cnt end as ANCnt, " +
                "case when vero.cnt is null then 0 else vero.cnt end as VeroCnt, " +
                "case when ep.cnt is null then 0 else ep.cnt end as EPCnt, " +
                "case when Other.cnt is null then 0 else Other.cnt end as OtherCnt, " +
                "case when an.cnt is null then 0 else an.cnt end + case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end as TotalCnt, " +
                "AVG(case when an.cnt is null then 0 else an.cnt end+ case when vero.cnt is null then 0 else vero.cnt end +other.cnt + case when ep.cnt is null then 0 else ep.cnt end) over(order by an.CancelDate Rows between 29 Preceding and current Row) as average " +
                "from (select(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end) as CancelDate, " +
                "COUNT(*) as cnt from ContractCancel cc inner join contract c on c.ContractID = cc.ContractID where AN = 1 and CancelDate >= @startDate and not CancelDate is null and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105) " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end)) an " +
                "left join(select(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end) " +
                "as CancelDate, COUNT(*) as cnt from ContractCancel cc inner join contract c on c.ContractID = cc.ContractID inner join VeritasMoxy.dbo.moxycontract vm on vm.ContractNo = c.ContractNo where agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and cc.CancelDate >= @startDate and not cc.CancelDate is null and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105) " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end)) vero on vero.CancelDate = an.CancelDate " +
                "left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end) as CancelDate," +
                "COUNT(*) as cnt from ContractCancel cc inner join contract c on c.ContractID = cc.ContractID where not cc.CancelDate is null and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105) and ContractNo in (select contractno from VeritasMoxy.dbo.EPContract)  and cc.CancelDate >= @startDate " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end)) ep on ep.CancelDate = an.CancelDate " +
                "left join(select (case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end) as CancelDate, " +
                "COUNT(*) as cnt from ContractCancel cc inner join contract c on c.ContractID = cc.ContractID where not cc.CancelDate is null and not programid in (47, 48, 51, 54, 55, 56, 57, 58, 59, 60, 67, 101, 103, 105)  and cc.CancelDate >= @startDate and not AN = 1and not ContractNo like '%EP%'and not ContractNo in (select ContractNo from VeritasMoxy.dbo.moxycontract vm where agentname in ('Vero LLC', 'Financial Indemnity Group', 'Michael Clause', 'David Bailes', 'David Collins', 'Paul Coughlin') and not ContractNo is null) " +
                "group by(case when DATEDIFF(day, @startDate, GETDATE()) < 91 then cast(cc.CancelDate as date) else CAST(cast(MONTH(cc.CancelDate) as nvarchar) + '/1/' + CAST(YEAR(cc.CancelDate) as nvarchar) as date) end)) Other on Other.CancelDate = an.CancelDate " +
                "order by an.CancelDate";
            htmlCancel.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            cDate.setDates(rblNewClaims.SelectedIndex);
            fillDropDownDates();
            try {             
                rgClaimsPaid.DataBind();
                rgClaimsAuthorized.DataBind();
                htmlAverageClose.DataBind();
                htmlHeadCount.DataBind();
                htmlSales.DataBind();
                htmlCancel.DataBind();
                htmlPaid.DataBind();
                htmlCancel.DataBind();
            } catch { }
        }
        else if (bManager)
        {
            sSQL = "select ui.FName + ' ' +  ui.LName as names, ui.UserID from UserInfo ui left join UserSecurityInfo usi on usi.UserID = ui.UserID where TeamID =" + lTeamID + " and Active = 1";
            ddlAgents.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            ddlAgents.DataTextField = "names";
            ddlAgents.DataValueField = "UserID";
            ddlAgents.DataBind();
            ddlAgents.Visible = true;
            btnRevertGraphs.Visible = true;
            btnChangeGraphs.Visible = true;

            sSQL = "select CallDate, (sum(InboundCt))/(COUNT(*)) as count from VeritasPhone.dbo.AgentDailyCallSummary " +
                "where UserID in (select UserID from UserSecurityInfo where TeamID = " + lTeamID + " and TeamLead != 1) " +
                "and CallDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' " +
                "group by CallDate";
            htmlInbound.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "select CallDate, (sum(OutboundCt))/(COUNT(*)) as count from VeritasPhone.dbo.AgentDailyCallSummary " +
                "where UserID in (select UserID from UserSecurityInfo where TeamID = " + lTeamID + " and TeamLead != 1) " +
                "and CallDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' " +
                "group by CallDate";
            htmlOutBound.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "select date, (sum(TotalDuration/60000))/(COUNT(*)) as count from VeritasPhone.dbo.AgentDnD " +
                "where UserID in (select UserID from UserSecurityInfo where TeamID = " + lTeamID + " and TeamLead != 1) " +
                "and date <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and date > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' " +
                "group by date";
            htmlDnD.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "select CreDate, count(*) as numOfClaimsOpened,0 as goal " +
                "from Claim where CreDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and CreDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "'" +
                "and CreBy in (select UserID from UserSecurityInfo where TeamID = " + lTeamID + " and TeamLead != 1) " +
                "group by CreDate";
            htmlClaimsOpened.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "select CloseDate, count(*) as numOfClaimsClosed,0 as goal " +
                "from Claim where CloseDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and CloseDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "'" +
                "and CloseBy in (select UserID from UserSecurityInfo where TeamID = " + lTeamID + " and TeamLead != 1) " +
                "group by CloseDate";
            htmlNumOfClaimsMovedFromOpen.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            htmlClaimsOpened.DataBind();



            htmlInbound.ChartTitle.Text = "Inbound Average";
            htmlOutBound.ChartTitle.Text = "Outbound Average";
            htmlDnD.ChartTitle.Text = "DnD Average";
            htmlClaimsOpened.ChartTitle.Text = "Number of opened claims";

            htmlNumOfClaimsMovedFromOpen.ChartTitle.Text = "Number of claims moved from open";
            sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster from Claim " +
           "cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID " +
           "left join userinfo ui on ui.userid = cl.CreBy " +
           "where Status = 'open' " +
           "and CreBy in (select userID from UserSecurityInfo where TeamID = " + lTeamID + ") order by ClaimAge desc ";
            rgClaimOpen.Columns[6].Visible = true;
            rgClaimOpen.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);

            sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster from Claim cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID left join userinfo ui on ui.userid = cl.CreBy where cl.ClaimActivityID = 3 order by ClaimAge desc ";
            rgClaimManagment.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgClaimManagment.Visible = true;

            htmlClaimsOpened.Visible = true;
            htmlNumOfClaimsMovedFromOpen.Visible = true;

            rgClaimManagment.MasterTableView.Caption = "Managers Claim Open";
            htmlInbound.Visible = true;
            htmlOutBound.Visible = true;
            htmlDnD.Visible = true;
            rgClaimOpen.Visible = true;
            try
            {
                //tables
                rgClaimOpen.DataBind();
                rgClaimManagment.DataBind();

                //graphs
                htmlInbound.DataBind();
                htmlOutBound.DataBind();
                htmlDnD.DataBind();
                htmlClaimsOpened.DataBind();
                htmlNumOfClaimsMovedFromOpen.DataBind();

            }
            catch
            {

            }
            rgClaimOpen.MasterTableView.Caption = "Claims Open";
            htmlInbound.ChartTitle.Text = "Inbound Stats";
            htmlOutBound.ChartTitle.Text = "Outbound";
            htmlDnD.ChartTitle.Text = "DnD";
        }
        else
        {
            sSQL = "select UserID, CallDate,'Inbound Calls' as title ,InboundCt as count, " +
                "(case when (select count(goal) from UserGoals where GoalsID = 1 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 1 and UserID = ia.UserID) end )as goal " +
                " from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = '" + lUserID + "' and CallDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' and ((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN (0, 1)";
            htmlInbound.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "select UserID, CallDate,'Outbound Calls' as title ,OutboundCt as count," +
                "(case when (select count(goal) from UserGoals where GoalsID = 2 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 2 and UserID = ia.UserID) end )as goal " +
                " from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = '" + lUserID + "' and CallDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' and ((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN (0, 1)";
            htmlOutBound.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            sSQL = "select userID, date as CallDate, 'DnD Total' as title, (TotalDuration/60000) as count," +
                "(case when (select count(goal) from UserGoals where GoalsID = 3 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 3 and UserID = ia.UserID) end )as goal " +
                " from VeritasPhone.dbo.AgentDnD ia where UserID = '" + lUserID + "' and date <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' and date > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' and ((DATEPART(dw, date) + @@DATEFIRST) % 7) NOT IN (0, 1)";
            htmlDnD.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            htmlInbound.ChartTitle.Text = "Inbound Stats";
            htmlOutBound.ChartTitle.Text = "Outbound";
            htmlDnD.ChartTitle.Text = "DnD";
            sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc from Claim " +
           "cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID " +
           "where Status = 'open' " +
           "and CreBy = " + lUserID + " order by ClaimAge desc ";
            rgClaimOpen.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            htmlInbound.Visible = true;
            htmlOutBound.Visible = true;
            htmlDnD.Visible = true;
            rgClaimOpen.Visible = true;
            try
            {
                //tables
                rgClaimOpen.DataBind();
                rgClaimManagment.DataBind();

                //graphs
                htmlInbound.DataBind();
                htmlOutBound.DataBind();
                htmlDnD.DataBind();
                htmlClaimsOpened.DataBind();
                htmlNumOfClaimsMovedFromOpen.DataBind();

            }
            catch
            {

            }
            rgClaimOpen.MasterTableView.Caption = "Claims Open";
            htmlInbound.ChartTitle.Text = "Inbound Stats";
            htmlOutBound.ChartTitle.Text = "Outbound";
            htmlDnD.ChartTitle.Text = "DnD";
        }





    }
    private void ShowError()
    {
        hfError.Value = "Visible";
        string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwError", script, true);
    }
    protected void btnErrorOK_Click(object sender, EventArgs e)
    {
        hfError.Value = "";
        string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        script = "function f(){$find(\"" + rwSignIn.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwSignIn", script, true);
        rwSignIn.VisibleOnPageLoad = true;
    }

    protected void btnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/default.aspx?sid=" + hfID.Value);
    }
    protected void btnAgents_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
    }
    protected void btnDealer_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
    }

    protected void btnContract_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
    }

    protected void btnClaim_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
    }

    protected void btnAccounting_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
    }

    protected void btnReports_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
    }

    protected void btnSettings_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
    }

    protected void btnUsers_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/default.aspx");
    }

    protected void rgClaimOpen_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        string SQL;
        clsDBO.clsDBO clSI = new clsDBO.clsDBO();
        DateTime sStartDate;
        DateTime sEndDate;
        sStartDate = DateTime.Today;
        sEndDate = DateTime.Today.AddDays(1);
        SQL = "select * from serverinfo " +
              "where systemid = '" + hfID.Value + "' " +
              "and signindate >= '" + sStartDate + "' " +
              "and signindate <= '" + sEndDate + "' ";
        clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        lUserID = 0;
        if (clSI.RowCount() > 0)
        {
            clSI.GetRow();
            lUserID = long.Parse(clSI.GetFields("userid"));
        }
        setDashboard();
    }

    protected void rgClaimOpen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + Convert.ToString(rgClaimOpen.SelectedValue));
    }

    protected void getTeamInfo()
    {
        clsDBO.clsDBO clTI = new clsDBO.clsDBO();
        string sSQL = "select TeamID, TeamLead from UserSecurityInfo where UserID = " + lUserID;
        clTI.OpenDB(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        if (clTI.RowCount() > 0)
        {
            clTI.GetRow();
            lTeamID = long.Parse(clTI.GetFields("TeamID"));
            bManager = Convert.ToBoolean((clTI.GetFields("TeamLead")));
            if (lTeamID == 6)
            {
                bExecutive = true;
            }
            else
            {
                bExecutive = false;
            }
        }

    }

    protected void ddlAgents_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnChangeGraphs_Click(object sender, EventArgs e)
    {
        string SQL = "";
        clsDBO.clsDBO cLR = new clsDBO.clsDBO();
        clsDBO.clsDBO clSI = new clsDBO.clsDBO();
        DateTime sStartDate;
        DateTime sEndDate;
        sStartDate = DateTime.Today;
        sEndDate = DateTime.Today.AddDays(1);
        SQL = "select * from serverinfo " +
              "where systemid = '" + hfID.Value + "' " +
              "and signindate >= '" + sStartDate + "' " +
              "and signindate <= '" + sEndDate + "' ";
        clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        lUserID = 0;
        if (clSI.RowCount() > 0)
        {
            clSI.GetRow();
            lUserID = long.Parse(clSI.GetFields("userid"));
        }
        getTeamInfo();
        int iSelectedAgent = Convert.ToInt32(ddlAgents.SelectedValue);
        int iselectedIndex = ddlAgents.SelectedIndex;
        string sSelectedAgent = ddlAgents.SelectedItem.Text;
        string sSQL = "select ui.FName + ' ' +  ui.LName as names, ui.UserID from UserInfo ui left join UserSecurityInfo usi on usi.UserID = ui.UserID where TeamID =" + lTeamID + " and Active = 1";
        ddlAgents.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString); ;
        ddlAgents.DataBind();
        ddlAgents.DataTextField = "names";
        ddlAgents.DataValueField = "UserID";
        ddlAgents.DataBind();
        ddlAgents.Visible = true;
        ddlAgents.SelectedIndex = iselectedIndex;
        btnRevertGraphs.Visible = true;
        btnChangeGraphs.Visible = true;



        sSQL = "select UserID, CallDate,'Inbound Calls' as title ,InboundCt as count, " +
                "(case when (select count(goal) from UserGoals where GoalsID = 1 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 1 and UserID = ia.UserID) end )as goal " +
                " from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = '" + iSelectedAgent + "' and CallDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' and ((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN (0, 1)";
        htmlInbound.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        sSQL = "select UserID, CallDate,'Outbound Calls' as title ,OutboundCt as count," +
            "(case when (select count(goal) from UserGoals where GoalsID = 2 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 2 and UserID = ia.UserID) end )as goal " +
            " from VeritasPhone.dbo.AgentDailyCallSummary ia where UserID = '" + iSelectedAgent + "' and CallDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' and CallDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' and ((DATEPART(dw, CallDate) + @@DATEFIRST) % 7) NOT IN (0, 1)";
        htmlOutBound.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        sSQL = "select userID, date as CallDate, 'DnD Total' as title, (TotalDuration/60000) as count," +
            "(case when (select count(goal) from UserGoals where GoalsID = 3 and UserID = ia.UserID) = 0 then '0' else (select goal from UserGoals where GoalsID = 3 and UserID = ia.UserID) end )as goal " +
            " from VeritasPhone.dbo.AgentDnD ia where UserID = '" + iSelectedAgent + "' and date <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' and date > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "' and ((DATEPART(dw, date) + @@DATEFIRST) % 7) NOT IN (0, 1)";
        htmlDnD.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        sSQL = "select CreDate, count(*) as numOfClaimsOpened " +
               "from Claim where CreDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
                "and CreDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "'" +
                "and CreBy in (" + iSelectedAgent + ") " +
                "group by CreDate";
        htmlClaimsOpened.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        sSQL = "select CloseDate, count(*) as numOfClaimsClosed " +
            "from Claim where CloseDate <= '" + DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy") + "' " +
            "and CloseDate > '" + DateTime.Today.AddDays(-8).ToString("MM/dd/yyyy") + "'" +
            "and CloseBy in (" + iSelectedAgent + ") " +
            "group by CloseDate";
        htmlNumOfClaimsMovedFromOpen.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);

        htmlInbound.ChartTitle.Text = sSelectedAgent + " Inbound Stats";
        htmlOutBound.ChartTitle.Text = sSelectedAgent + " Outbound Average";
        htmlDnD.ChartTitle.Text = sSelectedAgent + " DnD Average";
        htmlClaimsOpened.ChartTitle.Text = sSelectedAgent + " Number of opened claims Stats";
        htmlNumOfClaimsMovedFromOpen.ChartTitle.Text = sSelectedAgent + " Number of claims moved from open";
        sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster from Claim " +
           "cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID " +
           "left join userinfo ui on ui.userid = cl.CreBy " +
           "where Status = 'open' " +
            "and CreBy = " + iSelectedAgent + " order by ClaimAge desc ";
        htmlInbound.Visible = true;
        htmlOutBound.Visible = true;
        htmlDnD.Visible = true;
        rgClaimOpen.Visible = true;
        htmlClaimsOpened.Visible = true;
        htmlNumOfClaimsMovedFromOpen.Visible = true;
        rgClaimOpen.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);

        sSQL = "select claimid, ClaimNo, CreDate,ModDate, DATEDIFF(DAY,CreDate,GETDATE()) as ClaimAge, ca.ActivityDesc, ui.fname + ' ' + ui.lname as Adjuster from Claim cl left join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID left join userinfo ui on ui.userid = cl.CreBy where cl.ClaimActivityID = 3 order by ClaimAge desc ";
        rgClaimManagment.Visible = true;
        rgClaimManagment.DataSource = cLR.GetData(sSQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        rgClaimManagment.MasterTableView.Caption = "Managers Claim Open";
        rgClaimOpen.MasterTableView.Caption = "Claims Open";


        try
        {
            //tables
            rgClaimOpen.DataBind();
            rgClaimManagment.DataBind();

            //graphs
            htmlInbound.DataBind();
            htmlOutBound.DataBind();
            htmlDnD.DataBind();
            htmlClaimsOpened.DataBind();
            htmlNumOfClaimsMovedFromOpen.DataBind();
        }
        catch
        {

        }
    }

    protected void btnRevertGraphs_Click(object sender, EventArgs e)
    {
        string SQL;
        clsDBO.clsDBO clSI = new clsDBO.clsDBO();
        DateTime sStartDate;
        DateTime sEndDate;
        sStartDate = DateTime.Today;
        sEndDate = DateTime.Today.AddDays(1);
        SQL = "select * from serverinfo " +
              "where systemid = '" + hfID.Value + "' " +
              "and signindate >= '" + sStartDate + "' " +
              "and signindate <= '" + sEndDate + "' ";
        clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        lUserID = 0;
        if (clSI.RowCount() > 0)
        {
            clSI.GetRow();
            lUserID = long.Parse(clSI.GetFields("userid"));
        }
        setDashboard();
    }
    protected void fillDropDownDates()
    {
        ddlDates.Items.Clear();
        DateTime dateTime = DateTime.Now;
        int iCounter = 0;
        while (iCounter < 24)
        {
            ddlDates.Items.Add(dateTime.ToString("MM/yyyy"));
            dateTime = dateTime.AddMonths(-1);
            iCounter++;
        }
    }
    protected void rblNewClaims_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SQL;
        clsDBO.clsDBO clSI = new clsDBO.clsDBO();
        DateTime sStartDate;
        DateTime sEndDate;
        sStartDate = DateTime.Today;
        sEndDate = DateTime.Today.AddDays(1);
        SQL = "select * from serverinfo " +
              "where systemid = '" + hfID.Value + "' " +
              "and signindate >= '" + sStartDate + "' " +
              "and signindate <= '" + sEndDate + "' ";
        clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        lUserID = 0;
        if (clSI.RowCount() > 0)
        {
            clSI.GetRow();
            lUserID = long.Parse(clSI.GetFields("userid"));
        }
        setDashboard();
    }
}


