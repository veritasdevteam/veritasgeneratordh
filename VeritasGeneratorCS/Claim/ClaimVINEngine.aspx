﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimVINEngine.aspx.cs" Inherits="VeritasGeneratorCS.Claim.ClaimVINEngine" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtName" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Availability:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAvailability" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Aspiration:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAspiration" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Block Type:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtBlockType" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Bore:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtBore" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Cam Type:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCamType" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Compression:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCompression" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Cylinders:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCyclinders" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Displacement:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtDisplacement" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Electric Motor Config:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtElectricMotorConfig" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Electric Max HP:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtElectricMaxHP" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Electric Max KW:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtElectricMaxKW" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Electric Max Torque:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtElectricMaxTorque" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Engine Type:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtEngineType" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Fuel Induction:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtFuelInduction" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Fuel Quality:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtFuelQuality" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Fuel Type:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtFuelType" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Fleet:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkFleet" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Generator Desc:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtGeneratorDesc" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Generator Max HP:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtGeneratorMaxHP" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Max Hp:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtMaxHP" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Max HP at RPM:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtMaxHPatRPM" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Max Payload:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtMaxPayload" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Max Torque:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtMaxTorque" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Max Torque at RPM:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtMaxTorqueatRPM" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Oil Capacity:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtOilCapacity" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Red Line:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtRedLine" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Stroke:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtStroke" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Valve Timing:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtValveTiming" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Valves:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtValves" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Total Max HP:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtTotalMaxHP" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Total Max HP at RPM:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtTotalMaxHPatRPM" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Total Max Torque:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtTotalMaxTorque" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Total Max Torque at RPM:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtTotalMaxTorqueatRPM" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Order Code:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtOrderCode" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:HiddenField ID="hfContractID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfVIN" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
