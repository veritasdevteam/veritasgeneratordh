﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimAlert : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            if (!IsPostBack)
            {
                FillPage();
            }
        }
        private void FillPage()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clA = new clsDBO.clsDBO();
            SQL = "select * from dealernote dn " +
                  "inner join contract c on c.dealerid = dn.dealerid " +
                  "inner join claim cl on c.contractid = cl.contractid " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and (dn.claimalways <> 0 " +
                  "or claimnewentry <> 0) ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    txtDealerNote.Text = txtDealerNote.Text + clR.GetFields("note") + "\r\n" + "\r\n";
                }
            }

            SQL = "Select * from agentsnote an " +
                  "inner join Dealer d On d.agentsid = an.agentid " +
                  "inner join contract c On c.dealerid = d.dealerid " +
                  "inner join claim cl on c.contractid = cl.contractid " +
                  "where cl.claimid = " + hfClaimID.Value + " " +
                  "and (an.claimalways <> 0 " +
                  "or an.claimnewentry <> 0) ";
            clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clA.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clA.RowCount() - 1; cnt++)
                {
                    clA.GetRowNo(cnt);
                    //pvDealer.Selected = true;
                    txtDealerNote.Text = txtDealerNote.Text + clA.GetFields("note") + "\r\n" + "\r\n";
                }
            }

            SQL = "Select * from subagentnote an " +
                  "inner join Dealer d On d.subagentid = an.subagentid " +
                  "inner join contract c On c.dealerid = d.dealerid " +
                  "inner join claim cl on c.contractid = cl.contractid " +
                  "where cl.claimid = " + hfClaimID.Value + " " +
                  "and (an.claimalways <> 0 " +
                  "or an.claimnewentry <> 0) ";
            clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clA.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clA.RowCount() - 1; cnt++)
                {
                    clA.GetRowNo(cnt);
                    //pvDealer.Selected = true;
                    txtDealerNote.Text = txtDealerNote.Text + clA.GetFields("note") + "\r\n" + "\r\n";
                }
            }
        }
    }
}