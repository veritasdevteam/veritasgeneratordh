﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class HCC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hfClaimID.Value = Request.QueryString["claimid"];
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                FillScreen();
            }
        }
        private void FillScreen()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimhcc where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lblDateSubmitted.Text = DateTime.Parse(clR.GetFields("datesubmitted")).ToString("M/d/yyyy");
                if (clR.GetFields("adjusterid").Length > 0) 
                {
                    lblAdjusterName.Text = CalcUserName(long.Parse(clR.GetFields("adjusterid")));
                }
                CalcClaimInfo();
                CalcContractInfo();
                CalcDealer();
                CalcAgent();
                CalcServiceCenter();
                CalcInsCarrier();
                lblDaysIntoContract.Text = clR.GetFields("daysintocontract");
                lblMilesIntoContract.Text = clR.GetFields("milesintocontract");
                lblLimitofLiability.Text = clR.GetFields("LimitOfLiability");
                if (clR.GetFields("nadavalue").Length > 0)
                {
                    lblNADA.Text = Convert.ToDouble(clR.GetFields("NADAValue")).ToString("#,##0.00");
                }
                txtDescribeFailure.Text = clR.GetFields("DescFailure");

                if (clR.GetFields("claimcost").Length > 0) 
                {
                    lblClaimCost.Text = Convert.ToDouble(clR.GetFields("claimcost")).ToString("#,##0.00");
                }
                if (clR.GetFields("LaborCostPerHour").Length > 0) 
                {
                    lblLaborCostPerHour.Text = Convert.ToDouble(clR.GetFields("laborcostperhour")).ToString("#,##0.00");
                }
                if (clR.GetFields("averagecost").Length > 0) 
                {
                    lblAverageCost.Text = Convert.ToDouble(clR.GetFields("averagecost")).ToString("#,###.00");
                }
            }
        }
        private void CalcInsCarrier()
        {
            string SQL;
            clsDBO.clsDBO clI = new clsDBO.clsDBO();
            SQL = "select * from inscarrier where inscarrierid = " + hfInsCarrierID.Value;
            clI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clI.RowCount() > 0)
            {
                clI.GetRow();
                lblInsCarrier.Text = clI.GetFields("inscarriername");
            }
        }
        private void CalcServiceCenter()
        {
            string SQL;
            clsDBO.clsDBO clSC = new clsDBO.clsDBO();
            SQL = "select * from servicecenter where servicecenterid = " + hfServiceCenterID.Value;
            clSC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSC.RowCount() > 0)
            {
                clSC.GetRow();
                lblServiceCenterName.Text = clSC.GetFields("servicecentername");
            }
        }
        private void CalcAgent()
        {
            string SQL;
            clsDBO.clsDBO clA = new clsDBO.clsDBO();
            SQL = "select * from agents where agentid = " + hfAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                lblAgentName.Text = clA.GetFields("agentname");
            }
        }
        private void CalcDealer()
        {
            string SQL;
            clsDBO.clsDBO clD = new clsDBO.clsDBO();
            SQL = "select * from dealer where dealerid = " + hfDealerID.Value;
            clD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                lblDealerName.Text = clD.GetFields("dealername");
            }
        }
        private void CalcClaimInfo()
        {
            string SQL;
            clsDBO.clsDBO clCl = new clsDBO.clsDBO();
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clCl.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCl.RowCount() > 0)
            {
                clCl.GetRow();
                hfContractID.Value = clCl.GetFields("contractid");
                lblClaimNo.Text = clCl.GetFields("claimno");
                hfServiceCenterID.Value = clCl.GetFields("servicecenterid");
                if (clCl.GetFields("lossdate").Length > 0)
                {
                    lblClaimDate.Text = DateTime.Parse(clCl.GetFields("lossdate")).ToString("M/d/yyyy");
                }
                lblCurrentMile.Text = clCl.GetFields("lossmile");
            } 
            else 
            {
                hfContractID.Value = "0";
                hfServiceCenterID.Value = "0";
            }
        }
        private void CalcContractInfo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lblContractNo.Text = clR.GetFields("contractno");
                lblCustomerName.Text = clR.GetFields("fname") + " " + clR.GetFields("lname");
                hfDealerID.Value = clR.GetFields("dealerid");
                hfAgentID.Value = clR.GetFields("agentsid");
                hfInsCarrierID.Value = clR.GetFields("inscarrierid");
                if (clR.GetFields("saledate").Length > 0)
                {
                    lblSaleDate.Text = DateTime.Parse(clR.GetFields("saledate")).ToString("M/d/yyyy");
                }
                lblMakeModel.Text = clR.GetFields("Make") + "/" + clR.GetFields("model");
            } 
            else 
            {
                hfAgentID.Value = "0";
                hfDealerID.Value = "0";
                hfInsCarrierID.Value = "0";
            }
        }
        private string CalcUserName(long xUserID)
        {
            string SQL;
            clsDBO.clsDBO clU = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + xUserID;
            clU.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clU.RowCount() > 0)
            {
                clU.GetRow();
                return clU.GetFields("fname") + " " + clU.GetFields("lname");
            }
            return "";
        }
    }
}