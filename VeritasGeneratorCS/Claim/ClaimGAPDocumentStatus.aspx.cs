﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimGAPDocumentStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["ClaimID"];
            hfID.Value = Request.QueryString["sid"];
            if (!IsPostBack)
            {
                lblGenerateClaimNotice.Visible = false;
                lblStatusLetter.Visible = false;
                lblPaymentLetter.Visible = false;
                lblMissedOptions.Visible = false;
                lblGenerate60DayLetter.Visible = false;
                lblGapNoGapDueLetter.Visible = false;
                lblDenialLetter.Visible = false;
                GetServerInfo();
                GetInfo();
            }
        }
        private void GetInfo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimgapdocstatus where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                chk60DayLetter.Checked = Convert.ToBoolean(clR.GetFields("gap60dayletter"));
                chkDenialLetter.Checked = Convert.ToBoolean(clR.GetFields("gapdenielletter"));
                chkGAPClaimNotice.Checked = Convert.ToBoolean(clR.GetFields("gapclaimnotice"));
                chkGAPNoGAPDueLetter.Checked = Convert.ToBoolean(clR.GetFields("gapnogapdueletter"));
                chkMissedOptions.Checked = Convert.ToBoolean(clR.GetFields("gapmissedoptions"));
                chkPaymentHistory.Checked = Convert.ToBoolean(clR.GetFields("paymenthistory"));
                chkPaymentLetter.Checked = Convert.ToBoolean(clR.GetFields("gappaymentletter"));
                chkPoliceFireReport.Checked = Convert.ToBoolean(clR.GetFields("policefirereport"));
                chkProofInsPayments.Checked = Convert.ToBoolean(clR.GetFields("ProofInsPayments"));
                chkRISC.Checked = Convert.ToBoolean(clR.GetFields("risc"));
                chkStatusLetter.Checked = Convert.ToBoolean(clR.GetFields("gapstatusletter"));
                chkTotalLossInsSettlementLetter.Checked = Convert.ToBoolean(clR.GetFields("TotalLossSettlement"));
                chkTotalLossInsValuation.Checked = Convert.ToBoolean(clR.GetFields("TotalLossValuation"));
            }
            else
            {
                chk60DayLetter.Checked = false;
                chkDenialLetter.Checked = false;
                chkGAPClaimNotice.Checked = false;
                chkGAPNoGAPDueLetter.Checked = false;
                chkMissedOptions.Checked = false;
                chkPaymentHistory.Checked = false;
                chkPaymentLetter.Checked = false;
                chkPoliceFireReport.Checked = false;
                chkProofInsPayments.Checked = false;
                chkRISC.Checked = false;
                chkStatusLetter.Checked = false;
                chkTotalLossInsSettlementLetter.Checked = false;
                chkTotalLossInsValuation.Checked = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimgapdocstatus where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
            } 
            else 
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
            }
            clR.SetFields("risc", chkRISC.Checked.ToString());
            clR.SetFields("paymenthistory", chkPaymentHistory.Checked.ToString());
            clR.SetFields("totallossvaluation", chkTotalLossInsValuation.Checked.ToString());
            clR.SetFields("totallosssettlement", chkTotalLossInsSettlementLetter.Checked.ToString());
            clR.SetFields("policefirereport", chkPoliceFireReport.Checked.ToString());
            clR.SetFields("proofinspayments", chkProofInsPayments.Checked.ToString());
            clR.SetFields("gapclaimnotice", chkGAPClaimNotice.Checked.ToString());
            clR.SetFields("gap60dayletter", chk60DayLetter.Checked.ToString());
            clR.SetFields("gapdenielletter", chkDenialLetter.Checked.ToString());
            clR.SetFields("gapmissedoptions", chkMissedOptions.Checked.ToString());
            clR.SetFields("gapnogapdueletter", chkGAPNoGAPDueLetter.Checked.ToString());
            clR.SetFields("gappaymentletter", chkPaymentLetter.Checked.ToString());
            clR.SetFields("gapstatusletter", chkStatusLetter.Checked.ToString());
            if (clR.RowCount() == 0) 
            {
                clR.AddRow();
            }
            clR.SaveDB();
        }

        protected void btnDenialLetter_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "insert into ClaimGapDocumentRequest " +
                  "(userid, claimgapid, claimdoctypeid) " +
                  "values (" + hfUserID.Value + ", " +
                  hfClaimID.Value + ",9)";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            lblDenialLetter.Text = "Document is being created. You will notified by e-mail when complete.";
            lblDenialLetter.Visible = true;
        }

        protected void btnGAPNoGAPDueLetter_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "insert into ClaimGapDocumentRequest " +
                  "(userid, claimgapid, claimdoctypeid) " +
                  "values (" + hfUserID.Value + ", " +
                  hfClaimID.Value + ",11)";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            lblGapNoGapDueLetter.Text = "Document is being created. You will notified by e-mail when complete.";
            lblGapNoGapDueLetter.Visible = true;
        }

        protected void btnGenerate60DayLetter_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "insert into ClaimGapDocumentRequest " +
                  "(userid, claimgapid, claimdoctypeid) " +
                  "values (" + hfUserID.Value + ", " +
                  hfClaimID.Value + ",8)";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            lblGenerate60DayLetter.Text = "Document is being created. You will notified by e-mail when complete.";
            lblGenerate60DayLetter.Visible = true;
        }

        protected void btnGenerateClaimNotice_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "insert into ClaimGapDocumentRequest " +
                  "(userid, claimgapid, claimdoctypeid) " +
                  "values (" + hfUserID.Value + ", " +
                  hfClaimID.Value + ",7)";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            lblGenerateClaimNotice.Text = "Document is being created. You will notified by e-mail when complete.";
            lblGenerateClaimNotice.Visible = true;
        }

        protected void btnMissedOptions_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "insert into ClaimGapDocumentRequest " +
                  "(userid, claimgapid, claimdoctypeid) " +
                  "values (" + hfUserID.Value + ", " +
                  hfClaimID.Value + ",10)";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            lblMissedOptions.Text = "Document is being created. You will notified by e-mail when complete.";
            lblMissedOptions.Visible = true;
        }

        protected void btnPaymentLetter_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "insert into ClaimGapDocumentRequest " +
                  "(userid, claimgapid, claimdoctypeid) " +
                  "values (" + hfUserID.Value + ", " +
                  hfClaimID.Value + ",12)";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            lblPaymentLetter.Text = "Document is being created. You will notified by e-mail when complete.";
            lblPaymentLetter.Visible = true;
        }

        protected void btnStatusLetter_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "insert into ClaimGapDocumentRequest " +
                  "(userid, claimgapid, claimdoctypeid) " +
                  "values (" + hfUserID.Value + ", " +
                  hfClaimID.Value + ",13)";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            lblStatusLetter.Text = "Document is being created. You will notified by e-mail when complete.";
            lblStatusLetter.Visible = true;
        }
    }
}