﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimCKParts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsPart.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            hfID.Value = Request.QueryString["sid"];
            hfClaimID.Value = Request.QueryString["claimid"];
            if (!IsPostBack)
            {
                FillParts();
            }
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }
        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnInspectionWex_Click(object sender, EventArgs e)
        {

        }

        protected void btnCarfaxPayment_Click(object sender, EventArgs e)
        {

        }
        private void GetParts()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from ckquote where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                FillParts();
            } else {
                clR.GetRow();
                if (clR.GetFields("orderid").Length > 0)
                {
                    btnPurchase.Visible = false;
                    btnRebuild.Visible = false;
                }
                txtOrderID.Text = clR.GetFields("orderid");
                txtQuoteID.Text = clR.GetFields("quoteid");
                txtTotalPartCost.Text = clR.GetFields("partcost");
                txtShippingCost.Text = clR.GetFields("shippingcost");
                txtTotalCost.Text = clR.GetFields("totordercost");
                txtSentDate.Text = clR.GetFields("sentdate");
                rgPayment.Rebind();
            }
        }
        private void FillParts()
        {
            string SQL;
            double dCostAmt = 0;
            long lCKID = 0;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clB = new clsDBO.clsDBO();
            SQL = "select cl.claimid, cd.ClaimDetailID, cd.jobno, cd.reqamt, cd.reqQty,cdq.Qty, cdq.yourcost, cdq.PartNo, cdq.PartDesc from claim cl " +
                  "inner join ClaimDetail cd on cl.ClaimID = cd.ClaimID " +
                  "inner join ClaimDetailQuote cdq on cdq.ClaimDetailID = cd.ClaimDetailID " +
                  "where instock = 'yes' " +
                  "and cl.claimid = " + hfClaimID.Value + " " +
                  "and oem = 1 " +
                  "group by cl.claimid, cd.claimdetailid, reqamt, YourCost, cdq.partno, cdq.partdesc, cd.JobNo, cd.reqQty,cdq.Qty ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++) 
                {
                    clR.GetRowNo(cnt);
                    SQL = "select * from ckquote where claimid = " + hfClaimID.Value + " ";
                    clB.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clB.RowCount() > 0) 
                    {
                        clB.GetRow();
                        if (clB.GetFields("orderid").Length > 0) 
                        {
                            return;
                        }
                        goto MoveHere;
                    } 
                    else 
                    {
                        clB.NewRow();
                        clB.SetFields("claimid", hfClaimID.Value);
                    }
                    clB.SetFields("quoteid", "");
                    clB.SetFields("partcost", "0");
                    clB.SetFields("shippingcost", "0");
                    clB.SetFields("totalordercost", "0");
                    if (clB.RowCount() == 0) 
                    {
                        clB.AddRow();
                    }
                    clB.SaveDB();
                MoveHere:;
                    SQL = "select * from ckquote where claimid = " + hfClaimID.Value + " ";
                    clB.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clB.RowCount() > 0) 
                    {
                        clB.GetRow();
                        lCKID = long.Parse(clB.GetFields("ckid"));
                    }
                    SQL = "select * from ckpart " +
                          "where claimdetailid = " + clR.GetFields("claimdetailid") + " " +
                          "and ckid = " + lCKID;
                    clB.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clB.RowCount() > 0) 
                    {
                        clB.GetRow();
                    } 
                    else 
                    {
                        clB.NewRow();
                    }
                    clB.SetFields("ckid", lCKID.ToString());
                    clB.SetFields("claimdetailid", clR.GetFields("claimdetailid"));
                    clB.SetFields("jobno", clR.GetFields("jobno"));
                    clB.SetFields("partno", clR.GetFields("partno"));
                    clB.SetFields("partname", clR.GetFields("partdesc"));
                    clB.SetFields("qty", clR.GetFields("reqqty"));
                    clB.SetFields("reqamt", clR.GetFields("reqamt"));
                    clB.SetFields("yourcost", clR.GetFields("yourcost"));
                    clB.SetFields("totalcost", (Convert.ToDouble(clR.GetFields("yourcost")) * Convert.ToDouble(clR.GetFields("reqqty"))).ToString());
                    dCostAmt = dCostAmt + (Convert.ToDouble(clR.GetFields("yourcost")) * Convert.ToDouble(clR.GetFields("reqqty")));
                    if (clB.RowCount() == 0) 
                    {
                        clB.AddRow();
                    }
                    clB.SaveDB();
                    txtTotalPartCost.Text = dCostAmt.ToString();
                    txtShippingCost.Text = 42.5.ToString();
                    txtTotalCost.Text = (dCostAmt + 42.5).ToString();
                }
            }
        }

        protected void rgPayment_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            long lDeleteID;
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();

            if (e.CommandName == "DeleteRow")
            {
                lDeleteID = long.Parse(rgPayment.Items[e.Item.ItemIndex].GetDataKeyValue("ClaimDetailID").ToString());
                SQL = "delete from ckpart where claimdetailid = " + lDeleteID;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
        }
    }
}