﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimDealer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            OpenClaim();
            OpenContract();
            OpenDealer();
            OpenAgent();
        }
        private void OpenAgent()
        {
            if (hfAgentID.Value.Length == 0)
            {
                return;
            }

            string SQL;
            clsDBO.clsDBO clA = new clsDBO.clsDBO();
            SQL = "select * from agents where agentid = " + hfAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                txtAgentName.Text = clA.GetFields("agentname");
                txtAgentNo.Text = clA.GetFields("agentno");
            }
        }
        private void OpenDealer()
        {
            if (hfDealerID.Value.Length == 0)
            {
                return;
            }
            string SQL;
            clsDBO.clsDBO clD = new clsDBO.clsDBO();
            SQL = "select * from dealer where dealerid = " + hfDealerID.Value;
            clD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                txtDealerName.Text = clD.GetFields("dealername");
                txtDealerNo.Text = clD.GetFields("dealerno");
                txtAddr1.Text = clD.GetFields("addr1");
                txtAddr2.Text = clD.GetFields("addr2");
                txtAddr3.Text = clD.GetFields("city") + ", " + clD.GetFields("state") + " " + clD.GetFields("zip");
                txtPhone.Text = clD.GetFields("phone");
            }
        }
        private void OpenClaim()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select contractid from claim where claimid = " + hfClaimID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfContractID.Value = clC.GetFields("contractid");
            } 
            else 
            {
                hfContractID.Value = "0";
            }
        }
        private void OpenContract()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            if (hfContractID.Value.Length == 0)
            {
                return;
            }
            SQL = "select dealerid, agentsid from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfDealerID.Value = clC.GetFields("dealerid");
                hfAgentID.Value = clC.GetFields("agentsid");
            }
            else
            {
                hfDealerID.Value = "0";
                hfAgentID.Value = "0";
            }
        }
    }
}