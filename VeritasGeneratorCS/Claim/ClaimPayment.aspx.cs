﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimPayment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            dsPaid.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsToBePaid.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsWexMethod.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsPayment.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsClaimPayee.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlError.Visible = false;
                pnlList.Visible = true;
                pnlPaidDetail.Visible = false;
                pnlToBePaidDetail.Visible = false;
                pnlPaymentDetail.Visible = false;
                pnlSearchClaimPayee.Visible = false;
                pnlPaymentAdd.Visible = false;
                trACH.Visible = false;
                trCheck.Visible = false;
                trCC.Visible = false;
                trWex.Visible = false;
                FillStatusAdd();
                FillPayMethodAdd();
                rgPaid.Rebind();
                rgToBePaid.Rebind();
                rgPayment.Rebind();
                FillPaymentStatus();
                if (CheckLock())
                {
                    btnAddPayment.Visible = false;
                    btnProcessACH.Visible = false;
                    btnPaymentViewSave.Visible = false;
                    btnProcessACH.Visible = false;
                    btnProcessCC.Visible = false;
                    btnProcessCheck.Visible = false;
                    btnProcessWex.Visible = false;
                    btnSaveAdd.Visible = false;
                }
                else
                {
                    btnAddPayment.Visible = true;
                    btnProcessACH.Visible = true;
                    btnPaymentViewSave.Visible = true;
                    btnProcessACH.Visible = true;
                    btnProcessCC.Visible = true;
                    btnProcessCheck.Visible = true;
                    btnProcessWex.Visible = true;
                    btnSaveAdd.Visible = true;
                }
                ReadOnlyButtons();
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly").ToLower() == "true")
                {
                    btnAddPayment.Visible = false;
                    btnProcessACH.Visible = false;
                    btnPaymentViewSave.Visible = false;
                    btnProcessACH.Visible = false;
                    btnProcessCC.Visible = false;
                    btnProcessCheck.Visible = false;
                    btnProcessWex.Visible = false;
                    btnSaveAdd.Visible = false;
                }
            }
        }
        private bool CheckLock()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "Select claimid from claim " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "And lockuserid <> " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                return false;
            } 
            else
            {
                return true;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillPaymentStatus()
        {
            cboPaymentStatus.Items.Clear();
            cboPaymentStatus.Items.Add("");
            cboPaymentStatus.Items.Add("Paid");
            cboPaymentStatus.Items.Add("Payment Error");
            cboPaymentStatus.Items.Add("Transmitted To Wex");
            cboPaymentStatus.Items.Add("Void");
            cboPaymentMethod.Items.Add("Refund");
        }
        private void FillPayMethodAdd()
        {
            cboPayMethodAdd.Items.Clear();
            cboPayMethodAdd.Items.Add("");
            cboPayMethodAdd.Items.Add("ACH");
            cboPayMethodAdd.Items.Add("Check");
            cboPayMethodAdd.Items.Add("Credit Card");
            cboPayMethodAdd.Items.Add("Wex");
        }
        private void FillStatusAdd()
        {
            cboStatusAdd.Items.Add("Transmitted To Wex");
            cboStatusAdd.Items.Add("Paid");
            cboStatusAdd.Items.Add("Refund");
        }

        protected void rgPaid_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlPaidDetail.Visible = true;
            FillPaidDetail();
        }
        private void FillPaidDetail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select ClaimDetailID, cd.ClaimPayeeID,  cp.PayeeNo, " +
                  "cp.PayeeName,jobno  LossCode,DatePaid,PaidAmt from claimdetail cd " +
                  "left join ClaimPayee cp on cp.ClaimPayeeID = cd.ClaimPayeeID " +
                  "where cd.claimid = " + hfClaimID.Value + " " +
                  "and cd.claimpayeeid = " + rgPaid.SelectedValue + " " +
                  "and claimdetailstatus = 'Paid' ";
            rgPaidDetail.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgPaidDetail.DataBind();
        }

        protected void btnPaidDetailClose_Click(object sender, EventArgs e)
        {
            pnlList.Visible = true;
            pnlPaidDetail.Visible = false;
        }

        protected void rgToBePaid_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimPayeeID.Value = rgToBePaid.SelectedValue.ToString();
            pnlList.Visible = false;
            pnlToBePaidDetail.Visible = true;
            FillToBePaidDetail();
        }
        private void FillToBePaidDetail()
        {
            GetPayeeInfo();
            GetClaimAmt();
            FillPaymentMethod();
            txtConfirmNo.Text = "";
            txtCheckNo.Text = "";
        }
        private void FillPaymentMethod()
        {
            cboPaymentMethod.Items.Clear();
            cboPaymentMethod.Items.Add("");
            cboPaymentMethod.Items.Add("ACH");
            cboPaymentMethod.Items.Add("Check");
            cboPaymentMethod.Items.Add("Credit Card");
            cboPaymentMethod.Items.Add("Wex");
        }
        private void GetClaimAmt()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select sum(totalamt) as amt from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimpayeeid = " + hfClaimPayeeID.Value + " " +
                  "and (claimdetailstatus = 'approved' " +
                  "or claimdetailstatus = 'authorized') ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtPayAmt.Text = Convert.ToDouble(clR.GetFields("amt")).ToString("#,##0.00");
            }
        }
        private void GetPayeeInfo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimpayee where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtClaimPayeeNo.Text = clR.GetFields("payeeno");
                txtClaimPayeeName.Text = clR.GetFields("payeename");
                CheckForInvoiceNo();
            }
        }
        private void CheckForInvoiceNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim c " +
                  "inner join servicecenter sc on c.servicecenterid = sc.servicecenterid " +
                  "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (txtClaimPayeeNo.Text.Trim() == clR.GetFields("servicecenterno"))
                {
                    txtInvoiceNo.Text = clR.GetFields("ronumber");
                }
            }
        }

        protected void btnCloseToBePaidDetail_Click(object sender, EventArgs e)
        {
            pnlList.Visible = true;
            pnlToBePaidDetail.Visible = false;
            trACH.Visible = false;
            trCheck.Visible = false;
            rgToBePaid.SelectedIndexes.Clear();
        }

        protected void btnProcessACH_Click(object sender, EventArgs e)
        {
            ProcessPayment();
            pnlToBePaidDetail.Visible = false;
            trACH.Visible = false;
            pnlList.Visible = true;
            rgPaid.Rebind();
            rgToBePaid.Rebind();
            rgPayment.Rebind();
        }
        private void ProcessPayment()
        {
            string SQL;
            string SQLNote;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clCPL = new clsDBO.clsDBO();
            AddClaimPayment();
            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimpayeeid = " + hfClaimPayeeID.Value + " " +
                  "and (claimdetailstatus = 'approved' " +
                  "or claimdetailstatus = 'authorized') ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    SQL = "select * from claimpaymentlink " +
                          "where claimdetailid = " + clR.GetFields("claimdetailid") + " " +
                          "and claimpaymentid = " + hfClaimPaymentID.Value + " ";
                    clCPL.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clCPL.RowCount() == 0)
                    {
                        clCPL.NewRow();
                        clCPL.SetFields("claimdetailid", clR.GetFields("claimdetailid"));
                        clCPL.SetFields("claimpaymentid", hfClaimPaymentID.Value);
                        clCPL.AddRow();
                        clCPL.SaveDB();
                    }
                    SQL = "update claimdetail ";
                    if (cboPaymentMethod.Text != "Wex")
                    {
                        SQL = SQL + "set claimdetailstatus = 'Paid', " +
                                    "datepaid = '" + DateTime.Today + "', " +
                                    "modby = " + hfUserID.Value + ", " +
                                    "moddate = '" + DateTime.Today + "' ";
                    }
                    else
                    {
                        SQL = SQL + "set claimdetailstatus = 'Transmitted', " +
                                    "modby = " + hfUserID.Value + ", " +
                                    "moddate = '" + DateTime.Today + "' ";
                    }
                    SQL = SQL + "where claimdetailid = " + clR.GetFields("claimdetailid");
                    clCPL.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    SQLNote = "insert claimnote (claimid, claimnotetypeid, note, credate, creby, moddate, modby) " +
                              "values (" + hfClaimID.Value + "5,'Payment via " + cboPaymentMethod.Text + "', '" + DateTime.Today + "'," + hfUserID.Value + ", '" + DateTime.Today + "'," + hfUserID.Value + ")";
                    clCPL.RunSQL(SQLNote, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                }
            }
        }
        private void AddClaimPayment()
        {
            string SQL;
            clsDBO.clsDBO clCP = new clsDBO.clsDBO();
            SQL = "select * from claimpayment where claimpaymentid = 0 ";
            clCP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCP.RowCount() == 0)
            {
                clCP.NewRow();
                clCP.SetFields("claimpayeeid", hfClaimPayeeID.Value);
                clCP.SetFields("paymentamt", txtPayAmt.Text);
                if (cboPaymentMethod.Text == "ACH")
                {
                    clCP.SetFields("datepaid", DateTime.Today.ToString());
                    clCP.SetFields("achinfo", txtConfirmNo.Text);
                    clCP.SetFields("status", "Paid");
                }
                if (cboPaymentMethod.Text == "Check")
                {
                    clCP.SetFields("datepaid", DateTime.Today.ToString());
                    clCP.SetFields("checkno", txtCheckNo.Text);
                    clCP.SetFields("status", "Paid");
                }
                if (cboPaymentMethod.Text == "Credit Card")
                {
                    clCP.SetFields("datepaid", DateTime.Today.ToString());
                    clCP.SetFields("checkno", txtCCNo.Text);
                    clCP.SetFields("status", "Paid");
                }
                if (cboPaymentMethod.Text == "Wex")
                {
                    clCP.SetFields("datetransmitted", DateTime.Today.ToString());
                    clCP.SetFields("status", "Transmitted To Wex");
                    clCP.SetFields("ccno", hfWexCCno.Value);
                    clCP.SetFields("wexcode", hfWexCode.Value);
                    clCP.SetFields("wexdeliverymethod", cboWexMethod.SelectedText);
                    clCP.SetFields("wexdeliveryaddress", txtWexAddress.Text);
                    clCP.SetFields("companyno", hfCompanyNo.Value);
                }
                clCP.AddRow();
                clCP.SaveDB();
            }

            SQL = "select max(claimpaymentid) as mCPI from claimpayment where claimpayeeid = " + hfClaimPayeeID.Value;
            clCP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCP.RowCount() > 0)
            {
                clCP.GetRow();
                hfClaimPaymentID.Value = clCP.GetFields("mcpi");
            }
        }
        protected void cboPaymentMethod_TextChanged(object sender, EventArgs e)
        {
            trACH.Visible = false;
            trWex.Visible = false;
            trCheck.Visible = false;
            trCC.Visible = false;
            if (cboPaymentMethod.Text == "ACH") 
            {
                trACH.Visible = true;
            }
            if (cboPaymentMethod.Text == "Check") 
            {
                trCheck.Visible = true;
            }
            if (cboPaymentMethod.Text == "Credit Card")
            {
                trCC.Visible = true;
            }
            if (cboPaymentMethod.Text == "Wex") 
            {
                trWex.Visible = true;
            }
        }

        protected void btnProcessCheck_Click(object sender, EventArgs e)
        {
            ProcessPayment();
            pnlToBePaidDetail.Visible = false;
            trCheck.Visible = false;
            pnlList.Visible = true;
            rgPaid.Rebind();
            rgToBePaid.Rebind();
            rgPayment.Rebind();
        }

        protected void btnProcessCC_Click(object sender, EventArgs e)
        {
            ProcessPayment();
            pnlToBePaidDetail.Visible = false;
            trCC.Visible = false;
            pnlList.Visible = true;
            rgPaid.Rebind();
            rgToBePaid.Rebind();
            rgPayment.Rebind();
        }

        protected void btnProcessWex_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            txtWexAddress.Text = txtWexAddress.Text.Trim();
            if (cboWexMethod.SelectMethod == "Fax")
            {
                txtWexAddress.Text = txtWexAddress.Text.Replace("(", "");
                txtWexAddress.Text = txtWexAddress.Text.Replace(")", "");
                txtWexAddress.Text = txtWexAddress.Text.Replace(" ", "");
                txtWexAddress.Text = txtWexAddress.Text.Replace("-", "");
                txtWexAddress.Text = txtWexAddress.Text.Replace(".", "");
            }
            ProcessWexDLL();
            rgPaid.Rebind();
            rgToBePaid.Rebind();
            rgPayment.Rebind();
            pnlToBePaidDetail.Visible = false;
            trWex.Visible = false;
            pnlList.Visible = true;
            if (cboWexMethod.SelectedValue == "EMail")
            {
                SQL = "Update servicecenter " +
                      "set email = '" + txtWexAddress.Text + "' " +
                      "where servicecenterno = '" + txtClaimPayeeNo.Text.Trim() + "' ";
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
            if (cboWexMethod.SelectedValue == "Fax")
            {
                SQL = "Update servicecenter " +
                      "set fax = '" + txtWexAddress.Text + "' " +
                      "where servicecenterno = '" + txtClaimPayeeNo.Text.Trim() + "' ";
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
        }
        private void ProcessWexDLL()
        {
            clsDBO.clsDBO clCWP = new clsDBO.clsDBO();
            clsDBO.clsDBO clR = new clsDBO.clsDBO();

            GetClaimNo();
            if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.ToLower().Contains("test"))
            {
                return;
            }
            else
            {
                pnlError.Visible = false;
                if (CheckPayment())
                {
                    pnlError.Visible = true;
                    lblError.Text = "Already paid to " + txtWexAddress.Text + " for " + txtPayAmt.Text;
                    return;
                }
                GetCompanyNo();
                string SQL;
                clsDBO.clsDBO clWA = new clsDBO.clsDBO();
                SQL = "select * from wexapi where companyno = '" + hfCompanyNo.Value + "' ";
                clWA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clWA.RowCount() > 0) 
                {
                    clWA.GetRow();
                    SQL = "select * from claimwexpayment where claimwexpaymentid = 0 ";
                    clCWP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clCWP.RowCount() == 0)
                    {
                        clCWP.NewRow();
                        clCWP.SetFields("claimno", hfClaimNo.Value);
                        clCWP.SetFields("claimid", hfClaimID.Value);
                        clCWP.SetFields("rono", txtInvoiceNo.Text);
                        clCWP.SetFields("orggroup", clWA.GetFields("OrgGroup"));
                        clCWP.SetFields("username", clWA.GetFields("UserName"));
                        clCWP.SetFields("password", clWA.GetFields("Password"));
                        clCWP.SetFields("currency", clWA.GetFields("currency"));
                        clCWP.SetFields("wexaddress", txtWexAddress.Text);
                        clCWP.SetFields("wexmethod", cboWexMethod.SelectedValue);
                        clCWP.SetFields("bankno", clWA.GetFields("bankno"));
                        clCWP.SetFields("companyno", hfCompanyNo.Value);
                        clCWP.SetFields("paymentamt", txtPayAmt.Text);
                        clCWP.SetFields("contractno", hfContractNo.Value);
                        clCWP.SetFields("customername", hfCustomerName.Value);
                        clCWP.SetFields("Claimpayeename", txtClaimPayeeName.Text);
                        clCWP.SetFields("payeecontact", hfPayeeContact.Value);
                        clCWP.SetFields("vin", hfVIN.Value);
                        clCWP.AddRow();
                        clCWP.SaveDB();
                        SQL = "select max(claimwexpaymentid) as MCWP from claimwexpayment where claimid = " + hfClaimID.Value;
                        clCWP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                        if (clCWP.RowCount() > 0)
                        {
                            clCWP.GetRow();
                            if (!ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.ToLower().Contains("test"))
                            {
                                Process pr = new Process();
                                pr.StartInfo.FileName = @"C:\ProcessProgram\WexPayment\WexPayment.exe";
                                pr.StartInfo.Arguments = clCWP.GetFields("mcwp");
                                pr.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                                pr.Start();
                                pr.WaitForExit();
                                pr.Close();
                                SQL = "select * from claimwexpayment where claimwexpaymentid = " + clCWP.GetFields("mcwp");
                                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                                if (clR.RowCount() > 0)
                                {
                                    clR.GetRow();
                                    if (clR.GetFields("ReasonCode").ToLower() == "success")
                                    {
                                        hfWexCCno.Value = clR.GetFields("wexccno").Substring(0, 4);
                                        hfWexCode.Value = clR.GetFields("wexcode");
                                        ProcessPayment();
                                        pnlToBePaidDetail.Visible = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private bool CheckPayment()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimwexpayment " +
                  "where  wexaddress = '" + txtWexAddress.Text + "' " +
                  "and paymentamt = " + Convert.ToDouble(txtPayAmt.Text) + " " +
                  "and sent >= '" + DateTime.Today.AddDays(-5) + "' " +
                  "and claimno = '" + hfClaimNo.Value + "' " +
                  "and voiddate is null " +
                  "and not wexcode is null ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private void GetClaimNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimNo.Value = clR.GetFields("claimno");
                hfRONo.Value = clR.GetFields("RONumber");
                hfContractID.Value = clR.GetFields("contractid");
                OpenContract();
                hfPayeeContact.Value = clR.GetFields("sccontactinfo");
            }
        }
        private void OpenContract()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractNo.Value = clR.GetFields("contractno");
                hfCustomerName.Value = clR.GetFields("fname") + " " + clR.GetFields("lname");
                hfVIN.Value = clR.GetFields("vin");
            }
        }
        private void GetCompanyNo()
        {
            string SQL;
            clsDBO.clsDBO clCL = new clsDBO.clsDBO();
            SQL = "select * from claim cl " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                hfCompanyNo.Value = "0000976";
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 1 || Convert.ToInt32(clCL.GetFields("clipid")) == 2) 
                {
                    hfCompanyNo.Value = "0000978";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 3 || Convert.ToInt32(clCL.GetFields("clipid")) == 4 || Convert.ToInt32(clCL.GetFields("clipid")) == 11) 
                {
                    if (clCL.GetFields("contractno").Substring(0, 3) == "CHJ") 
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "DRV") 
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAC") 
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAD") 
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAN") 
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RDI") 
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "REP") 
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSA") 
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSD")
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSW") 
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "VEL")
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else 
                    {
                        hfCompanyNo.Value = "0000976";
                    }
                }
                if (Convert.ToInt32(clCL.GetFields("inscarrierid")) == 4) 
                {
                    if (isFL()) 
                    {
                        hfCompanyNo.Value = "0000977";
                    } 
                    else 
                    {
                        hfCompanyNo.Value = "0000976";
                    }
                }
                if (Convert.ToInt32(clCL.GetFields("inscarrierid")) == 5) 
                {
                    if (isFL())
                    {
                        hfCompanyNo.Value = "0000977";
                    } 
                    else
                    {
                        hfCompanyNo.Value = "0000976";
                    }
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 5 || Convert.ToInt32(clCL.GetFields("clipid")) == 6 || Convert.ToInt32(clCL.GetFields("clipid")) == 14) 
                {
                    hfCompanyNo.Value = "0000976";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 7 || Convert.ToInt32(clCL.GetFields("clipid")) == 8 || Convert.ToInt32(clCL.GetFields("clipid")) == 13) 
                {
                    hfCompanyNo.Value = "0000976";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 9 || Convert.ToInt32(clCL.GetFields("clipid")) == 10 || Convert.ToInt32(clCL.GetFields("clipid")) == 12) 
                {
                    hfCompanyNo.Value = "0000977";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 11) 
                {
                    hfCompanyNo.Value = "0000978";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 0) 
                {
                    if (clCL.GetFields("contractno").Substring(0, 3) == "VA1") 
                    {
                        hfCompanyNo.Value = "0000976";
                    }
                    if (clCL.GetFields("contractno").Substring(0, 3) == "VAO") 
                    {
                        hfCompanyNo.Value = "0000976";
                    }
                }
            }
        }
        private bool isFL()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select c.State from claim cl inner join contract c on c.contractid = cl.contractid where cl.ClaimID = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("state").ToUpper() == "FL")
                {
                    return true;
                }
            }
            return false;
        }

        protected void rgPayment_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlPaymentDetail.Visible = true;
            FillPaymentDetail();
        }
        private void FillPaymentDetail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            txtPaymentAmt.Text = "";
            txtPayeeName.Text = "";
            txtPayeeNo.Text = "";
            txtDatePaid.Text = "";
            txtDateTransmitted.Text = "";
            txtACHInfo.Text = "";
            txtCheckNoView.Text = "";
            cboPaymentStatus.Text = "";
            txtCCNoView.Text = "";
            txtWexMethod.Text = "";
            txtWexAddressView.Text = "";
            txtWexCode.Text = "";
            txtCompanyNo.Text = "";
            hfClaimPaymentID.Value = rgPayment.SelectedValue.ToString();
            SQL = "select * from claimpayment where claimpaymentid = " + rgPayment.SelectedValue;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                GetPayeeInfo2();
                txtPaymentAmt.Text = clR.GetFields("paymentamt");
                txtPayMethod.Text = clR.GetFields("paymethod");
                if (clR.GetFields("datetransmitted").Length > 0)
                {
                    txtDateTransmitted.Text = DateTime.Parse(clR.GetFields("datetransmitted")).ToString("M/d/yyyy");
                }
                if (clR.GetFields("datepaid").Length > 0)
                {
                    txtDatePaid.Text = DateTime.Parse(clR.GetFields("datepaid")).ToString("M/d/yyyy");
                }
                txtACHInfo.Text = clR.GetFields("achinfo");
                txtCheckNoView.Text = clR.GetFields("checkno");
                txtCCNoView.Text = clR.GetFields("ccno");
                txtWexAddressView.Text = clR.GetFields("wexdeliveryaddress");
                txtWexMethod.Text = clR.GetFields("wexdeliverymethod");
                txtWexCode.Text = clR.GetFields("wexcode");
                cboPaymentStatus.Text = clR.GetFields("status");
                txtCompanyNo.Text = clR.GetFields("companyno");
            }
        }
        private void GetPayeeInfo2()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimpayee where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtPayeeNo.Text = clR.GetFields("payeeno");
                txtPayeeName.Text = clR.GetFields("payeename");
            }
        }

        protected void btnPaymentViewClose_Click(object sender, EventArgs e)
        {
            pnlList.Visible = true;
            pnlPaymentDetail.Visible = false;
            rgPayment.SelectedIndexes.Clear();
        }

        protected void btnSeekPayee_Click(object sender, EventArgs e)
        {
            pnlSearchClaimPayee.Visible = true;
            pnlPaymentAdd.Visible = false;
        }

        protected void btnAddPayment_Click(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlPaymentAdd.Visible = true;
        }

        protected void btnCloseAdd_Click(object sender, EventArgs e)
        {
            pnlPaymentAdd.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnSaveAdd_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clP = new clsDBO.clsDBO();
            SQL = "select * from claimpayment where claimpaymentid = 0 ";
            clP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clP.RowCount() == 0)
            {
                clP.NewRow();
                clP.SetFields("claimpayeeid", hfClaimPayeeID.Value);
                clP.SetFields("paymentamt", txtPaymentAmtAdd.Text);
                if (rdpDateTransmittedAdd.SelectedDate != null)
                {
                    clP.SetFields("datetransmitted", rdpDateTransmittedAdd.SelectedDate.ToString());
                }
                clP.SetFields("datepaid", rdpDatePaidAdd.SelectedDate.ToString());
                clP.SetFields("paymethod", cboPayMethodAdd.SelectedValue);
                clP.SetFields("achinfo", txtACHInfo.Text);
                clP.SetFields("checkno", txtCheckNo.Text);
                clP.SetFields("ccno", txtCCNo.Text);
                clP.SetFields("wexdeliverymethod", txtWexDeliveryMethodAdd.Text);
                clP.SetFields("wexdeliveryaddress", txtWexDeliveryAddressAdd.Text);
                clP.SetFields("WexCode", txtWexCodeAdd.Text);
                clP.SetFields("status", cboStatusAdd.SelectedValue);
                clP.SetFields("companyno", txtCompanyNoAdd.Text);
                clP.AddRow();
                clP.SaveDB();
            }
            SQL = "select max(claimpaymentid) as MaxClaimPayID from claimpayment where claimpayeeid = " + hfClaimPayeeID.Value;
            clP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                hfClaimPaymentID.Value = clP.GetFields("maxclaimpayid");
            }
            SQL = "select * from claimdetail " +
                  "where claimpayeeid = " + hfClaimPayeeID.Value + " " +
                  "and claimid = " + hfClaimID.Value;
            clP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clP.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clP.RowCount() - 1; cnt++)
                {
                    clP.GetRowNo(cnt);
                    SQL = "select * from claimpaymentlink " +
                          "where claimdetailid = " + clP.GetFields("claimdetailid") + " " +
                          "and claimpaymentid = " + hfClaimPaymentID.Value;
                    clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clR.RowCount() == 0)
                    {
                        clR.NewRow();
                        clR.SetFields("Claimdetailid", clP.GetFields("claimdetailid"));
                        clR.SetFields("claimpaymentid", hfClaimPaymentID.Value);
                        clR.AddRow();
                        clR.SaveDB();
                    }
                }
            }
            pnlList.Visible = true;
            pnlPaymentAdd.Visible = false;
            rgPayment.Rebind();
        }

        protected void rgClaimPayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimPayeeID.Value = rgClaimPayee.SelectedValue.ToString();
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimpayee where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtPayeeNoAdd.Text = clR.GetFields("payeeno");
                txtPayeeNameAdd.Text = clR.GetFields("payeename");
                hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
            }
            pnlSearchClaimPayee.Visible = false;
            pnlPaymentAdd.Visible = true;
        }

        protected void btnPaymentViewSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimpayment where claimpaymentid = " + hfClaimPaymentID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("achinfo", txtACHInfo.Text);
                clR.SetFields("checkno", txtCheckNoView.Text);
                clR.SetFields("ccno", txtCCNoView.Text);
                clR.SetFields("status", cboPaymentStatus.Text);
                clR.SaveDB();
            }
            if (cboPaymentStatus.Text == "Void")
            {
                ProcessVoid();
            }
            pnlList.Visible = true;
            pnlPaymentDetail.Visible = false;
            rgPayment.Rebind();
            rgToBePaid.Rebind();
        }
        private void ProcessVoid()
        {
            string SQL;
            clsDBO.clsDBO clCWP = new clsDBO.clsDBO();
            clsDBO.clsDBO clCP = new clsDBO.clsDBO();
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimwexpayment " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and wexcode = '" + txtWexCode.Text + "' ";
            clCWP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCWP.RowCount() > 0)
            {
                clCWP.GetRow();
                Process pr = new Process();
                pr.StartInfo.FileName = @"C:\ProcessProgram\WexVoid\WexVoid.exe";
                pr.StartInfo.Arguments = clCWP.GetFields("claimwexpaymentid");
                pr.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                pr.Start();
                pr.WaitForExit();
                pr.Close();
            }
            clCWP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCWP.RowCount() > 0)
            {
                clCWP.GetRow();
                if (clCWP.GetFields("voiddate").Length > 0) 
                {
                    SQL = "update claimdetail " +
                          "set claimdetailstatus = 'Authorized' " +
                          "where claimdetailid in (" +
                          "select cd.claimdetailid from claimpayment cp " +
                          "inner join ClaimPaymentLink cpl on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                          "inner join claimdetail cd on cd.ClaimDetailID = cpl.ClaimDetailID " +
                          "where cd.claimid = " + hfClaimID.Value + " " +
                          "and wexcode = '" + txtWexCode.Text + "') ";
                    clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                }
            }
        }
        protected void cboWexMethod_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from servicecenter sc where servicecenterno = '" + txtClaimPayeeNo.Text.Trim() + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (cboWexMethod.SelectedValue == "EMail")
                {
                    txtWexAddress.Text = clR.GetFields("email").Trim();
                }
                if (cboWexMethod.SelectedValue == "Fax")
                {
                    txtWexAddress.Text = clR.GetFields("fax").Trim();
                    txtWexAddress.Text = txtWexAddress.Text.Replace("(", "");
                    txtWexAddress.Text = txtWexAddress.Text.Replace(")", "");
                    txtWexAddress.Text = txtWexAddress.Text.Replace(" ", "");
                    txtWexAddress.Text = txtWexAddress.Text.Replace("-", "");
                    txtWexAddress.Text = txtWexAddress.Text.Replace(".", "");
                }
            }
        }

    }
}