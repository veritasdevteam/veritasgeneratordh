﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimVehicle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfContractID.Value = Request.QueryString["ContractID"];
            pvMoxy.ContentUrl = "ClaimVINMoxy.aspx?ContractID=" + hfContractID.Value;
            pvBasicData.ContentUrl = "ClaimVINBasicData.aspx?ContractID=" + hfContractID.Value;
            pvEngine.ContentUrl = "ClaimVINEngine.aspx?ContractID=" + hfContractID.Value;
            pvTransmission.ContentUrl = "ClaimVINTransmission.aspx?ContractID=" + hfContractID.Value;
            pvWarranty.ContentUrl = "ClaimVINWarranty.aspx?ContractID=" + hfContractID.Value;
            if (!IsPostBack)
            {
                pvMoxy.Selected = true;
                tsClaim.SelectedIndex = 0;
                FillVehicleInfo();
            }
        }
        private void FillVehicleInfo()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            VeritasGlobalToolsV2.clsVIN clV = new VeritasGlobalToolsV2.clsVIN();
            if (hfContractID.Value.Length == 0)
            {
                return;
            }
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                clV.Contract = clC.GetFields("contractno");
                clV.VIN = clC.GetFields("vin");
                clV.ProcessVIN();
                hfVIN.Value = clC.GetFields("vin");
            }
        }

        protected void tsClaim_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsClaim.SelectedTab.Value == "Moxy")
            {
                pvMoxy.Selected = true;
            }
            else if (tsClaim.SelectedTab.Value == "Basic")
            {
                pvBasicData.Selected = true;
            }
            else if (tsClaim.SelectedTab.Value == "Engine")
            {
                pvEngine.Selected = true;
            }
            else if (tsClaim.SelectedTab.Value == "Transmission")
            {
                pvTransmission.Selected = true;
            }
            else if (tsClaim.SelectedTab.Value == "Warranty")
            {
                pvWarranty.Selected = true;
            }
        }
    }
}