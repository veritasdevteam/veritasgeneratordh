﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class RFClaimSubmit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsClaim.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsAssignedTo.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsJobs.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;

            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/Default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                if (hfUserID.Value == "1")
                {
                    btnLossCode.Visible = true;
                }
                else
                {
                    btnLossCode.Visible = false;
                }
                btnRFClaimSubmit.Enabled = false;
                CheckRFClaimSubmit();
                hfClaimID.Value = "0";
                pnlDetail.Visible = false;
                pnlAssignedToChange.Visible = false;
            }
        }
        private void CheckRFClaimSubmit()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and teamlead <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                SQL = "select * from veritasclaims.dbo.claim " +
                      "where not sendclaim is null " +
                      "and processclaimdate is null ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    btnRFClaimSubmit.Enabled = true;
                }
            }
        }
        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = "~\\users\\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO.clsDBO clTD = new clsDBO.clsDBO();
            SQL = "select * from usermessage " +
                  "where toid = " + hfUserID.Value + " " +
                  "and completedmessage = 0 " +
                  "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clTD.RowCount() > 0)
            {
                hlToDo.Visible = true;
            }
            else
            {
                hlToDo.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")) == true)
                {
                    btnUnlockClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimpayment")) == false)
                {
                    btnInspectionWex.Enabled = false;
                    btnCarfaxPayment.Enabled = false;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }
        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }
        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnInspectionWex_Click(object sender, EventArgs e)
        {

        }

        protected void btnCarfaxPayment_Click(object sender, EventArgs e)
        {

        }

        protected void rgClaim_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimID.Value = rgClaim.SelectedValue.ToString();
            FillClaimDetail();
        }
        private void FillClaimDetail()
        {
            hfContractID.Value = "0";
            hfServiceCenterID.Value = "0";
            txtContractNo.Focus();
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            OpenClaim();
            OpenContract();
            rgJobs.Rebind();
        }
        private void OpenClaim()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from veritasclaims.dbo.claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractID.Value = clR.GetFields("contractid");
                hfServiceCenterID.Value = clR.GetFields("servicecenterid");
            }
        }
        private void OpenContract()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from contract c " +
                  "left join InsCarrier ic on c.InsCarrierID = ic.InsCarrierID " +
                  "where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtContractNo.Text = clR.GetFields("contractno");
                txtStatus.Text = clR.GetFields("status");
                txtSaleDate.Text = clR.GetFields("saledate");
                txtPaidDate.Text = clR.GetFields("datepaid");
                txtFName.Text = clR.GetFields("fname");
                txtLName.Text = clR.GetFields("lname");
                txtInspCarrier.Text = clR.GetFields("inscarriername");
                hfPlanTypeID.Value = clR.GetFields("plantypeid");
                hfProgram.Value = clR.GetFields("programid");
                GetProgram();
                GetPlanType();
                txtTerm.Text = clR.GetFields("termmonth") + "/" + long.Parse(clR.GetFields("termmile")).ToString("#,##0");
                if (clR.GetFields("effdate").Length > 0) 
                {
                    txtEffDate.Text = DateTime.Parse(clR.GetFields("effdate")).ToString("M/d/yyyy");
                }

                if (clR.GetFields("expdate").Length > 0) 
                {
                    txtExpDate.Text = DateTime.Parse(clR.GetFields("expdate")).ToString("M/d/yyyy");
                }
                txtEffMile.Text = long.Parse(clR.GetFields("effmile")).ToString("#,##0");
                if (clR.GetFields("expmile").Length > 0) {
                    txtExpMile.Text = long.Parse(clR.GetFields("expmile")).ToString("#,##0");
                }
                if (clR.GetFields("decpage").Length == 0) 
                {
                    tcDec.Visible = false;
                    tcDesc2.Visible = false;
                } 
                else 
                {
                    tcDec.Visible = true;
                    tcDesc2.Visible = true;
                    hlDec.NavigateUrl = clR.GetFields("decpage");
                    hlDec.Text = clR.GetFields("decpage");
                }
                if (clR.GetFields("tcpage").Length == 0) 
                {
                    tcTC.Visible = false;
                    tcTC2.Visible = false;
                } 
                else 
                {
                    tcTC.Visible = true;
                    tcTC2.Visible = true;
                    hlTC.NavigateUrl = clR.GetFields("tcpage");
                    hlTC.Text = clR.GetFields("tcpage");
                }
            }
        }
        private void GetPlanType()
        {
            string SQL;
            clsDBO.clsDBO clPT = new clsDBO.clsDBO();
            SQL = "select * from plantype where plantypeid = " + hfPlanTypeID.Value;
            clPT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clPT.RowCount() > 0)
            {
                clPT.GetRow();
                txtPlan.Text = clPT.GetFields("plantype");
            }
        }
        private void GetProgram()
        {
            string SQL;
            clsDBO.clsDBO clP = new clsDBO.clsDBO();
            SQL = "select * from program where programid = " + hfProgram.Value;
            clP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                txtProgram.Text = clP.GetFields("programname");
            }
        }
        protected void btnSendClaim_Click(object sender, EventArgs e)
        {
            VeritasGlobalToolsV2.clsImportClaim clIC = new VeritasGlobalToolsV2.clsImportClaim();
            if (hfAssignedTo.Value == "")
            {
                hfAssignedTo.Value = hfUserID.Value;
            }
            if (hfAssignedTo.Value == "0")
            {
                hfAssignedTo.Value = hfUserID.Value;
            }
            clIC.VeritasClaimID = long.Parse(hfClaimID.Value);
            clIC.AssignedToID = long.Parse(hfAssignedTo.Value);
            clIC.UserID = long.Parse(hfUserID.Value);

            clIC.ProcessClaim();

            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgClaim.Rebind();
        }

        protected void btnChangeAssignedTo_Click(object sender, EventArgs e)
        {
            pnlAssignedToChange.Visible = true;
            pnlDetail.Visible = false;
        }

        protected void rgAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfAssignedTo.Value = "0";
            hfAssignedTo.Value = rgAssignedTo.SelectedValue.ToString();
            GetAssignedTo();
        }
        private void GetAssignedTo()
        {
            txtAssignedTo.Text = "";
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfAssignedTo.Value == "")
            {
                hfAssignedTo.Value = hfUserID.Value;
            }
            SQL = "select * from userinfo where userid = " + hfAssignedTo.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAssignedTo.Text = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
            pnlDetail.Visible = true;
            pnlAssignedToChange.Visible = false;
        }
        protected void rgClaim_DataBound(object sender, EventArgs e)
        {
            for (int cnt = 0; cnt <= rgClaim.Items.Count - 1; cnt++)
            {
                if (rgClaim.Items[cnt]["ServiceCenterName"].Text.ToLower().Contains("- an")) 
                {
                    rgClaim.Items[cnt]["ServiceCenterName"].BackColor = System.Drawing.Color.LightBlue;
                }
            }
        }
    }
}