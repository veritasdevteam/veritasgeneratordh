﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimVINMoxy.aspx.cs" Inherits="VeritasGeneratorCS.Claim.ClaimVINMoxy" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Table runat="server" Font-Names="Calibri" Font-Size="11">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Panel ID="pnlViewer" runat="server">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            VIN:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Label ID="lblVIN" runat="server" Text=""></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Class:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Label ID="lblClass" runat="server" Text=""></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Year:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Label ID="lblYear" runat="server" Text=""></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Make:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Label ID="lblMake" runat="server" Text=""></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Model:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Label ID="lblModel" runat="server" Text=""></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Trim:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Label ID="lblTrim" runat="server" Text=""></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkAWD" Text="AWD" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkTurbo" Text="Turbo" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkDiesel" Text="Diesel" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkHybrid" Text="Hybrid" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkCommercial" Text="Commercial" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkHydraulic" Text="Hydraulic" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkAirBladder" Text="Air Bladder" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkLiftKit" Text="Lift Kit" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkLuxury" Text="Luxury" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkLuxuryNC" Text="Luxury NC" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkSeals" Text="Seals" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkSnowPlow" Text="Snow Plow" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkLargerLiftKit" Text="Larger Lift Kit" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkRideShare" Text="Rideshare" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkChrome" Text="Chrome" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkCosmetic" Text="Cosmetic" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkSalvageTitle" Text="Salvage Title Authorized" Enabled="false" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            &nbsp
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
