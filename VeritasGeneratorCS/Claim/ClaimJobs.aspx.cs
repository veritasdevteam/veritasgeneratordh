﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimJobs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            dsStates.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsCDT.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsClaimDetailStatus.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsClaimReason.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsLossCode.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsWexMethod.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsClaimPayee.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsJobs.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;

            if (Global.bJobRefresh)
            {
                rgJobs.Rebind();
                Global.bJobRefresh = false;
            }
            if (!IsPostBack)
            {
                GetServerInfo();
                CheckPaymentSecurity();
                if (CheckLock())
                {
                    LockButtons();
                }
                else
                {
                    UnlockButtons();
                }
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                pnlAddClaimPayee.Visible = false;
                pnlLossCode.Visible = false;
                pnlSeekPayee.Visible = false;
                pnlToBePaidDetail.Visible = false;
                trACH.Visible = false;
                trCC.Visible = false;
                trCheck.Visible = false;
                trWex.Visible = false;
                FillPayeeTotal();
                FillRateType();
                FillJobsTotal();
                FillTotal();
                ReadOnlyButtons();
                FillJobNo();
                FillJobNo2();
                FillRateType2();
                if (hfSlushNote.Value == "Visible")
                {
                    ShowSlushNote();
                }
                else
                {
                    HideSlushNote();
                }
                if (hfFieldCheck.Value == "Visible")
                {
                    ShowFieldCheck();
                }
                else
                {
                    HideFieldCheck();
                }
            }
        }

        private void FillRateType2()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            ListItem i1 = new ListItem();
            cboRateType0.Items.Clear();
            i1.Value = 0.ToString();
            i1.Text = "";
            cboRateType0.Items.Add(i1);
            i1 = new ListItem();
            i1.Value = 1.ToString();
            i1.Text = "Claims Reserve";
            cboRateType0.Items.Add(i1);

            SQL = "select * from contract c " +
                  "inner join claim cl on c.contractid = cl.contractid " +
                  "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("programname") == "Veritas")
                {
                    i1 = new ListItem();
                    i1.Value = 2868.ToString();
                    i1.Text = "Veritas RMF";
                    cboRateType0.Items.Add(i1);
                }
                else
                {
                    i1 = new ListItem();
                    i1.Value = 2171.ToString();
                    i1.Text = "Red Shield RMF";
                    cboRateType0.Items.Add(i1);
                }
                if (clR.GetFields("contractno").Contains("AN"))
                {
                    i1 = new ListItem();
                    i1.Value = 1101.ToString();
                    i1.Text = "AN RMF";
                    cboRateType0.Items.Add(i1);
                }
                if (clR.GetFields("contractno").Contains("EP"))
                {
                    i1 = new ListItem();
                    i1.Value = 3072.ToString();
                    i1.Text = "EP RMF";
                    cboRateType0.Items.Add(i1);
                }
                i1 = new ListItem();
                i1.Value = 2834.ToString();
                i1.Text = "Seller RMF";
                cboRateType0.Items.Add(i1);
                i1 = new ListItem();
                i1.Value = 3071.ToString();
                i1.Text = "Agent RMF";
                cboRateType0.Items.Add(i1);
            }
        }
        private void FillJobNo2()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "";
            cboSlushJobNo.Items.Add(li);
            li = new ListItem();
            li.Value = "All";
            li.Text = "All";
            cboSlushJobNo.Items.Add(li);
            SQL = "select JobNo from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "group by Jobno " +
                  "order by JobNo ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    li = new ListItem();
                    li.Value = clR.GetFields("jobno");
                    li.Text = clR.GetFields("jobno");
                    cboSlushJobNo.Items.Add(li);
                }
            }
        }
        private void FillJobNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "";
            cboJobNo.Items.Add(li);
            SQL = "select JobNo from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "group by Jobno " +
                  "order by JobNo ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    li = new ListItem();
                    li.Value = clR.GetFields("jobno");
                    li.Text = clR.GetFields("jobno");
                    cboJobNo.Items.Add(li);
                }
            }
        }
        private void FillJobsTotal()
        {
            string SQL;
            clsDBO.clsDBO clCD = new clsDBO.clsDBO();
            ListItem li = new ListItem();
            li.Value = 0.ToString();
            li.Text = "All";
            cboJobsTotal.Items.Add(li);
            SQL = "select jobno from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "group by jobno " +
                  "order by jobno ";
            clCD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCD.RowCount() == 0)
            {
                for (int cnt = 0; cnt <= clCD.RowCount() - 1; cnt++)
                {
                    clCD.GetRowNo(cnt);
                    li = new ListItem();
                    li.Text = clCD.GetFields("jobno");
                    li.Value = (cnt + 1).ToString();
                    cboJobsTotal.Items.Add(li);
                }
            }
            cboJobsTotal.SelectedValue = 0.ToString();
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly").ToLower() == "true")
                {
                    LockButtons();
                }
                if (Convert.ToBoolean(clR.GetFields("AllowSlush")))
                {
                    tcManageSlush.Visible = true;
                }
                else
                {
                    tcManageSlush.Visible = false;
                }
                tcManagerAuth.Visible = true;
                if (Convert.ToBoolean(clR.GetFields("AllowAZDenied")))
                {
                    hfAllowAZDenied.Value = "true";
                }
                else
                {
                    hfAllowAZDenied.Value = "false";
                }
                if (Convert.ToBoolean(clR.GetFields("allowtotalpayment")))
                {
                    txtTotalAmt.ReadOnly = false;
                    txtPaidAmt.ReadOnly = false;
                }
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private bool CheckLock()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "Select claimid from claim " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "And lockuserid <> " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void CheckPaymentSecurity()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and claimpayment <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                btnMakePayment.Visible = true;
            }
            else
            {
                btnMakePayment.Visible = false;
            }
        }
        private void LockButtons()
        {
            btnAddClaimPayee.Enabled = false;
            btnAddDetail.Enabled = false;
            btnSaveClaimDetail.Enabled = false;
            btnSCSave.Enabled = false;
            btnSeekLossCode.Enabled = false;
            btnSeekPayee.Enabled = false;
            btnApproveIt.Enabled = false;
            btnAuthIt.Enabled = false;
            btnDenyIt.Enabled = false;
        }
        private void UnlockButtons()
        {
            btnAddClaimPayee.Enabled = true;
            btnAddDetail.Enabled = true;
            btnCancelClaimDetail.Enabled = true;
            btnSaveClaimDetail.Enabled = true;
            btnSCCancel.Enabled = true;
            btnSCSave.Enabled = true;
            btnSeekLossCode.Enabled = true;
            btnSeekPayee.Enabled = true;
        }
        protected void btnAddDetail_Click(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            cboClaimDetailType.ClearSelection();
            GetNextJobNo();
            cboClaimDetailStatus.ClearSelection();
            txtLossCode.Text = "";
            txtLossCodeDesc.Text = "";
            txtPayeeNo.Text = "";
            txtPayeeName.Text = "";
            txtReqAmt.Text = "0";
            txtAuthAmt.Text = "0";
            txtTaxAmt.Text = "0";
            txtPaidAmt.Text = "0";
            cboRateType0.SelectedValue = "1";
            rdpAuthorizedDate.SelectedDate = null;
            rdpDateApprove.SelectedDate = null;
            rdpDatePaid.SelectedDate = null;
            hfClaimDetailID.Value = "0";
        }
        private void GetNextJobNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfJobNo.Value.Length == 0) 
            {
                SQL = "select max(jobno) as mJob from claimdetail " +
                      "where claimid = " + hfClaimID.Value + " " +
                      "and jobno like 'j%' ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() == 0) 
                {
                    hfJobNo.Value = "J01";
                } 
                else 
                {
                    clR.GetRow();
                    if (clR.GetFields("mjob").Length == 0) 
                    {
                        hfJobNo.Value = "J01";

                    } 
                    else 
                    {
                        hfJobNo.Value = "J" + (long.Parse(clR.GetFields("mjob").Substring(clR.GetFields("mjob").Length - 2, 2)) + 1).ToString("00");
                    }
                }
            }

            SQL = "select max(jobno) as mJob from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and jobno like 'j%' " +
                  "and jobno <> '" + hfJobNo.Value + "' " +
                  "and losscode = '" + txtLossCode.Text.Trim() + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                if (clR.GetFields("mjob").Length > 0) 
                {
                    hfJobNo.Value = clR.GetFields("mJob");
                }
            }
            txtJobNo.Text = hfJobNo.Value;
        }
        private void FillRateType()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            cboSlushRateType.Items.Clear();
            ListItem i1 = new ListItem();
            i1.Value = "0";
            i1.Text = "";
            cboSlushRateType.Items.Add(i1);

            i1 = new ListItem();
            i1.Value = "1";
            i1.Text = "Claims Reserve";
            cboSlushRateType.Items.Add(i1);

            SQL = "select * from contract c " +
                  "inner join claim cl on c.contractid = cl.contractid " +
                  "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("programname") == "Veritas")
                {
                    i1 = new ListItem();
                    i1.Value = "2868";
                    i1.Text = "Veritas RMF";
                    cboSlushRateType.Items.Add(i1);
                }
                else 
                {
                    i1 = new ListItem();
                    i1.Value = "2171";
                    i1.Text = "Red Shield RMF";
                    cboSlushRateType.Items.Add(i1);
                }
                if (clR.GetFields("contractno").Contains("AN"))
                {
                    i1 = new ListItem();
                    i1.Value = "1101";
                    i1.Text = "AN RMF";
                    cboSlushRateType.Items.Add(i1);
                }
                if (clR.GetFields("contractno").Contains("EP"))
                {
                    i1 = new ListItem();
                    i1.Value = "3072";
                    i1.Text = "EP RMF";
                    cboSlushRateType.Items.Add(i1);
                }
                i1 = new ListItem();
                i1.Value = "2834";
                i1.Text = "Seller RMF";
                cboSlushRateType.Items.Add(i1);
                i1 = new ListItem();
                i1.Value = "3071";
                i1.Text = "Agent RMF";
                cboSlushRateType.Items.Add(i1);
            }
        }
        protected void rgJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("allowslush")))
                {
                    cboRateType0.Enabled = true;
                }
                else
                {
                    cboRateType0.Enabled = false;
                }
                if (Convert.ToBoolean(clR.GetFields("AutoNationOnly")) == true)
                {
                    return;
                }
                if (Convert.ToBoolean(clR.GetFields("VeroOnly")) == true)
                {
                    return;
                }
            }

            pnlList.Visible = false;
            pnlDetail.Visible = true;
            cboClaimDetailType.ClearSelection();
            txtJobNo.Text = "";
            cboClaimDetailStatus.ClearSelection();
            txtLossCode.Text = "";
            txtLossCodeDesc.Text = "";
            txtPayeeNo.Text = "";
            txtPayeeName.Text = "";
            txtReqAmt.Text = 0.ToString();
            txtAuthAmt.Text = 0.ToString();
            txtTaxAmt.Text = 0.ToString();
            txtPaidAmt.Text = 0.ToString();
            rdpAuthorizedDate.SelectedDate = null;
            rdpDateApprove.SelectedDate = null;
            rdpDatePaid.SelectedDate = null;
            cboRateType0.ClearSelection();
            hfClaimDetailID.Value = 0.ToString();
            //FillRateType();
            hfClaimDetailID.Value = rgJobs.SelectedValue.ToString();
            SQL = "select * from claimdetail cd where claimdetailid = " + rgJobs.SelectedValue;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                cboClaimDetailType.SelectedValue = clR.GetFields("claimdetailtype");
                txtJobNo.Text = clR.GetFields("jobno");
                cboClaimDetailStatus.SelectedValue = clR.GetFields("claimdetailstatus");
                txtLossCode.Text = clR.GetFields("losscode");
                GetLossCodeDesc();
                hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                GetPayee();

                txtReqAmt.Text = clR.GetFields("reqamt");
                txtAuthAmt.Text = clR.GetFields("authamt");
                txtTaxAmt.Text = clR.GetFields("taxamt");
                txtPaidAmt.Text = clR.GetFields("paidamt");
                txtReqQty.Text = clR.GetFields("reqqty");
                txtReqCost.Text = clR.GetFields("reqcost");
                txtAuthQty.Text = clR.GetFields("authqty");
                txtAuthCost.Text = clR.GetFields("authcost");
                txtPartNo.Text = clR.GetFields("partno");
                txtClaimDesc.Text = clR.GetFields("ClaimDesc");
                txtTotalAmt.Text = clR.GetFields("totalamt");
                cboReason.SelectedValue = clR.GetFields("claimreasonid");
                if (clR.GetFields("taxper").Length > 0) 
                {
                    if (Convert.ToInt32(clR.GetFields("taxper")) < 1) 
                    {
                        txtTaxRate.Text = (Convert.ToInt32(clR.GetFields("taxper")) * 100).ToString();
                    } 
                    else 
                    {
                        txtTaxRate.Text = clR.GetFields("taxper");
                    }
                }
                if (clR.GetFields("dateauth").Length > 0) 
                {
                    rdpAuthorizedDate.SelectedDate = DateTime.Parse(clR.GetFields("dateauth"));
                } 
                else 
                {
                    rdpAuthorizedDate.Clear();
                }
                if (clR.GetFields("dateapprove").Length > 0) 
                {
                    rdpDateApprove.SelectedDate = DateTime.Parse(clR.GetFields("dateapprove"));
                } 
                else 
                {
                    rdpDateApprove.Clear();
                }
                if (clR.GetFields("datepaid").Length > 0) 
                {
                    rdpDatePaid.SelectedDate = DateTime.Parse(clR.GetFields("datepaid"));
                } 
                else 
                {
                    rdpDatePaid.Clear();
                }
                if (clR.GetFields("ratetypeid").Length > 0) 
                {
                    try
                    {
                        cboRateType0.SelectedValue = clR.GetFields("ratetypeid");
                    }
                    catch
                    {
                        ListItem i1 = new ListItem();
                        if (clR.GetFields("ratetypeid") == "1101")
                        {
                            i1 = new ListItem();
                            i1.Value = "1101";
                            i1.Text = "AN RMF";
                            cboRateType0.Items.Add(i1);
                        }
                        if (clR.GetFields("ratetypeid") == "2868")
                        {
                            i1 = new ListItem();
                            i1.Value = "2868";
                            i1.Text = "Veritas RMF";
                            cboRateType0.Items.Add(i1);
                        }
                        if (clR.GetFields("ratetypeid") == "2171")
                        {
                            i1 = new ListItem();
                            i1.Value = "2171";
                            i1.Text = "Red Shield RMF";
                            cboRateType0.Items.Add(i1);
                        }
                        if (clR.GetFields("ratetypeid") == "2995")
                        {
                            i1 = new ListItem();
                            i1.Value = "2995";
                            i1.Text = "Auto Nation RMF";
                            cboRateType0.Items.Add(i1);
                        }
                        if (clR.GetFields("ratetypeid") == "3072") 
                        { 
                            i1 = new ListItem();
                            i1.Value = "3072";
                            i1.Text = "EP RMF";
                            cboRateType0.Items.Add(i1);
                        }
                        cboRateType0.SelectedValue = clR.GetFields("ratetypeid");
                    }
                }
                if (txtReqQty.Text.Length == 0) 
                {
                    txtReqQty.Text = 0.ToString();
                }
                if (txtReqCost.Text.Length == 0) 
                {
                    txtReqCost.Text = 0.ToString();
                }
                if (txtAuthCost.Text.Length == 0) 
                {
                    txtAuthCost.Text = 0.ToString();
                }
                if (txtAuthQty.Text.Length == 0) 
                {
                    txtAuthQty.Text = 0.ToString();
                }
                if (txtTaxRate.Text.Length == 0) 
                {
                    txtTaxRate.Text = 0.ToString();
                }
                if (txtReqAmt.Text.Length == 0) 
                {
                    txtReqAmt.Text = 0.ToString();
                }
                if (txtTotalAmt.Text.Length == 0) 
                {
                    txtTotalAmt.Text = 0.ToString();
                }
                LockAuthText();
                CheckSecurity();
                if (Functions.GetDaysSale(long.Parse(clR.GetFields("claimid"))) <= 45) 
                {
                    if (!Functions.GetAllow45(long.Parse(hfUserID.Value))) 
                    {
                        cboClaimDetailStatus.Enabled = false;
                    }
                }
            }
        }
        private void CheckSecurity()
        {
            string SQL;
            clsDBO.clsDBO clUSI = new clsDBO.clsDBO();
            cboClaimDetailStatus.Enabled = true;
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clUSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clUSI.RowCount() > 0)
            {
                clUSI.GetRow();
                if (cboClaimDetailStatus.SelectedValue == "Denied")
                {
                    if (Convert.ToBoolean(clUSI.GetFields("AllowUndenyClaim")))
                    {
                        cboClaimDetailStatus.Enabled = true;
                    }
                    else
                    {
                        cboClaimDetailStatus.Enabled = false;
                    }
                }
            }
        }
        private void LockAuthText()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("AllCoverage")))
                {
                    return;
                }
            }

            if (GetSaleDate() < DateTime.Parse("1/1/2019"))
            {
                return;
            }


            SQL = "select * from claimlosscode clc " +
                  "inner join contractcoverage cc " +
                  "on cc.CoverageCode = clc.LossCodePrefix " +
                  "inner join contract c " +
                  "on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID " +
                  "inner join claim cl " +
                  "on cl.ContractID = c.ContractID " +
                  "where cl.claimid = " + hfClaimID.Value + " ";
            if (txtLossCode.Text.Length == 0)
            {
                return;
            }
            SQL = SQL + "and cc.coveragecode = '" + txtLossCode.Text.Substring(0, 4) + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                SQL = "update claimdetail " +
                      "set losscodelock = 1 " +
                      "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                txtAuthCost.Enabled = false;
                txtAuthQty.Enabled = false;
                txtTaxRate.Enabled = false;
                cboClaimDetailStatus.Enabled = false;
                txtAuthAmt.Enabled = false;
                txtPaidAmt.Enabled = false;
                txtLossCode.BackColor = Color.Red;
            }
            else
            {
                SQL = "update claimdetail " +
                      "set losscodelock = 0 " +
                      "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                txtAuthCost.Enabled = true;
                txtAuthQty.Enabled = true;
                txtTaxRate.Enabled = true;
                cboClaimDetailStatus.Enabled = true;
                txtAuthAmt.Enabled = true;
                txtLossCode.BackColor = Color.White;
            }
        }
        private DateTime GetSaleDate()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select c.saledate from contract c inner join claim cl on cl.contractid = c.contractid ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return DateTime.Parse(clR.GetFields("saledate"));
            }
            return DateTime.Parse("1/1/1950");
        }
        private void GetPayee()
        {
            if (hfClaimPayeeID.Value == "")
            {
                hfClaimPayeeID.Value = 0.ToString();
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimpayee where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtPayeeName.Text = clR.GetFields("payeename");
                txtPayeeNo.Text = clR.GetFields("payeeno");
            }
        }
        private void GetLossCodeDesc()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimlosscode where losscode = '" + txtLossCode.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLossCodeDesc.Text = clR.GetFields("losscodedesc");
            }
        }
        protected void btnSeekLossCode_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlLossCode.Visible = true;
            rgLossCode.Rebind();
            ColorLossCode();
        }
        private void ColorLossCode()
        {
            //string SQL;
            //clsDBO.clsDBO clR = new clsDBO.clsDBO();

            if (GetSaleDate() < DateTime.Parse("1/1/2019"))
            {
                return;
            }

            rgLossCode.Columns[3].Visible = true;

            for (int cnt2 = 0; cnt2 <= rgLossCode.Items.Count - 1; cnt2++)
            {
                if (rgLossCode.Items[cnt2]["ACP"].Text == "1")
                {
                    rgLossCode.Items[cnt2]["losscode"].BackColor = Color.Red;
                }
            }

            rgLossCode.Columns[3].Visible = false;

            //SQL = "select coveragecode from contractcoverage cc "
            //SQL = SQL + "inner join contract c on c.programid = cc.programid and c.plantypeid = cc.plantypeid "
            //SQL = SQL + "inner join claim cl on cl.contractid = c.contractid "
            //SQL = SQL + "where cl.claimid = " & hfClaimID.Value
            //clR.OpenDB(SQL, AppSettings("connstring"))
            //If clR.RowCount > 0 Then
            //    For cnt = 0 To clR.RowCount - 1
            //        clR.GetRowNo(cnt)
            //        For cnt2 = 0 To rgLossCode.Items.Count - 1
            //            If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = clR.Fields("coveragecode") Then
            //                rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            //            End If
            //            If hfUserID.Value = "1" Or hfUserID.Value = "1205" Or hfUserID.Value = "1205" Then
            //                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "RR00" Then
            //                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            //                End If
            //                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "PA00" Then
            //                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            //                End If
            //                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZLEG" Then
            //                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            //                End If
            //                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZSET" Then
            //                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            //                End If
            //                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZATO" Then
            //                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            //                End If
            //            End If
            //        Next
            //    Next
            //End If
        }
        protected void btnSeekPayee_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSeekPayee.Visible = true;
        }

        protected void btnCancelClaimDetail_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgJobs.Rebind();
        }

        protected void btnSaveClaimDetail_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            //'FillRateType();
            SQL = "select * from claimdetail where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfStatus.Value = clR.GetFields("claimdetailstatus");
                hfOldTotalAmt.Value = clR.GetFields("totalamt");
            }
            else
            {
                clR.NewRow();
                hfStatus.Value = "New";
                hfOldTotalAmt.Value = "0";
            }
            if (cboClaimDetailStatus.SelectedValue == "Authorized" || cboClaimDetailStatus.SelectedValue == "Approved")
            {
                if (clR.GetFields("claimdetailstatus") != "Authorized") 
                {
                    if (cboClaimDetailStatus.SelectedValue != clR.GetFields("claimdetailstatus")) 
                    {
                       if (CheckMissingInfo()) 
                       {
                            return;
                       }
                   }
               }
            }
            if (cboClaimDetailStatus.Text == "Authorized" && !CheckInspect()) 
            {
                lblAuthorizedError.Text = "Inspection has not been completed.";
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                return;
            }
            if (txtAuthAmt.Text.Length == 0) 
            {
                txtFieldChk.Text = "Authorized Amt is missing";
                ShowFieldCheck();
                return;
            }
            if (Math.Round(Convert.ToDouble(txtAuthAmt.Text) + Convert.ToDouble(txtTaxAmt.Text), 2) != Convert.ToDouble(txtTotalAmt.Text)) 
            {
                txtFieldChk.Text = "The Authorized Amt plus Tax Amt does not equal Total Amt.";
                ShowFieldCheck();
                return;
            }
            if (cboClaimDetailStatus.Text == "Paid")
            {
                if (txtTotalAmt.Text != txtPaidAmt.Text)
                {
                    txtFieldChk.Text = "The Paid Amt does not equal Total Amt.";
                    ShowFieldCheck();
                    return;
                }
            }
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimdesc", txtClaimDesc.Text);
            clR.SetFields("claimdetailtype", cboClaimDetailType.SelectedValue);
            if (txtJobNo.Text.Length > 0) 
            {
                clR.SetFields("jobno", txtJobNo.Text);
            }
            clR.SetFields("losscode", txtLossCode.Text);
            clR.SetFields("reqamt", txtReqAmt.Text);
            clR.SetFields("claimpayeeid", hfClaimPayeeID.Value);
            clR.SetFields("authcost", txtAuthCost.Text);
            if (txtTaxRate.Text.Length > 0) 
            {
                clR.SetFields("taxper", Convert.ToDouble(txtTaxRate.Text).ToString());
            }
            if (txtTaxAmt.Text.Length == 0)
            {
                clR.SetFields("taxamt", 0.ToString());
            } 
            else 
            { 
                if (clR.GetFields("authqty").Length == 0) 
                {
                    clR.SetFields("authqty", 0.ToString());
                }
                clR.SetFields("taxamt", txtTaxAmt.Text);
            }
            clR.SetFields("totalamt", txtTotalAmt.Text);
            clR.SetFields("Authamt", txtAuthAmt.Text);
            clR.SetFields("paidamt", txtPaidAmt.Text);
            clR.SetFields("claimreasonid", cboReason.SelectedValue);
            if (rdpAuthorizedDate.SelectedDate != null) 
            {
                clR.SetFields("dateauth", rdpAuthorizedDate.SelectedDate.ToString());
            }
            else 
            {
                clR.SetFields("dateauth", "");
            }
            if (rdpDateApprove.SelectedDate != null)
            {
                clR.SetFields("dateapprove", rdpDateApprove.SelectedDate.ToString());
            } 
            else 
            {
                clR.SetFields("dateapprove", "");
            }
            if (rdpDatePaid.SelectedDate != null)
            {
                clR.SetFields("datepaid", rdpDatePaid.SelectedDate.ToString());
            } 
            else 
            {
                clR.SetFields("datepaid", "");
            }
            if (clR.GetFields("ratetypeid") == "1") 
            {
                if (cboRateType0.SelectedValue != "1") 
                {
                    clR.SetFields("slushbyid", hfUserID.Value);
                    clR.SetFields("slushdate", DateTime.Today.ToString());
                }
            }
            clR.SetFields("ratetypeid", cboRateType0.SelectedValue);
            clR.SetFields("reqqty", txtReqQty.Text);
            clR.SetFields("reqcost", txtReqCost.Text);
            clR.SetFields("authqty", txtAuthQty.Text);
            clR.SetFields("authcost", txtAuthCost.Text);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            lblApprovedError.Text = "";
            if (cboClaimDetailStatus.SelectedValue == "Denied") 
            {
                if (clR.GetFields("claimdetailstatus").Trim() != "Denied") 
                {
                    if (hfAllowAZDenied.Value.ToLower() == "false") 
                    {
                        if (Functions.GetServiceCenterState(long.Parse(hfClaimID.Value)).ToUpper() == "AZ") 
                        {
                            lblAuthorizedError.Text = "You can not deny an AZ claim. Please see manager.";
                            lblAuthorizedError.Visible = true;
                            goto MoveHere;
                        }
                    }
                }
                clR.SetFields("claimdetailstatus", cboClaimDetailStatus.SelectedValue);
                CheckClose();
                goto MoveToSave;
            }
            clR.SetFields("claimdetailstatus", cboClaimDetailStatus.SelectedValue);

            if (cboClaimDetailStatus.SelectedValue == "Cancelled") 
            {
                CheckClose();
                goto MoveToSave;
            }
            if (cboClaimDetailStatus.SelectedValue == "Requested") 
            {
                CheckClose();
                goto MoveToSave;
            }
            if (clR.GetFields("claimdetailstatus") != "Paid") 
            {
                if (cboClaimDetailStatus.SelectedValue == "Approved") 
                {
                    if (txtAuthAmt.Text.Length == 0) 
                    {
                        txtAuthAmt.Text = 0.ToString();
                    }
                    if (clR.GetFields("claimdetailid").Length > 0)
                    {
                        if (!CheckLimitApproved(Convert.ToDouble(txtAuthAmt.Text), long.Parse(clR.GetFields("claimdetailid"))))
                        {
                            lblApprovedError.Text = "Send to Manager for Approval";
                            goto MoveHere;
                        }
                    }
                    else 
                    { 
                        if (!CheckLimitApproved(Convert.ToDouble(txtAuthAmt.Text), 0)) 
                        {
                            lblApprovedError.Text = "Send to Manager for Approval";
                            goto MoveHere;
                        }
                    }
                }
                if (cboClaimDetailStatus.SelectedValue == "Authorized") 
                {
                    if (txtAuthAmt.Text.Length == 0) 
                    {
                        txtAuthAmt.Text = 0.ToString();
                    }
                    if (!CheckLimitAuthorized(Convert.ToDouble(txtAuthAmt.Text), long.Parse(clR.GetFields("claimdetailid")))) 
                    {
                        lblApprovedError.Text = "Send to Manager for Approval";
                        clR.SetFields("claimdetailstatus", "Requested");
                    }
                }
            }
        MoveToSave:

            if (hfTicket.Value.ToLower() == "true") 
            {
                hfTicket.Value = "false";
                PlaceTicket();
            }
            if (clR.RowCount() == 0) 
            {
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }
            clR.SaveDB();
            AddChange();
        MoveHere:
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgJobs.Rebind();
            FillTotal();
            SQL = "update claim " +
                  "set moddate = '" + DateTime.Today + "', " +
                  "modby = " + hfUserID.Value + " " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void AddChange()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail cd where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                if (hfStatus.Value == "Requested") 
                {
                    if (clR.GetFields("claimdetailstatus") == "Authorized") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "values ('" + DateTime.Today + "', 0, " + clR.GetFields("totalamt") + ", " + clR.GetFields("totalamt") +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Approved") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "values ('" + DateTime.Today + "', 0, " + clR.GetFields("totalamt") + ", " + clR.GetFields("totalamt") +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Paid") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "values ('" + DateTime.Today + "', 0, " + clR.GetFields("totalamt") + ", " + clR.GetFields("totalamt") +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                }
                if (hfStatus.Value == "Authorized") 
                {
                    if (hfOldTotalAmt.Value == "") 
                    {
                        hfOldTotalAmt.Value = "0";
                    }
                    if (Convert.ToDouble(hfOldTotalAmt.Value) != Convert.ToDouble(clR.GetFields("totalamt"))) 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "values ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              clR.GetFields("totalamt") + ", " + (Convert.ToDouble(clR.GetFields("totalamt")) - Convert.ToDouble(hfOldTotalAmt.Value)) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Requested") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "value ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              "0, " + (Convert.ToDouble(hfOldTotalAmt.Value) * -1) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Denied") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "value ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              "0, " + (Convert.ToDouble(hfOldTotalAmt.Value) * -1) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                }
                if (hfStatus.Value == "Approved") 
                {
                    if (hfOldTotalAmt.Value == "") 
                    {
                        hfOldTotalAmt.Value = "0";
                    }
                    if (Convert.ToDouble(hfOldTotalAmt.Value) != Convert.ToDouble(clR.GetFields("totalamt"))) 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "values ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              clR.GetFields("totalamt") + ", " + (Convert.ToDouble(clR.GetFields("totalamt")) - Convert.ToDouble(hfOldTotalAmt.Value)) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Requested") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "value ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              "0, " + (Convert.ToDouble(hfOldTotalAmt.Value) * -1) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Denied") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "value ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              "0, " + (Convert.ToDouble(hfOldTotalAmt.Value) * -1) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                }
                if (hfStatus.Value == "Transmitted") 
                {
                    if (hfOldTotalAmt.Value == "") {
                        hfOldTotalAmt.Value = "0";
                    }
                    if (Convert.ToDouble(hfOldTotalAmt.Value) != Convert.ToDouble(clR.GetFields("totalamt"))) 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "values ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              clR.GetFields("totalamt") + ", " + (Convert.ToDouble(clR.GetFields("totalamt")) - Convert.ToDouble(hfOldTotalAmt.Value)) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Requested") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "value ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              "0, " + (Convert.ToDouble(hfOldTotalAmt.Value) * -1) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Denied") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "value ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              "0, " + (Convert.ToDouble(hfOldTotalAmt.Value) * -1) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                }
                if (hfStatus.Value == "Paid") 
                {
                    if (hfOldTotalAmt.Value == "") 
                    {
                        hfOldTotalAmt.Value = "0";
                    }
                    if (Convert.ToDouble(hfOldTotalAmt.Value) != Convert.ToDouble(clR.GetFields("totalamt"))) 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "values ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              clR.GetFields("totalamt") + ", " + (Convert.ToDouble(clR.GetFields("totalamt")) - Convert.ToDouble(hfOldTotalAmt.Value)) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Requested") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "value ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              "0, " + (Convert.ToDouble(hfOldTotalAmt.Value) * -1) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Denied") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "value ('" + DateTime.Today + "', " + hfOldTotalAmt.Value + ", " +
                              "0, " + (Convert.ToDouble(hfOldTotalAmt.Value) * -1) +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                }
                if (hfStatus.Value == "New") 
                {
                    if (clR.GetFields("claimdetailstatus") == "Authorized") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "values ('" + DateTime.Today + "', 0, " + clR.GetFields("totalamt") + ", " + clR.GetFields("totalamt") +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Approved") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "values ('" + DateTime.Today + "', 0, " + clR.GetFields("totalamt") + ", " + clR.GetFields("totalamt") +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                    if (clR.GetFields("claimdetailstatus") == "Paid") 
                    {
                        SQL = "insert into claimdetailchange " +
                              "(changedate, fromamt, toamt, diffamt, changeby, claimdetailid) " +
                              "values ('" + DateTime.Today + "', 0, " + clR.GetFields("totalamt") + ", " + clR.GetFields("totalamt") +
                              ", " + hfUserID.Value + ", " + hfClaimDetailID.Value + ")";
                        clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    }
                }
            }
        }
        private bool CheckMissingInfo()
        {
            if (CheckBypassMissingInfo())
            {
                return false;
            }
            string sMessage;
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string sServiceCenter = "0";
            long l;
            sMessage = "Check your funding for claim and you are missing the following fields.\r\n";
            l = sMessage.Length;
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                if (clR.GetFields("lossmile").Length == 0) 
                {
                    sMessage = sMessage + "Loss Mile\r\n";
                } 
                else 
                {
                    if (clR.GetFields("lossmile") == "0") 
                    {
                        sMessage = sMessage + "Loss Mile\r\n";
                    }
                }
                if (clR.GetFields("servicecenterid").Length == 0) 
                {
                    sMessage = sMessage + "Service Center\r\n";
                } 
                else 
                {
                    if (clR.GetFields("servicecenterid") == "0") 
                    {
                        sMessage = sMessage + "Service Center\r\n";
                    }
                }
                if (clR.GetFields("claimactivityid").Length == 0) 
                {
                    sMessage = sMessage + "Claim Activity\r\n";
                } 
                else 
                {
                    if (clR.GetFields("claimactivityid") == "0") 
                    {
                        sMessage = sMessage + "Claim Activity\r\n";
                    }
                }
                if (clR.GetFields("sccontactinfo").Length == 0) 
                {
                    sMessage = sMessage + "Service Center Contact Info\r\n";
                }
                sServiceCenter = clR.GetFields("servicecenterid");
            }
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnotetypeid = 1 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0) 
            {
                sMessage = sMessage + "Complaint\r\n";
            }
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnotetypeid = 2 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0) 
            {
                sMessage = sMessage + "Corrective Action\r\n";
            }
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnotetypeid = 3 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0) 
            {
                sMessage = sMessage + "Cause\r\n";
            }
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and not claimnotetypeid in (1,2,3,8) ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0) 
            {
                sMessage = sMessage + "Notes\r\n";
            }
            if (!CheckAN()) 
            {
                SQL = "select * from claimnote " +
                      "where claimid = " + hfClaimID.Value + " " +
                      "and claimnotetypeid = 8 ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() == 0) 
                {
                    sMessage = sMessage + "Customer Statement\r\n";
                }
            }

            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimdetailtype in ('Part', 'Labor') ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    if (clR.GetFields("ReqQty").Length == 0)
                    {
                        sMessage = sMessage + clR.GetFields("jobno") + " Request Qty\r\n";
                    }
                    else
                    {
                        if (clR.GetFields("ReqCost") == "0") 
                        {
                            sMessage = sMessage + clR.GetFields("jobno") + " Request Cost\r\n";
                        }
                    }
                    if (clR.GetFields("AuthQty").Length == 0)
                    {
                        sMessage = sMessage + clR.GetFields("jobno") + " Authorized Qty\r\n";
                    }
                    else
                    {
                        if (clR.GetFields("AuthQty") == "0")
                        {
                            sMessage = sMessage + clR.GetFields("jobno") + " Authorized Qty\r\n";
                        }
                    }
                    if (clR.GetFields("AuthCost") == "0")
                    {
                        sMessage = sMessage + clR.GetFields("jobno") + " Authorized Cost\r\n";
                    }
                    if (clR.GetFields("losscode").Length == 0)
                    {
                        sMessage = sMessage + clR.GetFields("jobno") + " Loss Code\r\n";
                    }
                }
            }
            if (sMessage.Length > 72) 
            {
                txtFieldChk.Text = sMessage;
                ShowFieldCheck();
                return true;
            } 
            else 
            {
                txtFieldChk.Text = "Check your funding for claim!";
            }
            return false;
        }
        private bool CheckBypassMissingInfo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and BypassMissingInfo <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private bool CheckAN()
        {
            //CheckAN = False
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim cl " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "inner join dealer d on d.dealerid = c.dealerid " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and dealerno like '2%' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private void CheckClose()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimdetailid from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                CloseClaim();
                if (cboClaimDetailStatus.SelectedValue == "Authorized")
                {
                    UpdateAuthorizedDate();
                }
                if (cboClaimDetailStatus.SelectedValue == "Approved")
                {
                    UpdateApproveDate();
                }
            }
        }
        private void UpdateApproveDate()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claim " +
                  "set approvedate = '" + DateTime.Today + "', " +
                  "approveby = " + hfUserID.Value + " " +
                  "where claimid = " + hfClaimID.Value + " ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void UpdateAuthorizedDate()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claim " +
                  "set authdate = '" + DateTime.Today + "', " +
                  "authby = " + hfUserID.Value + " " +
                  "where claimid = " + hfClaimID.Value + " ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void CloseClaim()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claim " +
                  "set closedate = '" + DateTime.Today + "', " +
                  "closeby = " + hfUserID.Value + " " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and closedate is null ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private bool CheckLimitApproved(double xAmt, long xClaimDetailID) 
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            double dLimit;
            SQL = "select claimapprove from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimapprove"));
            }
            else
            {
                dLimit = 0;
            }
            SQL = "select sum(authamt) as Amt from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and (claimdetailstatus = 'approved' " +
                  "or claimdetailstatus = 'Authorized') " +
                  "and claimdetailid <> " + xClaimDetailID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt")) + xAmt)
                    {
                        return true;
                    }
                }
                else
                {
                    if (dLimit > xAmt)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private bool CheckLimitAuthorized(double xAmt, long xClaimDetailID) 
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            double dLimit;
            SQL = "select claimauth from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimauth"));
            }
            else
            {
                dLimit = 0;
            }
            SQL = "select sum(authamt) as Amt from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and (claimdetailstatus = 'approved' " +
                  "or claimdetailstatus = 'Authorized') " +
                  "and claimdetailid <> " + xClaimDetailID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt")) + xAmt)
                    {
                        return true;
                    }
                }
                else
                {
                    if (dLimit > xAmt)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        protected void rgLossCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtLossCode.Text = rgLossCode.SelectedValue.ToString();
            GetLossCodeDesc();
            GetNextJobNo();
            pnlLossCode.Visible = false;
            pnlDetail.Visible = true;
            LockAuthText();
        }

        protected void btnAddClaimPayee_Click(object sender, EventArgs e)
        {
            pnlSeekPayee.Visible = false;
            pnlAddClaimPayee.Visible = true;
            txtServiceCenterName.Text = "";
            txtZip.Text = "";
            txtAddr1.Text = "";
            txtAddr2.Text = "";
            cboState.ClearSelection();
            txtPhone.Text = "";
        }

        protected void rgClaimPayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimPayeeID.Value = rgClaimPayee.SelectedValue.ToString();
            GetPayee();
            pnlDetail.Visible = true;
            pnlSeekPayee.Visible = false;
        }

        protected void btnSCCancel_Click(object sender, EventArgs e)
        {
            pnlSeekPayee.Visible = true;
            pnlAddClaimPayee.Visible = false;
        }

        protected void btnSCSave_Click(object sender, EventArgs e)
        {
            AddServiceCenter();
            AddClaimPayee();
            FillClaimPayee();
            pnlSeekPayee.Visible = true;
            pnlAddClaimPayee.Visible = false;
        }
        private void FillClaimPayee()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimpayeeid, payeeno, payeename, city, state from claimpayee ";
            rgClaimPayee.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void AddClaimPayee()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimpayee where claimpayeeid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clR.NewRow();
            clR.SetFields("payeeno", hfServiceCenterNo.Value);
            clR.SetFields("payeename", txtServiceCenterName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.AddRow();
            clR.SaveDB();
        }
        private void AddServiceCenter()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from servicecenter where servicecenterid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clR.NewRow();
            GetNextServiceCenterNo();
            clR.SetFields("servicecenterno", hfServiceCenterNo.Value);
            clR.SetFields("servicecentername", txtServiceCenterName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.AddRow();
            clR.SaveDB();
        }
        private void GetNextServiceCenterNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select max(servicecenterno) as mSCN from servicecenter where servicecenterno like 'RF%' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);

            clR.GetRow();
            hfServiceCenterNo.Value = "RF" + (long.Parse(clR.GetFields("mscn").Substring(clR.GetFields("mscn").Length - 6)) + 1).ToString("0000000");
        }
        private bool CheckInspect()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select inspect from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("inspect").ToLower() == "true")
                {
                    return true;
                }
            }
            return false;
        }
        private void FillTotal()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select 'Requested' as ClaimProcess, (select sum(reqamt) as Part from claimdetail where ClaimDetailType = 'Part' ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Part, " +
                        "(select sum(reqamt) as Labor from claimdetail where ClaimDetailType = 'Labor' ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + " and claimid = " + hfClaimID.Value + ") as Labor, " +
                        "(select sum(reqamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' or ClaimDetailType = 'Other') ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + " and claimid = " + hfClaimID.Value + ") as Other, " +
                        "(select sum(reqamt) as SubTotal from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + " and claimdetailtype <> 'Deduct') as SubTotal, " +
                        "0 as taxAmt, " +
                        "(select sum(reqamt) as 'Deduct' from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0)
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.Text + "' ";
            }
            SQL = SQL + "and claimdetailtype = 'Deductible') AS Deduct, " +
                        "(select sum(reqamt) as Total from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + ")  as Total, 1 as orderby " +
                        "union " +
                        "select 'Adjusted' as ClaimProcess, (select sum(authamt) as Part from claimdetail where ClaimDetailType = 'Part' " +
                        "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied' ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Part, " +
                        "(select sum(authamt) as Labor from claimdetail where ClaimDetailType = 'Labor' and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied' ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Labor, " +
                        "(select sum(authamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' or ClaimDetailType = 'Other') " +
                        "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied' ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Other, " +
                        "(select sum(authamt) as SubTotal from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied' " +
                        "and claimdetailtype <> 'Deduct') as SubTotal, " +
                        "(select sum(taxamt) as TaxAmt from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied') as TaxAmt, " +
                        "(select sum(authamt) as 'Deduct' from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied'" +
                        "and claimdetailtype = 'Deductible') AS Deduct, " +
                        "(select sum(authamt) + sum(taxamt) as Total from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied')  as Total, 2 as orderby  " +
                        "union " +
                        "select 'To Pay' as ClaimProcess, (select sum(authamt) as Part from claimdetail where ClaimDetailType = 'Part' " +
                        "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Part, " +
                        "(select sum(authamt) as Labor from claimdetail where ClaimDetailType = 'Labor' and (ClaimDetailStatus = 'Approved' " +
                        " or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Labor, " +
                        "(select sum(authamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' or ClaimDetailType = 'Other') ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') and claimid = " + hfClaimID.Value + ") as Other, " +
                        "(select sum(authamt) as SubTotal from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' " +
                        "or ClaimDetailStatus = 'Paid') and claimdetailtype <> 'Deduct') as SubTotal, " +
                        "(select sum(taxamt) as TaxAmt from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' " +
                        "or ClaimDetailStatus = 'Paid')) as TaxAmt, " +
                        "(select sum(authamt) as 'Deduct' from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') " +
                        "and claimdetailtype = 'Deductible') AS Deduct, (select sum(authamt) + sum(taxamt) as Total from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid'))  as Total, 3 as orderby " +
                        "union " +
                        "select 'Paid' as ClaimProcess, (select sum(authamt) as Part from claimdetail where ClaimDetailType = 'Part' " +
                        "and ClaimDetailStatus = 'Paid' ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Part, " +
                        "(select sum(authamt) as Labor from claimdetail where ClaimDetailType = 'Labor' and (ClaimDetailStatus = 'Paid') ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Labor, (select sum(authamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' " +
                        "or ClaimDetailType = 'Other') and (ClaimDetailStatus = 'Paid') ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Other, " +
                        "(select sum(authamt) as SubTotal from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.Text + "' ";
            }
            SQL = SQL + "and (ClaimDetailStatus = 'Paid') " +
                        "and claimdetailtype <> 'Deduct') as SubTotal, (select sum(taxamt) as TaxAmt from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and (ClaimDetailStatus = 'Paid')) as TaxAmt, " +
                        "(select sum(authamt) as 'Deduct' from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and (ClaimDetailStatus = 'Paid') " +
                        "and claimdetailtype = 'Deductible') AS Deduct, (select sum(authamt) + sum(taxamt) as Total from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt32(cboPayeeTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";
            }
            if (Convert.ToInt32(cboJobsTotal.SelectedValue) > 0) 
            {
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";
            }
            SQL = SQL + "and (ClaimDetailStatus = 'Paid'))  as Total, 4 as orderby " +
                        "order by orderby ";
            rgClaimTotal.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgClaimTotal.DataBind();
        }
        private void FillPayeeTotal()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "All";
            cboPayeeTotal.Items.Add(li);
            SQL = "select cp.claimpayeeid, payeeno, payeename from claimdetail cd " +
                  "inner join claimpayee cp on cp.claimpayeeid = cd.claimpayeeid " +
                  "where cd.claimid = " + hfClaimID.Value + " " +
                  "group by cp.claimpayeeid, payeeno, payeename ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    li = new ListItem();
                    li.Value = clR.GetFields("claimpayeeid");
                    li.Text = clR.GetFields("payeeno") + "-" + clR.GetFields("payeename");
                    cboPayeeTotal.Items.Add(li);
                }
            }
        }
        protected void cboPayeeTotal_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillTotal();
        }
        private void FillPaymentMethod()
        {
            cboPaymentMethod.Items.Clear();
            cboPaymentMethod.Items.Add("");
            cboPaymentMethod.Items.Add("ACH");
            cboPaymentMethod.Items.Add("Check");
            cboPaymentMethod.Items.Add("Credit Card");
            cboPaymentMethod.Items.Add("Wex");
        }
        protected void btnMakePayment_Click(object sender, EventArgs e)
        {
            pnlToBePaidDetail.Visible = true;
            FillToBePaidDetail();
        }
        private void FillToBePaidDetail()
        {
            GetPayeeInfo();
            txtPaidAmt.Text = txtPayAmt.Text;
            FillPaymentMethod();
            txtConfirmNo.Text = "";
            txtCheckNo.Text = "";
        }
        private void GetPayeeInfo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimpayee where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtClaimPayeeNo.Text = clR.GetFields("payeeno");
                txtClaimPayeeName.Text = clR.GetFields("payeename");
            }
        }
        protected void btnProcessACH_Click(object sender, EventArgs e)
        {
            ProcessPayment();
            pnlToBePaidDetail.Visible = false;
            trACH.Visible = false;
            rgJobs.Rebind();
        }
        private void ProcessPayment()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clCPL = new clsDBO.clsDBO();
            AddClaimPayment();
            SQL = "select * from claimdetail " +
                  "where claimdetailid = " + hfClaimDetailID.Value + " " +
                  "and claimpayeeid = " + hfClaimPayeeID.Value + " " +
                  "and claimdetailstatus = 'approved' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    SQL = "select * from claimpaymentlink " +
                          "where claimdetailid = " + clR.GetFields("claimdetailid") + " " +
                          "and claimpaymentid = " + hfClaimPaymentID.Value + " ";
                    clCPL.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clCPL.RowCount() == 0)
                    {
                        clCPL.NewRow();
                        clCPL.SetFields("claimdetailid", clR.GetFields("claimdetailid"));
                        clCPL.SetFields("claimpaymentid", hfClaimPaymentID.Value);
                        clCPL.AddRow();
                        clCPL.SaveDB();
                    }
                    SQL = "update claimdetail ";
                    if (cboPaymentMethod.Text != "Wex")
                    {
                        SQL = SQL + "set claimdetailstatus = 'Paid', " +
                                    "datepaid = '" + DateTime.Today + "', " +
                                    "modby = " + hfUserID.Value + ", " +
                                    "moddate = '" + DateTime.Today + "' ";
                    }
                    else
                    {
                        SQL = SQL + "set claimdetailstatus = 'Transmitted To Wex', " +
                                    "modby = " + hfUserID.Value + ", " +
                                    "moddate = '" + DateTime.Today + "' ";
                    }
                    SQL = SQL + "where claimdetailid = " + clR.GetFields("claimdetailid");
                    clCPL.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                }
            }
        }
        private void AddClaimPayment()
        {
            string SQL;
            clsDBO.clsDBO clCP = new clsDBO.clsDBO();
            SQL = "select * from claimpayment where claimpaymentid = 0 ";
            clCP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCP.RowCount() == 0) 
            {
                clCP.NewRow();
                clCP.SetFields("claimpayeeid", hfClaimPayeeID.Value);
                clCP.SetFields("paymentamt", txtPayAmt.Text);
                if (cboPaymentMethod.Text == "ACH") 
                {
                    clCP.SetFields("datepaid", DateTime.Today.ToString());
                    clCP.SetFields("achinfo", txtConfirmNo.Text);
                    clCP.SetFields("status", "Paid");
                }
                if (cboPaymentMethod.Text == "Check") 
                {
                    clCP.SetFields("datepaid", DateTime.Today.ToString());
                    clCP.SetFields("checkno", txtCheckNo.Text);
                    clCP.SetFields("status", "Paid");
                }
                if (cboPaymentMethod.Text == "Credit Card") 
                {
                    clCP.SetFields("datepaid", DateTime.Today.ToString());
                    clCP.SetFields("checkno", txtCCNo.Text);
                    clCP.SetFields("status", "Paid");
                }
                if (cboPaymentMethod.Text == "Wex") 
                {
                    clCP.SetFields("datetransmitted", DateTime.Today.ToString());
                    clCP.SetFields("status", "Transmitted To Wex");
                    clCP.SetFields("ccno", hfWexCCno.Value);
                    clCP.SetFields("wexcode", hfWexCode.Value);
                    clCP.SetFields("wexdeliverymethod", cboWexMethod.SelectedText);
                    clCP.SetFields("wexdeliveryaddress", txtWexAddress.Text);
                    clCP.SetFields("companyno", hfCompanyNo.Value);
                }
                clCP.AddRow();
                clCP.SaveDB();
            }

            SQL = "select max(claimpaymentid) as mCPI from claimpayment where claimpayeeid = " + hfClaimPayeeID.Value;
            clCP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCP.RowCount() > 0) 
            {
                clCP.GetRow();
                hfClaimPaymentID.Value = clCP.GetFields("mcpi");
            }
        }
        protected void btnProcessCC_Click(object sender, EventArgs e)
        {
            ProcessPayment();
            pnlToBePaidDetail.Visible = false;
            trCheck.Visible = false;
            rgJobs.Rebind();
        }

        protected void cboPaymentMethod_TextChanged(object sender, EventArgs e)
        {
            trACH.Visible = false;
            trWex.Visible = false;
            trCheck.Visible = false;
            trCC.Visible = false;
            if (cboPaymentMethod.Text == "ACH")
            {
                trACH.Visible = true;
            }
            if (cboPaymentMethod.Text == "Check") 
            {
                trCheck.Visible = true;
            }
            if (cboPaymentMethod.Text == "Credit Card") 
            {
                trCC.Visible = true;
            }
            if (cboPaymentMethod.Text == "Wex") 
            {
                trWex.Visible = true;
            }
        }

        protected void btnProcessWex_Click(object sender, EventArgs e)
        {
            ProcessWexDLL();
            pnlToBePaidDetail.Visible = false;
            rgJobs.Rebind();
            trWex.Visible = false;
        }

        protected void btnProcessCheck_Click(object sender, EventArgs e)
        {
            ProcessPayment();
            pnlToBePaidDetail.Visible = false;
            trCheck.Visible = false;
            rgJobs.Rebind();
        }
        private void ProcessWexDLL()
        {
            clsDBO.clsDBO clCWP = new clsDBO.clsDBO();
            clsDBO.clsDBO clR = new clsDBO.clsDBO();

            GetClaimNo();
            if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.ToLower().Contains("test")) 
            {
                return;
            }
            else
            {
                GetCompanyNo();
                string SQL;
                clsDBO.clsDBO clWA = new clsDBO.clsDBO();
                SQL = "select * from wexapi where companyno = '" + hfCompanyNo.Value + "' ";
                clWA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clWA.RowCount() > 0)
                {
                    clWA.GetRow();
                    GetClaimNo();
                    SQL = "select * from claimwexpayment where claimwexpaymentid = 0 ";
                    clCWP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clCWP.RowCount() == 0)
                    {
                        clCWP.NewRow();
                        clCWP.SetFields("claimno", hfClaimNo.Value);
                        clCWP.SetFields("claimid", hfClaimID.Value);
                        clCWP.SetFields("rono", hfRONo.Value);
                        clCWP.SetFields("orggroup", clWA.GetFields("OrgGroup"));
                        clCWP.SetFields("username", clWA.GetFields("UserName"));
                        clCWP.SetFields("password", clWA.GetFields("Password"));
                        clCWP.SetFields("currency", clWA.GetFields("currency"));
                        clCWP.SetFields("wexaddress", txtWexAddress.Text);
                        clCWP.SetFields("wexmethod", cboWexMethod.SelectedValue);
                        clCWP.SetFields("bankno", clWA.GetFields("bankno"));
                        clCWP.SetFields("companyno", hfCompanyNo.Value);
                        clCWP.SetFields("paymentamt", txtPayAmt.Text);
                        clCWP.SetFields("contractno", hfContractNo.Value);
                        clCWP.SetFields("customername", hfCustomerName.Value);
                        clCWP.SetFields("Claimpayeename", txtClaimPayeeName.Text);
                        clCWP.SetFields("payeecontact", hfPayeeContact.Value);
                        clCWP.SetFields("vin", hfVIN.Value);
                        clCWP.AddRow();
                        clCWP.SaveDB();
                        SQL = "select max(claimwexpaymentid) as MCWP from claimwexpayment where claimid = " + hfClaimID.Value;
                        clCWP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                        if (clCWP.RowCount() > 0)
                        {
                            clCWP.GetRow();
                            if (!ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.ToLower().Contains("test")) 
                            {
                                Process pr = new Process();
                                pr.StartInfo.FileName = @"C:\ProcessProgram\WexPayment\WexPayment.exe";
                                pr.StartInfo.Arguments = clCWP.GetFields("mcwp");
                                pr.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                                pr.Start();
                                pr.WaitForExit();
                                pr.Close();
                                SQL = "select * from claimwexpayment where claimwexpaymentid = " + clCWP.GetFields("mcwp");
                                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                                if (clR.RowCount() > 0)
                                {
                                    clR.GetRow();
                                    if (clR.GetFields("ReasonCode").ToLower() == "success")
                                    {
                                        hfWexCCno.Value = clR.GetFields("wexccno").Substring(0, 4);
                                        hfWexCode.Value = clR.GetFields("wexcode");
                                        ProcessPayment();
                                        pnlToBePaidDetail.Visible = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private void GetClaimNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimNo.Value = clR.GetFields("claimno");
                hfRONo.Value = clR.GetFields("RONumber");
                hfContractID.Value = clR.GetFields("contractid");
                OpenContract();
                hfPayeeContact.Value = clR.GetFields("sccontactinfo");
            }
        }
        private void GetCompanyNo()
        {
            string SQL;
            clsDBO.clsDBO clCL = new clsDBO.clsDBO();
            SQL = "select * from claim cl " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 1 || Convert.ToInt32(clCL.GetFields("clipid")) == 2) 
                {
                    hfCompanyNo.Value = "0000978";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 3 || Convert.ToInt32(clCL.GetFields("clipid")) == 4) 
                {
                    if (clCL.GetFields("contractno").Substring(0, 3) == "CHJ")
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "DRV")
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAC")
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAD")
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAN")
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RDI")
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "REP")
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSA")
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSD")
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSW")
                    {
                        hfCompanyNo.Value = "0000978";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "VEL")
                    {
                        hfCompanyNo.Value = "0000978";
                    } 
                    else 
                    {
                        hfCompanyNo.Value = "0000976";
                    }
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 5 || Convert.ToInt32(clCL.GetFields("clipid")) == 6) 
                {
                    hfCompanyNo.Value = "0000976";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 7 || Convert.ToInt32(clCL.GetFields("clipid")) == 8) 
                {
                    hfCompanyNo.Value = "0000976";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 9 || Convert.ToInt32(clCL.GetFields("clipid")) == 10) 
                {
                    hfCompanyNo.Value = "0000977";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 11) 
                {
                    hfCompanyNo.Value = "0000978";
                }
            }
        }
        private void OpenContract()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractNo.Value = clR.GetFields("contractno");
                hfCustomerName.Value = clR.GetFields("fname") + " " + clR.GetFields("lname");
                hfVIN.Value = clR.GetFields("vin");
            }
        }
        protected void txtReqQty_TextChanged(object sender, EventArgs e)
        {
            double dQty;
            double dCost;
            if (txtReqQty.Text.Length > 0)
            {
                dQty = Convert.ToDouble(txtReqQty.Text);
            }
            else
            {
                dQty = 0;
            }
            if (txtReqCost.Text.Length > 0)
            {
                dCost = Convert.ToDouble(txtReqCost.Text);
            }
            else
            {
                dCost = 0;
            }
            txtReqAmt.Text = (dQty * dCost).ToString();
            txtReqCost.Focus();
        }

        protected void txtReqCost_TextChanged(object sender, EventArgs e)
        {
            double dQty;
            double dCost;
            if (txtReqQty.Text.Length > 0)
            {
                dQty = Convert.ToDouble(txtReqQty.Text);
            }
            else
            {
                dQty = 0;
            }
            if (txtReqCost.Text.Length > 0)
            {
                dCost = Convert.ToDouble(txtReqCost.Text);
            }
            else
            {
                dCost = 0;
            }
            txtReqAmt.Text = (dQty * dCost).ToString();
            txtReqAmt.Focus();
        }

        protected void txtAuthQty_TextChanged(object sender, EventArgs e)
        {
            double dQty;
            double dCost;
            double dTax;
            double dTaxRate;
            if (txtAuthQty.Text.Length > 0) 
            {
                dQty = Convert.ToDouble(txtAuthQty.Text);
            } 
            else 
            {
                dQty = 0;
            }
            if (txtAuthCost.Text.Length > 0) 
            {
                dCost = Convert.ToDouble(txtAuthCost.Text);
            } 
            else 
            {
                dCost = 0;
            }
            if (txtTaxRate.Text.Length > 0) 
            {
                if (Convert.ToInt32(txtTaxRate.Text) < 1) 
                {
                    dTaxRate = Convert.ToDouble(txtTaxRate.Text);
                } 
                else 
                {
                    dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
                }
            }
            else 
            {
                dTaxRate = 0;
            }
            dTax = (dQty * dCost) * dTaxRate;
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtAuthCost.Focus();
        }

        protected void txtAuthCost_TextChanged(object sender, EventArgs e)
        {
            double dQty;
            double dCost;
            double dTax;
            double dTaxRate;
            if (txtAuthQty.Text.Length > 0) 
            {
                dQty = Convert.ToDouble(txtAuthQty.Text);
            } 
            else 
            {
                dQty = 0;
            }
            if (txtAuthCost.Text.Length > 0) 
            {
                dCost = Convert.ToDouble(txtAuthCost.Text);
            } 
            else 
            {
                dCost = 0;
            }
            if (txtTaxRate.Text.Length > 0) 
            {
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            } 
            else 
            {
                dTaxRate = 0;
            }
            dTax = (dQty * dCost) * dTaxRate;
            dTax = Math.Round(dTax, 2);
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtTaxRate.Focus();
        }

        protected void txtTaxRate_TextChanged(object sender, EventArgs e)
        {
            double dQty;
            double dCost;
            double dTax;
            double dTaxRate;
            if (txtAuthQty.Text.Length > 0) 
            {
                dQty = Convert.ToDouble(txtAuthQty.Text);
            } 
            else
            {
                dQty = 0;
            }
            if (txtAuthCost.Text.Length > 0)
            {
                dCost = Convert.ToDouble(txtAuthCost.Text);
            } 
            else 
            {
                dCost = 0;
            }
            if (txtTaxRate.Text.Length > 0)
            {
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            } 
            else 
            {
                dTaxRate = 0;
            }
            dTax = (dQty * dCost) * dTaxRate;
            dTax = Math.Round(dTax * 100) / 100;
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dCost * dQty) + dTax).ToString();
            txtTotalAmt.Focus();
        }

        protected void cboClaimDetailStatus_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (cboClaimDetailStatus.SelectedValue == "Approved") 
            {
                if (rdpDateApprove.SelectedDate == null) 
                {
                    rdpDateApprove.SelectedDate = DateTime.Today;
                    CalcAuth();
                }
                if (rdpAuthorizedDate.SelectedDate == null) 
                {
                    rdpAuthorizedDate.SelectedDate = DateTime.Today;
                }
            }
            if (cboClaimDetailStatus.SelectedValue == "Authorized") 
            {
                if (rdpDateApprove.SelectedDate != null) 
                {
                    rdpDateApprove.Clear();
                }
                if (rdpAuthorizedDate.SelectedDate == null) 
                {
                    rdpAuthorizedDate.SelectedDate = DateTime.Today;
                }
            }
            if (cboClaimDetailStatus.SelectedValue == "Cancelled") 
            {
                rdpDateApprove.Clear();
                rdpAuthorizedDate.Clear();
            }
            if (cboClaimDetailStatus.SelectedValue == "Denied") 
            {
                rdpDateApprove.Clear();
                rdpAuthorizedDate.Clear();
                if (txtJobNo.Text.Substring(0, 1) == "J") 
                {
                    hfTicket.Value = "true";
                }
            }
            if (cboClaimDetailStatus.SelectedValue == "Paid") 
            {
                rdpDatePaid.SelectedDate = DateTime.Today;
                txtPaidAmt.Text = txtTotalAmt.Text;
            }
            CheckClose();
        }
        private void PlaceTicket()
        {
            string SQL;
            clsDBO.clsDBO clT = new clsDBO.clsDBO();
            GetContractID();
            SQL = "select * from veritasclaimticket.dbo.ticket " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimdetailid = " + hfClaimDetailID.Value + " ";
            clT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clT.RowCount() == 0)
            {
                if (!CheckContract())
                {
                    return;
                }
                clT.NewRow();
                clT.SetFields("ticketno", CalcTicketNo());
                clT.SetFields("contractid", hfContractID.Value);
                clT.SetFields("claimid", hfClaimID.Value);
                clT.SetFields("claimdetailid", hfClaimDetailID.Value);
                clT.SetFields("credate", DateTime.Today.ToString());
                clT.SetFields("creby", CalcUserName());
                clT.SetFields("contractno", GetContractNo());
                clT.SetFields("claimno", GetClaimNo2());
                clT.AddRow();
                clT.SaveDB();
            } 
            else 
            { 
                clT.GetRow();
                clT.SetFields("statusid", 1.ToString());
                clT.SaveDB();
            }
            //SQL = "select * from veritasclaimticket.dbo.ticket "
            //SQL = SQL + "where claimid = " & hfClaimID.Value & " "
            //clT.OpenDB(SQL, AppSettings("connstring"))
            //If clT.RowCount = 1 Then
            //    clT.GetRow()
            //    Dim clSTE As New VeritasGlobalTools.clsSendTASAEMail
            //    clSTE.ClaimDetailID = hfClaimDetailID.Value
            //    clSTE.ClaimID = hfClaimID.Value
            //    clSTE.UserID = hfUserID.Value
            //    clSTE.TicketNo = clT.Fields("ticketno")
            //    clSTE.SendEMail()
            //End If
        }
        private string GetClaimNo2()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("claimno");
            }
            return "";
        }
        private string GetContractNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("contractno");
            }
            return "";
        }
        private string CalcUserName()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("username");
            }
            return "";
        }
        private void GetContractID()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractID.Value = clR.GetFields("contractid");
            }
        }
        private string CalcTicketNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            long lTicketNo;
            SQL = "select * from veritasclaimticket.dbo.ticket where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("ticketno");
            }
            SQL = "select * from veritasclaimticket.dbo.ticket ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                return "T000000";
            }
            SQL = "select max(ticketno) as TicketNo from veritasclaimticket.dbo.ticket ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lTicketNo = long.Parse(clR.GetFields("ticketno").Replace("T", ""));
                lTicketNo = lTicketNo + 1;
                return "T" + lTicketNo.ToString("000000");
            }
            return "";
        }
        private bool CheckContract()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from contract where contractid in (select contractid from contract " +
                  "where dealerid in (select dealerid from VeritasClaimTicket.dbo.Dealer)) " +
                  "and contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private void CalcAuth()
        {
            double dQty;
            double dCost;
            double dTax;
            double dTaxRate;
            if (txtAuthQty.Text.Length > 0)
            {
                dQty = Convert.ToDouble(txtAuthQty.Text);
            } 
            else
            {
                dQty = 0;
            }
            if (txtAuthCost.Text.Length > 0)
            {
                dCost = Convert.ToDouble(txtAuthCost.Text);
            }
            else
            {
                dCost = 0;
            }
            if (txtTaxRate.Text.Length > 0)
            {
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            }
            else
            {
                dTaxRate = 0;
            }
            dTax = (dQty * dCost) * dTaxRate;
            dTax = Math.Round(dTax * 100) / 100;
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dCost * dQty) + dTax).ToString();
        }
        protected void cboJobsTotal_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillTotal();
        }
        protected void cboClaimDetailType_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (cboClaimDetailType.SelectedItem.Text == "Deductible")
            {
                txtJobNo.Text = "A01";
            }
            if (cboClaimDetailType.SelectedItem.Text == "Other")
            {
                txtJobNo.Text = "A04";
            }
            if (cboClaimDetailType.SelectedItem.Text == "Inspect")
            {
                txtJobNo.Text = "A02";
            }
        }

        protected void btnAuthIt_Click(object sender, EventArgs e)
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clCD = new clsDBO.clsDBO();
            string SQL;
            double dQty;
            double dCost;
            double dTax;
            double dTaxRate;
            bool bCopyReq;
            if (cboJobNo.SelectedValue == "0")
            {
                return;
            }
            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and jobno = '" + cboJobNo.SelectedValue + "' " +
                  "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    SQL = "select * from claimdetail where claimdetailid = " + clR.GetFields("claimdetailid");
                    clCD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clCD.RowCount() > 0)
                    {
                        clCD.GetRow();
                        hfClaimDetailID.Value = clCD.GetFields("claimdetailid");
                        hfStatus.Value = "Requested";
                        hfOldTotalAmt.Value = "0";
                        bCopyReq = false;
                        if (clCD.GetFields("authqty").Length > 0) 
                        {
                            if (Convert.ToInt32(clCD.GetFields("authqty")) == 0) 
                            {
                                clCD.SetFields("authqty", clCD.GetFields("reqqty"));
                                bCopyReq = true;
                            }
                        } 
                        else 
                        {
                            clCD.SetFields("authqty", clCD.GetFields("reqqty"));
                            bCopyReq = true;
                        }
                        if (clCD.GetFields("authcost").Length > 0) 
                        {
                            if (Convert.ToInt32(clCD.GetFields("authcost")) == 0) 
                            {
                                clCD.SetFields("authcost", clCD.GetFields("reqcost"));
                                bCopyReq = true;
                            }
                        } 
                        else 
                        {
                            clCD.SetFields("authcost", clCD.GetFields("reqcost"));
                            bCopyReq = true;
                        }

                        if (clCD.GetFields("authqty").Length > 0) 
                        {
                            dQty = Convert.ToDouble(clCD.GetFields("authqty"));
                        } 
                        else 
                        {
                            dQty = 0;
                        }
                        if (clCD.GetFields("authcost").Length > 0) 
                        {
                            dCost = Convert.ToDouble(clCD.GetFields("authcost"));
                        } 
                        else 
                        {
                            dCost = 0;
                        }
                        if (clCD.GetFields("taxper").Length > 0) 
                        {
                            dTaxRate = Convert.ToDouble(clCD.GetFields("taxper")) / 100;
                            if (clCD.GetFields("taxper") == "0") 
                            {
                                dTaxRate = GetTaxRate(clCD.GetFields("claimdetailtype"));
                                clCD.SetFields("taxper", dTaxRate.ToString());
                            }
                        } 
                        else 
                        {
                            dTaxRate = GetTaxRate(clCD.GetFields("claimdetailtype"));
                            clCD.SetFields("taxper", dTaxRate.ToString());
                        }
                        dTax = (dQty * dCost) * dTaxRate;
                        dTax = Math.Round(dTax * 100) / 100;
                        clCD.SetFields("taxamt", dTax.ToString());
                        clCD.SetFields("authamt", (dQty * dCost).ToString());
                        clCD.SetFields("totalamt", ((dCost * dQty) + dTax).ToString());
                        if (!CheckLimitAuthorized(Convert.ToDouble(clCD.GetFields("totalamt")), long.Parse(clCD.GetFields("claimdetailid")))) 
                        {
                            lblAuthorizedError.Text = "Send to Manager for Authorization";
                            return;
                        }
                        clCD.SetFields("claimdetailstatus", "Authorized");
                        clCD.SetFields("authby", hfUserID.Value);
                        clCD.SetFields("moddate", DateTime.Today.ToString());
                        clCD.SetFields("modby", hfUserID.Value);
                        clCD.SetFields("dateauth", DateTime.Today.ToString());
                        clCD.SaveDB();
                        AddChange();
                    }
                }
            }
            SQL = "select claimdetailid from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0) 
            {
                CloseClaim();
                UpdateAuthorizedDate();
            }
            rgJobs.Rebind();
        }

        protected void btnApproveIt_Click(object sender, EventArgs e)
        {

        }
        private double GetTaxRate(string xClaimDetailType)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (xClaimDetailType == "Deductible") 
            {
                return 0;
            }
            if (xClaimDetailType == "Inspect") 
            {
                return 0;
            }
            if (xClaimDetailType == "Other") 
            {
                return 0;
            }
            if (xClaimDetailType == "") 
            {
                return 0;
            }
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                if (xClaimDetailType == "Part") 
                {
                    if (clR.GetFields("parttax").Length > 0) 
                    {
                        return Convert.ToDouble(clR.GetFields("parttax"));
                    }
                }
                if (xClaimDetailType == "Labor") 
                {
                    if (clR.GetFields("labortax").Length > 0) 
                    {
                        return Convert.ToDouble(clR.GetFields("parttax"));
                    }
                }
            }
            return 0;
        }
        protected void btnDenyIt_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clCD = new clsDBO.clsDBO();
            if (cboJobNo.SelectedValue == "0") 
            {
                return;
            }
            if (hfAllowAZDenied.Value == "False") 
            {
                if (Functions.GetServiceCenterState(long.Parse(hfClaimID.Value)) == "AZ") 
                {
                    lblAuthorizedError.Text = "You can not deny an AZ claim. Please see manager.";
                    lblAuthorizedError.Visible = true;
                    return;
                }
            }
            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and jobno = '" + cboJobNo.SelectedValue + "' " +
                  "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    SQL = "select * from claimdetail where claimdetailid = " + clR.GetFields("claimdetailid");
                    clCD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clCD.RowCount() > 0) 
                    {
                        clCD.GetRow();
                        clCD.SetFields("claimdetailstatus", "Denied");
                        clCD.SetFields("modby", hfUserID.Value);
                        clCD.SetFields("moddate", DateTime.Today.ToString());
                        clCD.SaveDB();
                    }
                }
            }
            SQL = "select claimdetailid from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0) 
            {
                CloseClaim();
            }
            rgJobs.Rebind();
        }
        private void ShowSlushNote()
        {
            hfSlushNote.Value = "Visible";
            string script = "function f(){$find(\"" + rwSlushNote.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        private void HideSlushNote()
        {
            hfSlushNote.Value = "";
            string script = "function f(){$find(\"" + rwSlushNote.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        private void ShowFieldCheck() 
        {
            hfFieldCheck.Value = "Visible";
            string script = "function f(){$find(\"" + rwFieldCheck.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency2", script, true);
        }

        private void HideFieldCheck()
        {
            hfFieldCheck.Value = "";
            string script = "function f(){$find(\"" + rwFieldCheck.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUpdateSlush_Click(object sender, EventArgs e)
        {
            if (cboSlushJobNo.SelectedValue == "0")
            {
                return;
            }
            if (cboSlushRateType.SelectedValue == "0")
            {
                return;
            }
            hfSlushNote.Value = "Visible";
            ShowSlushNote();
        }
        private void UpdateSlushClaimDetail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (cboSlushJobNo.SelectedValue == "0")
            {
                return;
            }
            if (cboSlushJobNo.SelectedValue == "All")
            {
                SQL = "update claimdetail " +
                      "set ratetypeid = " + cboSlushRateType.SelectedValue + ", " +
                      "moddate = '" + DateTime.Today + "', " +
                      "modby = " + hfUserID.Value + ", ";
                if (Convert.ToInt32(cboSlushRateType.SelectedValue) > 1)
                {
                    SQL = SQL + "slushbyid = " + hfUserID.Value + ", " +
                                "slushdate = '" + DateTime.Today + "' ";
                }
                else
                {
                    SQL = SQL + "slushbyid = null, slushdate = null ";
                }
                SQL = SQL + "where claimid = " + hfClaimID.Value;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
            SQL = "update claimdetail " +
                  "set ratetypeid = " + cboSlushRateType.SelectedValue + ", " +
                  "moddate = '" + DateTime.Today + "', " +
                  "modby = " + hfUserID.Value + ", ";
            if (Convert.ToInt32(cboSlushRateType.SelectedValue) > 1)
            {
                SQL = SQL + "slushbyid = " + hfUserID.Value + ", " +
                            "slushdate = '" + DateTime.Today + "' ";
            }
            else
            {
                SQL = SQL + "slushbyid = null, slushdate = null ";
            }
            SQL = SQL + "where claimid = " + hfClaimID.Value + " " +
                        "and jobno = '" + cboJobNo.SelectedValue + "' ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        protected void btnCloseSlushNote_Click(object sender, EventArgs e)
        {
            hfSlushNote.Value = "";
            HideSlushNote();
        }

        protected void btnSaveSlushNote_Click(object sender, EventArgs e)
        {
            lblAuthorizedError.Text = "";
            if (txtSlushNote.Text.Trim().Length < 25)
            {
                lblAuthorizedError.Text = "You need to put in a proper Note. Nothing was saved.";
                HideSlushNote();
                return;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote where claimnoteid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimnotetypeid", "5");
                clR.SetFields("note", txtSlushNote.Text);
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.SetFields("NoteText", txtSlushNote.Text);
                clR.AddRow();
                clR.SaveDB();
            }
            UpdateSlushClaimDetail();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            HideFieldCheck();
        }
    }
}
