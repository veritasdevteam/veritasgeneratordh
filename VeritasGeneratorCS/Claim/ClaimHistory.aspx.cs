﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsHistory.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsHistory2.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsClaimOpenHistory.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            hfClaimID.Value = Request.QueryString["claimid"];
            if (!IsPostBack)
            {
                GetContractNo();
            }
        }
        private void GetContractNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractID.Value = clR.GetFields("contractid");
                GetTotClaimsPaid();
            }
        }
        private void GetTotClaimsPaid()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select sum(paidamt) as Amt from claimdetail " +
                  "where claimid in (select claimid from claim where contractid = " + hfContractID.Value + ") ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    txtTotalClaimsPaid.Text = Convert.ToDouble(clR.GetFields("amt")).ToString("#,##0.00");
                }
                else
                {
                    txtTotalClaimsPaid.Text = "0.00";
                }
            }
            else
            {
                txtTotalClaimsPaid.Text = "0.00";
            }
        }

        protected void rgHistory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void rgHistory_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //'rgHistory.MasterTableView.Items(0).Expanded = True
                //'rgHistory.MasterTableView.Items(0).ChildItem.NestedTableViews(0).Items(0).Expanded = True
            }
        }
    }
}