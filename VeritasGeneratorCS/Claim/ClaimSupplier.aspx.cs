﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimSupplier : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            dsClaimDetail.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                GetServerInfo();
            }
            //'pnlAfterMarket.Visible = False
            //'pnlOEM.Visible = False
            FillPurchase();
            //'CalcTotal()
        }
        private void FillPurchase()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetailinvoice where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                return;
            }
            else
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    if (Convert.ToBoolean(clR.GetFields("oem")))
                    {
                        pnlOEM.Visible = true;
                        FillOEM();
                    }
                    else
                    {
                        pnlAfterMarket.Visible = true;
                        FillAfterMarket();
                    }
                }
            }
        }
        private void FillOEM()
        {

        }
        private void FillAfterMarket()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from claimdetailinvoice " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and oem = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("orderid").Length > 0)
                {
                    rgAMToBePurchase.Columns[9].Visible = false;
                    btnAMCompletePurchase.Visible = false;
                }
                txtAMOrderID.Text = clR.GetFields("orderid");
                txtAMOrderBy.Text = Functions.GetUserInfo(long.Parse(clR.GetFields("orderby")));
                txtAMOrderDate.Text = DateTime.Parse(clR.GetFields("orderdate")).ToString("M/d/yyyy");
                SQL = "select ClaimDetailPurchaseID, ClaimDetailID,OrderID,Companyname,PartNo,PartDesc,YourCost,TotalOrderCost from claimdetailpurchase cdp " +
                      "inner join claimcompany cc on cdp.claimcompanyid = cc.claimcompanyid " +
                      "where cdp.claimdetailinvoiceid = " + clR.GetFields("claimdetailinvoiceid");
                rgAMToBePurchase.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                UnlockButtons();
            }
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                if (Convert.ToBoolean(clSI.GetFields("readonly")))
                {
                    rgClaimDetailQuote.Enabled = false;
                }
            }
        }

        protected void rgClaimDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select cdq.ClaimDetailQuoteID, cdq.ClaimDetailID, cc.CompanyName, cdq.PartNo, cdq.ListPrice, cdq.YourCost, cdq.oem " +
                  "from ClaimDetailQuote cdq inner join ClaimCompany cc on cc.ClaimCompanyID = cdq.ClaimCompanyID " +
                  "and cdq.claimdetailid = " + rgClaimDetail.SelectedValue;
            rgClaimDetailQuote.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgClaimDetailQuote.Rebind();
        }

        protected void rgClaimDetailQuote_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            GridDataItem da;
            if (e.CommandName == "Purchase")
            {
                da = (GridDataItem)e.Item;
                hfClaimDetailQuoteID.Value = da.GetDataKeyValue("ClaimDetailQuoteID").ToString();
                AddPurchase();
                FillPurchase();
                CalcAMTotal();
            }
        }
        private void CalcAMTotal()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            txtAMShippingAmt.Text = "0.00";
            txtAMSubTotal.Text = "0.00";
            SQL = "select sum(yourcost) as amt, sum(shipping) as ship, oem from claimdetailpurchase " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "group by oem";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    if (Convert.ToBoolean(clR.GetFields("oem")) == false)
                    {
                        if (clR.GetFields("amt").Length > 0)
                        {
                            txtAMSubTotal.Text = Convert.ToDouble(clR.GetFields("amt")).ToString("#0.00");
                        }
                        if (clR.GetFields("ship").Length > 0)
                        {
                            txtAMShippingAmt.Text = Convert.ToDouble(clR.GetFields("ship")).ToString("#0.00");
                        }
                    }
                    else
                    {
                        if (clR.GetFields("amt").Length > 0)
                        {
                            txtOEMSubtotal.Text = Convert.ToDouble(clR.GetFields("amt")).ToString("#0.00");
                        }
                        if (clR.GetFields("ship").Length > 0)
                        {
                            txtOEMShippingAmt.Text = Convert.ToDouble(clR.GetFields("ship")).ToString("#0.00");
                        }
                    }
                }
            }
            SQL = "select * from claimdetailpurchase " +
                  "where claimcompanyid = 1 " +
                  "and claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    if (Convert.ToBoolean(clR.GetFields("oem")) == true)
                    {
                        txtOEMShippingAmt.Text = (Convert.ToDouble(txtOEMShippingAmt.Text) + 42.5).ToString("#0.00");
                        txtOEMTotal.Text = (Convert.ToDouble(txtOEMSubtotal.Text) + Convert.ToDouble(txtOEMShippingAmt.Text)).ToString("#0.00");
                    }
                    else
                    {
                        txtAMShippingAmt.Text = (Convert.ToDouble(txtAMShippingAmt.Text) + 42.5).ToString("#0.00");
                        txtAMTotal.Text = (Convert.ToDouble(txtAMSubTotal.Text) + Convert.ToDouble(txtAMShippingAmt.Text)).ToString("#0.00");
                    }
                }
            }
        }
        private void AddPurchase()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clCDP = new clsDBO.clsDBO();
            SQL = "select * from claimdetailquote where claimdetailquoteid = " + hfClaimDetailQuoteID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                SQL = "select * from claimdetailpurchase where claimdetailquoteid = " + clR.GetFields("claimdetailquoteid");
                clCDP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clCDP.RowCount() == 0)
                {
                    clCDP.NewRow();
                    clCDP.SetFields("claimid", hfClaimID.Value);
                    clCDP.SetFields("claimdetailid", clR.GetFields("claimdetailid"));
                    clCDP.SetFields("claimdetailquoteid", hfClaimDetailQuoteID.Value);
                    clCDP.SetFields("claimcompanyid", clR.GetFields("claimcompanyid"));
                    clCDP.SetFields("partno", clR.GetFields("partno"));
                    clCDP.SetFields("partdesc", clR.GetFields("partdesc"));
                    clCDP.SetFields("qty", clR.GetFields("qty"));
                    clCDP.SetFields("listprice", clR.GetFields("listprice"));
                    clCDP.SetFields("yourcost", clR.GetFields("yourcost"));
                    clCDP.SetFields("OEM", clR.GetFields("oem"));
                    clCDP.SetFields("partcost", clR.GetFields("partcost"));
                    clCDP.SetFields("shipping", clR.GetFields("shipping"));
                    clCDP.SetFields("orderby", hfUserID.Value);
                    clCDP.SetFields("orderdate", DateTime.Today.ToString());
                    clCDP.AddRow();
                    clCDP.SaveDB();
                }
            }
        }
        protected void btnAMCompletePurchase_Click(object sender, EventArgs e)
        {
            ProcessPurchases();
        }
        private void ProcessPurchases()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetailpurchase cdp " +
                  "where cdp.claimid = " + hfClaimID.Value + " " +
                  "and orderid is null ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    ProcessStartInfo p = new ProcessStartInfo();
                    p.FileName = @"C:\ProcessProgram\CKPartPurchase\CKPartPurchase.exe";
                    p.Arguments = " " + clR.GetFields("claimdetailid") + " " + hfUserID.Value;
                    p.WindowStyle = ProcessWindowStyle.Hidden;
                    Process pr = new Process();
                    pr.StartInfo = p;
                    pr.Start();
                    pr.WaitForExit();
                }
            }
            //CalcTotal();
        }
        protected void rgAMToBePurchase_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            GridDataItem da;
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (e.CommandName == "Delete")
            {
                da = (GridDataItem)e.Item;
                SQL = "delete claimdetailpurchase where claimdetailpurchaseid = " + da.GetDataKeyValue("ClaimDetailPurchaseID");
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                FillAfterMarket();
            }
        }

        protected void rgOEMPurchase_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            GridDataItem da;
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (e.CommandName == "Delete")
            {
                da = (GridDataItem)e.Item;
                SQL = "delete claimdetailpurchase where claimdetailpurchaseid = " + da.GetDataKeyValue("ClaimDetailPurchaseID");
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                FillOEM();
            }
        }

        protected void btnOEMCompletePurchase_Click(object sender, EventArgs e)
        {
            ProcessPurchases();
        }
    }
}