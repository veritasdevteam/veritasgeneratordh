﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Drawing;
using System.IO;

namespace VeritasGeneratorCS.Claim
{
    public partial class Claim : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsActivity.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsServiceCenter.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsClaimStatus.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsContract.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsStates.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsLossCode.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsAssignedTo.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                btnChangeAssignedTo.Visible = false;
                GetServerInfo();
                tsClaim.Tabs[6].Visible = false;
                //tsClaim.Tabs[5].Enabled = false;
                txtAssignedTo.Enabled = false;
                pvCustomer.Selected = true;
                //pvDealer.Selected = true;
                hfClaimID.Value = Request.QueryString["claimid"];
                pvNotes.ContentUrl = "~/claim/ClaimNotes.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pv3C.ContentUrl = "~/claim/Claim3C.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                //pvDealer.ContentUrl = "~/claim/ClaimDealer.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvDocuments.ContentUrl = "~/claim/ClaimDocument.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvInspection.ContentUrl = "~/claim/ClaimInspection.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvParts.ContentUrl = "~/claim/ClaimPart.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvJob.ContentUrl = "~/claim/ClaimJobs.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvPayment.ContentUrl = "~/claim/ClaimPayment.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvAlert.ContentUrl = "~/claim/ClaimAlert.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvHistory.ContentUrl = "~/claim/ClaimHistory.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvHCC.ContentUrl = "~/claim/ClaimHCC.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvCK.ContentUrl = "~/claim/ClaimSupplier.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvContractNote.ContentUrl = "~/claim/ContractNote.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value;
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/Default.aspx");
                }
                if (hfClaimID.Value == "")
                {
                    hfClaimID.Value = "0";
                }
                if (hfClaimID.Value == "0")
                {
                    ClearScreen();
                }
                else
                {
                    FillScreen();
                }
                pnlSeekSC.Visible = false;
                pnlSeekContract.Visible = false;
                pnlContractConfirm.Visible = false;
                pnlSeekLossCode.Visible = false;
                pnlLossCode.Visible = false;
                pnlAddSC.Visible = false;
                pnlAssignedToChange.Visible = false;
                tbLock.Visible = false;
                if (!CheckLock())
                {
                    hfClaimLocked.Value = false.ToString();
                    LockClaim();
                }
                else
                {
                    ShowLockedClaim();
                }
                CheckNewNote();
                CheckAllNote();
                if (hfUserID.Value == "1")
                {
                    btnLossCode.Visible = true;
                }
                else
                {
                    btnLossCode.Visible = false;
                }
                ReadOnlyButtons();
                GetHasSlush();
                InsertAccess();
                btnRFClaimSubmit.Enabled = false;
                CheckRFClaimSubmit();
                UpdateWebView();

            }
            if (hfDenied.Value == "Visible")
            {
                ShowDenied();
            }
            else
            {
                HideDenied();
            }
            if (hfExpireContract.Value == "Visible")
            {
                ShowExpire();
            }
            else
            {
                HideExpire();
            }

            if (hfOpen.Value == "Visible")
            {
                ShowOpen();
            }
            else
            {
                HideOpen();
            }
            if (hfAlert.Value == "Visible")
            {
                ShowAlert();
            }
            else
            {
                HideAlert();
            }
            if (hfTW.Value == "Visible")
            {
                ShowTW();
            } else {
                HideTW();
            }
            if (hfAlert.Value.Length == 0 && hfTW.Value.Length > 0)
            {
                hfTW.Value = "";
            }
            if (hfAlert.Value.Length > 0 && hfTW.Value.Length == 0)
            {
                hfAlert.Value = "";
            }
            if (hfAlert.Value.Length > 0 && hfTW.Value.Length > 0)
            {
                hfAlert.Value = "";
            }
            if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
            }
        }
        private void UpdateWebView()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claim " +
                  "set webview = 1 " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and webclaim <> 0 " +
                  "and webview = 0 ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void CheckRFClaimSubmit()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and teamlead <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                SQL = "select * from veritasclaims.dbo.claim " +
                      "where not sendclaim is null " +
                      "and processclaimdate is null ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    btnRFClaimSubmit.Enabled = true;
                }
            }
        }
        private void InsertAccess()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "insert into claimaccess (claimid, userid, accessdate) " +
                  "values (" + hfClaimID.Value + ", " + hfUserID.Value + ", " + 
                  "'" + DateTime.Now + "') ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void GetHasSlush()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and ratetypeid > 1 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                chkHasSlush.Checked = true;
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("AllowAZDenied")))
                {
                    hfAllowAZDenied.Value = "true";
                }
                else
                {
                    hfAllowAZDenied.Value = "false";
                }
                if (clR.GetFields("readonly").ToLower() == "true") 
                {
                    hfReadOnly.Visible = true;
                    btnAddSC.Enabled = false;
                    btnChangeAssignedTo.Enabled = false;
                    btnSeekContract.Enabled = false;
                    btnSeekLossCode.Enabled = false;
                    btnServiceCenters.Enabled = false;
                    btnCalcLabor.Enabled = false;
                    btnCalcPart.Enabled = false;
                }
                if (Convert.ToBoolean(clR.GetFields("AllowClaimAudit"))) 
                {
                    btnClaimAudit.Enabled = true;
                }
                else 
                { 
                    btnClaimAudit.Enabled = false;
                }
                if (clR.GetFields("agentid").Length > 0)
                {
                    if (clR.GetFields("agentid") != "0")
                    {
                        tsClaim.Tabs[5].Visible = false;
                        tsClaim.Tabs[7].Visible = false;
                    }
                }
                if (Convert.ToBoolean(clR.GetFields("AutoNationOnly"))) 
                {
                    tsClaim.Tabs[5].Visible = false;
                    tsClaim.Tabs[7].Visible = false;
                }
                if (Convert.ToBoolean(clR.GetFields("veroonly"))) 
                {
                    tsClaim.Tabs[5].Visible = false;
                    tsClaim.Tabs[7].Visible = false;
                }
            }
        }
        private void CheckNewNote()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clA = new clsDBO.clsDBO();

            SQL = "Select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAlertPopup.Text = "";
                if (Convert.ToBoolean(clR.GetFields("addnew")))
                {
                    if (Convert.ToInt32(clR.GetFields("contractid")) > 0)
                    {
                        SQL = "Select * from dealernote dn " +
                              "inner join contract c On c.dealerid = dn.dealerid " +
                              "where contractid = " + clR.GetFields("contractid") + " " +
                              "And dn.contractnewentry <> 0 ";
                        clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                        if (clA.RowCount() > 0)
                        {
                            tsClaim.Tabs[6].Visible = true;
                            tsClaim.Tabs[0].Selected = true;
                            pvCustomer.Selected = true;
                            for (int cnt = 0; cnt <= clA.RowCount() - 1; cnt++)
                            {
                                clA.GetRowNo(cnt);
                                //pvDealer.Selected = true;
                                txtAlertPopup.Text = txtAlertPopup.Text + clA.GetFields("note") + "\r\n" + "\r\n";
                            }
                        }
                    }
                }
            }

            SQL = "Select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("addnew")))
                {
                    if (Convert.ToInt32(clR.GetFields("contractid")) > 0)
                    {
                        SQL = "Select * from agentsnote an " +
                              "inner join Dealer d On d.agentsid = an.agentid " +
                              "inner join contract c On c.dealerid = d.dealerid " +
                              "where contractid = " + clR.GetFields("contractid") + " " +
                              "And an.claimnewentry <> 0 ";
                        clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                        if (clA.RowCount() > 0)
                        {
                            tsClaim.Tabs[6].Visible = true;
                            tsClaim.Tabs[0].Selected = true;
                            pvCustomer.Selected = true;
                            for (int xnt = 0; xnt <= clA.RowCount() - 1; xnt++)
                            {
                                clA.GetRowNo(xnt);
                                //pvDealer.Selected = true;
                                txtAlertPopup.Text = txtAlertPopup.Text + clA.GetFields("note") + "\r\n" + "\r\n";
                            }
                        }
                    }
                }
            }

            SQL = "Select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("addnew")))
                {
                    if (Convert.ToInt32(clR.GetFields("contractid")) > 0)
                    {
                        SQL = "Select * from subagentnote an " +
                              "inner join Dealer d On d.subagentid = an.subagentid " +
                              "inner join contract c On c.dealerid = d.dealerid " +
                              "where contractid = " + clR.GetFields("contractid") + " " +
                              "And an.claimnewentry <> 0 ";
                        clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                        if (clA.RowCount() > 0) 
                        {
                            tsClaim.Tabs[6].Visible = true;
                            tsClaim.Tabs[0].Selected = true;
                            pvCustomer.Selected = true;
                            for (int ynt = 0; ynt <= clA.RowCount() - 1; ynt++)
                            {
                                clA.GetRowNo(ynt);
                                //pvDealer.Selected = true;
                                txtAlertPopup.Text = txtAlertPopup.Text + clA.GetFields("note") + "\r\n" + "\r\n";
                            }
                        }
                    }
                }
            }

            if (txtAlertPopup.Text.Length > 0)
            {
                hfAlert.Value = "Visible";
            }
        }
        private void CheckAllNote()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            txtAlertPopup.Text = "";
            SQL = "Select * from dealernote dn " +
                  "inner join contract c On c.dealerid = dn.dealerid " +
                  "inner join claim cl On c.contractid = cl.contractid " +
                  "where claimid = " + hfClaimID.Value + " " + 
                  "And dn.claimalways <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                tsClaim.Tabs[6].Visible = true;
                tsClaim.Tabs[0].Selected = true;
                pvCustomer.Selected = true;
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    txtAlertPopup.Text = txtAlertPopup.Text + clR.GetFields("note") + "\r\n" + "\r\n";
                }
                //pvDealer.Selected = true;
            }

            SQL = "Select * from agentsnote an " +
                  "inner join Dealer d On d.agentsid = an.agentid " +
                  "inner join contract c On d.dealerid = c.dealerid " +
                  "inner join claim cl On c.contractid = cl.contractid " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "And an.claimalways <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                tsClaim.Tabs[6].Visible = true;
                tsClaim.Tabs[0].Selected = true;
                pvCustomer.Selected = true;
                for (int xnt = 0; xnt <= clR.RowCount() - 1; xnt++)
                {
                    clR.GetRowNo(xnt);
                    txtAlertPopup.Text = txtAlertPopup.Text + clR.GetFields("note") + "\r\n" + "\r\n";
                }
                //pvDealer.Selected = true;
            }
            if (txtAlertPopup.Text.Length > 0)
            {
                hfAlert.Value = "Visible";
            }

            SQL = "Select * from subagentnote an " +
                  "inner join Dealer d On d.subagentid = an.subagentid " +
                  "inner join contract c On d.dealerid = c.dealerid " +
                  "inner join claim cl On c.contractid = cl.contractid " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "And an.claimalways <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                tsClaim.Tabs[6].Visible = true;
                tsClaim.Tabs[0].Selected = true;
                pvCustomer.Selected = true;
                for (int ynt = 0; ynt <= clR.RowCount() - 1; ynt++)
                {
                    clR.GetRowNo(ynt);
                    txtAlertPopup.Text = txtAlertPopup.Text + clR.GetFields("note") + "\r\n" + "\r\n";
                }
                //pvDealer.Selected = true;
            }
            if (txtAlertPopup.Text.Length > 0)
            {
                hfAlert.Value = "Visible";
            }

            SQL = "select * from contractnote cn " +
                  "inner join claim cl On cn.contractid = cl.contractid " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and cn.claimalways <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int znt = 0; znt <= clR.RowCount() - 1; znt++)
                {
                    clR.GetRowNo(znt);
                    txtAlertPopup.Text = txtAlertPopup.Text + clR.GetFields("note") + "\r\n" + "\r\n";
                }
            }
        }
        private void ShowLockedClaim()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "Select userid, fname, lname from userinfo ui " +
                  "inner join claim cl On ui.userid = cl.lockuserid " +
                  "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("userid") != hfUserID.Value)
                {
                    hfClaimLocked.Value = true.ToString();
                    tbLock.Visible = true;
                    lblLocked.Text = "Claim Is locked by " + clR.GetFields("fname") + " " + clR.GetFields("lname");
                }
            }
        }
        private bool CheckLock()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "Select claimid from claim " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "And lockuserid <> " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void LockClaim()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claim " +
                  "Set lockuserid = " + hfUserID.Value + ", " +
                  "lockdate = '" + DateTime.Today + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void ClearScreen()
        {
            GetClaimNo();
            Response.Redirect("claim.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value);
        }
        private void GetClaimNo()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            string sClaimNo;
            long lClaimNo = 0;
        MoveHere:
            sClaimNo = "";
            SQL = "select max(claimno) as claimno from claim where claimno like 'C1%' ";
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                sClaimNo = clC.GetFields("claimno");
                sClaimNo = sClaimNo.Replace("C1", "");
                lClaimNo = long.Parse(sClaimNo);
                lClaimNo = lClaimNo + 1;
                sClaimNo = lClaimNo.ToString("100000000");
                sClaimNo = "C" + sClaimNo;
            }
            if (sClaimNo.Length > 0)
            {
                SQL = "select * from claim where claimno = '" + sClaimNo + "' ";
                clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clC.RowCount() > 0)
                {
                    goto MoveHere;
                }
                else
                {
                    clC.NewRow();
                    clC.SetFields("claimno", sClaimNo);
                    clC.SetFields("status", "Open");
                    if (hfContractID.Value.Length > 0)
                    {
                        clC.SetFields("contractid", hfContractID.Value);
                    }
                    else
                    {
                        clC.SetFields("contractid", 0.ToString());
                    }
                    clC.SetFields("lossdate", DateTime.Today.ToString());
                    clC.SetFields("claimactivityid", 1.ToString());
                    clC.SetFields("creby", hfUserID.Value);
                    clC.SetFields("assignedto", hfUserID.Value);
                    clC.SetFields("Modby", hfUserID.Value);
                    clC.SetFields("credate", DateTime.Today.ToString());
                    clC.SetFields("moddate", DateTime.Today.ToString());
                    clC.SetFields("addnew", true.ToString());
                    clC.AddRow();
                    clC.SaveDB();
                }
            }
            SQL = "select * from claim where claimno = '" + sClaimNo + "' ";
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtClaimNo.Text = clC.GetFields("claimno");
                hfClaimID.Value = clC.GetFields("claimid");
            }
            CheckClaimDuplicate();
            SQL = "insert into claimopenhistory " +
                  "(claimid, opendate, openby) " +
                  "values (" + hfClaimID.Value + ",'" +
                  DateTime.Today + "'," + hfUserID.Value + ") ";
            clC.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        private void GetServerInfo()
        {
            tcLR1.Visible = false;
            tcLR2.Visible = false;
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                Unlockbuttons();
                CheckAssignedToChange();
                if (hfUserID.Value == "12" || hfUserID.Value == "5" || hfUserID.Value == "17" || hfUserID.Value == "18" || hfUserID.Value == "1")
                {
                    tcLR2.Visible = true;
                    tcLR1.Visible = true;
                }
            }
        }
        private void CheckAssignedToChange()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from userinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("username").ToLower() == "srhanna") 
                {
                    btnChangeAssignedTo.Visible = true;
                }
                if (clR.GetFields("username").ToLower() == "jfager") 
                {
                    btnChangeAssignedTo.Visible = true;
                }
                if (clR.GetFields("username").ToLower() == "kfulton") 
                {
                    btnChangeAssignedTo.Visible = true;
                }
                if (clR.GetFields("username").ToLower() == "zpeters") 
                {
                    btnChangeAssignedTo.Visible = true;
                }
                if (clR.GetFields("username").ToLower() == "jsteenbergen") 
                {
                    btnChangeAssignedTo.Visible = true;
                }
                if (clR.GetFields("username").ToLower() == "dhanley") 
                {
                    btnChangeAssignedTo.Visible = true;
                }
                if (clR.GetFields("username").ToLower() == "clutz" && ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test")) 
                {
                    btnChangeAssignedTo.Visible = true;
                }
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }

        private void Unlockbuttons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true) 
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true) 
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")) == true) 
                {
                    btnUnlockClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimpayment")) == false)
                {
                    tsClaim.Tabs[10].Enabled = false;
                    btnInspectionWex.Enabled = false;
                    btnCarfaxPayment.Enabled = false;
                }
            }
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        private void ShowDenied()
        {
            hfDenied.Value = "Visible";
            string script = "function f(){$find(\"" + rwDenied.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        private void ShowOpen() 
        { 
            hfOpen.Value = "Visible";
            string script = "function f(){$find(\"" + rwOpen.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwOpen", script, true);
            rwOpen.Visible = true;
        }

        private void ShowExpire()
        {
            hfExpireContract.Value = "Visible";
            string script = "function f(){$find(\"" + rwExpireContract.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwExpireContract", script, true);
            rwExpireContract.Visible = true;
        }

        private void HideExpire()
        {
            hfExpireContract.Value = "";
            string script = "function f(){$find(\"" + rwExpireContract.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
            rwExpireContract.Visible = false;
        }

        private void ShowAlert()
        {
            hfAlert.Value = "Visible";
            string script = "function f(){$find(\"" + rwAlert.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        private void ShowTW()
        { 
            hfTW.Value = "Visible";
            string script = "function f(){$find(\"" + rwTW.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwTW", script, true);
        }

        private void btnErrorOK_Click(object sender, EventArgs e) 
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        private void HideDenied() 
        {
            hfDenied.Value = "";
            string script = "function f(){$find(\"" + rwDenied.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        private void HideOpen() 
        {
            hfOpen.Value = "";
            string script = "function f(){$find(\"" + rwOpen.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
            rwOpen.Visible = false;
        }

        private void HideAlert() 
        {
            hfAlert.Value = "";
            string script = "function f(){$find(\"" + rwAlert.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        private void HideTW() 
        {
            hfTW.Value = "";
            string script = "function f(){$find(\"" + rwTW.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }
        private void FillScreen()
        {
            clsDBO.clsDBO clCL = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from claim cl " +
                  "left join contract c on cl.contractid = c.contractid " +
                  "where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                txtClaimNo.Text = clCL.GetFields("claimno");
                cboClaimStatus.SelectedValue = clCL.GetFields("status");
                tsClaim.SelectedIndex = 0;
                if (clCL.GetFields("claimactivityid") == "0")
                {
                    cboActivity.SelectedValue = 1.ToString();
                }
                else
                {
                    cboActivity.SelectedValue = clCL.GetFields("claimactivityid");
                }
                txtLossDate.Text = clCL.GetFields("lossdate");
                txtLossMile.Text = clCL.GetFields("lossmile");
                if (clCL.GetFields("saledate").Length > 0)
                {
                    if (clCL.GetFields("lossdate").Length > 0)
                    {
                        txtDaysSale.Text = (DateTime.Parse(clCL.GetFields("lossdate")) - DateTime.Parse(clCL.GetFields("saledate"))).Days.ToString(); //DateDiff(DateInterval.Day, CDate(clCL.Fields("saledate")), CDate(clCL.Fields("lossdate")))
                        if ((DateTime.Parse(clCL.GetFields("lossdate")) - DateTime.Parse(clCL.GetFields("saledate"))).Days <= 45) {
                            if (!Functions.GetAllow45(long.Parse(hfUserID.Value)))
                            {
                                cboClaimStatus.Enabled = false;
                            }
                        }
                    }
                    else
                    {
                        txtDaysSale.Text = 0.ToString();
                    }
                }
                else
                {
                    txtDaysSale.Text = 0.ToString();
                }
                PlaceServiceCenter(clCL.GetFields("servicecenterid"));
                txtServiceCenterContact.Text = clCL.GetFields("sccontactinfo");
                hfServiceCenterID.Value = clCL.GetFields("servicecenterid");
                txtShopRate.Text = clCL.GetFields("shoprate");
                if (clCL.GetFields("rodate").Length > 0)
                {
                    if (clCL.GetFields("rodate") != "1/1/1900")
                    {
                        rdpRODate.SelectedDate = DateTime.Parse(clCL.GetFields("rodate"));
                    }
                }
                txtRONumber.Text = clCL.GetFields("ronumber");
                if (clCL.GetFields("clipid") == "")
                {
                    if (clCL.GetFields("contractno").Length > 0)
                    {
                        if (clCL.GetFields("contractno").Substring(0, 2) == "VA")
                        {
                            txtCompanyInfo.Text = "0000976 Veritas GPS";
                        }
                    }
                    goto MoveHere;
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 1 || Convert.ToInt32(clCL.GetFields("clipid")) == 2)
                {
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 0)
                {
                    if (clCL.GetFields("contractno").Substring(0, 2) == "VA") {
                        txtCompanyInfo.Text = "0000976 Veritas GPS";
                    }
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 3 || Convert.ToInt32(clCL.GetFields("clipid")) == 4 || Convert.ToInt32(clCL.GetFields("clipid")) == 11)
                {
                    if (clCL.GetFields("contractno").Substring(0, 3) == "CHJ")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "DRV")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAC")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAD")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAN")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RDI")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "REP")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSA")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSD")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSW")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "VEL")
                    {
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    }
                    else
                    {
                        txtCompanyInfo.Text = "0000976 Veritas GPS";
                    }
                }
                if (Convert.ToInt32(clCL.GetFields("inscarrierid")) == 4)
                {
                    if (isFL())
                    {
                        txtCompanyInfo.Text = "0000977 Veritas GPA Central Admin Srv FL";
                    }
                    else
                    {
                        txtCompanyInfo.Text = "0000976 Veritas GPS";
                    }
                }
                if (Convert.ToInt32(clCL.GetFields("inscarrierid")) == 5)
                {
                    if (isFL())
                    {
                        txtCompanyInfo.Text = "0000977 Veritas GPA Central Admin Srv FL";
                    }
                    else
                    {
                        txtCompanyInfo.Text = "0000976 Veritas GPS";
                    }
                }

                if (Convert.ToInt32(clCL.GetFields("clipid")) == 5 || Convert.ToInt32(clCL.GetFields("clipid")) == 6 || Convert.ToInt32(clCL.GetFields("clipid")) == 14) {
                    txtCompanyInfo.Text = "0000976 Veritas GPS";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 7 || Convert.ToInt32(clCL.GetFields("clipid")) == 8 || Convert.ToInt32(clCL.GetFields("clipid")) == 13) {
                    txtCompanyInfo.Text = "0000976 Veritas GPS";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 9 || Convert.ToInt32(clCL.GetFields("clipid")) == 10 || Convert.ToInt32(clCL.GetFields("clipid")) == 12)
                {
                    txtCompanyInfo.Text = "0000977 Veritas GPA Central Admin Srv FL";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 11)
                {
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                }
                if (Convert.ToInt32(clCL.GetFields("clipid")) == 0)
                {
                    if (clCL.GetFields("contractno").Length > 3)
                    {
                        if (clCL.GetFields("contractno").Substring(0, 3) == "VA1")
                        {
                            txtCompanyInfo.Text = "0000976 Veritas GPS";
                        }
                    }
                }
            MoveHere:

                if (clCL.GetFields("parttax").Length == 0)
                {
                    clCL.SetFields("parttax", 0.ToString());
                }
                if (clCL.GetFields("labortax").Length == 0)
                {
                    clCL.SetFields("labortax", 0.ToString());
                }
                chkHCC.Checked = Convert.ToBoolean(clCL.GetFields("hcc"));
                if (chkHCC.Checked)
                {
                    tsClaim.Tabs[10].Visible = true;
                }
                else
                {
                    tsClaim.Tabs[10].Visible = false;
                }
                txtPartTax.Text = (Convert.ToDouble(clCL.GetFields("parttax")) * 100).ToString();
                txtLaborTax.Text = (Convert.ToDouble(clCL.GetFields("labortax")) * 100).ToString();
                hfContractID.Value = clCL.GetFields("contractid");
                pvVehicle.ContentUrl = "~/claim/ClaimVehicle.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value;
                pvCustomer.ContentUrl = "~/claim/ClaimCustomer.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value;

                txtLossCodeClaim.Text = clCL.GetFields("losscode");
                GetLossCodeInfo();
                GetContractInfo();
                if (txtLossMile.Text.Length > 0)
                {
                    if (txtEffMile.Text.Length > 0)
                    {
                        txtMileSale.Text = (Convert.ToDouble(txtLossMile.Text) - Convert.ToDouble(txtEffMile.Text)).ToString();
                    }
                }
                hfAssignedTo.Value = clCL.GetFields("assignedto");
                GetAssignedTo();
                FillDealerInfo();
                CheckSecurity();
                if (clCL.GetFields("plantypeid") == "136" || clCL.GetFields("plantypeid") == "137")
                {
                    ShowTW();
                }
                if (clCL.GetFields("plantypeid") == "138" || clCL.GetFields("plantypeid") == "139")
                {
                    ShowTW();
                }
                if (clCL.GetFields("plantypeid") == "141" || clCL.GetFields("plantypeid") == "142")
                {
                    ShowTW();
                }
                if (clCL.GetFields("plantypeid") == "145" || clCL.GetFields("plantypeid") == "151")
                {
                    ShowTW();
                }
                if (clCL.GetFields("plantypeid") == "152")
                {
                    ShowTW();
                }
            }
        }
        private void CheckSecurity()
        {
            string SQL;
            clsDBO.clsDBO clUSI = new clsDBO.clsDBO();
            cboClaimStatus.Enabled = true;
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clUSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clUSI.RowCount() > 0)
            {
                clUSI.GetRow();
                if (cboClaimStatus.SelectedValue == "Denied" || cboClaimStatus.SelectedValue == "Paid")
                {
                    if (Convert.ToBoolean(clUSI.GetFields("AllowUndenyClaim")))
                    {
                        cboClaimStatus.Enabled = true;
                    }
                    else
                    {
                        cboClaimStatus.Enabled = false;
                    }
                }
            }
        }
        private bool isFL()
        {
            //isFL = False
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select c.State from claim cl inner join contract c on c.contractid = cl.contractid " +
                  "where cl.ClaimID = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("state").ToUpper() == "FL")
                {
                    return true;
                }
            }
            return false;
        }
        private void FillDealerInfo()
        {
            OpenContract();
            OpenDealer();
            OpenLossRatio();
            OpenAgent();
        }
        private void OpenLossRatio()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            txtLossRatio.Text = "";
            SQL = "select lossratio from VeritasReports.dbo.DealerRatio where dealerid = " + hfDealerID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("lossratio").Length > 0)
                {
                    txtLossRatio.Text = Convert.ToDouble(clR.GetFields("lossratio")).ToString("P");
                }
            }
        }
        private void OpenAgent()
        {
            if (hfAgentID.Value.Length == 0)
            {
                txtAgentName.Text = "";
                txtAgentNo.Text = "";
                txtAgentContact.Text = "";
                txtAgentEMail.Text = "";
                return;
            }
            if (hfAgentID.Value == "0")
            {
                txtAgentName.Text = "";
                txtAgentNo.Text = "";
                txtAgentContact.Text = "";
                txtAgentEMail.Text = "";
                return;
            }
            clsDBO.clsDBO clA = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from agents where agentid = " + hfAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                txtAgentName.Text = clA.GetFields("agentname");
                txtAgentNo.Text = clA.GetFields("agentno");
                txtAgentContact.Text = clA.GetFields("contact") + " " + clA.GetFields("phone");
                txtAgentEMail.Text = clA.GetFields("email");
            }
            if (hfSubAgentID.Value.Length == 0)
            {
                txtSubAgentContact.Text = "";
                txtSubAgentEmail.Text = "";
                txtSubAgentName.Text = "";
                txtSubAgentNo.Text = "";
                return;
            }
            if (hfSubAgentID.Value == "0")
            {
                txtSubAgentContact.Text = "";
                txtSubAgentEmail.Text = "";
                txtSubAgentName.Text = "";
                txtSubAgentNo.Text = "";
                return;
            }
            clsDBO.clsDBO clSA = new clsDBO.clsDBO();
            SQL = "select * from subagents where subagentid = " + hfSubAgentID.Value;
            clSA.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSA.RowCount() > 0)
            {
                clSA.GetRow();
                txtSubAgentNo.Text = clSA.GetFields("subagentno");
                txtSubAgentName.Text = clSA.GetFields("subagentname");
                txtSubAgentContact.Text = clSA.GetFields("contact") + " " + clSA.GetFields("phone");
                txtSubAgentEmail.Text = clSA.GetFields("email");
            }
        }
        private void OpenDealer()
        {
            if (hfDealerID.Value.Length == 0)
            {
                txtDealerAddr1.Text = "";
                txtDealerAddr2.Text = "";
                txtDealerAddr3.Text = "";
                txtDealerName.Text = "";
                txtDealerNo.Text = "";
                txtDealerPhone.Text = "";
                return;
            }
            string SQL;
            clsDBO.clsDBO clD = new clsDBO.clsDBO();
            SQL = "select * from dealer where dealerid = " + hfDealerID.Value;
            clD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                txtDealerName.Text = clD.GetFields("dealername");
                txtDealerNo.Text = clD.GetFields("dealerno");
                txtDealerAddr1.Text = clD.GetFields("addr1");
                txtDealerAddr2.Text = clD.GetFields("addr2");
                txtDealerAddr3.Text = clD.GetFields("city") + ", " + clD.GetFields("state") + " " + clD.GetFields("zip");
                txtDealerPhone.Text = clD.GetFields("phone");
            }
        }
        private void OpenContract()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            if (hfContractID.Value.Length == 0)
            {
                return;
            }
            SQL = "Select dealerid, agentsid, subagentid from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfDealerID.Value = clC.GetFields("dealerid");
                hfAgentID.Value = clC.GetFields("agentsid");
                hfSubAgentID.Value = clC.GetFields("subagentid");
            }
            else
            {
                hfDealerID.Value = 0.ToString();
                hfAgentID.Value = 0.ToString();
                hfSubAgentID.Value = 0.ToString();
            }
        }
        private void GetAssignedTo()
        {
            txtAssignedTo.Text = "";
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfAssignedTo.Value == "")
            {
                hfAssignedTo.Value = hfUserID.Value;
            }
            SQL = "select * from userinfo where userid = " + hfAssignedTo.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAssignedTo.Text = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
        }
        private void GetLossCodeInfo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimlosscode where losscode = '" + txtLossCodeClaim.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLossCodeDescClaim.Text = clR.GetFields("losscodedesc").Trim();
            }
        }
        private void GetContractInfo()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            if (hfContractID.Value.Length == 0)
            {
                return;
            }
            SQL = "select * from contract c " +
                  "left join InsCarrier ic on c.InsCarrierID = ic.InsCarrierID " +
                  "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtContractNo.Text = clC.GetFields("contractno");
                txtStatus.Text = clC.GetFields("status");
                if (txtStatus.Text == "Cancelled") 
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Cancelled Before Paid") 
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Expired") 
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Pending Expired")
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Invalid")
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Pending Cancel") 
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "PendingCancel")
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Quote")
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Rejected")
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Sale pending")
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "test")
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Training")
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Void")
                {
                    txtStatus.BackColor = Color.Red;
                }
                if (txtStatus.Text == "Pending") 
                {
                    txtStatus.BackColor = Color.Yellow;
                }
                if (txtStatus.Text == "Paid") 
                {
                    txtStatus.BackColor = Color.Green;
                }
                if (Convert.ToInt32(clC.GetFields("programid")) == 93 || Convert.ToInt32(clC.GetFields("programid")) == 94 || Convert.ToInt32(clC.GetFields("programid")) == 95 || Convert.ToInt32(clC.GetFields("programid")) == 96 || Convert.ToInt32(clC.GetFields("programid")) == 97) 
                {
                    lblLossMile.Text = "Loss KM:";
                    lblMileSince.Text = "KM Since Sale:";
                    lblEffMile.Text = "Effective KM:";
                    lblExpMile.Text = "Expired KM:";
                }
                txtPaidDate.Text = clC.GetFields("datepaid");
                txtSaleDate.Text = DateTime.Parse(clC.GetFields("saledate")).ToString("M/d/yyyy");
                txtFName.Text = clC.GetFields("fname");
                txtLName.Text = clC.GetFields("lname");
                txtYear.Text = clC.GetFields("year");
                txtVIN.Text = clC.GetFields("vin");
                txtMake.Text = clC.GetFields("make");
                txtInspCarrier.Text = clC.GetFields("inscarriername");
                txtModel.Text = clC.GetFields("model");
                hfPlanTypeID.Value = clC.GetFields("plantypeid");

                hfProgram.Value = clC.GetFields("programid");
                txtTerm.Text = clC.GetFields("termmonth") + "/" + long.Parse(clC.GetFields("termmile")).ToString("#,##0");
                if (clC.GetFields("effdate").Length > 0)
                {
                    txtEffDate.Text = DateTime.Parse(clC.GetFields("effdate")).ToString("M/d/yyyy");
                }

                if (clC.GetFields("expdate").Length > 0)
                {
                    txtExpDate.Text = DateTime.Parse(clC.GetFields("expdate")).ToString("M/d/yyyy");
                }
                txtEffMile.Text = long.Parse(clC.GetFields("effmile")).ToString("#,##0");
                if (clC.GetFields("expmile").Length > 0)
                {
                    txtExpMile.Text = long.Parse(clC.GetFields("expmile")).ToString("#,##0");
                }
                GetProgram();
                GetPlanType();
                if (clC.GetFields("decpage").Length == 0)
                {
                    tcDec.Visible = false;
                    tcDesc2.Visible = false;
                }
                else
                {
                    tcDec.Visible = true;
                    tcDesc2.Visible = true;
                    hlDec.NavigateUrl = clC.GetFields("decpage");
                    hlDec.Text = clC.GetFields("decpage");
                }
                if (clC.GetFields("tcpage").Length == 0)
                {
                    tcTC.Visible = false;
                    tcTC2.Visible = false;
                }
                else
                {
                    tcTC.Visible = true;
                    tcTC2.Visible = true;
                    hlTC.NavigateUrl = clC.GetFields("tcpage");
                    hlTC.Text = clC.GetFields("tcpage");
                }
            }
        }
        private void GetPlanType()
        {
            string SQL;
            clsDBO.clsDBO clPT = new clsDBO.clsDBO();
            SQL = "select * from plantype where plantypeid = " + hfPlanTypeID.Value;
            clPT.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clPT.RowCount() > 0)
            {
                clPT.GetRow();
                txtPlan.Text = clPT.GetFields("plantype");
            }
        }
        private void GetProgram()
        {
            string SQL;
            clsDBO.clsDBO clP = new clsDBO.clsDBO();
            SQL = "select * from program where programid = " + hfProgram.Value;
            clP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                txtProgram.Text = clP.GetFields("programname");
            }
        }
        private void PlaceServiceCenter(string xServiceCenterID)
        {
            clsDBO.clsDBO clSC = new clsDBO.clsDBO();
            string SQL;
            if (xServiceCenterID.Length == 0)
            {
                return;
            }
            SQL = "select * from servicecenter where servicecenterid = " + xServiceCenterID;
            clSC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSC.RowCount() > 0)
            {
                clSC.GetRow();
                txtServiceCenter.Text = clSC.GetFields("servicecenterno");
                txtServiceInfo.Text = clSC.GetFields("servicecentername") + " " + clSC.GetFields("addr1") + " " + clSC.GetFields("addr2") + " " + clSC.GetFields("city") + " " + clSC.GetFields("state") + " " + clSC.GetFields("zip") + " " + clSC.GetFields("phone");
                txtSCEmail.Text = clSC.GetFields("email");
                txtSCFax.Text = clSC.GetFields("fax");
                if (clSC.GetFields("zip").Length > 0)
                {
                    GetLaborInfo(clSC.GetFields("zip"));
                }
            }
        }
        private void GetLaborInfo(string xZip)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            VeritasGlobalToolsV2.clsWISZipCode clWZ = new VeritasGlobalToolsV2.clsWISZipCode();
            clWZ.ZipCode = xZip;
            clWZ.RunZip();
            SQL = "select * from laborrate where zipcode = '" + xZip + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAvgRate.Text = Convert.ToDouble(clR.GetFields("avgrate")).ToString("#,##0.00");
            }
            else
            {
                txtAvgRate.Text = 0.ToString("#,##0.00");
            }
        }
        protected void btnSeekServiceCenter_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSeekSC.Visible = true;
            rgServiceCenter.DataSourceID = "dsServiceCenter";
            rgServiceCenter.Rebind();
        }

        protected void rgServiceCenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlSeekSC.Visible = false;
            pnlDetail.Visible = true;
            hfServiceCenterID.Value = rgServiceCenter.SelectedValue.ToString();
            PlaceServiceCenter(hfServiceCenterID.Value);
            UpdateClaimPayee();
            UpdateClaim();
            UpdateDeduct();
        }
        private void UpdateClaimPayee()
        {
            string SQL;
            clsDBO.clsDBO clS = new clsDBO.clsDBO();
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "select * from claimpayee where payeeno = '" + txtServiceCenter.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                SQL = "select * from servicecenter where servicecenterno = '" + txtServiceCenter.Text + "' ";
                clS.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clS.RowCount() > 0)
                {
                    clS.GetRow();
                    clR.NewRow();
                    clR.SetFields("payeeno", clS.GetFields("servicecenterno"));
                    clR.SetFields("payeename", clS.GetFields("servicecentername"));
                    clR.SetFields("addr1", clS.GetFields("addr1"));
                    clR.SetFields("addr2", clS.GetFields("addr2"));
                    clR.SetFields("city", clS.GetFields("city"));
                    clR.SetFields("state", clS.GetFields("state"));
                    clR.SetFields("zip", clS.GetFields("zip"));
                    clR.SetFields("phone", clS.GetFields("phone"));
                    clR.AddRow();
                    clR.SaveDB();
                }
            }
        }
        private void UpdateDeduct()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clCP = new clsDBO.clsDBO();
            clsDBO.clsDBO clCD = new clsDBO.clsDBO();
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "select * from servicecenter where servicecenterid = " + hfServiceCenterID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                SQL = "select * from claimpayee where payeeno = '" + clR.GetFields("servicecenterno") + "' ";
                clCP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clCP.RowCount() > 0)
                {
                    clCP.GetRow();
                    SQL = "update claimdetail " +
                          "set claimpayeeid = " + clCP.GetFields("claimpayeeid") + " " +
                          "where claimid = " + hfClaimID.Value + " " +
                          "and jobno = 'A01' ";
                    clCD.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                }
            }
        }
        protected void btnSeekContract_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSeekContract.Visible = true;
            rgContract.DataSourceID = "dsContract";
            rgContract.Rebind();
        }
        private bool AllowCancelExpire()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("allowCancelExpire")))
                {
                    return true;
                }
            }
            return false;
        }
        protected void rgContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            pnlContract.Visible = false;
            SQL = "select * from contract where contractid = " + rgContract.SelectedValue;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (clC.GetFields("status") == "Paid")
                {
                    hfContractID.Value = rgContract.SelectedValue.ToString();
                    pvVehicle.ContentUrl = "~/claim/ClaimVehicle.aspx?sid=" + hfID.Value + "&ContractID=" + hfContractID.Value;
                    pvCustomer.ContentUrl = "~/claim/ClaimCustomer.aspx?sid=" + hfID.Value + "&contractid=" + hfClaimID.Value;
                    UpdateClaim();
                    AddClaimDetail();
                    FillDealerInfo();
                    pnlDetail.Visible = true;
                    Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + hfClaimID.Value);
                    return;
                }
                else
                {
                    hfContractID.Value = rgContract.SelectedValue.ToString();
                    UpdateClaim();
                    txtContractStatus2.Text = clC.GetFields("status");
                    pnlContractConfirm.Visible = true;
                    FillDealerInfo();
                    AddClaimDetail();
                    return;
                }
            }
            UpdateClaim();
            UpdateDeduct();
        }
        private void AddClaimDetail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            long lDeductID = 0;
            double dDeduct = 0;
            SQL = "select deductid from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lDeductID = long.Parse(clR.GetFields("deductid"));
            }
            SQL = "select * from deductible where deductid = " + lDeductID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dDeduct = Convert.ToDouble(clR.GetFields("deductamt"));
            }
            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and jobno = 'A01' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
            }
            else
            {
                clR.GetRow();
            }
            GetClaimPayee();
            clR.SetFields("ratetypeid", 1.ToString());
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimdetailtype", "Deductible");
            clR.SetFields("losscode", "Deductible");
            clR.SetFields("jobno", "A01");
            clR.SetFields("reqqty", "1");
            clR.SetFields("reqcost", (dDeduct * -1).ToString());
            clR.SetFields("Claimpayeeid", hfClaimPayeeID.Value);
            clR.SetFields("claimdetailstatus", "Requested");
            clR.SetFields("reqamt", (dDeduct * -1).ToString());
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("credate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("moddate", DateTime.Today.ToString());
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
        }

        private void GetClaimPayee()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clSC = new clsDBO.clsDBO();
            if (txtServiceCenter.Text.Length == 0)
            {
                hfClaimPayeeID.Value = 0.ToString();
                return;
            }
            SQL = "select * from claimpayee where payeeno = '" + txtServiceCenter.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                SQL = "select * from servicecenter where servicecenterno = '" + txtServiceCenter.Text + "' ";
                clSC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clSC.RowCount() > 0)
                {
                    clSC.GetRow();
                    clR.NewRow();
                    clR.SetFields("payeeno", clSC.GetFields("servicecenterno"));
                    clR.SetFields("payeename", clSC.GetFields("servicecentername"));
                    clR.SetFields("addr1", clSC.GetFields("addr1"));
                    clR.SetFields("addr2", clSC.GetFields("addr2"));
                    clR.SetFields("city", clSC.GetFields("city"));
                    clR.SetFields("state", clSC.GetFields("state"));
                    clR.SetFields("zip", clSC.GetFields("zip"));
                    clR.SetFields("phone", clSC.GetFields("phone"));
                    clR.AddRow();
                    clR.SaveDB();
                    SQL = "select * from claimpayee where payeeno = '" + txtServiceCenter.Text + "' ";
                    clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clR.RowCount() > 0)
                    {
                        clR.GetRow();
                        hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                    }
                }
            }
            else
            {
                clR.GetRow();
                hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
            }
        }
        private void UpdateClaim()
        {
            string SQL;
            clsDBO.clsDBO clClaim = new clsDBO.clsDBO();
            VeritasGlobalToolsV2.clsClaimHistory clCH = new VeritasGlobalToolsV2.clsClaimHistory();
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clClaim.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clClaim.RowCount() > 0)
            {
                clClaim.GetRow();
            }
            else
            {
                clClaim.NewRow();
            }
            if (hfContractID.Value == "")
            {
                hfContractID.Value = 0.ToString();
            }
            if (clClaim.GetFields("contractid") != hfContractID.Value)
            {
                clCH.ClaimID = long.Parse(hfClaimID.Value);
                clCH.FieldName = "ContractID";
                clCH.PrevFieldInfo = clClaim.GetFields("contractid");
                clCH.FieldInfo = hfContractID.Value;
                clCH.CreBy = long.Parse(hfUserID.Value);
                clCH.AddClaimHistory();
            }
            clClaim.SetFields("contractid", hfContractID.Value);
            if (hfContractID.Value != "0")
            {
                clClaim.SetFields("ClaimCntNo", (GetClaimCnt() + 1).ToString());
            }
            if (clClaim.GetFields("claimactivityid") != cboActivity.SelectedValue)
            {
                clCH.ClaimID = long.Parse(hfClaimID.Value);
                clCH.FieldName = "ClaimActivityID";
                clCH.PrevFieldInfo = clClaim.GetFields("claimactivityid");
                clCH.FieldInfo = cboActivity.SelectedValue;
                clCH.CreBy = long.Parse(hfUserID.Value);
                clCH.AddClaimHistory();
            }
            clClaim.SetFields("claimactivityid", cboActivity.SelectedValue);
            if (clClaim.GetFields("status") != cboClaimStatus.SelectedValue)
            {
                clCH.ClaimID = long.Parse(hfClaimID.Value);
                clCH.FieldName = "Status";
                clCH.PrevFieldInfo = clClaim.GetFields("status");
                clCH.FieldInfo = cboClaimStatus.SelectedValue;
                clCH.CreBy = long.Parse(hfUserID.Value);
                clCH.AddClaimHistory();
            }
            clClaim.SetFields("status", cboClaimStatus.SelectedValue);
            if (txtLossMile.Text.Length > 0)
            {
                if (clClaim.GetFields("lossmile") != txtLossMile.Text)
                {
                    clCH.ClaimID = long.Parse(hfClaimID.Value);
                    clCH.FieldName = "LossMile";
                    clCH.PrevFieldInfo = clClaim.GetFields("lossmile");
                    clCH.FieldInfo = txtLossMile.Text;
                    clCH.CreBy = long.Parse(hfUserID.Value);
                    clCH.AddClaimHistory();
                }
                clClaim.SetFields("lossmile", txtLossMile.Text);
            }
            if (hfServiceCenterID.Value == "")
            {
                hfServiceCenterID.Value = 0.ToString();
            }
            if (clClaim.GetFields("servicecenterid") != hfServiceCenterID.Value)
            {
                clCH.ClaimID = long.Parse(hfClaimID.Value);
                clCH.FieldName = "ServiceCenterID";
                clCH.PrevFieldInfo = clClaim.GetFields("servicecenterid");
                clCH.FieldInfo = hfServiceCenterID.Value;
                clCH.CreBy = long.Parse(hfUserID.Value);
                clCH.AddClaimHistory();
            }
            clClaim.SetFields("servicecenterid", hfServiceCenterID.Value);
            if (clClaim.GetFields("sccontactinfo") != txtServiceCenterContact.Text) 
            {
                clCH.ClaimID = long.Parse(hfClaimID.Value);
                clCH.FieldName = "SCContactInfo";
                clCH.PrevFieldInfo = clClaim.GetFields("sccontactinfo");
                clCH.FieldInfo = txtServiceCenterContact.Text;
                clCH.CreBy = long.Parse(hfUserID.Value);
                clCH.AddClaimHistory();
            }

            clClaim.SetFields("sccontactinfo", txtServiceCenterContact.Text);
            if (clClaim.RowCount() > 0)
            {
                clClaim.SetFields("moddate", DateTime.Today.ToString());
                clClaim.SetFields("modby", hfUserID.Value);
            }
            else
            {
                clClaim.SetFields("credate", DateTime.Today.ToString());
                clClaim.SetFields("creby", hfUserID.Value);
                clClaim.SetFields("assignedto", hfUserID.Value);
                clClaim.SetFields("moddate", DateTime.Today.ToString());
                clClaim.SetFields("modby", hfUserID.Value);
                clClaim.AddRow();
            }
            clClaim.SaveDB();
            CheckClaimDuplicate();
        }
        private long GetClaimCnt()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from claim where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            return clR.RowCount();
        }
        protected void btnProceedClaim_Click(object sender, EventArgs e)
        {
            UpdateClaimNote();
            UpdateContractClaim();
            pnlContractConfirm.Visible = false;
            pnlDetail.Visible = true;
            Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + hfClaimID.Value);
        }

        protected void btnCancelClaim_Click(object sender, EventArgs e)
        {
            UpdateContractClaimCancel();
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }
        private void UpdateContractClaimCancel()
        {
            clsDBO.clsDBO clCL = new clsDBO.clsDBO();
            string SQL;
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                clCL.SetFields("status", "Void");
                clCL.SetFields("contractid", hfContractID.Value);
                clCL.SaveDB();
            }
        }
        private void UpdateContractClaim()
        {
            clsDBO.clsDBO clCL = new clsDBO.clsDBO();
            string SQL;
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                clCL.SetFields("contractid", hfContractID.Value);
                clCL.SaveDB();
            }
        }
        private void UpdateClaimNote()
        {
            if (txtNote.Text.Length == 0)
            {
                return;
            }
            clsDBO.clsDBO clCN = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnoteid = 0 ";
            clCN.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clCN.NewRow();
            clCN.SetFields("claimid", hfClaimID.Value);
            clCN.SetFields("claimnotetypeid", 5.ToString());
            clCN.SetFields("note", txtNote.Text);
            clCN.SetFields("credate", DateTime.Today.ToString());
            clCN.SetFields("creby", hfUserID.Value);
            clCN.AddRow();
            clCN.SaveDB();
        }
        protected void tsClaim_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsClaim.SelectedTab.Value == "Payment") {
                pvPayment.Selected = true;
            }
            /*if (tsClaim.SelectedTab.Value == "Dealer") {
                pvDealer.Selected = true;
            }*/
            if (tsClaim.SelectedTab.Value == "Customer") {
                pvCustomer.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "3C") {
                pv3C.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "Vehicle") {
                pvVehicle.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "Documents") {
                pvDocuments.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "Insp") {
                pvInspection.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "Part") {
                pvParts.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "Notes") {
                pvNotes.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "Jobs") {
                pvJob.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "Alert") {
                pvAlert.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "History") {
                pvHistory.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "HCC") {
                pvHCC.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "CK") {
                pvCK.Selected = true;
            }
            if (tsClaim.SelectedTab.Value == "ContractNote") {
                pvContractNote.Selected = true;
            }
        }
        private bool CheckAlert()
        {
            if (tsClaim.Tabs[6].Visible == false)
            {
                return false;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and Alert <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        protected void txtLossMile_TextChanged(object sender, EventArgs e)
        {
            if (txtLossMile.Text.Length > 0)
            {
                if (txtEffMile.Text.Length > 0)
                {
                    txtMileSale.Text = (long.Parse(txtLossMile.Text) - long.Parse(txtEffMile.Text)).ToString();
                }
                if (txtExpMile.Text.Length > 0)
                {
                    if (long.Parse(txtLossMile.Text) > long.Parse(txtExpMile.Text))
                    {
                        ShowExpire();
                    }
                }
            }
            UpdateClaim();
        }
        protected void txtServiceCenterContact_TextChanged(object sender, EventArgs e)
        {
            UpdateClaim();
        }

        protected void cboActivity_TextChanged(object sender, EventArgs e)
        {
            UpdateNote();
            UpdateClaim();
            int ConvertedSelectedValue = Convert.ToInt32(cboActivity.SelectedValue);

            if (ConvertedSelectedValue == 11) 
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 14) 
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 15) 
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 16) 
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 17) 
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 18) 
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 21) 
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 22) 
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 23) 
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 24)
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 25) 
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 26) 
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 28)
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 29)
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 30)
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 31) 
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 32)
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 33)
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 35) 
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 36)
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 37)
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 38)
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 39)
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 40) 
            {
                CloseClaim();
                VeritasGlobalToolsV2.clsANClaim clAN = new VeritasGlobalToolsV2.clsANClaim();
                clAN.ProcessANClaim(hfClaimID.Value, long.Parse(cboActivity.SelectedValue), long.Parse(hfUserID.Value));
            }
            if (ConvertedSelectedValue == 42)
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 43) 
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 45) 
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 46)
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 47) 
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 48) 
            {
                CloseClaim();
            }
            if (ConvertedSelectedValue == 54) 
            {
                CloseClaim();
            }

            if (ConvertedSelectedValue == 16 || ConvertedSelectedValue == 18 || ConvertedSelectedValue == 35 || 
                ConvertedSelectedValue == 36 || ConvertedSelectedValue == 37 || ConvertedSelectedValue == 38 || 
                ConvertedSelectedValue == 39 || ConvertedSelectedValue == 42 || ConvertedSelectedValue == 43 || 
                ConvertedSelectedValue == 48 || ConvertedSelectedValue == 54) 
            {
                if (!DeniedNotes()) 
                {
                    AddDeniedActivity();
                    ShowDenied();
                }
            }

            if (cboClaimStatus.SelectedValue == "Open") {
                if (!CheckNoteOpen()) {
                    if (ConvertedSelectedValue != 11 && ConvertedSelectedValue != 14) 
                    {
                        if (ConvertedSelectedValue != 15 && ConvertedSelectedValue != 17) 
                        {
                            if (ConvertedSelectedValue != 18 && ConvertedSelectedValue != 21) 
                            {
                                if (ConvertedSelectedValue != 22 && ConvertedSelectedValue != 23) 
                                {
                                    if (ConvertedSelectedValue != 24 && ConvertedSelectedValue != 25) 
                                    {
                                        if (ConvertedSelectedValue != 26 && ConvertedSelectedValue != 28) 
                                        {
                                            if (ConvertedSelectedValue != 29 && ConvertedSelectedValue != 30) 
                                            {
                                                if (ConvertedSelectedValue != 31 && ConvertedSelectedValue != 32) 
                                                {
                                                    if (ConvertedSelectedValue != 33 && ConvertedSelectedValue != 35) 
                                                    {
                                                        if (ConvertedSelectedValue != 36 && ConvertedSelectedValue != 37) 
                                                        {
                                                            if (ConvertedSelectedValue != 38 && ConvertedSelectedValue != 39) 
                                                            {
                                                                if (ConvertedSelectedValue != 40 && ConvertedSelectedValue != 42) 
                                                                {
                                                                    if (ConvertedSelectedValue != 43 && ConvertedSelectedValue != 45) 
                                                                    {
                                                                        if (ConvertedSelectedValue != 46 && ConvertedSelectedValue != 47) 
                                                                        {
                                                                            if (ConvertedSelectedValue != 48) 
                                                                            {
                                                                                ShowOpen();
                                                                                return;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private bool DeniedNotes()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnoteid = 7 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private void AddDeniedActivity()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claim " +
                  "set DeniedActivity = 1 " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void UpdateNote()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select ca.ActivityDesc from claim cl " +
                  "inner join ClaimActivity ca on cl.ClaimActivityID = ca.ClaimActivityID " +
                  "where claimid = " + hfClaimID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                SQL = "insert into claimnote " +
                      "(claimid, claimnotetypeid, note, credate, creby, moddate, modby, notetext) " +
                      "values (" + hfClaimID.Value + ", 5, 'Activity Change: " +
                      clC.GetFields("ActivityDesc") + " ----- " + cboActivity.Text + "', " +
                      "'" + DateTime.Today + "', " + hfUserID.Value + ", '" + DateTime.Today + "', " + hfUserID.Value + ", " +
                      "'Activity Change: " + clC.GetFields("ActivityDesc") + " ----- " + cboActivity.Text + "')";
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
        }
        private void CloseClaim()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "update claim " +
                  "set closedate = '" + DateTime.Today + "', " +
                  "closeby = " + hfUserID.Value + " " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and closedate is null ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "update claimopenhistory " +
                  "set closedate = '" + DateTime.Today + "', " +
                  "closeby = " + hfUserID.Value + " " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and closedate is null ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        protected void txtPartTax_TextChanged(object sender, EventArgs e)
        {
            if (txtPartTax.Text == "")
            {
                txtPartTax.Text = 0.ToString();
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claim " +
                  "set parttax = " + Convert.ToDouble(txtPartTax.Text) / 100 + " " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        protected void txtLaborTax_TextChanged(object sender, EventArgs e)
        {
            if (txtLaborTax.Text == "")
            {
                txtLaborTax.Text = 0.ToString();
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "update claim " +
                  "set labortax = " + Convert.ToDouble(txtLaborTax.Text) / 100 + " " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void ClearLock()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "update claim " +
                  "set lockuserid = null, lockdate = null " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and lockuserid = " + hfUserID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private bool CheckNote()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        protected void btnCalcPart_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail " +
                  "where claimdetailtype = 'Part' " +
                  "and claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    ProcessClaimTax(long.Parse(clR.GetFields("claimdetailid")));
                }
            }
        }
        private void ProcessClaimTax(long xClaimDetailID)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "select * from claimdetail where claimdetailid = " + xClaimDetailID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("claimdetailtype") == "Part")
                {
                    if (clR.GetFields("authamt").Length > 0)
                    {
                        if (Convert.ToDouble(clR.GetFields("authamt")) > 0)
                        {
                            clR.SetFields("taxper", txtPartTax.Text);
                            clR.SetFields("taxamt", (Decimal.Round(Convert.ToDecimal(clR.GetFields("authamt")) * Convert.ToDecimal(txtPartTax.Text)) / 100).ToString());
                            clR.SetFields("totalamt", (Convert.ToDouble(clR.GetFields("authamt")) + Convert.ToDouble(clR.GetFields("taxamt"))).ToString());
                        }
                        else
                        {
                            clR.SetFields("taxper", txtPartTax.Text);
                            clR.SetFields("taxamt", 0.ToString());
                            clR.SetFields("totalamt", 0.ToString());
                        }
                    }
                    else
                    {
                        clR.SetFields("taxper", txtPartTax.Text);
                        clR.SetFields("taxamt", 0.ToString());
                        clR.SetFields("totalamt", 0.ToString());
                    }
                }
                if (clR.GetFields("claimdetailtype") == "Labor")
                {
                    if (clR.GetFields("authamt").Length > 0)
                    {
                        if (Convert.ToDouble(clR.GetFields("authamt")) > 0)
                        {
                            clR.SetFields("taxper", txtLaborTax.Text);
                            clR.SetFields("taxamt", (Decimal.Round(Convert.ToDecimal(clR.GetFields("authamt")) * Convert.ToDecimal(txtLaborTax.Text)) / 100).ToString());
                            clR.SetFields("totalamt", (Convert.ToDouble(clR.GetFields("authamt")) + Convert.ToDouble(clR.GetFields("taxamt"))).ToString());
                        }
                        else
                        {
                            clR.SetFields("taxper", txtLaborTax.Text);
                            clR.SetFields("taxamt", 0.ToString());
                            clR.SetFields("totalamt", 0.ToString());
                        }
                    }
                    else
                    {
                        clR.SetFields("taxper", txtLaborTax.Text);
                        clR.SetFields("taxamt", 0.ToString());
                        clR.SetFields("totalamt", 0.ToString());
                    }
                }
                clR.SaveDB();
            }
        }
        protected void btnCalcLabor_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail " +
                  "where claimdetailtype = 'Labor' " +
                  "and claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    ProcessClaimTax(long.Parse(clR.GetFields("claimdetailid")));
                }
            }
        }
        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            ClearLock();
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnAddSC_Click(object sender, EventArgs e)
        {
            pnlAddSC.Visible = true;
            pnlSeekSC.Visible = false;
        }

        protected void btnSCCancel_Click(object sender, EventArgs e)
        {
            pnlSeekSC.Visible = true;
            pnlAddSC.Visible = false;
        }

        protected void btnSCSave_Click(object sender, EventArgs e)
        {
            AddServiceCenter();
            rgServiceCenter.Rebind();
            pnlSeekSC.Visible = true;
            pnlAddSC.Visible = false;
        }
        private void AddServiceCenter()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from servicecenter where servicecenterid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clR.NewRow();
            GetNextServiceCenterNo();
            clR.SetFields("servicecenterno", hfServiceCenterNo.Value);
            clR.SetFields("servicecentername", txtServiceCenterName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.SetFields("fax", txtFax.Text);
            clR.SetFields("email", txtEMail.Text);
            clR.AddRow();
            clR.SaveDB();
        }
        private void GetNextServiceCenterNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select max(servicecenterno) as mSCN from servicecenter where servicecenterno like 'rf0%' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clR.GetRow();
            hfServiceCenterNo.Value = "RF" + (long.Parse(clR.GetFields("mscn").Substring(clR.GetFields("mscn").Length - 6)) + 1).ToString("0000000");
        }
        protected void cboClaimStatus_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            tbLock.Visible = false;
            if (cboClaimStatus.SelectedValue == "Denied")
            {
                if (hfAllowAZDenied.Value == "False") {
                    if (Functions.GetServiceCenterState(long.Parse(hfClaimID.Value)) == "AZ") {
                        lblLocked.Text = "You can not deny an AZ claim. Please see manager.";
                        tbLock.Visible = true;
                        cboClaimStatus.SelectedValue = "Open";
                        return;
                    }
                }

                if (CheckRequested()) {
                    tbLock.Visible = true;
                    lblLocked.Text = "You must change all Jobs from requested to denied or authorized.";
                    cboClaimStatus.SelectedValue = "Open";
                    return;
                }
            }

            if (CheckPaidDenied()) {
                if (!CheckUndeny()) {
                    tbLock.Visible = true;
                    lblLocked.Text = "Only Jeff, Zach, Darius or Joe can reactivate a claim";
                    return;
                }
            }
            SQL = "update claim set status = '" + cboClaimStatus.Text + "', ";
            if (cboClaimStatus.Text == "Paid") {
                SQL = SQL + "datepaid = '" + DateTime.Today + "' ";
            }
            else 
            {
                SQL = SQL + "datepaid = null ";
            }
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (cboClaimStatus.SelectedValue == "Denied") {
                if (!CheckNoteDenied()) {
                    ShowDenied();
                }
            }

            if (cboClaimStatus.SelectedValue == "Open") {
                if (!CheckNoteOpen()) {
                    if (Convert.ToInt32(cboActivity.SelectedValue) != 11 && Convert.ToInt32(cboActivity.SelectedValue) != 14) {
                        if (Convert.ToInt32(cboActivity.SelectedValue) != 15 && Convert.ToInt32(cboActivity.SelectedValue) != 17) {
                            if (Convert.ToInt32(cboActivity.SelectedValue) != 18 && Convert.ToInt32(cboActivity.SelectedValue) != 21) {
                                if (Convert.ToInt32(cboActivity.SelectedValue) != 22 && Convert.ToInt32(cboActivity.SelectedValue) != 23) {
                                    if (Convert.ToInt32(cboActivity.SelectedValue) != 24 && Convert.ToInt32(cboActivity.SelectedValue) != 25) {
                                        if (Convert.ToInt32(cboActivity.SelectedValue) != 26 && Convert.ToInt32(cboActivity.SelectedValue) != 28) {
                                            if (Convert.ToInt32(cboActivity.SelectedValue) != 29 && Convert.ToInt32(cboActivity.SelectedValue) != 30) {
                                                if (Convert.ToInt32(cboActivity.SelectedValue) != 31 && Convert.ToInt32(cboActivity.SelectedValue) != 32) {
                                                    if (Convert.ToInt32(cboActivity.SelectedValue) != 33 && Convert.ToInt32(cboActivity.SelectedValue) != 35) {
                                                        if (Convert.ToInt32(cboActivity.SelectedValue) != 36 && Convert.ToInt32(cboActivity.SelectedValue) != 37) {
                                                            if (Convert.ToInt32(cboActivity.SelectedValue) != 38 && Convert.ToInt32(cboActivity.SelectedValue) != 39) {
                                                                if (Convert.ToInt32(cboActivity.SelectedValue) != 40 && Convert.ToInt32(cboActivity.SelectedValue) != 42) {
                                                                    if (Convert.ToInt32(cboActivity.SelectedValue) != 43 && Convert.ToInt32(cboActivity.SelectedValue) != 45) {
                                                                        if (Convert.ToInt32(cboActivity.SelectedValue) != 46 && Convert.ToInt32(cboActivity.SelectedValue) != 47) {
                                                                            if (Convert.ToInt32(cboActivity.SelectedValue) != 48) {
                                                                                ShowOpen();
                                                                                return;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private bool CheckUndeny()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo " +
                  "where userid = " + hfUserID.Value + " " +
                  "and allowundenyclaim <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private bool CheckRequested()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private bool CheckPaidDenied()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim " +
                  "where (status =  'Paid' " +
                  "or status = 'Denied') " +
                  "and claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private bool CheckNoteOpen()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnotetypeid = 6 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        private bool CheckNoteDenied()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnotetypeid = 7 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                return true;
            }
            return false;
        }
        protected void btnAlertOK_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        protected void txtRONumber_TextChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "update claim " +
                  "set ronumber = '" + txtRONumber.Text + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            CheckClaimDuplicate();
        }

        protected void rdpRODate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfReadOnly.Value.ToLower() == "true")
            {
                return;
            }
            SQL = "update claim " +
                  "set rodate = '" + rdpRODate.SelectedDate + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        protected void txtShopRate_TextChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clCD = new clsDBO.clsDBO();
            if (txtShopRate.Text.Length == 0)
            {
                return;
            }
            SQL = "update claim " +
                  "set shoprate = " + txtShopRate.Text + " " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimdetailtype = 'Labor' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    SQL = "select * from claimdetail where claimdetailid = " + clR.GetFields("claimdetailid");
                    clCD.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clCD.RowCount() > 0)
                    {
                        clCD.GetRow();
                        clCD.SetFields("reqcost", txtShopRate.Text);
                        clCD.SetFields("reqamt", (Convert.ToDouble(txtShopRate.Text) * Convert.ToDouble(clR.GetFields("reqqty"))).ToString());
                        clCD.SaveDB();
                    }
                }
            }
        }

        protected void btnSeekLossCode_Click(object sender, EventArgs e)
        {
            pnlSeekLossCode.Visible = true;
            pnlDetail.Visible = false;
            rgLossCode.Rebind();
            ColorLossCode();
        }

        protected void rgLossCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            txtLossCodeClaim.Text = rgLossCode.SelectedValue.ToString();
            SQL = "select * from claimlosscode where losscode = '" + rgLossCode.SelectedValue + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLossCodeDescClaim.Text = clR.GetFields("losscodedesc");
            }
            pnlDetail.Visible = true;
            pnlSeekLossCode.Visible = false;
            SQL = "update claim " +
                  "set losscode = '" + rgLossCode.SelectedValue + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        protected void btnAddLossCode_Click(object sender, EventArgs e)
        {
            pnlSeekLossCode.Visible = false;
            pnlLossCode.Visible = true;
        }

        protected void btnCancelLossCode_Click(object sender, EventArgs e)
        {
            pnlSeekLossCode.Visible = true;
            pnlLossCode.Visible = false;
        }

        protected void btnSaveLossCode_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimlosscode where losscode = '" + txtLossCode.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("losscode", txtLossCode.Text);
                clR.SetFields("losscodedesc", txtLossCodeDesc.Text);
                clR.AddRow();
                clR.SaveDB();
            }
            rgLossCode.Rebind();
            pnlSeekLossCode.Visible = true;
            pnlLossCode.Visible = false;
        }

        protected void btnChangeAssignedTo_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlAssignedToChange.Visible = true;
        }

        protected void rgAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            hfAssignedTo.Value = rgAssignedTo.SelectedValue.ToString();
            if (hfReadOnly.Value == "true")
            {
                return;
            }
            GetAssignedTo();
            SQL = "update claim " +
                  "set assignedto = " + hfAssignedTo.Value + " " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            pnlAssignedToChange.Visible = false;
            pnlDetail.Visible = true;
        }

        protected void txtSCEmail_TextChanged(object sender, EventArgs e)
        {
            if (txtServiceCenter.Text.Length == 0)
            {
                return;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from servicecenter where servicecenterno = '" + txtServiceCenter.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("email", txtSCEmail.Text);
                clR.SaveDB();
            }
            SQL = "update claim " +
                  "set sccontactemail = '" + txtSCEmail.Text + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        protected void txtSCFax_TextChanged(object sender, EventArgs e)
        {
            if (txtServiceCenter.Text.Length == 0)
            {
                return;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from servicecenter where servicecenterno = '" + txtServiceCenter.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("fax", txtSCFax.Text);
                clR.SaveDB();
            }
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            if (!CheckNote()) 
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            ClearLock();
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void chkHCC_CheckedChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (chkHCC.Checked)
            {
                SQL = "select * from claimhcc where claimid = " + hfClaimID.Value;
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() == 0)
                {
                    clR.NewRow();
                    clR.SetFields("Adjusterid", hfUserID.Value);
                    clR.SetFields("DateSubmitted", DateTime.Today.ToString());
                    clR.SetFields("claimid", hfClaimID.Value);
                    clR.AddRow();
                    clR.SaveDB();
                }
                tsClaim.Tabs[10].Visible = true;
                SQL = "update claim " +
                      "set hcc = 1 " +
                      "where claimid = " + hfClaimID.Value;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
            else
            {
                tsClaim.Tabs[10].Visible = false;
                SQL = "update claim " +
                      "set hcc = 0 " +
                      "where claimid = " + hfClaimID.Value;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
        }

        protected void btnDeniedClose_Click(object sender, EventArgs e)
        {
            HideDenied();
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select status from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                cboClaimStatus.SelectedValue = clR.GetFields("status");
            }
            SQL = "update claim " +
                  "set status = '" + cboClaimStatus.Text + "', " +
                  "closedate = null, opendate = '" + DateTime.Today + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "insert into claimopenhistory " +
                  "(claimid, opendate, openby) " +
                  "values (" + hfClaimID.Value + ",'" +
                  DateTime.Today + "'," + hfUserID.Value + ") ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        protected void btnDeniedSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnoteid = 7 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
            }
            else
            {
                clR.NewRow();
            }
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimnotetypeid", 7.ToString());
            clR.SetFields("note", txtDeniedNote.Text);
            clR.SetFields("credate", DateTime.Now.ToString());
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("notetext", txtDeniedNote.Text);
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
            SQL = "update claim " +
                  "set status = '" + cboClaimStatus.Text + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            RemoveDeniedActivity();
            HideDenied();
        }
        private void RemoveDeniedActivity()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claim " +
                  "set deniedactivity = 0 " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        protected void btnOpenClose_Click(object sender, EventArgs e)
        {
            HideOpen();
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string SQL;
            SQL = "select status, ClaimActivityID from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                cboClaimStatus.SelectedValue = clR.GetFields("status");
                cboActivity.SelectedValue = clR.GetFields("ClaimActivityID");
            }
            SQL = "update claim " +
                  "set status = '" + cboClaimStatus.Text + "', " +
                  "closedate = null, opendate = '" + DateTime.Today + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "insert into claimopenhistory " +
                  "(claimid, opendate, openby) " +
                  "values (" + hfClaimID.Value + ",'" +
                  DateTime.Today + "'," + hfUserID.Value + ") ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        protected void btnOpenSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (txtOpenNote.Text.Length < 25)
            {
                return;
            }
            SQL = "select * from claimnote " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and claimnoteid = 6 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
            }
            else
            {
                clR.NewRow();
            }
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimnotetypeid", 6.ToString());
            clR.SetFields("note", txtOpenNote.Text);
            clR.SetFields("credate", DateTime.Now.ToString());
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("notetext", txtOpenNote.Text);
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
            hfOpen.Value = "";
            SQL = "update claim " +
                  "set status = '" + cboClaimStatus.Text + "', " +
                  "closedate = null, opendate = '" + DateTime.Today + "' " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "insert into claimopenhistory " +
                  "(claimid, opendate, openby) " +
                  "values (" + hfClaimID.Value + ",'" +
                  DateTime.Today + "'," + hfUserID.Value + ") ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            HideOpen();
        }
        private void CheckClaimDuplicate()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            tbLock.Visible = false;
            if (hfContractID.Value.Length > 0)
            {
                SQL = "select * from claim " +
                      "where contractid = " + hfContractID.Value + " " +
                      "and credate > '" + DateTime.Today.AddDays(-3) + "' ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 1)
                {
                    lblLocked.Text = "There is a claim already entered within the last 3 days.";
                    tbLock.Visible = true;
                    return;
                }
            }
            if (hfServiceCenterID.Value.Length > 0)
            {
                if (txtRONumber.Text.Length > 0)
                {
                    SQL = "select * from claim " +
                          "where servicecenterid = " + hfServiceCenterID.Value + " " +
                          "and ronumber = '" + txtRONumber.Text + "' ";
                    clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clR.RowCount() > 1)
                    {
                        lblLocked.Text = "There is a claim already have RO Number for this Service Ceneter.";
                        tbLock.Visible = true;
                        return;
                    }
                }
            }
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }
        private void ColorLossCode()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();

            rgLossCode.Columns[3].Visible = true;
            for (int cnt2 = 0; cnt2 <= rgLossCode.Items.Count - 1; cnt2++)
            {
                if (rgLossCode.Items[cnt2]["ACP"].Text == "1")
                {
                    rgLossCode.Items[cnt2]["losscode"].BackColor = Color.Red;
                }
            }
            rgLossCode.Columns[3].Visible = false;
        }
        protected void btnExpireYes_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update contract " +
                  "set status = 'Expired' " +
                  "where status = 'Paid' " +
                  "and contractid = " + hfContractID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "insert into contractnote (contractid, note, creby, credate, modby, moddate) " +
                  "values (" + hfContractID.Value + ", " +
                  "'Cancelled due to claim mileage of " + txtLossMile.Text + ".', " +
                  hfUserID.Value + ", " +
                  "'" + DateTime.Today + "', " +
                  hfUserID.Value + ", " +
                  "'" + DateTime.Today + "') ";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            HideExpire();
            GetContractInfo();
        }

        protected void btnExpireNo_Click(object sender, EventArgs e)
        {
            HideExpire();
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnTWOK_Click(object sender, EventArgs e)
        {
            HideTW();
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {

        }

        protected void btnInspectionWex_Click(object sender, EventArgs e)
        {

        }

        protected void btnCarfaxPayment_Click(object sender, EventArgs e)
        {

        }
    }
}