﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimPart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            dsStates.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsClaimPayee.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsLossCode.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsParts.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsLabor.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlPartList.Visible = true;
                pnlPartDetail.Visible = false;
                pnlAddClaimPayee.Visible = false;
                pnlSearchClaimPayee.Visible = false;
                pnlSeekLossCode.Visible = false;
                pnlLaborDetail.Visible = false;
                GetShopRate();
                //FillLossCode();
                if (CheckLock())
                {
                    LockButtons();
                }
                else
                {
                    UnlockButtons();
                }
            }
        }
        private void GetShopRate()
        {
            hfShopLabor.Value = 0.ToString();
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select shoprate from claim where claimid = " + hfClaimID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfShopLabor.Value = clC.GetFields("shoprate");
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private bool CheckLock()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "Select claimid from claim " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "And lockuserid <> " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void LockButtons()
        {
            btnAddClaimPayee.Enabled = false;
            btnAddLabor.Enabled = false;
            btnAddLossCode.Enabled = false;
            btnAddPart.Enabled = false;
            btnSCSave.Enabled = false;
            btnSeekClaimPayeeLabor.Enabled = false;
            btnSeekLossCodeLabor.Enabled = false;
            btnSeekLossCodePart.Enabled = false;
            btnSeekPayee.Enabled = false;
            btnUpdateLabor.Enabled = false;
            btnUpdatePart.Enabled = false;
        }
        private void UnlockButtons()
        {
            btnAddClaimPayee.Enabled = true;
            btnAddLabor.Enabled = true;
            btnAddLossCode.Enabled = true;
            btnAddPart.Enabled = true;
            btnSCSave.Enabled = true;
            btnSeekClaimPayeeLabor.Enabled = true;
            btnSeekLossCodeLabor.Enabled = true;
            btnSeekLossCodePart.Enabled = true;
            btnSeekPayee.Enabled = true;
            btnUpdateLabor.Enabled = true;
            btnUpdatePart.Enabled = true;
        }
        private void FillLossCode()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select losscodeid, LossCodeDesc, losscode from claimlosscode " +
                  "where losscodeid > 0 " + GetLossCodeFilter();
            dsLossCode.SelectCommand = SQL;
            rgLossCode.Rebind();
            //rgLossCode.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            //rgLossCode.DataBind();
        }
        private string GetLossCodeFilter()
        {
            long lContractID;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string sTemp = "";
            string SQL;
            lContractID = GetContractID();
            if (lContractID == 0)
            {
                return "";
            }

            SQL = "select * from contract where contractid = " + lContractID;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                return "";
            }
            else
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("hybrid")))
                {
                    sTemp = sTemp + "or losscode like 'hy%' ";
                }
                if (Convert.ToBoolean(clR.GetFields("AWD"))) {
                    sTemp = sTemp + "or losscode like 'TC%' ";
                }
                if (Convert.ToBoolean(clR.GetFields("hydraulic"))) {
                    sTemp = sTemp + "or losscode like 'SU%' ";
                }
                if (Convert.ToBoolean(clR.GetFields("LiftKit"))) {
                    sTemp = sTemp + "or losscode like 'SU%' ";
                }
                if (Convert.ToBoolean(clR.GetFields("AirBladder"))) {
                    sTemp = sTemp + "or losscode like 'SU%' ";
                }
                if (Convert.ToBoolean(clR.GetFields("LargerLiftKit"))) {
                    sTemp = sTemp + "or losscode like 'SU%' ";
                }
                if (Convert.ToBoolean(clR.GetFields("Luxury"))) {
                    sTemp = sTemp + "or losscode like 'SP%' ";
                }
                if (Convert.ToBoolean(clR.GetFields("LuxuryNC"))) {
                    sTemp = sTemp + "or losscode like 'SP%' ";
                }
                sTemp = sTemp + "or losscode like 'rr%' " + 
                                "or losscode like 'zs%' " + 
                                "or losscode like 'zl%' " +
                                "or losscode like 'zc%' " +
                                "or losscode like 'zb%' " +
                                "or losscode like 'zat%' " +
                                "or losscode like 'w%' ";

                SQL = "select * from claimlosscodelimit " +
                      "where programid = " + clR.GetFields("programid") + " " +
                      "and plantypeid = " + clR.GetFields("plantypeid");
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                    {
                        clR.GetRowNo(cnt);
                        sTemp = sTemp + "or losscode like '" + clR.GetFields("losscodeprefix") + "%' ";
                    }
                }
            }
            if (sTemp.Length > 0)
            {
                sTemp = sTemp.Substring(2, sTemp.Length - 2);
                return "and (" + sTemp + ") ";
            }
            return "";
        }
        private long GetContractID()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("contractid").Length > 0)
                {
                    return long.Parse(clR.GetFields("contractid"));
                }
            }
            return 0;
        }

        protected void rgParts_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlPartList.Visible = false;
            pnlPartDetail.Visible = true;
            hfAddPart.Value = "false";
            FillPartDetail();
            FillPartQuotes();
            LockAuthPartText();
        }
        private void FillPartDetail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            trPartLabor.Visible = false;
            SQL = "select * from claimdetail where claimdetailid = " + rgParts.SelectedValue;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimDetailID.Value = clR.GetFields("claimdetailid");
                txtPartNo.Text = clR.GetFields("partno");
                txtPartDesc.Text = clR.GetFields("Claimdesc");
                txtReqCost.Text = clR.GetFields("reqcost");
                txtReqQty.Text = clR.GetFields("reqqty");
                hfJobNo.Value = clR.GetFields("jobno");
                txtAuthQty.Text = clR.GetFields("AuthQty");
                txtAuthCost.Text = clR.GetFields("Authcost");
                txtPartTax.Text = clR.GetFields("taxper");
                if (Convert.ToDouble(clR.GetFields("taxper")) == 0)
                {
                    txtPartTax.Text = hfPartTax.Value;
                }
                txtShipping.Text = clR.GetFields("shippingcost");
                txtLossCodePart.Text = clR.GetFields("losscode");
                txtLossCodePartDesc.Text = GetLossCodeDesc(clR.GetFields("losscode"));
                hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                hfJobNo.Value = clR.GetFields("jobno");
                hfLossCodeSeek.Value = "Part";
                GetPartLabor();
                GetPayee();
            }
        }
        private void GetPartLabor()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail " +
                  "where claimid = '" + hfClaimID.Value + "' " +
                  "and claimdetailtype = 'Labor' " +
                  "and jobno = '" + hfJobNo.Value + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLaborReqAmtPart.Text = clR.GetFields("reqqty");
                txtLaborReqRatePart.Text = hfShopLabor.Value;
                txtLaborAuthAmtPart.Text = clR.GetFields("authqty");
                txtLaborAuthRatePart.Text = clR.GetFields("authcost");
            }
        }
        private string GetLossCodeDesc(string xLossCode)
        {
            if (xLossCode.Length == 0)
            {
                return "";
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimlosscode where losscode = '" + xLossCode + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("losscodedesc");
            }
            return "";
        }
        private void GetPayee()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfClaimPayeeID.Value == "")
            {
                return;
            }
            SQL = "select * from claimpayee where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (hfLossCodeSeek.Value == "Labor")
                {
                    txtClaimPayeeNameLabor.Text = clR.GetFields("payeename");
                    txtClaimPayeeNoLabor.Text = clR.GetFields("payeeno");
                }
                else
                {
                    txtClaimPayee.Text = clR.GetFields("payeename");
                    txtPayeeNo.Text = clR.GetFields("payeeno");
                }
            }
        }
        protected void btnAddPart_Click(object sender, EventArgs e)
        {
            hfJobNo.Value = "";
            txtLossCodePart.Text = "";
            CreClaimDetail();
            pnlPartList.Visible = false;
            pnlPartDetail.Visible = true;
            txtPayeeNo.Text = "";
            txtPartNo.Text = "";
            txtPartDesc.Text = "";
            txtAuthCost.Text = 0.ToString();
            txtReqQty.Text = 0.ToString();
            txtReqCost.Text = 0.ToString();
            txtAuthQty.Text = 0.ToString();
            txtAuthQty.Text = 0.ToString();
            txtPayeeNo.Text = "";
            txtClaimPayee.Text = "";
            txtLossCodePart.Text = "";
            txtLossCodePartDesc.Text = "";
            txtShipping.Text = 0.ToString();
            trPartLabor.Visible = true;
            GetClaimPayeeID();
            txtLaborReqAmtPart.Text = hfShopLabor.Value;
            SetLossCodePart();
        }
        private void CreClaimDetail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail where claimdetailid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimdetailtype", "Part");
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("claimdetailstatus", "Requested");
                clR.SetFields("creby", hfUserID.Value);
                GetNextJobNo();
                clR.SetFields("jobno", hfJobNo.Value);
                hfAddPart.Value = "true";
                clR.AddRow();
                clR.SaveDB();
            }
            SQL = "update claim " +
                  "set moddate = '" + DateTime.Today + "', " +
                  "modby = " + hfUserID.Value + " " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "select max(claimdetailid) as mcd from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and creby = " + hfUserID.Value + " " +
                  "and jobno = '" + hfJobNo.Value + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimDetailID.Value = clR.GetFields("mcd");
            }
        }
        private void SetLossCodePart()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select losscode from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLossCodePart.Text = clR.GetFields("losscode");
            }
            if (txtLossCodePart.Text.Length > 0)
            {
                SQL = "select * from claimlosscode where losscode = '" + txtLossCodePart.Text + "' ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    txtLossCodePartDesc.Text = clR.GetFields("losscodedesc");
                }
            }
        }
        private void GetClaimPayeeID()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clCP = new clsDBO.clsDBO();
            SQL = "select * from claim c " +
                  "inner join servicecenter sc on sc.servicecenterid = c.servicecenterid " +
                  "where claimid = " + hfClaimID.Value;
            clCP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCP.RowCount() > 0)
            {
                clCP.GetRow();
                SQL = "select * from claimpayee where payeeno = '" + clCP.GetFields("servicecenterno") + "' ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                }
                else
                {
                    clR.NewRow();
                    clR.SetFields("payeeno", clCP.GetFields("servicecenterno"));
                    clR.SetFields("payeename", clCP.GetFields("servicecentername"));
                    clR.SetFields("addr1", clCP.GetFields("addr1"));
                    clR.SetFields("addr2", clCP.GetFields("addr2"));
                    clR.SetFields("city", clCP.GetFields("city"));
                    clR.SetFields("zip", clCP.GetFields("zip"));
                    clR.SetFields("phone", clCP.GetFields("phone"));
                    clR.SetFields("email", clCP.GetFields("email"));
                    clR.AddRow();
                    clR.SaveDB();
                    SQL = "select * from claimpayee where payeeno = '" + clCP.GetFields("servicecenterno") + "' ";
                    clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                    if (clR.RowCount() > 0)
                    {
                        clR.GetRow();
                        hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                    }
                }
            }
        }
        protected void btnSeekPayee_Click(object sender, EventArgs e)
        {
            hfLossCodeSeek.Value = "true";
            pnlPartDetail.Visible = false;
            pnlSearchClaimPayee.Visible = true;
        }

        protected void btnCancelPart_Click(object sender, EventArgs e)
        {
            pnlPartDetail.Visible = false;
            pnlPartList.Visible = true;
            if (hfAddPart.Value.ToLower() == "true")
            {
                DeletePart();
            }
            rgParts.Rebind();
        }
        private void DeletePart()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select max(claimdetailid) as ClaimDetailID from claimdetail " +
                  "where creby = " + hfUserID.Value + " " +
                  "and claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                SQL = "delete claimdetail where claimdetailid = " + clR.GetFields("claimdetailid");
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            }
        }
        protected void btnUpdatePart_Click(object sender, EventArgs e)
        {
            UpdatePart();
            hfAddPart.Value = "false";
            rgParts.Rebind();
            pnlPartDetail.Visible = false;
            pnlPartList.Visible = true;
        }
        private void UpdatePart()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            bool bAdd = false;
            SQL = "select * from claimdetail where claimdetailid = " + hfClaimDetailID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                GetNextJobNo();
            }
            else
            {
                clR.NewRow();
                GetNextJobNo();
            }
            if (hfAddPart.Value.ToLower() == "true")
            {
                bAdd = true;
            }
            clR.SetFields("jobno", hfJobNo.Value);
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimdetailtype", "Part");
            //clR.SetFields("claimdetailstatus", "Requested");
            clR.SetFields("reqqty", txtReqQty.Text);
            clR.SetFields("partno", txtPartNo.Text);
            clR.SetFields("Claimdesc", txtPartDesc.Text);
            clR.SetFields("ReqCost", txtReqCost.Text);
            if (txtAuthCost.Text.Length == 0)
            {
                txtAuthCost.Text = 0.ToString();
            }
            clR.SetFields("Authcost", txtAuthCost.Text);
            clR.SetFields("AuthQty", txtAuthQty.Text);
            if (txtPartTax.Text.Length == 0)
            {
                txtPartTax.Text = 0.ToString();
            }
            clR.SetFields("taxPer", Convert.ToDouble(txtPartTax.Text).ToString());
            if (clR.GetFields("claimdetailstatus") == "")
            {
                clR.SetFields("claimdetailstatus", "Requested");
            }
            if (txtShipping.Text.Length > 0)
            {
                clR.SetFields("shippingcost", txtShipping.Text);
            }
            GetTax();
            clR.SetFields("losscode", txtLossCodePart.Text);
            
            if (txtReqQty.Text.Length == 0)
            {
                txtReqQty.Text = 0.ToString();
            }
            if (txtReqCost.Text.Length == 0)
            {
                txtReqCost.Text = 0.ToString();
            }
            clR.SetFields("reqamt", ((Convert.ToDouble(txtReqCost.Text) * Convert.ToDouble(txtReqQty.Text)) + Convert.ToDouble(txtShipping.Text)).ToString());
            if (txtAuthQty.Text.Length == 0)
            {
                txtAuthQty.Text = 0.ToString();
            }
            if (txtAuthCost.Text.Length == 0)
            {
                txtAuthCost.Text = 0.ToString();
            }
            if (txtPartTax.Text.Length == 0)
            {
                txtPartTax.Text = 0.ToString();
            }
            clR.SetFields("ratetypeid", 1.ToString());
            clR.SetFields("taxamt", (Convert.ToDouble(txtAuthCost.Text) * Convert.ToDouble(txtAuthQty.Text) * (Convert.ToDouble(txtPartTax.Text) / 100)).ToString());
            clR.SetFields("taxamt", (Decimal.Round(Convert.ToDecimal(clR.GetFields("taxamt")) * 100) / 100).ToString());
            clR.SetFields("authamt", ((Convert.ToDouble(txtAuthCost.Text) * Convert.ToDouble(txtAuthQty.Text)) + Convert.ToDouble(txtShipping.Text)).ToString());
            clR.SetFields("TotalAmt", (Convert.ToDouble(clR.GetFields("authamt")) + Convert.ToDouble(clR.GetFields("taxamt"))).ToString());
            clR.SetFields("claimpayeeid", hfClaimPayeeID.Value);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
        
            if (clR.RowCount() == 0)
            {
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }
            clR.SaveDB();
            if (bAdd == true)
            {
                SaveLaborPart();
            }
            rgParts.Rebind();
            rgLabor.Rebind();
            SQL = "update claim " +
                  "set moddate = '" + DateTime.Today + "', " +
                  "modby = " + hfUserID.Value + " " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private bool CheckInspect()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select inspect from claim " +
                  "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("inspect").ToLower() == "True")
                {
                    return true;
                }
            }
            return false;
        }
        private void SaveLaborPart()
        {
            if (txtLaborReqRatePart.Text.Length == 0)
            {
                return;
            }
            if (txtLaborReqAmtPart.Text.Length == 0)
            {
                return;
            }
            if (Convert.ToDouble(txtLaborReqAmtPart.Text) == 0)
            {
                return;
            }
            if (Convert.ToDouble(txtLaborReqRatePart.Text) == 0)
            {
                return;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and jobno = '" + hfJobNo.Value + "' " +
                  "and claimdetailtype = 'Labor' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
            }
            else
            {
                clR.NewRow();
            }
            if (txtLaborAuthAmtPart.Text.Length == 0) 
            {
                txtLaborAuthAmtPart.Text = 0.ToString();
            }
            if (txtLaborAuthRatePart.Text.Length == 0)
            {
                txtLaborAuthRatePart.Text = 0.ToString();
            }
            if (txtLaborReqAmtPart.Text.Length == 0)
            {
                txtLaborReqAmtPart.Text = 0.ToString();
            }
            if (txtLaborReqRatePart.Text.Length == 0)
            {
                txtLaborReqRatePart.Text = 0.ToString();
            }
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("jobno", hfJobNo.Value);
            clR.SetFields("claimdetailtype", "Labor");
            clR.SetFields("reqqty", txtLaborReqAmtPart.Text);
            clR.SetFields("claimdetailstatus", "Requested");
            clR.SetFields("reqcost", txtLaborReqRatePart.Text);
            clR.SetFields("ratetypeid", 1.ToString());
            clR.SetFields("authcost", txtLaborAuthRatePart.Text);
            clR.SetFields("authqty", txtLaborAuthAmtPart.Text);
            GetTax();
            clR.SetFields("losscode", txtLossCodePart.Text);
            clR.SetFields("reqamt", (Convert.ToDouble(clR.GetFields("reqqty")) * Convert.ToDouble(clR.GetFields("reqcost"))).ToString());
            if (txtLaborTax.Text.Length == 0)
            {
                txtLaborTax.Text = "0";
            }
            clR.SetFields("taxamt", (Decimal.Round(Convert.ToDecimal(txtLaborAuthRatePart.Text) * Convert.ToDecimal(txtLaborAuthAmtPart.Text) * Convert.ToDecimal(txtLaborTax.Text)) / 100).ToString());
            clR.SetFields("authamt", (Convert.ToDouble(txtLaborAuthRatePart.Text) * Convert.ToDouble(txtLaborAuthAmtPart.Text)).ToString());
            clR.SetFields("totalamt", (Convert.ToDouble(clR.GetFields("authamt")) + Convert.ToDouble(clR.GetFields("taxamt"))).ToString());
            clR.SetFields("claimpayeeid", hfClaimPayeeID.Value);
            clR.SetFields("claimdesc", txtPartDesc.Text);

            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            if (clR.RowCount() == 0)
            {
                clR.SetFields("ratetypeid", 1.ToString());
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }
            clR.SaveDB();
        }
        private void GetTax()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("parttax").Length > 0)
                {
                    hfPartTax.Value = (Convert.ToDouble(clR.GetFields("parttax")) * 100).ToString();
                }
                if (clR.GetFields("labortax").Length > 0)
                {
                    hfLaborTax.Value = (Convert.ToDouble(clR.GetFields("labortax")) * 100).ToString();
                }
            }
            if (hfPartTax.Value.Length == 0)
            {
                hfPartTax.Value = 0.ToString();
            }
            if (hfLaborTax.Value.Length == 0)
            {
                hfLaborTax.Value = 0.ToString();
            }
        }
        private void GetClaimDetailID()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and jobno = '" + hfJobNo.Value + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimDetailID.Value = clR.GetFields("claimdetailid");
            }
        }

        private void GetNextJobNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfJobNo.Value.Length == 0)
            {
                SQL = "select max(jobno) as mJob from claimdetail " +
                      "where claimid = " + hfClaimID.Value + " " +
                      "and jobno like 'j%' ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() == 0)
                {
                    hfJobNo.Value = "J01";
                }
                else
                {
                    clR.GetRow();
                    if (clR.GetFields("mjob").Length == 0)
                    {
                        hfJobNo.Value = "J01";
                    }
                    else
                    {
                        hfJobNo.Value = "J" + (long.Parse(clR.GetFields("mjob").Substring(clR.GetFields("mjob").Length - 2, 2)) + 1).ToString("00");
                    }
                }
            }

            SQL = "select min(jobno) as mJob from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and jobno like 'j%' ";
            if (txtLossCodePart.Text.Length > 0)
            {
                SQL = SQL + "and losscode = '" + txtLossCodePart.Text.Trim() + "' ";
            }
            else
            {
                SQL = SQL + "and losscode is null ";
            }
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("mjob").Length > 0)
                {
                    hfJobNo.Value = clR.GetFields("mJob");
                }
            }
        }
        private void GetNextJobNoLabor()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfJobNo.Value.Length == 0)
            {
                SQL = "select max(jobno) as mJob from claimdetail " +
                      "where claimid = " + hfClaimID.Value + " " +
                      "and jobno like 'j%' ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() == 0)
                {
                    hfJobNo.Value = "J01";
                }
                else
                {
                    clR.GetRow();
                    if (clR.GetFields("mjob").Length == 0)
                    {
                        hfJobNo.Value = "J01";
                    }
                    else
                    {
                        hfJobNo.Value = "J" + (long.Parse(clR.GetFields("mjob").Substring(clR.GetFields("mjob").Length - 2, 2)) + 1).ToString("00");
                    }
                }
            }

            SQL = "select min(jobno) as mJob from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and jobno like 'j%' ";
            if (txtLossCodeLabor.Text.Length > 0)
            {
                SQL = SQL + "and losscode = '" + txtLossCodeLabor.Text.Trim() + "' ";
            }
            else
            {
                SQL = SQL + "and losscode is null ";
            }
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("mjob").Length > 0)
                {
                    hfJobNo.Value = clR.GetFields("mJob");
                }
            }
        }
        protected void btnAddClaimPayee_Click(object sender, EventArgs e)
        {
            pnlSearchClaimPayee.Visible = false;
            pnlAddClaimPayee.Visible = true;
            txtServiceCenterName.Text = "";
            txtZip.Text = "";
            txtAddr1.Text = "";
            txtAddr2.Text = "";
            cboState.ClearSelection();
            txtPhone.Text = "";
        }

        protected void btnSCCancel_Click(object sender, EventArgs e)
        {
            pnlSearchClaimPayee.Visible = true;
            pnlAddClaimPayee.Visible = false;
        }

        protected void btnSCSave_Click(object sender, EventArgs e)
        {
            AddServiceCenter();
            AddClaimPayee();
            rgClaimPayee.Rebind();
            pnlSearchClaimPayee.Visible = true;
            pnlAddClaimPayee.Visible = false;
        }
        private void AddClaimPayee()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimpayee where claimpayeeid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clR.NewRow();
            clR.SetFields("payeeno", hfServiceCenterNo.Value);
            clR.SetFields("payeename", txtServiceCenterName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.SetFields("fax", txtFax.Text);
            clR.SetFields("email", txtEMail.Text);
            clR.AddRow();
            clR.SaveDB();
        }
        private void AddServiceCenter()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from servicecenter where servicecenterid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clR.NewRow();
            GetNextServiceCenterNo();
            clR.SetFields("servicecenterno", hfServiceCenterNo.Value);
            clR.SetFields("servicecentername", txtServiceCenterName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.SetFields("fax", txtFax.Text);
            clR.SetFields("email", txtEMail.Text);
            clR.AddRow();
            clR.SaveDB();
        }
        private void GetNextServiceCenterNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select max(servicecenterno) as mSCN from servicecenter where servicecenterno like 'RF0%' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clR.GetRow();
            hfServiceCenterNo.Value = "RF" + (long.Parse(clR.GetFields("mscn").Substring(clR.GetFields("mscn").Length - 6, 6)) + 1).ToString("0000000");
        }
        protected void rgClaimPayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimPayeeID.Value = rgClaimPayee.SelectedValue.ToString();
            string SQL;
            clsDBO.clsDBO clCP = new clsDBO.clsDBO();
            SQL = "select * from claimpayee where claimpayeeid = " + rgClaimPayee.SelectedValue;
            clCP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clCP.RowCount() > 0)
            {
                clCP.GetRow();
                if (hfLossCodeSeek.Value == "Labor")
                {
                    txtClaimPayeeNameLabor.Text = clCP.GetFields("payeename");
                    txtClaimPayeeNoLabor.Text = clCP.GetFields("payeeno");
                    pnlLaborDetail.Visible = true;
                    pnlSearchClaimPayee.Visible = false;
                }
                else
                {
                    txtClaimPayee.Text = clCP.GetFields("payeename");
                    txtPayeeNo.Text = clCP.GetFields("payeeno");
                    pnlPartDetail.Visible = true;
                    pnlSearchClaimPayee.Visible = false;
                }
            }
        }

        protected void btnSeekLossCodePart_Click(object sender, EventArgs e)
        {
            hfLossCodeSeek.Value = "Part";
            pnlPartDetail.Visible = false;
            pnlSeekLossCode.Visible = true;
            rgLossCode.Rebind();
            ColorLossCode();
        }

        protected void rgLossCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            if (hfLossCodeSeek.Value == "Part")
            {
                txtLossCodePart.Text = rgLossCode.SelectedValue.ToString();
                SQL = "select * from claimlosscode where losscode = '" + rgLossCode.SelectedValue + "' ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    txtLossCodePartDesc.Text = clR.GetFields("losscodedesc");
                }
                pnlPartDetail.Visible = true;
                pnlSeekLossCode.Visible = false;
                GetNextJobNo();
                LockAuthPartText();
            }
            if (hfLossCodeSeek.Value == "Labor")
            {
                txtLossCodeLabor.Text = rgLossCode.SelectedValue.ToString();
                SQL = "select * from claimlosscode where losscode = '" + rgLossCode.SelectedValue + "' ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    txtLossCodeDescLabor.Text = clR.GetFields("losscodedesc");
                }
                pnlLaborDetail.Visible = true;
                pnlSeekLossCode.Visible = false;
                GetNextJobNoLabor();
                LockAuthLaborText();
            }
        }
        private void LockAuthLaborText() 
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("AllCoverage")))
                {
                    return;
                }
            }

            if (GetSaleDate() < DateTime.Parse("1/1/2019"))
            {
                return;
            }

            SQL = "select * from claimlosscode clc " +
                  "inner join contractcoverage cc " +
                  "on cc.CoverageCode = clc.LossCodePrefix " +
                  "inner join contract c " +
                  "on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID " +
                  "inner join claim cl " +
                  "on cl.ContractID = c.ContractID " +
                  "where cl.claimid = " + hfClaimID.Value + " ";
            if (txtLossCodeLabor.Text.Length == 0)
            {
                return;
            }
            SQL = SQL + "and cc.coveragecode = '" + txtLossCodeLabor.Text.Substring(0, 4) + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                SQL = "update claimdetail " +
                      "set losscodelock = 1 " +
                      "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                txtLaborAuthHours.Enabled = false;
                txtLaborAuthRate.Enabled = false;
                txtLaborTax.Enabled = false;
                txtLossCodeLabor.BackColor = Color.Red;
            }
            else
            {
                SQL = "update claimdetail " +
                      "set losscodelock = 0 " +
                      "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                txtLaborAuthHours.Enabled = true;
                txtLaborAuthRate.Enabled = true;
                txtLaborTax.Enabled = true;
                txtAuthCost.Enabled = true;
                txtLossCodeLabor.BackColor = Color.White;
            }
        }
        private DateTime GetSaleDate()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select c.saledate from contract c inner join claim cl on cl.contractid = c.contractid ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return DateTime.Parse(clR.GetFields("saledate"));
            }
            return DateTime.Parse("1/1/1950");
        }
        private void LockAuthPartText() 
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("AllCoverage")))
                {
                    return;
                }
            }
            if (GetSaleDate() < DateTime.Parse("1/1/2019"))
            {
                return;
            }

            SQL = "select * from claimlosscode clc " +
                  "inner join contractcoverage cc " +
                  "on cc.CoverageCode = clc.LossCodePrefix " +
                  "inner join contract c " +
                  "on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID " +
                  "inner join claim cl " +
                  "on cl.ContractID = c.ContractID " +
                  "where cl.claimid = " + hfClaimID.Value + " ";
            if (txtLossCodePart.Text.Length == 0)
            {
                return;
            }
            SQL = SQL + "and cc.coveragecode = '" + txtLossCodePart.Text.Substring(0, 4) + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                SQL = "update claimdetail " +
                      "set losscodelock = 1 " +
                      "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                txtAuthCost.Enabled = false;
                txtAuthQty.Enabled = false;
                txtPartTax.Enabled = false;
                txtLaborAuthAmtPart.Enabled = false;
                txtLaborAuthRatePart.Enabled = false;
                txtLossCodePart.BackColor = Color.Red;
            }
            else
            {
                SQL = "update claimdetail " +
                      "set losscodelock = 0 " +
                      "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                txtAuthCost.Enabled = true;
                txtAuthQty.Enabled = true;
                txtPartTax.Enabled = true;
                txtLaborAuthAmtPart.Enabled = true;
                txtLaborAuthRatePart.Enabled = true;
                txtLossCodePart.BackColor = Color.White;
            }
        }

        protected void btnAddLabor_Click(object sender, EventArgs e)
        {
            pnlPartList.Visible = false;
            pnlLaborDetail.Visible = true;
            hfClaimDetailID.Value = 0.ToString();
            ClearLabor();
            txtLaborReqRate.Text = hfShopLabor.Value;
            hfJobNo.Value = "";
            txtLossCodePart.Text = "";
            SetLaborLossCode();
        }
        private void SetLaborLossCode()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select losscode from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLossCodeLabor.Text = clR.GetFields("losscode");
            }
            if (txtLossCodePart.Text.Length > 0)
            {
                SQL = "select * from claimlosscode where losscode = '" + txtLossCodePart.Text + "' ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    txtLossCodeDescLabor.Text = clR.GetFields("losscodedesc");
                }
            }
        }
        private void ClearLabor()
        {
            txtClaimPayee.Text = "";
            txtPayeeNo.Text = "";
            txtLaborReqHours.Text = "1";
            txtLaborReqRate.Text = "";
            txtLaborAuthHours.Text = "1";
            txtLaborAuthRate.Text = "";
            hfClaimDetailID.Value = 0.ToString();
            GetClaimPayeeID();
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimpayee where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtClaimPayeeNoLabor.Text = clR.GetFields("payeeno");
                txtClaimPayeeNameLabor.Text = clR.GetFields("payeename");
            }
        }
        protected void btnSeekLossCodeLabor_Click(object sender, EventArgs e)
        {
            hfLossCodeSeek.Value = "Labor";
            pnlLaborDetail.Visible = false;
            pnlSeekLossCode.Visible = true;
            rgLossCode.Rebind();
            ColorLossCode();
        }

        protected void btnSeekClaimPayeeLabor_Click(object sender, EventArgs e)
        {
            hfLossCodeSeek.Value = "Labor";
            pnlLaborDetail.Visible = false;
            pnlSearchClaimPayee.Visible = true;
        }

        protected void btnCancelLabor_Click(object sender, EventArgs e)
        {
            pnlLaborDetail.Visible = false;
            pnlPartList.Visible = true;
            rgLabor.Rebind();
        }

        protected void btnUpdateLabor_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                GetNextJobNoLabor();
            }
            else
            {
                clR.NewRow();
                GetNextJobNoLabor();
                clR.SetFields("claimid", hfClaimID.Value);
            }
            clR.SetFields("claimdetailtype", "Labor");
            if (txtLaborReqRate.Text == "") 
            {
                txtLaborReqRate.Text = 0.ToString();
            }
            if (txtLaborReqHours.Text == "") 
            {
                txtLaborReqHours.Text = 0.ToString();
            }
            if (txtLaborAuthRate.Text == "") 
            {
                txtLaborAuthRate.Text = 0.ToString();
            }
            if (txtLaborAuthHours.Text == "") 
            {
                txtLaborAuthHours.Text = 0.ToString();
            }
            if (txtLaborTaxRate.Text.Length == 0) 
            {
                txtLaborTaxRate.Text = 0.ToString();
            }
            clR.SetFields("jobno", hfJobNo.Value);
            clR.SetFields("taxper", txtLaborTaxRate.Text);
            clR.SetFields("Reqqty", txtLaborReqHours.Text);
            clR.SetFields("reqcost", txtLaborReqRate.Text);
            clR.SetFields("Authqty", txtLaborAuthHours.Text);
            clR.SetFields("Authcost", txtLaborAuthRate.Text);
            clR.SetFields("ratetypeid", 1.ToString());
            clR.SetFields("losscode", txtLossCodeLabor.Text);
            clR.SetFields("reqamt", (Convert.ToDouble(txtLaborReqHours.Text) * Convert.ToDouble(txtLaborReqRate.Text)).ToString());
            if (hfLaborTax.Value == "") 
            {
                hfLaborTax.Value = 0.ToString();
            }
            if (txtLaborTaxRate.Text.Length == 0) 
            {
                txtLaborTaxRate.Text = 0.ToString();
            }
            clR.SetFields("taxper", Convert.ToDouble(txtLaborTaxRate.Text).ToString());
            clR.SetFields("taxamt", (Convert.ToDouble(txtLaborAuthHours.Text) * Convert.ToDouble(txtLaborAuthRate.Text) * (Convert.ToDouble(txtLaborTaxRate.Text) / 100)).ToString());
            clR.SetFields("taxamt", (Decimal.Round(Convert.ToDecimal(clR.GetFields("taxamt")) * 100) / 100).ToString());
            clR.SetFields("authamt", (Convert.ToDouble(txtLaborAuthHours.Text) * Convert.ToDouble(txtLaborAuthRate.Text)).ToString());
            clR.SetFields("totalamt", (Convert.ToDouble(clR.GetFields("authamt")) + Convert.ToDouble(clR.GetFields("taxamt"))).ToString());
            clR.SetFields("claimpayeeid", hfClaimPayeeID.Value);
            clR.SetFields("claimdesc", txtLaborDesc.Text);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            if (clR.GetFields("claimdetailstatus") == "Authorized" && !CheckInspect()) 
            {
                lblAuthorizedError.Text = "Inspection has not been completed.";
                pnlPartList.Visible = true;
                pnlPartDetail.Visible = false;
                pnlLaborDetail.Visible = false;
                clR.SetFields("claimdetailstatus", "Requested");
            }
            if (clR.GetFields("claimdetailstatus") == "Approved" && !CheckInspect()) 
            {
                lblAuthorizedError.Text = "Inspection has not been completed.";
                pnlPartList.Visible = true;
                pnlPartDetail.Visible = false;
                pnlLaborDetail.Visible = false;
                clR.SetFields("claimdetailstatus", "Requested");
            }
            if (clR.GetFields("claimdetailstatus") == "") 
            {
                clR.SetFields("claimdetailstatus", "Requested");
            }

            if (clR.RowCount() == 0) 
            {
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }
            clR.SaveDB();
            pnlPartList.Visible = true;
            pnlLaborDetail.Visible = false;
            rgLabor.Rebind();
            SQL = "update claim " +
                  "set moddate = '" + DateTime.Today + "', " +
                  "modby = " + hfUserID.Value + " " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private bool CheckLimitApproved(double xAmt)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            double dLimit;
            SQL = "select claimapprove from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimapprove"));
            }
            else
            {
                dLimit = 0;
            }
            SQL = "select sum(authamt) as Amt from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and (claimdetailstatus = 'approved' " +
                  "or claimdetailstatus = 'Authorized') ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt")))
                    {
                        return true;
                    }
                }
                else
                {
                    if (dLimit > xAmt)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private bool CheckLimitAuthorized(double xAmt)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            double dLimit;
            SQL = "select claimauth from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimauth"));
            }
            else
            {
                dLimit = 0;
            }
            SQL = "select sum(authamt) as Amt from claimdetail " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and (claimdetailstatus = 'approved' " +
                  "or claimdetailstatus = 'Authorized') ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt")))
                    {
                        return true;
                    }
                }
                else
                {
                    if (dLimit > xAmt)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        protected void rgLabor_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlPartList.Visible = false;
            pnlLaborDetail.Visible = true;
            hfClaimDetailID.Value = rgLabor.SelectedValue.ToString();
            FillLaborDetail();
            LockAuthLaborText();
        }
        private void FillLaborDetail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdetail where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("qty") == "")
                {
                    clR.SetFields("qty", 0.ToString());
                }
                if (clR.GetFields("cost") == "")
                {
                    clR.SetFields("cost", 0.ToString());
                }
                txtLaborReqHours.Text = clR.GetFields("reqqty");
                txtLaborReqRate.Text = clR.GetFields("reqcost");
                txtLaborAuthRate.Text = clR.GetFields("authcost");
                txtLaborAuthHours.Text = clR.GetFields("authqty");
                txtLaborTaxRate.Text = Convert.ToDouble(clR.GetFields("taxper")).ToString();
                if (Convert.ToDouble(clR.GetFields("taxper")) == 0)
                {
                    txtLaborTaxRate.Text = hfLaborTax.Value;
                }
                txtLaborTax.Text = clR.GetFields("taxamt");
                if (txtLaborAuthHours.Text.Length == 0)
                {
                    txtLaborAuthHours.Text = 0.ToString();
                }
                if (txtLaborAuthRate.Text.Length == 0)
                {
                    txtLaborAuthRate.Text = 0.ToString();
                }
                if (txtLaborTax.Text.Length == 0)
                {
                    txtLaborTax.Text = 0.ToString();
                }
                txtLossCodeLabor.Text = clR.GetFields("losscode");
                txtLossCodeDescLabor.Text = GetLossCodeDesc(clR.GetFields("losscode"));
                hfLossCodeSeek.Value = "Labor";
                hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                txtLaborDesc.Text = clR.GetFields("claimdesc");
                GetPayee();
            }
        }
        protected void txtPartNo_TextChanged(object sender, EventArgs e)
        {
            GetPartInfo();
            txtPartDesc.Focus();
        }
        private void GetPartInfo()
        {
            string sVIN;
            string sMake;
            string sPartDesc;
            long lQty;
            if (txtReqQty.Text.Length == 0)
            {
                lQty = 1;
            }
            else
            {
                lQty = long.Parse(txtReqQty.Text);
            }

            sVIN = hfClaimDetailID.Value;
            sVIN = GetVIN();
            sMake = GetMake(sVIN);
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claimdetail " +
                  "set partno = '" + txtPartNo.Text + "' " +
                  "where claimdetailid = " + hfClaimDetailID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);

            Process.Start(@"C:\ProcessProgram\CKParts\CKParts.exe", hfClaimDetailID.Value).WaitForExit();
            sPartDesc = GetPartDesc();
            if (sPartDesc.Length > 0)
            {
                txtPartDesc.Text = sPartDesc;
            }
            FillPartQuotes();
        }
        private void FillPartQuotes()
        {
            /*'string SQL;
            'clsDBO.clsDBO clR = new clsDBO.clsDBO();
            'SQL = "select claimdetailquoteid, companyname, partno, partdesc, instock, qty, listprice, yourcost, "
            'SQL = SQL + "case when oem <> 0 then "
            'SQL = SQL + "'OEM' else 'Aftermarket' end as QuoteType, partcost, shipping, orderid, totalordercost "
            'SQL = SQL + "from claimdetailquote cdq "
            'SQL = SQL + "inner join claimcompany cc on cc.claimcompanyid = cdq.claimcompanyid "
            'SQL = SQL + "where claimdetailid = " & hfClaimDetailID.Value
            'rgClaimDetailQuote.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            'rgClaimDetailQuote.DataBind()*/
        }
        private string GetPartDesc()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimdesc from claimdetail where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("claimdesc");
            }
            return "";
        }
        private string GetMake(string xVIN)
        {
            if (xVIN.Length == 0)
            {
                return "";
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from vin.dbo.vin v " +
                  "inner join vin.dbo.basicdata bd on v.vinid = bd.vinid " +
                  "where v.vin = '" + xVIN.Substring(0, 11) + "' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("make");
            }
            return "";
        }
        private string GetVIN()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select vin from contract c " +
                  "inner join claim cl on c.contractid = cl.contractid " +
                  "where cl.claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("vin");
            }
            return "";
        }
        protected void btnSendQuote_Click(object sender, EventArgs e)
        {
            string sVIN;
            VeritasGlobalToolsV2.clsParts clPart = new VeritasGlobalToolsV2.clsParts();
            sVIN = GetVIN();
            clPart.PartNo = txtPartNo.Text;
            clPart.PartName = txtPartDesc.Text;
            clPart.ClaimID = long.Parse(hfClaimID.Value);
            clPart.VIN = sVIN;
            clPart.SendPartRequest();
        }

        private void ProcessPurchase()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            double dPartCost = 0;
            string sJobNo = 0.ToString();
            string sLossCode = 0.ToString();
            lblApprovedError.Text = "";

            SQL = "select * from claimdetailquote here claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("orderid") != "")
                {
                    lblApprovedError.Text = "This part has been ordered all ready";
                    return;
                }
                if (Convert.ToDouble(clR.GetFields("youcost")) < 50)
                {
                    lblApprovedError.Text = "This part is to low of cost for purchase.";
                    return;
                }
                if (!CheckLimitAuthorized(Convert.ToDouble(clR.GetFields("yourcost")) + 42.5))
                {
                    lblApprovedError.Text = "You are not authorized to make purchase.";
                    return;
                }
                Process.Start(@"C:\ProcessProgram\CKPartPurchase\CKPartPurchase.exe", clR.GetFields("claimdetailquoteid") + " " + hfUserID.Value).WaitForExit();
                FillPartQuotes();

            }
            SQL = "select * from claimdetailquote where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("orderid") == "")
                {
                    return;
                }
                dPartCost = Convert.ToDouble(clR.GetFields("partcost"));
            }

            SQL = "select * from claimdetail where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("authamt", dPartCost.ToString());
                clR.SetFields("taxper", 0.ToString());
                clR.SetFields("taxamt", 0.ToString());
                clR.SetFields("totalamt", dPartCost.ToString());
                clR.SetFields("claimdetailstatus", "Authorized");
                clR.SetFields("claimpayeeid", "7351");
                clR.SetFields("dateauth", DateTime.Today.ToString());
                clR.SetFields("authby", hfUserID.Value.ToString());
                sJobNo = clR.GetFields("jobno");
                sLossCode = clR.GetFields("losscode");
                clR.SaveDB();
            }
            SQL = "insert into claimdetail " +
                  "(claimid,claimdetailtype, JobNo; reqqty,reqcost,authqty, quthcost,reqamt,losscode,authamt,taxper,taxamt,claimdetailstatus, dateauth, authby, credate, creby, moddate, modby,ClaimDesc, claimpayeeid) " +
                  "values " +
                  "(" + hfClaimID.Value + ",'Part','" + sJobNo + "', 1, 42.50, 1, 42.50, 42.50,'" + sLossCode + "',42.50,0,0, 'Authorized','" + DateTime.Today + "'," + hfUserID.Value + ",'" + DateTime.Today + "'," + hfUserID.Value + ",'" + DateTime.Today + "'," + hfUserID.Value + ",'Shipping',7351)";
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        private void ColorLossCode()
        {
            /*string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            
            'If GetSaleDate() < CDate("1/1/2019") Then
            '    Exit Sub
            'End If*/

            rgLossCode.Columns[3].Visible = true;

            for (int cnt2 = 0; cnt2 <= rgLossCode.Items.Count - 1; cnt2++)
            {
                if (rgLossCode.Items[cnt2]["ACP"].Text == "1")
                {
                    rgLossCode.Items[cnt2]["losscode"].BackColor = Color.Red;
                }
            }

            rgLossCode.Columns[3].Visible = false;

            /*'SQL = "select coveragecode from contractcoverage cc "
            'SQL = SQL + "inner join contract c on c.programid = cc.programid and c.plantypeid = cc.plantypeid "
            'SQL = SQL + "inner join claim cl on cl.contractid = c.contractid "
            'SQL = SQL + "where cl.claimid = " & hfClaimID.Value
            'clR.OpenDB(SQL, AppSettings("connstring"))
            'If clR.RowCount > 0 Then
            '    For cnt = 0 To clR.RowCount - 1
            '        clR.GetRowNo(cnt)
            '        For cnt2 = 0 To rgLossCode.Items.Count - 1
            '            If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = clR.Fields("coveragecode") Then
            '                rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            '            End If
            '            If hfUserID.Value = "1" Or hfUserID.Value = "1205" Or hfUserID.Value = "1205" Then
            '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "RR00" Then
            '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            '                End If
            '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "PA00" Then
            '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            '                End If
            '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZLEG" Then
            '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            '                End If
            '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZSET" Then
            '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            '                End If
            '                If rgLossCode.Items(cnt2).Item("losscode").Text.Substring(0, 4) = "ZATO" Then
            '                    rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.White
            '                End If
            '            End If
            '        Next
            '    Next
            'End If*/
        }
    }
}