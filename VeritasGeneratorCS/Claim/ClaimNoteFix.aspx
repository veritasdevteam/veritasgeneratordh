﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimNoteFix.aspx.cs" Inherits="VeritasGeneratorCS.Claim.ClaimNoteFix" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Claim Note Fix</title>
</head>
<body>
    <form id="form1" runat="server">
            <asp:ScriptManager runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnClean" OnClick="btnClean_Click" runat="server" Text="Clean" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadEditor ID="txtNote" EditModes="Design" Width="1000" ToolbarMode="ShowOnFocus" runat="server"></telerik:RadEditor>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
