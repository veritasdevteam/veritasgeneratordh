﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimNotes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsNoteType.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsNote.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            hfClaimID.Value = Request.QueryString["claimid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                //'FillList()
                rgNote.Rebind();
                FillNoteHeader();
                ReadOnlyButtons();
                FillCommonNote();
            }
        }
        private void FillCommonNote()
        {
            ListItem lstA = new ListItem();
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            lstA.Value = 0.ToString();
            lstA.Text = "";
            cboCommonNote.Items.Add(lstA);
            SQL = "select * from commonnote order by commonnoteheader ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    lstA = new ListItem();
                    lstA.Value = clR.GetFields("commonnoteid");
                    lstA.Text = clR.GetFields("commonnoteheader");
                    cboCommonNote.Items.Add(lstA);
                }
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly").ToLower() == "true")
                {
                    btnAddNote.Enabled = false;
                    btnSave.Enabled = false;
                }
            }
        }
        private void FillNoteHeader()
        {
            cboHeader.Items.Add("None");
            cboHeader.Items.Add("Statement");
            cboHeader.Items.Add("Parts Sourcing / Ordered");
            cboHeader.Items.Add("TSB");
            cboHeader.Items.Add("Contract Verification");
            cboHeader.Items.Add("Approval");
            cboHeader.Items.Add("Service Center Communication ");
            cboHeader.Items.Add("Management");
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillList()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimnoteid, ClaimNoteTypeDesc, notetext, cn.CreDate, username from claimnote cn " +
                  "left join userinfo ui on ui.userid = cn.creby " +
                  "left join claimnotetype nt on nt.claimnotetypeid = cn.claimnotetypeid " +
                  "where claimid = " + hfClaimID.Value + " " +
                  "and (cn.claimnotetypeid = 4 or cn.claimnotetypeid = 5 or cn.claimnotetypeid = 6 or cn.claimnotetypeid = 7 ) " +
                  "order by credate desc ";
            rgNote.DataSource = clR.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            rgNote.DataBind();
        }

        protected void btnAddNote_Click(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            cboNoteType.ClearSelection();
            cboHeader.Text = "None";
            txtNote.Content = "";
            hfClaimNoteID.Value = 0.ToString();
            btnSave.Visible = true;
        }

        protected void rgNote_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            hfClaimNoteID.Value = rgNote.SelectedValue.ToString();
            SQL = "select * from claimnote cn " +
                  "inner join userinfo ui on ui.userid = cn.creby " +
                  "where claimnoteid = " + hfClaimNoteID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                cboNoteType.SelectedValue = clR.GetFields("claimnotetypeid");
                txtNote.Content = clR.GetFields("note");
                txtCreBy.Text = clR.GetFields("username");
                txtCreDate.Text = clR.GetFields("credate");
                btnSave.Visible = true;
                if (hfUserID.Value != clR.GetFields("creby"))
                {
                    btnSave.Visible = false;
                }
                if ((DateTime.Now - DateTime.Parse(txtCreDate.Text)).TotalHours > 24)
                {
                    btnSave.Visible = false;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnAddNote_Click(sender, e);
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgNote.SelectedIndexes.Clear();
            rgNote.Rebind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote cn where claimnoteid = " + hfClaimNoteID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
            }
            else
            {
                clR.NewRow();
            }
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimnotetypeid", cboNoteType.SelectedValue);
            string sTemp;
            sTemp = txtNote.Text;
            //'txtNote.Text = txtNote.Text.Replace(vbCrLf, vbCr)
            if (cboHeader.Text == "None")
            {
                clR.SetFields("note", txtNote.Content);
                clR.SetFields("notetext", txtNote.Text);
            }
            else
            {
                clR.SetFields("note", "***** " + cboHeader.Text + " *****" + "\r\n" + "\r\n" + txtNote.Content);
                clR.SetFields("notetext", "***** " + cboHeader.Text + " *****" + "\r\n" + "\r\n" + txtNote.Text);
            }
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            if (clR.RowCount() == 0)
            {
                clR.SetFields("credate", DateTime.Now.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }
            clR.SaveDB();
            if (Convert.ToInt32(cboNoteType.SelectedValue) == 7)
            {
                ClearDeniedActivity();
            }
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgNote.Rebind();
            //FillList()
            if (chkReminder.Checked)
            {
                SQL = "select * from usermessage where idx = 0 ";
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                clR.NewRow();
                clR.SetFields("toid", hfUserID.Value);
                clR.SetFields("fromid", hfUserID.Value);
                clR.SetFields("header", "Reminder from Claim: " + GetClaimNo());
                clR.SetFields("message", sTemp);
                clR.SetFields("messagedate", DateTime.Now.ToString());
                clR.AddRow();
                clR.SaveDB();
            }
        }
        private void ClearDeniedActivity()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claim " +
                  "set deniedactivity = 0 " +
                  "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        protected void rgNote_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv")
            {
                rgNote.ExportSettings.ExportOnlyData = false;
                rgNote.ExportSettings.IgnorePaging = true;
                rgNote.ExportSettings.OpenInNewWindow = true;
                rgNote.ExportSettings.UseItemStyles = true;
                rgNote.ExportSettings.FileName = "Notes";
                rgNote.ExportSettings.Csv.FileExtension = "csv";
                rgNote.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Comma;
                rgNote.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                rgNote.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "ExportToExcel")
            {
                rgNote.ExportSettings.ExportOnlyData = false;
                rgNote.ExportSettings.IgnorePaging = true;
                rgNote.ExportSettings.OpenInNewWindow = true;
                rgNote.ExportSettings.UseItemStyles = true;
                rgNote.ExportSettings.FileName = "Notes";
                rgNote.ExportSettings.Excel.FileExtension = "xlsx";
                rgNote.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), GridExcelExportFormat.Xlsx.ToString());
                rgNote.MasterTableView.ExportToExcel();
            }
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort" || e.CommandName == "Page")
            {
                rgNote.Rebind();
            }
        }

        protected void btnCopy_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from commonnote where commonnoteid = " + cboCommonNote.SelectedValue;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtNote.Content = clR.GetFields("commonnote");
            }
        }

        private string GetClaimNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("claimno");
            }
            return "";
        }
    }
}