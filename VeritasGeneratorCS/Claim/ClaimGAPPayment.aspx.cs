﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimGAPPayment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["ClaimID"];
            hfID.Value = Request.QueryString["sid"];
            dsClaimPayment.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsPaymentType.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsStates.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsPayee.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlError.Visible = false;
                pnlDetail.Visible = false;
                pnlSeekPayee.Visible = false;
                pnlAddClaimPayee.Visible = false;
                rgClaimPayment.Rebind();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnAddPayment_Click(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            txtPayeeNo.Text = "";
            txtPayeeName.Text = "";
            txtPaymentAuth.Text = "0";
            rdpAuthDate.Clear();
            rdpPaidDate.Clear();
            hfClaimPaymentID.Value = "0";
        }

        protected void rgClaimPayment_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            hfClaimPaymentID.Value = rgClaimPayment.SelectedValue.ToString();
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            SQL = "select * from claimgappayment where claimpaymentid = " + hfClaimPaymentID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                hfPayeeID.Value = clR.GetFields("claimgappayeeid");
                cboPaymentType.SelectedValue = clR.GetFields("ClaimGAPPaymentTypeID");
                txtPaymentAuth.Text = clR.GetFields("paymentauth");
                if (clR.GetFields("authdate").Length > 0) 
                {
                    rdpAuthDate.SelectedDate = DateTime.Parse(clR.GetFields("authdate"));
                } 
                else 
                {
                    rdpAuthDate.Clear();
                }
                if (clR.GetFields("paiddate").Length > 0) 
                {
                    rdpPaidDate.SelectedDate = DateTime.Parse(clR.GetFields("paiddate"));
                } 
                else 
                {
                    rdpPaidDate.Clear();
                }
                hfClaimPaymentID.Value = clR.GetFields("claimpaymentid");
                GetPayeeInfo();
                GetBankVIN();
            }
        }
        private void GetBankVIN()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select * from claimgap where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                SQL = "select right(vin, 6) as Right6 from contract where contractid = " + clR.GetFields("contractid");
                clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clC.RowCount() > 0)
                {
                    clC.GetRow();
                    txtLast6.Text = clC.GetFields("right6");
                }
                SQL = "select * from claimgaploaninfo where claimgapid = " + hfClaimID.Value;
                clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    txtBankLoad.Text = clR.GetFields("AccountNo");
                }
            }
        }
        private void GetPayeeInfo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimgappayee where claimgappayeeid = " + hfPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtPayeeNo.Text = clR.GetFields("payeeno");
                txtPayeeName.Text = clR.GetFields("payeename");
            } 
            else
            {
                txtPayeeNo.Text = "";
                txtPayeeName.Text = "";
            }
        }
        protected void btnSeekPayee_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSeekPayee.Visible = true;
            rgClaimPayee.Rebind();
        }

        protected void btnDetailClose_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgClaimPayment.Rebind();
        }

        protected void btnDetailUpdate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimgappayment where claimpaymentid = " + hfClaimPaymentID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", hfUserID.Value);
            }
            else
            {
                clR.NewRow();
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                if (hfAuthDate.Value.Length > 0)
                {
                    clR.SetFields("authdate", hfAuthDate.Value);
                    clR.SetFields("authby", hfUserID.Value);
                }
            }
            clR.SetFields("claimgapid", hfClaimID.Value);
            clR.SetFields("claimgappaymenttypeid", cboPaymentType.SelectedValue);
            clR.SetFields("claimgappayeeid", hfPayeeID.Value);
            clR.SetFields("paymentauth", txtPaymentAuth.Text);
            if (rdpAuthDate.SelectedDate != null)
            {
                clR.SetFields("authdate", rdpAuthDate.SelectedDate.ToString());
            }
            if (rdpPaidDate.SelectedDate != null)
            {
                clR.SetFields("paiddate", rdpPaidDate.SelectedDate.ToString());
            }
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
        }

        protected void rdpAuthDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            hfAuthDate.Value = rdpAuthDate.SelectedDate.ToString();
            if (hfClaimPaymentID.Value.Length == 0)
            {
                return;
            }
            if (Convert.ToInt32(hfClaimPaymentID.Value) == 0)
            {
                return;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claimpayment " +
                  "set authdate = '" + rdpAuthDate.SelectedDate + "', " +
                  "authby = " + hfUserID.Value + " " +
                  "where claimpaymentid = " + hfClaimPaymentID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        protected void btnAddClaimPayee_Click(object sender, EventArgs e)
        {
            pnlSeekPayee.Visible = false;
            pnlAddClaimPayee.Visible = true;
            hfPayeeID.Value = "0";
            GetNextPayeeNo();
        }

        protected void rdpPaidDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            hfAuthDate.Value = rdpAuthDate.SelectedDate.ToString();
            if (hfClaimPaymentID.Value.Length == 0)
            {
                return;
            }
            if (Convert.ToInt32(hfClaimPaymentID.Value) == 0)
            {
                return;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claimpayment " +
                  "set paiddate = '" + rdpAuthDate.SelectedDate + "', " +
                  "paidby = " + hfUserID.Value + " " +
                  "where claimpaymentid = " + hfClaimPaymentID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }

        protected void btnPayeeCancel_Click(object sender, EventArgs e)
        {
            pnlAddClaimPayee.Visible = false;
            pnlSeekPayee.Visible = true;
            rgClaimPayee.Rebind();
        }

        protected void btnPayeeSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimgappayee where claimgappayeeid = " + hfPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
            }
            else
            {
                clR.GetRow();
            }
            clR.SetFields("payeeno", txtGAPPayeeNo.Text);
            clR.SetFields("payeename", txtGapPayeeName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("phone", txtPhone.Text);
            clR.SetFields("email", txtEMail.Text);
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
            pnlAddClaimPayee.Visible = false;
            pnlSeekPayee.Visible = true;
            rgClaimPayee.Rebind();
        }
        private void GetNextPayeeNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select max(payeeno) as mSCN from claimgappayee where payeeno like 'GP0%' ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clR.GetRow();
            if (clR.GetFields("mscn").Length == 0)
            {
                txtGAPPayeeNo.Text = "GP0000000";
            } 
            else
            {
                txtGAPPayeeNo.Text = "GP" + (long.Parse(clR.GetFields("mscn").Substring(clR.GetFields("mscn").Length - 6)) + 1).ToString("0000000");
            }
        }
        protected void rgClaimPayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfPayeeID.Value = rgClaimPayee.SelectedValue.ToString();
            GetPayeeInfo();
            pnlSeekPayee.Visible = false;
            pnlDetail.Visible = true;
        }

        protected void btnBackToPayments_Click(object sender, EventArgs e)
        {
            rgClaimPayee.SelectedIndexes.Clear();
            rgClaimPayment.SelectedIndexes.Clear();
            pnlSeekPayee.Visible = false;
            pnlList.Visible = true;
        }
    }
}