﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimVINEngine : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfContractID.Value = Request.QueryString["contractid"];
            if (!IsPostBack)
            {
                GetVIN();
                if (hfVIN.Value.Length > 0)
                {
                    FillPage();
                }
            }
        }
        private void GetVIN()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select vin from contract c where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfVIN.Value = clC.GetFields("vin");
            }
        }
        private void FillPage()
        {
            string SQL;
            clsDBO.clsDBO clV = new clsDBO.clsDBO();
            SQL = "select * from vin.dbo.vin v " +
                  "inner join vin.dbo.engine bd on bd.vinid = v.vinid " +
                  "where vin = '" + hfVIN.Value.Substring(0, 11) + "' ";
            clV.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clV.RowCount() > 0)
            {
                clV.GetRow();
                txtName.Text = clV.GetFields("name");
                txtAvailability.Text = clV.GetFields("availability");
                txtAspiration.Text = clV.GetFields("aspiration");
                txtBlockType.Text = clV.GetFields("blocktype");
                txtBore.Text = clV.GetFields("bore");
                txtCamType.Text = clV.GetFields("camtype");
                txtCompression.Text = clV.GetFields("compression");
                txtCyclinders.Text = clV.GetFields("cyclinders");
                txtDisplacement.Text = clV.GetFields("displacement");
                txtElectricMotorConfig.Text = clV.GetFields("electricmotorconfig");
                txtElectricMaxHP.Text = clV.GetFields("electricmaxhp");
                txtElectricMaxKW.Text = clV.GetFields("electricmaxhp");
                txtElectricMaxTorque.Text = clV.GetFields("electricmaxtorque");
                txtEngineType.Text = clV.GetFields("enginetype");
                txtFuelInduction.Text = clV.GetFields("fuelinduction");
                txtFuelQuality.Text = clV.GetFields("fuelquality");
                txtFuelType.Text = clV.GetFields("FuelType");
                chkFleet.Checked = Convert.ToBoolean(clV.GetFields("fleet"));
                txtGeneratorDesc.Text = clV.GetFields("generatordesc");
                txtGeneratorMaxHP.Text = clV.GetFields("generatormaxhp");
                txtMaxHP.Text = clV.GetFields("maxhp");
                txtMaxHPatRPM.Text = clV.GetFields("maxhpatrpm");
                txtMaxPayload.Text = clV.GetFields("maxpayload");
                txtMaxTorque.Text = clV.GetFields("maxtorque");
                txtMaxTorqueatRPM.Text = clV.GetFields("maxtorqueatrpm");
                txtOilCapacity.Text = clV.GetFields("oilcapacity");
                txtRedLine.Text = clV.GetFields("redline");
                txtStroke.Text = clV.GetFields("totalmaxhp");
                txtValves.Text = clV.GetFields("valves");
                txtValveTiming.Text = clV.GetFields("valvetiming");
                txtTotalMaxHP.Text = clV.GetFields("totalmaxhp");
                txtTotalMaxHPatRPM.Text = clV.GetFields("totalmaxhpatrpm");
                txtTotalMaxTorque.Text = clV.GetFields("totalmaxtorque");
                txtTotalMaxTorqueatRPM.Text = clV.GetFields("TotalMaxTorqueatRPM");
                txtOrderCode.Text = clV.GetFields("ordercode");
            }
        }
    }
}