﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimVehicle.aspx.cs" Inherits="VeritasGeneratorCS.Claim.ClaimVehicle" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <telerik:RadTabStrip ID="tsClaim" OnTabClick="tsClaim_TabClick" runat="server" Width="1500">
                    <Tabs>
                        <telerik:RadTab Text="Moxy Info" Value="Moxy" Width="150"></telerik:RadTab>
                        <telerik:RadTab Text="Basic Data" Value="Basic" Width="150"></telerik:RadTab>
                        <telerik:RadTab Text="Engine" Value="Engine" Width="150"></telerik:RadTab>
                        <telerik:RadTab Text="Transmission" Value="Transmission" Width="150"></telerik:RadTab>
                        <telerik:RadTab Text="Warranty" Value="Warranty" Width="150"></telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>

                <telerik:radmultipage ID="rmClaim" runat="server">
                    <telerik:RadPageView ID="pvMoxy" Height="400" runat="server">
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="pvBasicData" Height="400" runat="server">
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="pvEngine" Height="400" runat="server">
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="pvTransmission" Height="400" runat="server">
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="pvWarranty" Height="400" runat="server">
                    </telerik:RadPageView>
                </telerik:radmultipage>

                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
                <asp:HiddenField ID="hfVIN" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
