﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Claim
{
    public partial class ClaimGAPDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            dsDocs.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsDocType.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsDocType2.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlList.Visible = true;
                pnlAdd.Visible = false;
                pnlDetail.Visible = false;
                rgClaimDocument.Rebind();
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = 0.ToString();
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlAdd.Visible = true;
            pnlList.Visible = false;
            txtDocName.Text = "";
            txtDocDesc.Text = "";
        }

        protected void btnCloseAdd_Click(object sender, EventArgs e)
        {
            pnlAdd.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string sDocLink;
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            GetClaimNo();
            string folderPath = Server.MapPath("~") + @"\documents\gap\" + hfClaimNo.Value + "_" + RadUpload1.UploadedFiles[0].FileName;

            sDocLink = "~/documents/gap/" + hfClaimNo.Value + "_" + RadUpload1.UploadedFiles[0].FileName;
            RadUpload1.UploadedFiles[0].SaveAs(folderPath);

            SQL = "select * from claimgapdocument where claimgapid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clR.NewRow();
            clR.SetFields("claimgapid", hfClaimID.Value);
            clR.SetFields("documentname", txtDocName.Text);
            clR.SetFields("documentdesc", txtDocDesc.Text);
            clR.SetFields("documentlink", sDocLink);
            clR.SetFields("claimgapdoctypeid", cboDocType2.SelectedValue);
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("credate", DateTime.Now.ToString());
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.AddRow();
            clR.SaveDB();
            pnlAdd.Visible = false;
            pnlList.Visible = true;
            rgClaimDocument.Rebind();
        }
        private void GetClaimNo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select claimno from claimgap where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimNo.Value = clR.GetFields("claimno");
            }
        }
        protected void rgClaimDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            hfDocID.Value = rgClaimDocument.SelectedValue.ToString();
            FillDetail();
        }
        private void FillDetail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimgapdocument where claimgapdocumentid = " + hfDocID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtCreBy.Text = Functions.GetUserInfo(long.Parse(clR.GetFields("creby")));
                txtCreDate.Text = clR.GetFields("credate");
                txtDescDetail.Text = clR.GetFields("documentdesc");
                if (clR.GetFields("modby").Length > 0)
                {
                    txtModBy.Text = Functions.GetUserInfo(long.Parse(clR.GetFields("modby")));
                }
                txtTitleDetail.Text = clR.GetFields("documentname");
                txtModDate.Text = clR.GetFields("moddate");
                cboDocType.SelectedValue = clR.GetFields("claimgapdoctypeid");
            }
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claimgapdocument " +
                  "set deleted = 1 " +
                  "where claimgapdocumentid = " + hfDocID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgClaimDocument.Rebind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimgapdocument where claimgapdocumentid = " + hfDocID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (cboDocType.SelectedValue != clR.GetFields("claimgapdoctypeid"))
                {
                    ChangeDocStatus(long.Parse(clR.GetFields("claimgapdoctypeid")), long.Parse(cboDocType.SelectedValue));
                }
                clR.SetFields("claimgapdoctypeid", cboDocType.SelectedValue);
                clR.SetFields("documentname", txtTitleDetail.Text);
                clR.SetFields("documentdesc", txtDescDetail.Text);
                clR.SetFields("modby", hfUserID.Value);
                clR.SetFields("moddate", DateTime.Now.ToString());
                clR.SaveDB();
            }
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgClaimDocument.Rebind();
        }
        private void ChangeDocStatus(long xOld, long xNew)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update claimgapdocstatus ";
            if (xOld == 1) 
            {
                SQL = SQL + "set risc = 0 ";
            }
            if (xOld == 2) 
            {
                SQL = SQL + "set paymenthistory = 0 ";
            }
            if (xOld == 3) 
            {
                SQL = SQL + "set TotalLossValuation = 0 ";
            }
            if (xOld == 4) 
            {
                SQL = SQL + "set TotalLossSettlement = 0 ";
            }
            if (xOld == 5) 
            {
                SQL = SQL + "set PoliceFireReport = 0 ";
            }
            if (xOld == 6) 
            {
                SQL = SQL + "set ProofInsPayments = 0 ";
            }
            if (xOld == 7) 
            {
                SQL = SQL + "set GAPClaimNotice = 0 ";
            }
            if (xOld == 8) 
            {
                SQL = SQL + "set GAP60DayLetter = 0 ";
            }
            if (xOld == 9) 
            {
                SQL = SQL + "set GAPDenielLetter = 0 ";
            }
            if (xOld == 10) 
            {
                SQL = SQL + "set GAPMissedOptions = 0 ";
            }
            if (xOld == 11)
            {
                SQL = SQL + "set GAPNoGAPDueLetter = 0 ";
            }
            if (xOld == 12) 
            {
                SQL = SQL + "set GAPPaymentLetter = 0 ";
            }
            if (xOld == 13) 
            {
                SQL = SQL + "set GAPStatusLetter = 0 ";
            }
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            SQL = "update claimgapdocstatus ";
            if (xNew == 1) 
            {
                SQL = SQL + "set risc = 1 ";
            }
            if (xNew == 2) 
            {
                SQL = SQL + "set paymenthistory = 1 ";
            }
            if (xNew == 3) 
            {
                SQL = SQL + "set TotalLossValuation = 1 ";
            }
            if (xNew == 4) 
            {
                SQL = SQL + "set TotalLossSettlement = 1 ";
            }
            if (xNew == 5) 
            {
                SQL = SQL + "set PoliceFireReport = 1 ";
            }
            if (xNew == 6) 
            {
                SQL = SQL + "set ProofInsPayments = 1 ";
            }
            if (xNew == 7) 
            {
                SQL = SQL + "set GAPClaimNotice = 1 ";
            }
            if (xNew == 8) 
            {
                SQL = SQL + "set GAP60DayLetter = 1 ";
            }
            if (xNew == 9) 
            {
                SQL = SQL + "set GAPDenielLetter = 1 ";
            }
            if (xNew == 10) 
            {
                SQL = SQL + "set GAPMissedOptions = 1 ";
            }
            if (xNew == 11) 
            {
                SQL = SQL + "set GAPNoGAPDueLetter = 1 ";
            }
            if (xNew == 12)
            {
                SQL = SQL + "set GAPPaymentLetter = 1 ";
            }
            if (xNew == 13) 
            {
                SQL = SQL + "set GAPStatusLetter = 1 ";
            }
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
    }
}