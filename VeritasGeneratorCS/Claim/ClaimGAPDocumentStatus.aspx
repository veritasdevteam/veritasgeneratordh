﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimGAPDocumentStatus.aspx.cs" Inherits="VeritasGeneratorCS.Claim.ClaimGAPDocumentStatus" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            RISC (Retail Installment Sales Contract / Loan Contract)
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkRISC" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Payment History
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkPaymentHistory" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Total loss Insurer's valuation
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkTotalLossInsValuation" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Total loss Insurer's settlement letter
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkTotalLossInsSettlementLetter" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Police or Fire Report
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkPoliceFireReport" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Proof of Insurer Payments
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkProofInsPayments" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            GAP Claim Notice 
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkGAPClaimNotice" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnGenerateClaimNotice" OnClick="btnGenerateClaimNotice_Click" BackColor="#1eabe2" runat="server" Text="Generate" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblGenerateClaimNotice" ForeColor="#1eabe2" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            GAP 60 Day Letter
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chk60DayLetter" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnGenerate60DayLetter" OnClick="btnGenerate60DayLetter_Click" BackColor="#1eabe2" runat="server" Text="Generate" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblGenerate60DayLetter" ForeColor="#1eabe2" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            GAP Denial Letter
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkDenialLetter" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnDenialLetter" OnClick="btnDenialLetter_Click" BackColor="#1eabe2" runat="server" Text="Generate" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblDenialLetter" ForeColor="#1eabe2" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            GAP Missed Options
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkMissedOptions" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnMissedOptions" OnClick="btnMissedOptions_Click" BackColor="#1eabe2" runat="server" Text="Generate" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblMissedOptions" ForeColor="#1eabe2" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            GAP No GAP Due Letter
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkGAPNoGAPDueLetter" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnGAPNoGAPDueLetter" OnClick="btnGAPNoGAPDueLetter_Click" BackColor="#1eabe2" runat="server" Text="Generate" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblGapNoGapDueLetter" ForeColor="#1eabe2" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            GAP Payment Letter
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkPaymentLetter" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnPaymentLetter" OnClick="btnPaymentLetter_Click" BackColor="#1eabe2" runat="server" Text="Generate" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblPaymentLetter" ForeColor="#1eabe2" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            GAP Status Letter
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkStatusLetter" runat="server" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnStatusLetter" OnClick="btnStatusLetter_Click" BackColor="#1eabe2" runat="server" Text="Generate" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblStatusLetter" ForeColor="#1eabe2" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" BackColor="#1eabe2" runat="server" Text="Update" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
