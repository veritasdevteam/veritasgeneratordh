﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Users
{
    public partial class UserSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsUser.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
                pnlSearch.Visible = true;
                pnlDetail.Visible = false;
                FillInsCarrier();
                FillTeams();
                FillAgents();
            }
        }
        private void FillAgents()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "All";
            cboAgent.Items.Add(li);
            SQL = "select * from agents " +
                  "where not agentno is null " +
                  "and statusid = 2 " +
                  "order by agentname ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    li = new ListItem();
                    li.Value = clR.GetFields("agentid");
                    li.Text = clR.GetFields("agentname");
                    cboAgent.Items.Add(li);
                }
            }
        }
        private void FillInsCarrier()
        {
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "All";
            cboInsCarrier.Items.Add(li);
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from inscarrier order by inscarrierid ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    li = new ListItem();
                    li.Value = clR.GetFields("inscarrierid");
                    li.Text = clR.GetFields("inscarriername");
                    cboInsCarrier.Items.Add(li);
                }
            }
        }
        private void FillTeams()
        {
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "";
            cboTeams.Items.Add(li);
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from userteam ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    li = new ListItem();
                    li.Value = clR.GetFields("teamid");
                    li.Text = clR.GetFields("teamname");
                    cboTeams.Items.Add(li);
                }
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnReports.Enabled = false;
            btnUserSettings.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("usersettings")) == true)
                {
                    btnUserSettings.Enabled = true;
                }
            }
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnToDoReader_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/todoreader.aspx?sid=" + hfID.Value);
        }

        protected void btnToDoCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/todocreate.aspx?sid=" + hfID.Value);
        }

        protected void btnUserSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/usersettings.aspx?sid=" + hfID.Value);
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/ChangePassword.aspx?sid=" + hfID.Value);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlSearch.Visible = false;
            pnlDetail.Visible = true;
            hfUserDetailID.Value = "0";
            ClearFields();
        }
        private void ClearFields()
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtEMail.Text = "";
            txtPassword.Text = "";
            txtClaimMax.Text = "0";
            txtMaxAuth.Text = "0";
            chkActive.Checked = false;
            chkUserSettings.Checked = false;
            chkAccounting.Checked = false;
            chkSettings.Checked = false;
            chkAgents.Checked = false;
            chkDealer.Checked = false;
            chkClaim.Checked = false;
            txtClaimMax.Text = "0";
            chkContract.Checked = false;
            chkSaleReport.Checked = false;
            chkClaimsReports.Checked = false;
            chkAccountReports.Checked = false;
            chkCustomReports.Checked = false;
            chkCancel.Checked = false;
            chkCancelGap.Checked = false;
            chkCancelMod.Checked = false;
            chkContractMod.Checked = false;
            chkHCCApprovalCEO.Checked = false;
            chkHCCApprovalManager.Checked = false;
            chkPhoneReports.Checked = false;
            cboTeams.SelectedValue = "0";
            chkTeamLead.Checked = false;
            chkANOnly.Checked = false;
            chkVeroOnly.Checked = false;
            chkCancel100.Checked = false;
            chkViewBreakdown.Checked = false;
            chkTotalPayment.Checked = false;
            chkAllow45Days.Checked = false;
            chkAllowAZDenied.Checked = false;
            chkCES.Checked = false;
        }

        protected void rgUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfUserDetailID.Value = rgUser.SelectedValue.ToString();
            ClearFields();
            FillUser();
            pnlDetail.Visible = true;
            pnlSearch.Visible = false;
        }
        private void FillUser()
        {
            string SQL;
            clsDBO.clsDBO clU = new clsDBO.clsDBO();
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + hfUserDetailID.Value;
            clU.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clU.RowCount() > 0)
            {
                clU.GetRow();
                txtEMail.Text = clU.GetFields("email");
                txtFName.Text = clU.GetFields("fname");
                txtLName.Text = clU.GetFields("lname");
                txtPassword.Text = clU.GetFields("password");
                chkActive.Checked = Convert.ToBoolean(clU.GetFields("active"));
            }

            SQL = "select * from usersecurityinfo where userid = " + hfUserDetailID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                chkUserSettings.Checked = Convert.ToBoolean(clSI.GetFields("usersettings"));
                chkAccounting.Checked = Convert.ToBoolean(clSI.GetFields("Accounting"));
                chkSettings.Checked = Convert.ToBoolean(clSI.GetFields("settings"));
                chkAgents.Checked = Convert.ToBoolean(clSI.GetFields("agents"));
                chkDealer.Checked = Convert.ToBoolean(clSI.GetFields("dealer"));
                chkActivateContract.Checked = Convert.ToBoolean(clSI.GetFields("activatecontract"));
                chkClaim.Checked = Convert.ToBoolean(clSI.GetFields("claim"));
                chkContract.Checked = Convert.ToBoolean(clSI.GetFields("contract"));
                chkSaleReport.Checked = Convert.ToBoolean(clSI.GetFields("salesreports"));
                chkClaimsReports.Checked = Convert.ToBoolean(clSI.GetFields("claimsreports"));
                chkAccountReports.Checked = Convert.ToBoolean(clSI.GetFields("accountreports"));
                chkCustomReports.Checked = Convert.ToBoolean(clSI.GetFields("customreports"));
                chkCancelMod.Checked = Convert.ToBoolean(clSI.GetFields("cancelmodification"));
                chkCancelGap.Checked = Convert.ToBoolean(clSI.GetFields("cancelgap"));
                chkCancel.Checked = Convert.ToBoolean(clSI.GetFields("cancellation"));
                chkHCCApprovalManager.Checked = Convert.ToBoolean(clSI.GetFields("hccapprovalManager"));
                chkHCCApprovalCEO.Checked = Convert.ToBoolean(clSI.GetFields("hccapprovalCEO"));
                chkCancelPayment.Checked = Convert.ToBoolean(clSI.GetFields("CancelPay"));
                chkUnlockClaim.Checked = Convert.ToBoolean(clSI.GetFields("UnlockClaim"));
                chkClaimPayment.Checked = Convert.ToBoolean(clSI.GetFields("ClaimPayment"));
                txtClaimMax.Text = clSI.GetFields("claimapprove");
                txtMaxAuth.Text = clSI.GetFields("claimauth");
                cboInsCarrier.SelectedValue = clSI.GetFields("inscarrierid");
                chkReadOnly.Checked = Convert.ToBoolean(clSI.GetFields("readonly"));
                chkContractMod.Checked = Convert.ToBoolean(clSI.GetFields("contractmodification"));
                chkTeamLead.Checked = Convert.ToBoolean(clSI.GetFields("TeamLead"));
                chkANOnly.Checked = Convert.ToBoolean(clSI.GetFields("autonationonly"));
                chkVeroOnly.Checked = Convert.ToBoolean(clSI.GetFields("veroonly"));
                cboAgent.SelectedValue = clSI.GetFields("agentid");
                cboSubAgent.SelectedValue = clSI.GetFields("subagentid");
                chkAllowCancelExpire.Checked = Convert.ToBoolean(clSI.GetFields("allowcancelexpire"));
                chkWithIn3Days.Checked = Convert.ToBoolean(clSI.GetFields("within3days"));
                chkAllCoverage.Checked = Convert.ToBoolean(clSI.GetFields("allcoverage"));
                chkAllowSlush.Checked = Convert.ToBoolean(clSI.GetFields("allowslush"));
                chkCancel100.Checked = Convert.ToBoolean(clSI.GetFields("cancel100"));
                chkViewBreakdown.Checked = Convert.ToBoolean(clSI.GetFields("viewbreakdown"));
                chkAllowUndeny.Checked = Convert.ToBoolean(clSI.GetFields("AllowUndenyClaim"));
                chkClaimAudit.Checked = Convert.ToBoolean(clSI.GetFields("allowclaimaudit"));
                chkBypassMissingInfo.Checked = Convert.ToBoolean(clSI.GetFields("bypassmissinginfo"));
                chkAllow45Days.Checked = Convert.ToBoolean(clSI.GetFields("allow45days"));
                if (clSI.GetFields("TeamID").Length == 0)
                {
                    clSI.SetFields("TeamID", "0");
                }
                cboTeams.SelectedValue = clSI.GetFields("TeamID");
                chkTotalPayment.Checked = Convert.ToBoolean(clSI.GetFields("allowtotalpayment"));
                chkAllowAZDenied.Checked = Convert.ToBoolean(clSI.GetFields("allowazdenied"));
                chkPhoneReports.Checked = Convert.ToBoolean(clSI.GetFields("phonereports"));
                chkCES.Checked = Convert.ToBoolean(clSI.GetFields("ces"));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clU = new clsDBO.clsDBO();
            clsDBO.clsDBO clUSI = new clsDBO.clsDBO();
            SQL = "select * from userinfo ";
            if (hfUserDetailID.Value.Length == 0) 
            {
                SQL = SQL + "where userid = 0 ";
            } 
            else 
            {
                SQL = SQL + "where userid = " + hfUserDetailID.Value;
            }
            clU.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clU.RowCount() > 0) 
            {
                clU.GetRow();
            } 
            else 
            {
                clU.NewRow();
            }
            clU.SetFields("email", txtEMail.Text);
            clU.SetFields("password", txtPassword.Text);
            clU.SetFields("fname", txtFName.Text);
            clU.SetFields("lname", txtLName.Text);
            clU.SetFields("active", chkActive.Checked.ToString());
            if (clU.RowCount() == 0) 
            {
                clU.AddRow();
            }
            clU.SaveDB();
            GetUserInfo();
            SQL = "select * from usersecurityinfo where userid = " + hfUserDetailID.Value;
            clUSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clUSI.RowCount() > 0) 
            {
                clUSI.GetRow();
            } 
            else 
            {
                clUSI.NewRow();
            }
            clUSI.SetFields("userid", hfUserDetailID.Value);
            clUSI.SetFields("usersettings", chkUserSettings.Checked.ToString());
            clUSI.SetFields("accounting", chkAccounting.Checked.ToString());
            clUSI.SetFields("settings", chkSettings.Checked.ToString());
            clUSI.SetFields("agents", chkAgents.Checked.ToString());
            clUSI.SetFields("dealer", chkDealer.Checked.ToString());
            clUSI.SetFields("claim", chkClaim.Checked.ToString());
            clUSI.SetFields("unlockClaim", chkUnlockClaim.Checked.ToString());
            clUSI.SetFields("contract", chkContract.Checked.ToString());
            clUSI.SetFields("salesreports", chkSaleReport.Checked.ToString());
            clUSI.SetFields("activatecontract", chkActivateContract.Checked.ToString());
            clUSI.SetFields("claimsreports", chkClaimsReports.Checked.ToString());
            clUSI.SetFields("accountreports", chkAccountReports.Checked.ToString());
            clUSI.SetFields("customreports", chkCustomReports.Checked.ToString());
            clUSI.SetFields("Cancellation", chkCancel.Checked.ToString());
            clUSI.SetFields("cancelgap", chkCancelGap.Checked.ToString());
            clUSI.SetFields("cancelmodification", chkCancelMod.Checked.ToString());
            clUSI.SetFields("hccapprovalmanager", chkHCCApprovalManager.Checked.ToString());
            clUSI.SetFields("hccapprovalceo", chkHCCApprovalCEO.Checked.ToString());
            clUSI.SetFields("claimpayment", chkClaimPayment.Checked.ToString());
            clUSI.SetFields("cancelpay", chkCancelPayment.Checked.ToString());
            clUSI.SetFields("claimauth", txtMaxAuth.Text.ToString());
            clUSI.SetFields("claimapprove", txtClaimMax.Text.ToString());
            clUSI.SetFields("readonly", chkReadOnly.Checked.ToString());
            clUSI.SetFields("inscarrierid", cboInsCarrier.SelectedValue);
            clUSI.SetFields("contractmodification", chkContractMod.Checked.ToString());
            clUSI.SetFields("teamid", cboTeams.SelectedValue);
            clUSI.SetFields("teamlead", chkTeamLead.Checked.ToString());
            clUSI.SetFields("autonationonly", chkANOnly.Checked.ToString());
            clUSI.SetFields("veroonly", chkVeroOnly.Checked.ToString());
            clUSI.SetFields("agentid", cboAgent.SelectedValue);
            clUSI.SetFields("allowcancelexpire", chkAllowCancelExpire.Checked.ToString());
            clUSI.SetFields("within3days", chkWithIn3Days.Checked.ToString());
            clUSI.SetFields("allcoverage", chkAllCoverage.Checked.ToString());
            clUSI.SetFields("allowslush", chkAllowSlush.Checked.ToString());
            clUSI.SetFields("cancel100", chkCancel100.Checked.ToString());
            clUSI.SetFields("viewbreakdown", chkViewBreakdown.Checked.ToString());
            clUSI.SetFields("AllowUndenyClaim", chkAllowUndeny.Checked.ToString());
            clUSI.SetFields("allowclaimaudit", chkClaimAudit.Checked.ToString());
            clUSI.SetFields("bypassmissinginfo", chkBypassMissingInfo.Checked.ToString());
            clUSI.SetFields("allowtotalpayment", chkTotalPayment.Checked.ToString());
            clUSI.SetFields("allow45days", chkAllow45Days.Checked.ToString());
            clUSI.SetFields("allowazdenied", chkAllowAZDenied.Checked.ToString());
            clUSI.SetFields("phonereports", chkPhoneReports.Checked.ToString());
            clUSI.SetFields("ces", chkCES.Checked.ToString());
            if (cboSubAgent.SelectedValue.Length == 0) 
            {
                clUSI.SetFields("subagentid", "0");
            } 
            else 
            {
                clUSI.SetFields("subagentid", cboSubAgent.SelectedValue);
            }
            if (clUSI.RowCount() == 0) 
            {
                clUSI.AddRow();
            }
            clUSI.SaveDB();
            ClearFields();
            rgUser.Rebind();
            pnlDetail.Visible = false;
            pnlSearch.Visible = true;
        }
        private void GetUserInfo()
        {
            string SQL;
            clsDBO.clsDBO clU = new clsDBO.clsDBO();
            SQL = "select * from userinfo " +
                  "where email = '" + txtEMail.Text + "' " +
                  "and password = '" + txtPassword.Text + "' ";
            clU.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clU.RowCount() > 0)
            {
                clU.GetRow();
                hfUserDetailID.Value = clU.GetFields("userid");
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();
            rgUser.Rebind();
            pnlDetail.Visible = false;
            pnlSearch.Visible = true;
        }

        protected void cboAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem li = new ListItem();
            cboSubAgent.Items.Clear();
            li.Value = "0";
            li.Text = "All";
            cboSubAgent.Items.Add(li);
            if (Convert.ToInt32(cboAgent.SelectedValue) == 0)
            {
                return;
            }
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from subagents " +
                  "where agentid = " + cboAgent.SelectedValue + " " +
                  "order by subagentname ";
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clR.RowCount() - 1; cnt++)
                {
                    clR.GetRowNo(cnt);
                    li = new ListItem();
                    li.Value = clR.GetFields("subagentid");
                    li.Text = clR.GetFields("subagentname");
                    cboSubAgent.Items.Add(li);
                }
            }
        }
    }
}