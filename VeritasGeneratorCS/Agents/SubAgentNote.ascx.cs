﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Agents
{
    public partial class SubAgentNote : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsAgentNote.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
            if (!IsPostBack)
            {
                hfToday.Value = DateTime.Today.ToString("M/d/yyyy");
                hfSubAgentID.Value = Request.QueryString["subagentID"];
                pnlControl.Visible = true;
                pnlDetail.Visible = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void rgSubAgentNote_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlControl.Visible = false;
            pnlDetail.Visible = true;
            hfSubAgentNoteID.Value = rgSubAgentNote.SelectedValue.ToString();
            FillNote();
        }
        private void FillNote()
        {
            string SQL;
            clsDBO.clsDBO clRN = new clsDBO.clsDBO();
            SQL = "select note, createdate, moddate, cre.email as CEmail, mod.email as MEMail, claimalways, claimnewentry from subagentnote rn " +
                  "left join userinfo cre on rn.createby = cre.userid " +
                  "left join userinfo mod on mod.userid = rn.modby " +
                  "where subagentnoteid = " + hfSubAgentNoteID.Value;

            clRN.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clRN.RowCount() > 0)
            {
                clRN.GetRow();
                txtCreBy.Text = clRN.GetFields("CEMail");
                txtCreDate.Text = clRN.GetFields("createdate");
                txtModBy.Text = clRN.GetFields("MEMail");
                txtModDate.Text = clRN.GetFields("moddate");
                txtNote.Text = clRN.GetFields("note");
                chkClaimAlways.Checked = Convert.ToBoolean(clRN.GetFields("claimalways"));
                chkClaimNewEntry.Checked = Convert.ToBoolean(clRN.GetFields("claimnewentry"));
            }
            else
            {
                txtCreBy.Text = "";
                txtCreDate.Text = "";
                txtModBy.Text = "";
                txtModDate.Text = "";
                txtNote.Text = "";
                chkClaimNewEntry.Checked = false;
                chkClaimAlways.Checked = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlControl.Visible = true;
            pnlDetail.Visible = false;
            rgSubAgentNote.Rebind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clRN = new clsDBO.clsDBO();
            SQL = "select * from subAgentnote where subagentnoteid = " + hfSubAgentNoteID.Value;
            clRN.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clRN.RowCount() == 0)
            {
                clRN.NewRow();
                clRN.SetFields("createby", hfUserID.Value);
                clRN.SetFields("createdate", DateTime.Today.ToString());
            }
            else
            {
                clRN.GetRow();
                clRN.SetFields("modby", hfUserID.Value);
                clRN.SetFields("moddate", DateTime.Today.ToString());
            }
            clRN.SetFields("note", txtNote.Text);
            clRN.SetFields("subagentid", hfSubAgentID.Value);
            clRN.SetFields("claimalways", chkClaimAlways.Checked.ToString());
            clRN.SetFields("claimnewentry", chkClaimNewEntry.Checked.ToString());
            if (clRN.RowCount() == 0)
            {
                clRN.AddRow();
            }
            clRN.SaveDB();
            pnlControl.Visible = true;
            pnlDetail.Visible = false;
            rgSubAgentNote.Rebind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlControl.Visible = false;
            pnlDetail.Visible = true;
            hfSubAgentNoteID.Value = 0.ToString();
            FillNote();
        }
    }
}