﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Agents
{
    public partial class AgentSite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            SqlDataSource2.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            SqlDataSource3.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                pnlList.Visible = true;
                pnlDetail.Visible = false;
                pnlAgentSearch.Visible = false;
                pnlSubAgentSeek.Visible = false;
                btnSend.Visible = false;
            }
            if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void rgAgents_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            hfAgentID.Value = rgAgents.SelectedValue.ToString();
            SQL = "select * from veritasagent.dbo.userinfo where userid = " + hfAgentID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                pnlDetail.Visible = true;
                pnlList.Visible = false;
                clR.GetRow();
                txtEMail.Text = clR.GetFields("email");
                txtFName.Text = clR.GetFields("fname");
                txtLName.Text = clR.GetFields("lname");
                txtPassword.Text = clR.GetFields("password");
                chkActive.Checked = Convert.ToBoolean(clR.GetFields("active"));
                txtAgent.Text = Functions.GetAgentInfo(long.Parse(clR.GetFields("agentid")));
                if (clR.GetFields("subagentid").Length > 0)
                {
                    txtSubAgent.Text = Functions.GetSubAgentInfo(long.Parse(clR.GetFields("subagentid")));
                }
                else
                {
                    txtSubAgent.Text = "";
                }
                btnSend.Visible = true;
            }
        }

        protected void btnSeekAgent_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlAgentSearch.Visible = true;
        }

        protected void btnAgentSeekCancel_Click(object sender, EventArgs e)
        {
            pnlAgentSearch.Visible = false;
            pnlDetail.Visible = true;
        }

        protected void rgAgentSeek_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfAgentSeekID.Value = rgAgentSeek.SelectedValue.ToString();
            pnlAgentSearch.Visible = false;
            pnlDetail.Visible = true;
            txtAgent.Text = Functions.GetAgentInfo(long.Parse(hfAgentSeekID.Value));
        }

        protected void btnSeekSubAgent_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSubAgentSeek.Visible = true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnSubAgentSeekCancel_Click(object sender, EventArgs e)
        {
            pnlSubAgentSeek.Visible = false;
            pnlDetail.Visible = true;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            txtAgent.Text = "";
            txtEMail.Text = "";
            txtFName.Text = "";
            txtLName.Text = "";
            txtPassword.Text = "";
            txtSubAgent.Text = "";
            btnSend.Visible = false;
            hfAgentID.Value = 0.ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from veritasagent.dbo.userinfo where userid = " + hfAgentID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
            }
            else
            {
                clR.GetRow();
            }
            clR.SetFields("email", txtEMail.Text);
            clR.SetFields("password", txtPassword.Text);
            clR.SetFields("fname", txtFName.Text);
            clR.SetFields("lname", txtLName.Text);
            clR.SetFields("active", chkActive.Checked.ToString());
            clR.SetFields("agentid", hfAgentSeekID.Value);
            clR.SetFields("subagentid", hfSubAgentSeekID.Value);
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
            rgAgents.Rebind();
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void rgSubAgents_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfSubAgentSeekID.Value = rgSubAgents.SelectedValue.ToString();
            pnlDetail.Visible = true;
            pnlSubAgentSeek.Visible = false;
            txtSubAgent.Text = Functions.GetSubAgentInfo(long.Parse(hfSubAgentSeekID.Value));
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            VeritasGlobalToolsV2.clsEMailAgentSite clA = new VeritasGlobalToolsV2.clsEMailAgentSite();
            clA.UserID = long.Parse(hfAgentID.Value);
            clA.SendEmail();
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }
        protected void btnSubAgent_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/SubAgentSearch.aspx?sid=" + hfID.Value);
        }
        protected void btnAgentSite_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Agents/AgentSite.aspx?sid=" + hfID.Value);
        }
    }
}