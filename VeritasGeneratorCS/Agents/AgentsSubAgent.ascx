﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AgentsSubAgent.ascx.cs" Inherits="VeritasGeneratorCS.Agents.AgentsSubAgent" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <telerik:RadGrid ID="rgAgentSubAgents" OnSelectedIndexChanged="rgAgentSubAgents_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="SQLDataSource1">
                <GroupingSettings CaseSensitive="false" />
                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="SubAgentID" PageSize="10" ShowFooter="true">
                    <Columns>
                        <telerik:GridBoundColumn DataField="SubAgentID" ReadOnly="true" Visible="false" UniqueName="SubAgentID"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SubAgentNo" UniqueName="SubAgentNo" HeaderText="Sub Agent No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SubAgentName" UniqueName="SubAgentName" HeaderText="Sub Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
            </telerik:RadGrid>
            <asp:SqlDataSource ID="SqlDataSource1"
            ProviderName="System.Data.SqlClient" 
            SelectCommand="select SubAgentid, subagentno, subagentname, City, State from subagents 
            where Agentid = @AgentID" runat="server">
            <SelectParameters>
                <asp:ControlParameter ControlID="hfAgentID" Name="AgentID" PropertyName="Value" Type="Int32" />
            </SelectParameters>
            </asp:SqlDataSource>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfAgentID" runat="server" />
