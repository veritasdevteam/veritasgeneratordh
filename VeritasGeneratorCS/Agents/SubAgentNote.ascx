﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubAgentNote.ascx.cs" Inherits="VeritasGeneratorCS.Agents.SubAgentNote" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
           <asp:Panel runat="server" ID="pnlControl">
               <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" BackColor="#1eabe2" BorderColor="#1eabe2" Text="Add Agent Note" />
                        </asp:TableCell>
                    </asp:TableRow>
                   <asp:TableRow>
                       <asp:TableCell>
                            <telerik:RadGrid ID="rgSubAgentNote" OnSelectedIndexChanged="rgSubAgentNote_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowAutomaticUpdates="true" 
                                AllowSorting="true" AllowPaging="true" AllowFilteringByColumn="true" 
                                ShowFooter="true" DataSourceID="dsAgentNote">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="SubAgentNoteID" ShowFooter="true" DataSourceID="dsAgentNote">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="AgentNoteID" UniqueName="AgentNoteID" Visible="false" ReadOnly="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AgentID" UniqueName="AgentID" Visible="false" ReadOnly="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Note" UniqueName="Note" HeaderText="Note" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Create Date" UniqueName="CreateDate" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ModDate" HeaderText="Modify Date" UniqueName="ModDate" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CreateBy" HeaderText="Create By" UniqueName="CreateBy" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ModBy" HeaderText="Modify By" UniqueName="ModBy" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource runat="server" ID="dsAgentNote" 
                                ProviderName="System.Data.SqlClient"
                                SelectCommand="select dn.SubAgentNoteID, dn.SubAgentid, dn.note, dn.CreateDate, dn.Moddate, uic.email as CreateBy, uim.email as ModBy
                                from SubAgentNote dn 
                                left join UserInfo uic on uic.userid = dn.createby
                                left join UserInfo uim on uim.userid = dn.ModBy
                                where SubAgentid = @SubAgentid">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hfSubAgentID" Name="SubAgentID" PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                       </asp:TableCell>
                   </asp:TableRow>
               </asp:Table>
           </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server">
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlDetail">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Note:
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Claim Always:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkClaimAlways" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Claim New Entry:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkClaimNewEntry" runat="server" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtNote" TextMode="MultiLine" Width="1000" Height="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        Create Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadTextBox ID="txtCreDate" runat="server"></telerik:RadTextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Create By:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadTextBox ID="txtCreBy" runat="server"></telerik:RadTextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Modify Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadTextBox ID="txtModDate" runat="server"></telerik:RadTextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Modify By:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadTextBox ID="txtModBy" runat="server"></telerik:RadTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>    
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right"> 
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White" Text="Save" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" BorderColor="#1eabe2" BackColor="#1eabe2" Text="Cancel" />
                                </asp:TableCell>
                            </asp:TableRow>                            
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

<asp:HiddenField ID="hfSubAgentID" runat="server" />
<asp:HiddenField ID="hfSubAgentNoteID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfToday" runat="server" />
