﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace VeritasGeneratorCS.FulFillment
{
    public partial class Fulfillment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsItem.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsOrder1.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsOrder2.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsOrder3.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsOrder4.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            dsOrder5.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            GetServerInfo();
            if (!IsPostBack)
            {
                if (Convert.ToInt32(hfUserID.Value) == 0)
                {
                    Response.Redirect("~/Default.aspx");
                }
                TabStrip1.Tabs[0].Selected = true;
                
                if (ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString.Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                    Image1.BackColor = System.Drawing.Color.FromArgb(26, 70, 136);
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                    Image1.BackColor = System.Drawing.Color.FromArgb(30, 171, 226);
                }
            }
            if (hfError.Value == "Visible")
            {
                rwError.VisibleOnPageLoad = true;
            } 
            else
            {
                rwError.VisibleOnPageLoad = false;
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
            clsDBO.clsDBO clUI = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + hfUserID.Value;
            clUI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clUI.RowCount() > 0)
            {
                clUI.GetRow();
                hfUserEMail.Value = clUI.GetFields("email");
            }
        }
        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }
        private void UnlockButtons()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")) == true)
                {
                    btnAccounting.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Settings")) == true)
                {
                    btnSettings.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Agents")) == true)
                {
                    btnAgents.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("Dealer")) == true)
                {
                    btnDealer.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claim")) == true)
                {
                    btnClaim.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("contract")) == true)
                {
                    btnContract.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("salesreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")) == true)
                {
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")) == true)
                {
                    btnReports.Enabled = true;
                }
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }
        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/settings/settings.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnSubAgent_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/SubAgentSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnFulFillment_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/fulfillment/fulfillment.aspx?sid=" + hfID.Value);
        }

        protected void btnFulfillmentStock_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/fulfillment/fulfillmentstock.aspx?sid=" + hfID.Value);
        }

        protected void TabStrip1_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (e.Tab.Value == "Approve") 
            {
                rgOrder.DataSourceID = "dsOrder1";
                TabStrip1.Tabs[0].Selected = true;
                rgOrder.Rebind();
            }
            if (e.Tab.Value == "Pend") 
            {
                rgOrder.DataSourceID = "dsOrder2";
                TabStrip1.Tabs[1].Selected = true;
                rgOrder.Rebind();
            }
            if (e.Tab.Value == "Wait") 
            {
                rgOrder.DataSourceID = "dsOrder3";
                TabStrip1.Tabs[2].Selected = true;
                rgOrder.Rebind();
            }
            if (e.Tab.Value == "Shipped") 
            {
                rgOrder.DataSourceID = "dsOrder4";
                TabStrip1.Tabs[3].Selected = true;
                rgOrder.Rebind();
            }
            if (e.Tab.Value == "Cancel") 
            {
                rgOrder.DataSourceID = "dsOrder5";
                TabStrip1.Tabs[4].Selected = true;
                rgOrder.Rebind();
            }
        }

        protected void btnCancelOrder_Click(object sender, EventArgs e)
        {
            string sOrderNo;
            string SQL;
            clsDBO.clsDBO clSO = new clsDBO.clsDBO();
            foreach (GridDataItem dgi in rgOrder.SelectedItems)
            {
                sOrderNo = dgi["OrderNo"].Text;
                SQL = "select * from shiporder " + 
                      "where orderno = '" + sOrderNo + "' ";
                      //"and shiporderstatusid = 1 "
                clSO.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clSO.RowCount() > 0)
                {
                    clSO.GetRow();
                    clSO.SetFields("shiporderstatusid", "5");
                    clSO.SaveDB();
                }
            }
        }

        protected void btnApproveOrders_Click(object sender, EventArgs e)
        {
            string sOrderNo;
            string SQL;
            clsDBO.clsDBO clSO = new clsDBO.clsDBO();
            foreach (GridDataItem dgi in rgOrder.SelectedItems)
            {
                sOrderNo = dgi["OrderNo"].Text;
                SQL = "select * from shiporder " +
                      "where orderno = '" + sOrderNo + "' " +
                      "and (shiporderstatusid = 1 " +
                      "or shiporderstatusid = 5) ";
                clSO.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clSO.RowCount() > 0)
                {
                    clSO.GetRow();
                    clSO.SetFields("ShipOrderStatusid", "2");
                    clSO.SetFields("authdate", DateTime.Today.ToString());
                    clSO.SetFields("authtoship", hfUserEMail.Value);
                    ProcessItems(long.Parse(clSO.GetFields("orderid")), clSO.GetFields("idno"));

                    clSO.SaveDB();
                }
            }
            rgOrder.Rebind();
            rgItem.Rebind();
        }
        private void ProcessItems(long xOrderID, string xOrderTo)
        {
            string SQL;
            clsDBO.clsDBO clSOI = new clsDBO.clsDBO();
            long lItemID;
            long lOrderID;
            long lProductDetailID;
            long cnt;
            SQL = "select * from shiporderitem where orderid = " + xOrderID;
            clSOI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSOI.RowCount() > 0)
            {
                for (cnt = 0; cnt <= clSOI.RowCount() - 1; cnt++)
                {
                    clSOI.GetRowNo((int)cnt);
                    lOrderID = long.Parse(clSOI.GetFields("Orderid"));
                    lItemID = long.Parse(clSOI.GetFields("itemid"));
                    lProductDetailID = GetProductDetailID(long.Parse(clSOI.GetFields("ProductID")));
                    if (lProductDetailID > 0)
                    {
                        ProcessDetailID(lItemID, lOrderID, xOrderTo, lProductDetailID, long.Parse(clSOI.GetFields("QTY")));
                    }
                }
            }
        }
        private void ProcessDetailID(long xItemID, long xOrderID, string xOrderTo, long xProductDetailID, long xQty)
        {
            string SQL;
            clsDBO.clsDBO clSOI = new clsDBO.clsDBO();
            clsDBO.clsDBO clSDI = new clsDBO.clsDBO();
            string sSize;
            long cnt;
            long lProductID;
            SQL = "select * from shipproductdetailitem where productdetailid = " + xProductDetailID;
            clSDI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSDI.RowCount() == 0)
            {
                return;
            }
            if (xOrderTo == "0000")
            {
                sSize = "Small";
            } 
            else 
            {
                sSize = GetSize(xOrderTo);
            }
            for (cnt = 0; cnt <= clSDI.RowCount() - 1; cnt++)
            {
                clSDI.GetRowNo((int)cnt);
                lProductID = GetProductID(long.Parse(clSDI.GetFields("productdetailitem")));
                SQL = "select * from shiporderitem " +
                      "where orderid = " + xOrderID + " " +
                      "and productid = " + lProductID;
                clSOI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clSOI.RowCount() > 0)
                {
                    clSOI.GetRow();
                    if (sSize == "Small") 
                    {
                        clSOI.SetFields("qty", (long.Parse(clSOI.GetFields("qty")) + (xQty * long.Parse(clSDI.GetFields("smallqty")))).ToString());
                    }
                    if (sSize == "Medium") 
                    {
                        clSOI.SetFields("qty", (long.Parse(clSOI.GetFields("qty")) + (xQty * long.Parse(clSDI.GetFields("mediumqty")))).ToString());
                    }
                    if (sSize == "Large") 
                    {
                        clSOI.SetFields("qty", (long.Parse(clSOI.GetFields("qty")) + (xQty * long.Parse(clSDI.GetFields("largeqty")))).ToString());
                    }
                }
                else
                {
                    clSOI.NewRow();
                    clSOI.SetFields("orderid", xOrderID.ToString());
                    clSOI.SetFields("productid", lProductID.ToString());
                    if (sSize == "Small")
                    {
                        clSOI.SetFields("qty", (xQty * long.Parse(clSDI.GetFields("smallqty"))).ToString());
                    }
                    if (sSize == "Medium") 
                    {
                        clSOI.SetFields("qty", (xQty * long.Parse(clSDI.GetFields("mediumqty"))).ToString());
                    }
                    if (sSize == "Large")
                    {
                        clSOI.SetFields("qty", (xQty * long.Parse(clSDI.GetFields("largeqty"))).ToString());
                    }
                    clSOI.AddRow();
                }
                clSOI.SaveDB();
            }
            SQL = "delete shiporderitem where itemid = " + xItemID;
            clSOI.RunSQL(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private long GetProductID(long xProductDetailID)
        {
            string SQL;
            clsDBO.clsDBO clSP = new clsDBO.clsDBO();
            SQL = "select * from shipproduct where productdetailid = " + xProductDetailID;
            clSP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSP.RowCount() > 0)
            {
                clSP.GetRow();
                return long.Parse(clSP.GetFields("productid"));
            }
            return 0;
        }
        private string GetSize(string xOrderTo)
        {
            string SQL;
            clsDBO.clsDBO clSize = new clsDBO.clsDBO();
            if (xOrderTo.Substring(0, 1) == "A")
            {
                SQL = "select * from shipagentlevel where agentno = '" + xOrderTo + "' ";
                clSize.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clSize.RowCount() > 0)
                {
                    clSize.GetRow();
                    return clSize.GetFields("agentlevel");
                }
            } 
            else 
            {
                SQL = "select * from shipdealerlevel where dealerno = '" + xOrderTo + "' ";
                clSize.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
                if (clSize.RowCount() > 0)
                {
                    clSize.GetRow();
                    return clSize.GetFields("dealerlevel");
                }
            }
            return "Small";
        }
        public long GetProductDetailID(long xProductID)
        {
            string SQL;
            clsDBO.clsDBO clSP = new clsDBO.clsDBO();
            SQL = "select * from shipproduct where productid = " + xProductID;
            clSP.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSP.RowCount() > 0)
            {
                clSP.GetRow();
                return long.Parse(clSP.GetFields("productdetailid"));
            }
            return 0;
        }
        protected void chkOvernight_CheckedChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clSO = new clsDBO.clsDBO();
            long lOrderID;
            lOrderID = long.Parse(rgOrder.SelectedValue.ToString());
            SQL = "select * from shiporder where orderid = " + lOrderID + " ";
            clSO.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSO.RowCount() > 0)
            {
                clSO.GetRow();
                clSO.SetFields("overnight", chkOvernight.Checked.ToString());
                clSO.SaveDB();
            }
        }

        protected void btnSaveComment_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clSO = new clsDBO.clsDBO();
            long lOrderID;
            lOrderID = long.Parse(rgOrder.SelectedValue.ToString());
            SQL = "select * from shiporder where orderid = " + lOrderID + " ";
            clSO.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSO.RowCount() > 0)
            {
                clSO.GetRow();
                clSO.SetFields("comments", txtComments.Text);
                clSO.SaveDB();
            }
        }
    }
}