﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VeritasGeneratorCS.Contract
{
    public partial class ContractNotes : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsNotes.ConnectionString = ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString;
            if (!IsPostBack)
            {
                pnlNote.Visible = true;
                pnlChange.Visible = false;
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                //FillTable();
                ReadOnlyButtons();
            }
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly").ToLower() == "true")
                {
                    btnSave.Enabled = false;
                    btnAdd.Enabled = false;
                }
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
        private void FillTable()
        {
            /*string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select contractnoteid, note, cre.fname " + " cre.lname as creby, credate, mod.fname " + " mod.lname as modby, moddate from contractnote cn " + 
                  "left join userinfo cre on cre.UserID = cn.CreBy " + 
                  "left join userinfo mod on mod.UserID = cn.ModBy " + 
                  "where contractid = " + hfContractID.Value;
            rgNote.DataSource = clC.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);*/
            rgNote.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            txtNote.ReadOnly = false;
            pnlChange.Visible = true;
            pnlNote.Visible = false;
            hfContractNoteID.Value = 0.ToString();
            FillNote();
        }

        protected void rgNote_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlChange.Visible = true;
            pnlNote.Visible = false;
            hfContractNoteID.Value = rgNote.SelectedValue.ToString();
            FillNote();
        }
        private void FillNote()
        {
            string SQL;
            clsDBO.clsDBO clN = new clsDBO.clsDBO();
            txtNote.ReadOnly = false;
            btnSave.Visible = true;
            SQL = "select * from contractnote where contractnoteid = " + hfContractNoteID.Value;
            clN.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clN.RowCount() > 0)
            {
                clN.GetRow();
                txtNote.Text = clN.GetFields("note");
                txtModDate.Text = clN.GetFields("moddate");
                txtCreDate.Text = clN.GetFields("credate");
                if (clN.GetFields("creby") != hfUserID.Value)
                {
                    txtNote.ReadOnly = true;
                    btnSave.Visible = false;
                }
                if ((DateTime.Now - DateTime.Parse(clN.GetFields("moddate"))).Hours > 24)
                {
                    txtNote.ReadOnly = true;
                    btnSave.Visible = false;
                }
                txtCreBy.Text = GetUserInfo(long.Parse(clN.GetFields("creby")));
                if (clN.GetFields("modby").Length > 0)
                {
                    txtModBy.Text = GetUserInfo(long.Parse(clN.GetFields("modby")));
                }
                chkClaimAlways.Checked = Convert.ToBoolean(clN.GetFields("claimalways"));
            }
            else
            {
                txtNote.Text = "";
                txtModDate.Text = "";
                txtCreDate.Text = "";
                txtCreBy.Text = "";
                txtModBy.Text = "";
                chkClaimAlways.Checked = false;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlNote.Visible = true;
            pnlChange.Visible = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clN = new clsDBO.clsDBO();
            btnSave.Visible = true;
            SQL = "select * from contractnote where contractnoteid = " + hfContractNoteID.Value;
            clN.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clN.RowCount() == 0)
            {
                clN.NewRow();
            }
            else
            {
                clN.GetRow();
            }
            clN.SetFields("contractid", hfContractID.Value);
            clN.SetFields("note", txtNote.Text);
            clN.SetFields("claimalways", chkClaimAlways.Checked.ToString());
            if (clN.RowCount() == 0)
            {
                clN.SetFields("creby", hfUserID.Value);
                clN.SetFields("credate", DateTime.Now.ToString());
                clN.SetFields("modby", hfUserID.Value);
                clN.SetFields("moddate", DateTime.Now.ToString());
                clN.AddRow();
            }
            else
            {
                clN.SetFields("modby", hfUserID.Value);
                clN.SetFields("moddate", DateTime.Now.ToString());
            }
            clN.SaveDB();
            FillTable();
            pnlChange.Visible = false;
            pnlNote.Visible = true;
        }
        public string GetUserInfo(long xUserID)
        {
            string SQL;
            clsDBO.clsDBO clU = new clsDBO.clsDBO();
            SQL = "select * from userinfo where userid = " + xUserID;
            clU.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clU.RowCount() > 0)
            {
                clU.GetRow();
                return clU.GetFields("fname") + clU.GetFields("lname");
            }
            return "";
        }
    }
}