﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractModification.ascx.cs" Inherits="VeritasGeneratorCS.Contract.ContractModification" %>
<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            New Sale Mile:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtNewSaleMile" TextMode="Number" runat="server"></asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Button ID="btnUpdateSaleMile" OnClick="btnUpdateSaleMile_Click" runat="server" BorderColor="#1eabe2" BackColor="#1eabe2" Text="Update Sale Miles" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            New Sale Date:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtNewSaleDate" TextMode="Date" runat="server"></asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Button ID="btnUpdateSaleDate" OnClick="btnUpdateSaleDate_Click" runat="server" BorderColor="#1eabe2" BackColor="#1eabe2" Text="Update Sale Date" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgHistory">
                <telerik:RadGrid ID="rgHistory" OnItemCommand="rgHistory_ItemCommand" runat="server" AutoGenerateColumns="false" AllowSorting="true">
                    <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                        CommandItemDisplay="TopAndBottom">
                        <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" />
                        <Columns>
                            <telerik:GridBoundColumn DataField="ContractHistoryID" UniqueName="ContractHistoryID" Visible="false"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FieldName" UniqueName="FieldName" HeaderText="Field Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="OldValue" UniqueName="OldValue" HeaderText="Old Value"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="NewValue" UniqueName="NewValue" HeaderText="New Value"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CreDate" UniqueName="Credate" HeaderText="Mod Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="UserName" UniqueName="UserName" HeaderText="Mod By"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="false">
                        <Selecting AllowRowSelect="false" />
                    </ClientSettings>
                </telerik:RadGrid>
            </telerik:RadAjaxPanel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>


<asp:HiddenField ID="hfContractID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />

