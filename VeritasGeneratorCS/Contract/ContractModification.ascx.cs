﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasGeneratorCS.Contract
{
    public partial class ContractModification : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                ReadOnlyButtons();
                FillHistory();
            }
        }
        private void FillHistory()
        {
            clsDBO.clsDBO clH = new clsDBO.clsDBO();
            string SQL;
            SQL = "select ContractHistoryID, ContractID, fieldname, oldvalue, newvalue, credate, username from contracthistory ch " +
                  "inner join UserInfo ui on ui.userid = ch.CreBy " +
                  "where contractid = " + hfContractID.Value + " " +
                  "order by credate desc ";
            rgHistory.DataSource = clH.GetData(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
        }
        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (!Convert.ToBoolean(clR.GetFields("contractmodification")))
                {
                    btnUpdateSaleDate.Enabled = false;
                    btnUpdateSaleMile.Enabled = false;
                    //btnSave.Visible = False;
                }
            }
        }
        private void GetServerInfo()
        {
            string SQL;
            clsDBO.clsDBO clSI = new clsDBO.clsDBO();
            DateTime sStartDate;
            DateTime sEndDate;
            sStartDate = DateTime.Today;
            sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo " +
                  "where systemid = '" + hfID.Value + "' " +
                  "and signindate >= '" + sStartDate + "' " +
                  "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnUpdateSaleMile_Click(object sender, EventArgs e)
        {
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            string SQL;
            long lMileDiff;
            if (txtNewSaleMile.Text.Length == 0)
            {
                return;
            }
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                lMileDiff = long.Parse(clC.GetFields("effmile")) - long.Parse(clC.GetFields("salemile"));
                UpdateHistory("SaleMile", clC.GetFields("salemile"), txtNewSaleMile.Text);
                clC.SetFields("salemile", txtNewSaleMile.Text);
                clC.SetFields("effmile", (long.Parse(txtNewSaleMile.Text) + lMileDiff).ToString());
                clC.SetFields("expmile", (long.Parse(clC.GetFields("effmile")) + long.Parse(clC.GetFields("termmile"))).ToString());
                clC.SaveDB();
                Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
            }
        }

        protected void btnUpdateSaleDate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            long lDayDiff;
            if (txtNewSaleDate.Text.Length == 0)
            {
                return;
            }
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                //lDayDiff = DateDiff(DateInterval.Day, CDate(clC.Fields("saledate")), CDate(clC.Fields("effdate")))
                lDayDiff = (DateTime.Parse(clC.GetFields("effdate")) - DateTime.Parse(clC.GetFields("saledate"))).Days;
                UpdateHistory("SaleDate", clC.GetFields("saledate"), txtNewSaleDate.Text);
                clC.SetFields("saledate", txtNewSaleDate.Text);
                clC.SetFields("effdate", DateTime.Parse(clC.GetFields("saledate")).AddDays(lDayDiff).ToString()); //DateAdd(DateInterval.Day, lDayDiff, CDate(clC.Fields("saledate")))) 
                clC.SetFields("expdate", DateTime.Parse(clC.GetFields("effdate")).AddMonths(Convert.ToInt32(clC.GetFields("termmonth"))).ToString()); //DateAdd(DateInterval.Month, CLng(clC.Fields("termmonth")), CDate(clC.Fields("effdate")))
                clC.SaveDB();
                Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
            }
        }
        private void UpdateHistory(string xFieldName, string xOldValue, string xNewValue)
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            SQL = "select * from contracthistory where contracthistoryid =  0 ";
            clC.OpenDB(SQL, ConfigurationManager.ConnectionStrings["veritasdb"].ConnectionString);
            clC.NewRow();
            clC.SetFields("contractid", hfContractID.Value);
            clC.SetFields("fieldname", xFieldName);
            clC.SetFields("OldValue", xOldValue);
            clC.SetFields("newvalue", xNewValue);
            clC.SetFields("creby", hfUserID.Value);
            clC.SetFields("credate", DateTime.Today.ToString());
            clC.AddRow();
            clC.SaveDB();
        }

        protected void rgHistory_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid" || e.CommandName == "Sort")
            {
                FillHistory();
            }
        }
    }
}