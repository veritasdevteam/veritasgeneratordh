﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        * {
            font-family: Helvetica, Arial, sans-serif;
            font-size: small;
        }
    </style>
    <title>Home</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>

        <asp:Table runat="server">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                            <asp:Table runat="server" Width="250">
                                                <asp:TableRow>
                                                    <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                                        Menu
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadButton ID="btnHome" runat="server" BorderStyle="None" Text="Home" OnClick="btnHome_Click" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                            <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                                        </telerik:RadButton>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadButton ID="btnAgents" runat="server" BorderStyle="None" Text="Agents" OnClick="btnAgents_Click" EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                            <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                                        </telerik:RadButton>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadButton ID="btnDealer" runat="server" BorderStyle="None" Text="Dealerships" OnClick="btnDealer_Click" EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                            <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                                        </telerik:RadButton>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadButton ID="btnContract" runat="server" BorderStyle="None" Text="Contracts" OnClick="btnContract_Click" EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                            <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                                        </telerik:RadButton>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadButton ID="btnClaim" runat="server" BorderStyle="None" Text="Claims" OnClick="btnClaim_Click" EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                            <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                                        </telerik:RadButton>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadButton ID="btnAccounting" runat="server" BorderStyle="None" Text="Accounting" OnClick="btnAccounting_Click" EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                            <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                                        </telerik:RadButton>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadButton ID="btnReports" runat="server" BorderStyle="None" Text="Reports" OnClick="btnReports_Click" EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                            <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                                        </telerik:RadButton>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadButton ID="btnSettings" runat="server" BorderStyle="None" Text="Settings" OnClick="btnSettings_Click" EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                            <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                                        </telerik:RadButton>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadButton ID="btnUsers" runat="server" BorderStyle="None" Text="Users" OnClick="btnUsers_Click" EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                            <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                                        </telerik:RadButton>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadButton ID="btnLogOut" runat="server" BorderStyle="None" Text="Log Out" OnClick="btnLogOut_Click" EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                            <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                                        </telerik:RadButton>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                        &nbsp
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:MultiView runat="server" ID="mvDashboardType">
                        <asp:View ID="executive" runat="server">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Table runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>         
                                                <telerik:RadHtmlChart ID="htmlNewClaims" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>                                                            
                                                            <telerik:LineSeries DataFieldY ="numOfClaims" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= dataItem.numOfClaims# <br /> #=dataItem.CreateDate#" />
                                                            </telerik:LineSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="CreateDate">
                                                            <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis >                                                            
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>                                                            
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="New claims"></ChartTitle>
                                                </telerik:RadHtmlChart>  
                                                <telerik:RadRadioButtonList runat="server" ID="rblNewClaims" RepeatDirection="Horizontal" Direction="Horizontal" OnSelectedIndexChanged="rblNewClaims_SelectedIndexChanged">
                                                       <Items>
                                                           <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                                           <telerik:ButtonListItem Text="30 Days"/>
                                                           <telerik:ButtonListItem Text="90 Days"/>
                                                           <telerik:ButtonListItem Text="1 Year"/>
                                                       </Items>
                                                   </telerik:RadRadioButtonList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <asp:DropDownList ID="ddlDates" runat="server">
                                                                <asp:ListItem Selected="True" Value="All Time">All Time</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnUpdateDates" runat="server" Text="Set Date"  OnClick="rblNewClaims_SelectedIndexChanged"/>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <label id="lblClaimOpenCount" runat="server">Claims Currently Open:</label>
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox runat="server" ID="txtClaimOpen"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <label id="lblSeverity" runat="server">Paid Severity:</label>
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox runat="server" ID="txtSeverity"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <label id="lblClaimsDividedByTotalAuth" runat="server">Authorized Severity:</label>
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox runat="server" ID="txtClaimsOverAuth"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <telerik:RadGrid ID="rgClaimsPaid" runat="server" AutoGenerateColumns="true" AllowFilteringByColumn="false" AllowSorting="true" ShowFooter="true" ShowHeader="true" AllowPaging="true" PagerStyle-HorizontalAlign="Left" PagerStyle-Position="bottom" >
                                                    <MasterTableView AutoGenerateColumns="true" PageSize="15" ShowFooter="true">
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                                <telerik:RadGrid ID="rgClaimsAuthorized" runat="server" AutoGenerateColumns="true" AllowFilteringByColumn="false" AllowSorting="true" ShowFooter="true" ShowHeader="true" AllowPaging="true" PagerStyle-HorizontalAlign="Left" PagerStyle-Position="bottom" >
                                                    <MasterTableView AutoGenerateColumns="true" PageSize="15" ShowFooter="true">
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                                <telerik:RadRadioButtonList runat="server" ID="rblClaimsInfo" RepeatDirection="Horizontal" Direction="Horizontal" OnSelectedIndexChanged="rblNewClaims_SelectedIndexChanged">
                                                       <Items>
                                                           <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                                           <telerik:ButtonListItem Text="30 Days"/>
                                                           <telerik:ButtonListItem Text="90 Days"/>
                                                           <telerik:ButtonListItem Text="1 Year"/>
                                                       </Items>
                                                   </telerik:RadRadioButtonList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadHtmlChart ID="htmlAverageClose" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:LineSeries DataFieldY="average"></telerik:LineSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="closeDates">
                                                            <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis>
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="Average close"></ChartTitle>
                                                </telerik:RadHtmlChart>
                                                <telerik:RadRadioButtonList runat="server" ID="rblAverageClose" RepeatDirection="Horizontal" Direction="Horizontal" OnSelectedIndexChanged="rblNewClaims_SelectedIndexChanged">
                                                       <Items>
                                                           <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                                           <telerik:ButtonListItem Text="30 Days"/>
                                                           <telerik:ButtonListItem Text="90 Days"/>
                                                           <telerik:ButtonListItem Text="1 Year"/>
                                                       </Items>
                                                   </telerik:RadRadioButtonList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <telerik:RadHtmlChart ID="htmlHeadCount" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:LineSeries DataFieldY="numberOfUsers"></telerik:LineSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="date">
                                                            <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis>
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="Head Count"></ChartTitle>
                                                </telerik:RadHtmlChart>
                                                <telerik:RadRadioButtonList runat="server" ID="rblHeadCount" RepeatDirection="Horizontal" Direction="Horizontal" OnSelectedIndexChanged="rblNewClaims_SelectedIndexChanged">
                                                       <Items>
                                                           <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                                           <telerik:ButtonListItem Text="30 Days"/>
                                                           <telerik:ButtonListItem Text="90 Days"/>
                                                           <telerik:ButtonListItem Text="1 Year"/>
                                                       </Items>
                                                   </telerik:RadRadioButtonList>
                                            </asp:TableCell>                                           
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadHtmlChart ID="htmlSales" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries Name="AN" DataFieldY="ANCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.ANCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.salesDate#" />
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="Vero" DataFieldY="VeroCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.VeroCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.salesDate#" />
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="EP" DataFieldY="EPCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.EPCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.salesDate#" />
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="All Others" DataFieldY="OtherCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.OtherCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.salesDate#" />
                                                            </telerik:ColumnSeries>                      
                                                            <telerik:LineSeries Name="Average" DataFieldY ="average" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="Average: #= dataItem.average# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.salesDate#" />
                                                            </telerik:LineSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="salesDate">
                                                            <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis>
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="true"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="Sales"></ChartTitle>
                                                </telerik:RadHtmlChart>
                                                <telerik:RadRadioButtonList runat="server" ID="rblSales" RepeatDirection="Horizontal" Direction="Horizontal" OnSelectedIndexChanged="rblNewClaims_SelectedIndexChanged">
                                                       <Items>
                                                           <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                                           <telerik:ButtonListItem Text="30 Days"/>
                                                           <telerik:ButtonListItem Text="90 Days"/>
                                                           <telerik:ButtonListItem Text="1 Year"/>
                                                       </Items>
                                                   </telerik:RadRadioButtonList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <telerik:RadHtmlChart ID="htmlPaid" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries Name="AN" DataFieldY="ANCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.ANCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.paidDates#" />
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="Vero" DataFieldY="VeroCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.VeroCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.paidDates#" />
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="EP" DataFieldY="EPCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.EPCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.paidDates#" />
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="All Others" DataFieldY="OtherCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.OtherCnt#  <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.paidDates#" />
                                                            </telerik:ColumnSeries>              
                                                            <telerik:LineSeries Name="Average" DataFieldY ="average" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="Average: #= dataItem.average# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.paidDates#" />
                                                            </telerik:LineSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="paidDates" >
                                                            <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis>
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="true"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="Paid"></ChartTitle>
                                                </telerik:RadHtmlChart>
                                                <telerik:RadRadioButtonList runat="server" ID="rblPaid" RepeatDirection="Horizontal" Direction="Horizontal" OnSelectedIndexChanged="rblNewClaims_SelectedIndexChanged">
                                                       <Items>
                                                           <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                                           <telerik:ButtonListItem Text="30 Days"/>
                                                           <telerik:ButtonListItem Text="90 Days"/>
                                                           <telerik:ButtonListItem Text="1 Year"/>
                                                       </Items>
                                                   </telerik:RadRadioButtonList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <telerik:RadHtmlChart ID="htmlCancel" runat="server" Visible="true" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>               
                                                            <telerik:ColumnSeries Name="AN" DataFieldY="ANCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.ANCnt# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.CancelDate#" />
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="Vero" DataFieldY="VeroCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.VeroCnt# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.CancelDate#" />
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="EP" DataFieldY="EPCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.EPCnt# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.CancelDate#" />
                                                            </telerik:ColumnSeries>
                                                            <telerik:ColumnSeries Name="All Others" DataFieldY="OtherCnt" Stacked="true" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="#= series.name#: #= dataItem.OtherCnt# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.CancelDate#" />
                                                            </telerik:ColumnSeries>         
                                                            <telerik:LineSeries Name="Average" DataFieldY ="average" >
                                                                <LabelsAppearance Visible="false"></LabelsAppearance>
                                                                <TooltipsAppearance ClientTemplate="Average: #= dataItem.average# <br /> Total: #= dataItem.TotalCnt# <br /> #=dataItem.CancelDate#" />
                                                            </telerik:LineSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="CancelDate">
                                                            <LabelsAppearance RotationAngle="90" ></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis >
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="true"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="Cancels"></ChartTitle>
                                                </telerik:RadHtmlChart>
                                                <telerik:RadRadioButtonList runat="server" ID="rblCancel" RepeatDirection="Horizontal" Direction="Horizontal" OnSelectedIndexChanged="rblNewClaims_SelectedIndexChanged">
                                                       <Items>
                                                           <telerik:ButtonListItem Text="7 Days" Selected="true"/>
                                                           <telerik:ButtonListItem Text="30 Days"/>
                                                           <telerik:ButtonListItem Text="90 Days"/>
                                                           <telerik:ButtonListItem Text="1 Year"/>
                                                       </Items>
                                                   </telerik:RadRadioButtonList>
                                            </asp:TableCell>
                                        </asp:TableRow>                                        
                                    </asp:Table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:View>
                        <asp:View ID="everyoneElse" runat="server">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Table runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>

                                                <telerik:RadHtmlChart ID="htmlInbound" runat="server" Visible="false" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:VerticalBulletSeries DataCurrentField="count" DataTargetField="goal">
                                                            </telerik:VerticalBulletSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="CallDate">
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis MaxValue="40">
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="Stats"></ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <telerik:RadHtmlChart ID="htmlOutBound" runat="server" Visible="false" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:VerticalBulletSeries DataCurrentField="count" DataTargetField="goal">
                                                            </telerik:VerticalBulletSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="CallDate">
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis MaxValue="40">
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="Stats"></ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <telerik:RadHtmlChart ID="htmlDnD" runat="server" Visible="false" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:VerticalBulletSeries DataCurrentField="count" DataTargetField="goal">
                                                            </telerik:VerticalBulletSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="CallDate">
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis MaxValue="400">
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="Stats"></ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadHtmlChart ID="htmlClaimsOpened" runat="server" Visible="false" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:VerticalBulletSeries DataCurrentField="numOfClaimsOpened" DataTargetField="goal">
                                                            </telerik:VerticalBulletSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="CreDate">
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis MaxValue="50">
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="Stats"></ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <telerik:RadHtmlChart ID="htmlNumOfClaimsMovedFromOpen" runat="server" Visible="false" Width="400" Height="400" Transitions="false">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:VerticalBulletSeries DataCurrentField="numOfClaimsClosed" DataTargetField="goal">
                                                            </telerik:VerticalBulletSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="CloseDate">
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                            <MajorGridLines Visible="false"></MajorGridLines>
                                                        </XAxis>
                                                        <YAxis MaxValue="50">
                                                            <LabelsAppearance DataFormatString="{0}"></LabelsAppearance>
                                                            <MinorGridLines Visible="false"></MinorGridLines>
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false"></Appearance>
                                                    </Legend>
                                                    <ChartTitle Text="Stats"></ChartTitle>
                                                </telerik:RadHtmlChart>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                                                <telerik:RadGrid ID="rgClaimOpen" OnSelectedIndexChanged="rgClaimOpen_SelectedIndexChanged" runat="server" AutoGenerateColumns="true" AllowFilteringByColumn="false" AllowSorting="true" ShowFooter="true" ShowHeader="true" AllowPaging="true" PagerStyle-HorizontalAlign="Left" PagerStyle-Position="bottom" Visible="false" OnNeedDataSource="rgClaimOpen_NeedDataSource">
                                                    <PagerStyle EnableAllOptionInPagerComboBox="true" />
                                                    <MasterTableView AutoGenerateColumns="false" PageSize="15" ShowFooter="true" DataKeyNames="ClaimID">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="ClaimID" ReadOnly="true" Visible="false" UniqueName="ClaimID"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CreDate" UniqueName="CreDate" HeaderText="Loss date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ModDate" UniqueName="ModDate" HeaderText="Last Activity" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimAge" UniqueName="ClaimAge" HeaderText="Claim Age"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ActivityDesc" UniqueName="ActivityDesc" HeaderText="Activity"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Adjuster" UniqueName="Adjuster" HeaderText="Adjuster" Visible="false"></telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </asp:TableCell>
                                            <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                                                <telerik:RadGrid ID="rgClaimManagment" OnSelectedIndexChanged="rgClaimOpen_SelectedIndexChanged" runat="server" AutoGenerateColumns="true" AllowFilteringByColumn="false" AllowSorting="true" ShowFooter="true" ShowHeader="true" AllowPaging="true" PagerStyle-HorizontalAlign="Left" PagerStyle-Position="bottom" Visible="false" OnNeedDataSource="rgClaimOpen_NeedDataSource">
                                                    <PagerStyle EnableAllOptionInPagerComboBox="true" />
                                                    <MasterTableView AutoGenerateColumns="false" PageSize="15" ShowFooter="true" DataKeyNames="ClaimID">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="ClaimID" ReadOnly="true" Visible="false" UniqueName="ClaimID"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CreDate" UniqueName="CreDate" HeaderText="Loss date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ModDate" UniqueName="ModDate" HeaderText="Last Activity" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimAge" UniqueName="ClaimAge" HeaderText="Claim Age"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ActivityDesc" UniqueName="ActivityDesc" HeaderText="Activity"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Adjuster" UniqueName="Adjuster" HeaderText="Adjuster"></telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </asp:TableCell>
                                            <asp:TableCell VerticalAlign="Top">
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <asp:DropDownList ID="ddlAgents" runat="server" Visible="false"></asp:DropDownList>
                                                            <asp:Button ID="btnChangeGraphs" Text="Select" runat="server" OnClick="btnChangeGraphs_Click" Visible="false" BackColor="#1a4688" ForeColor="White" BorderColor="#1a4688" Width="75" />
                                                            <asp:Button ID="btnRevertGraphs" Text="Undo" runat="server" OnClick="btnRevertGraphs_Click" Visible="false" />
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Visible="false" ID="tblecSetGoals">
                                                            <asp:Table runat="server">
                                                                <asp:TableRow>
                                                                    <asp:TableCell>
                                                                        <asp:Label runat="server" Text="Inbond goal"></asp:Label>
                                                                        <asp:TextBox runat="server" ID="txbSetGoalInbound"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                </asp:TableRow>
                                                                <asp:TableRow>
                                                                    <asp:TableCell>
                                                                        <asp:Label runat="server" Text="Outbond goal"></asp:Label>
                                                                        <asp:TextBox runat="server" ID="txbSetGoalOutbound"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                </asp:TableRow>
                                                                <asp:TableRow>
                                                                    <asp:TableCell>
                                                                        <asp:Label runat="server" Text="DnD goal"></asp:Label>
                                                                        <asp:TextBox runat="server" ID="txbSetGoalDnD"></asp:TextBox>
                                                                    </asp:TableCell>
                                                                </asp:TableRow>
                                                            </asp:Table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:View>
                    </asp:MultiView>

                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:HiddenField ID="hfSigninPopup" runat="server" Value="Visible" />
        <telerik:RadWindow ID="rwSignIn" runat="server" Width="500" Height="200" Behaviors="Close" EnableViewState="false" ReloadOnShow="true" EnableShadow="true">
            <ContentTemplate>
                <asp:Table runat="server" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell>
                            E-Mail:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtEMail" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Password:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtPassword" TextMode="Password" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnOK" runat="server" OnClick="btnOK_Click" BackColor="#1a4688" ForeColor="White" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError" runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" OnClick="btnErrorOK_Click" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
    </form>
</body>
</html>
