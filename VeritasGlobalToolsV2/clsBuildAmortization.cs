﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsBuildAmortization
    {
        private long lClaimGapID;
        private double dPaymentAmt;
        private double dRatePerMonth;
        private double dLoanAmt;
        private double dLoanAmtInt;
        private long lNoPayment;
        private string sStartDate;
        private bool bMonthly;
        private bool bBiWeekly;

        public long ClaimGapID
        {
            get
            {
                ClaimGapID = lClaimGapID;
                return ClaimGapID;
            }
            set
            {
                lClaimGapID = value;
            }
        }
        public double LoanAmt
        {
            get
            {
                LoanAmt = dLoanAmt;
                return LoanAmt;
            }
            set
            {
                dLoanAmt = value;
            }
        }
        public double LoanAmtInt
        {
            get
            {
                LoanAmtInt = dLoanAmtInt;
                return LoanAmtInt;
            }
            set
            {
                dLoanAmtInt = value;
            }
        }
        public string StartDate
        {
            get
            {
                StartDate = sStartDate;
                return StartDate;
            }
            set
            {
                sStartDate = value;
            }
        }
        public long NoPayment
        {
            get
            {
                NoPayment = lNoPayment;
                return NoPayment;
            }
            set
            {
                lNoPayment = value;
            }
        }
        public double PaymentAmt
        {
            get
            {
                PaymentAmt = dPaymentAmt;
                return PaymentAmt;
            }
            set
            {
                dPaymentAmt = value;
            }
        }
        public double RatePerMonth
        {
            get
            {
                RatePerMonth = dRatePerMonth;
                return RatePerMonth;
            }
            set
            {
                dRatePerMonth = value;
            }
        }
        public bool Monthly
        {
            get
            {
                Monthly = bMonthly;
                return Monthly;
            }
            set
            {
                bMonthly = value;
            }
        }
        public bool BiWeekly
        {
            get
            {
                BiWeekly = bBiWeekly;
                return BiWeekly;
            }
            set
            {
                bBiWeekly = value;
            }
        }

        public void BuildIt()
        {
            long cnt;
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string sMonth = "";
            double dOldBalance;
            double dOldBalanceInt;
            double dTotInt = 0;
            dOldBalanceInt = dLoanAmtInt;
            dOldBalance = dLoanAmt;
            for (cnt = 1;cnt < lNoPayment; cnt++)
            {
                SQL = "select * from claimgapamortization " +
                      "where claimgapid = " + lClaimGapID + " " +
                      "and paymentno = " + cnt;
                clR.OpenDB(SQL, Global.sCON);
                if (clR.RowCount() == 0)
                {
                    clR.NewRow();
                    clR.SetFields("claimgapid", lClaimGapID.ToString());
                    clR.SetFields("paymentno", cnt.ToString());
                }
                else
                {
                    clR.GetRow();
                }
                if (bMonthly)
                {
                    //sMonth = DateAdd(DateInterval.Month, cnt - 1, DateTime.Parse(sStartDate));
                    sMonth = DateTime.Parse(sStartDate).AddMonths((int)cnt-1).ToString();
                }
                if (bBiWeekly)
                {
                    //sMonth = DateAdd(DateInterval.Day, (cnt - 1) * 14, DateTime.Parse(sStartDate));
                    sMonth = DateTime.Parse(sStartDate).AddDays(((int)cnt-1)*14).ToString();
                }

                clR.SetFields("monthdate", sMonth);
                clR.SetFields("payment", dPaymentAmt.ToString());
                clR.SetFields("interest", (dOldBalance * dRatePerMonth).ToString());
                clR.SetFields("principal", (dPaymentAmt - Convert.ToDouble((clR.GetFields("interest")))).ToString());
                dTotInt = dTotInt + Convert.ToDouble((clR.GetFields("interest")));
                clR.SetFields("totalinterest", dTotInt.ToString());
                dOldBalance = dOldBalance - Convert.ToDouble((clR.GetFields("principal")));
                clR.SetFields("balremaining" , dOldBalance.ToString());
                dOldBalanceInt = dOldBalanceInt - dPaymentAmt;
                clR.SetFields("loanremaining", dOldBalanceInt.ToString());
                if (clR.RowCount() == 0)
                {
                    clR.AddRow();
                }
                clR.SaveDB();
            }
        }
    }
}
